<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<?php twentyseventeen_edit_link( get_the_ID() ); ?>
	</header><!-- .entry-header -->
	<div class="breadcrumb">
		<a href="<?php bloginfo('url')?>/agents/"><i class="fas fa-caret-left"></i> All Agents</a>
	</div>
	
	<div class="entry-content agent-left">
		<?php
			the_content();
		?>
	</div><!-- .entry-content -->
	
	<div class="agent-right">
	<?php

	/*
	 * If a regular post or page, and not the front page, show the featured image.
	 * Using get_queried_object_id() here since the $post global may not be set before a call to the_post().
	 */
	if ( ( is_single() || ( is_page() && ! twentyseventeen_is_frontpage() ) ) && has_post_thumbnail( get_queried_object_id() ) ) :
		echo '<div class="single-featured-image-header">';
		echo get_the_post_thumbnail( get_queried_object_id(), 'twentyseventeen-featured-image' );
		echo '</div><!-- .single-featured-image-header -->'; 
		endif;
	?>
		<div class="agent-stats">
			<p style="margin-bottom: 0px"><span><?php the_field('primary_office_name'); ?></span></p>
			<p><?php the_field('primary_office_address'); ?></p>
			<p><span>Phone:</span> <?php the_field('phone'); ?><br/>
				<span>Fax:</span> <?php the_field('fax'); ?><br/>
				 <a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a><br/>
			</p>
			<?php if( get_field('lifetime_sales') ): ?>
			<p><span>Lifetime sales:</span> <?php the_field('lifetime_sales'); ?><br/>
				<em>Last updated <?php the_field('last_updated_on'); ?></em>
			</p>
			<?php endif; ?>
			<?php if( get_field('lifetime_units_closed') ): ?>
				<p><span>Lifetime units closed:</span> <?php the_field('lifetime_units_closed'); ?></p>
			<?php endif; ?>
			
			<p><?php echo get_the_term_list( $post->ID, 'languages', 'Additional Languages Spoken: ', ', ', '' ); ?> </p>
		
		</div>
	
	</div>
</article><!-- #post-## -->
