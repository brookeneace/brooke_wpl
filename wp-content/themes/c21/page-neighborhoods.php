<?php
/**
 * Template Name: Neighborhoods
 */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/page/content', 'page' );


			endwhile; // End of the loop.
			?>
			
					
		

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<div id="neighborhoods">
				<h1>Neighborhood Profiles</h1>
				
				<div class="wrap">
					<a href="<?php bloginfo('url')?>/neighborhoods/lincoln-park/">
					<article>
						<div class="article-image"><img src="<?php bloginfo('template_url')?>/assets/images/Neighborhood-LincolnPark.jpg"></div>
						<h3>Lincoln Park</h3>
					</article></a>
					<a href="<?php bloginfo('url')?>/neighborhoods/pilsen/">
					<article>
						<div class="article-image"><img src="<?php bloginfo('template_url')?>/assets/images/Neighborhood-Pilsen.jpg"></div>
						<h3>Pilsen</h3>
					</article></a>
					<a href="<?php bloginfo('url')?>/neighborhoods/south-loop/">
					<article>
						<div class="article-image"><img src="<?php bloginfo('template_url')?>/assets/images/Neighborhood-SouthLoop.jpg"></div>
						<h3>South Loop</h3>
					</article></a>
				</div>
	
			<div class="view-all-neighborhoods">
				<div class="wrap">
				<h2>View all Chicago neighborhoods</h2>
				<ul>
					<li><h4>North</h4>

						<?php
						
						        // The Arguments
						        $args = array(
						            'post_type' => 'neighborhoods', 
						            'tax_query' => array(
								        array(
								            'taxonomy' => 'neighborhoods_categories',
								            'field'    => 'slug',
								            'terms' => 'north' //if field is ID you can reference by cat/term number
								        )
								    )
						        );  
						
						        // The Query
						        $the_query = new WP_Query( $args ); ?>
						
						        <?php
						
						        // If we have the posts...
						        if ( $the_query->have_posts() ) : ?>
						
						        <!-- Start the loop the loop --> 
						            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						
						                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						
						            <?php endwhile; endif; // end of the loop. ?>
						
						        <?php wp_reset_postdata(); ?>
						
						
					</li>
					<li><h4>Northwest</h4>
					<?php
						
						        // The Arguments
						        $args = array(
						            'post_type' => 'neighborhoods', 
						            'tax_query' => array(
								        array(
								            'taxonomy' => 'neighborhoods_categories',
								            'field'    => 'slug',
								            'terms' => 'northwest' //if field is ID you can reference by cat/term number
								        )
								    )
						        );  
						
						        // The Query
						        $the_query = new WP_Query( $args ); ?>
						
						        <?php
						
						        // If we have the posts...
						        if ( $the_query->have_posts() ) : ?>
						
						        <!-- Start the loop the loop --> 
						            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						
						                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						
						            <?php endwhile; endif; // end of the loop. ?>
						
						        <?php wp_reset_postdata(); ?>

					</li>
					<li><h4>South</h4>
					<?php
						
						        // The Arguments
						        $args = array(
						            'post_type' => 'neighborhoods', 
						            'tax_query' => array(
								        array(
								            'taxonomy' => 'neighborhoods_categories',
								            'field'    => 'slug',
								            'terms' => 'south' //if field is ID you can reference by cat/term number
								        )
								    )
						        );  
						
						        // The Query
						        $the_query = new WP_Query( $args ); ?>
						
						        <?php
						
						        // If we have the posts...
						        if ( $the_query->have_posts() ) : ?>
						
						        <!-- Start the loop the loop --> 
						            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						
						                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						
						            <?php endwhile; endif; // end of the loop. ?>
						
						        <?php wp_reset_postdata(); ?>

					</li>
					<li><h4>Southwest</h4>
					<?php
						
						        // The Arguments
						        $args = array(
						            'post_type' => 'neighborhoods', 
						            'tax_query' => array(
								        array(
								            'taxonomy' => 'neighborhoods_categories',
								            'field'    => 'slug',
								            'terms' => 'southwest' //if field is ID you can reference by cat/term number
								        )
								    )
						        );  
						
						        // The Query
						        $the_query = new WP_Query( $args ); ?>
						
						        <?php
						
						        // If we have the posts...
						        if ( $the_query->have_posts() ) : ?>
						
						        <!-- Start the loop the loop --> 
						            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						
						                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						
						            <?php endwhile; endif; // end of the loop. ?>
						
						        <?php wp_reset_postdata(); ?>

					</li>
					
					<li><h4>Suburbs – North</h4>
			
					</li>
					<li><h4>Suburbs – Northwest</h4>
					
					</li>
					<li><h4>Suburbs – West</h4>
					
					</li>
					<li><h4>Suburbs – South</h4>
					
					</li>
				</ul>
				</div>
			</div>
		</div>

<?php get_footer();
