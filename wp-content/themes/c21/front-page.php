<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<div id="home-top-search">
			<div class="fade"></div>
			<section class="inner-nav home-inner">
				<div class="wrap">
					<div class="logo-main"><a href="<?php bloginfo('url') ?>"><img src="<?php bloginfo('template_url') ?>/assets/images/logo-c21sgr-white.png" alt="Century 21 SGR Chicago Real Estate"></a></div>
					<ul>
						<li><a href="<?php bloginfo('url') ?>/agents">Agents</a></li>
					<li><a href="<?php bloginfo('url') ?>/properties">Properties</a></li>
					<li><a href="<?php bloginfo('url') ?>/neighborhood-pricing">Neighborhoods</a></li>
					<li><a href="<?php bloginfo('url') ?>/about-us">About Us</a></li>
					</ul>
				</div>
			</section>
			<div class="wrap">
				<div id="prop-search">
					<div class="sale-rent">
						<span class="active" data-form="buy">Buy</span><span data-form="rent">Rent</span>
					</div>
					<div class="home-form buy show-form">
					<?php echo do_shortcode('[wpl_widget_instance id="wpl_search_widget-3"]')?>
					</div>
					<div class="home-form rent">
					<?php echo do_shortcode('[wpl_widget_instance id="wpl_search_widget-5"]')?>
					</div>
					
				</div>
			</div>
			
			
			<div class="home-backgrounds">
				<div class="home0"></div>
					<div class="home1"></div>
						<div class="home2"></div>
							<div class="home3"></div>
								<div class="home4"></div>
			</div>
		</div>
		
			
		
		
	
		
		
	

	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer();
