# WordPress MySQL database migration
#
# Generated: Monday 5. November 2018 00:19 UTC
# Hostname: localhost
# Database: `c21sgr`
# URL: //nervedevelopment.com/c21
# Path: /Applications/MAMP/htdocs/c21sgr
# Tables: wp_commentmeta, wp_comments, wp_links, wp_options, wp_postmeta, wp_posts, wp_term_relationships, wp_term_taxonomy, wp_termmeta, wp_terms, wp_usermeta, wp_users, wp_wpl_activities, wp_wpl_addons, wp_wpl_cronjobs, wp_wpl_dbcat, wp_wpl_dbst, wp_wpl_dbst_types, wp_wpl_events, wp_wpl_extensions, wp_wpl_filters, wp_wpl_item_categories, wp_wpl_items, wp_wpl_kinds, wp_wpl_listing_types, wp_wpl_location1, wp_wpl_location2, wp_wpl_location3, wp_wpl_location4, wp_wpl_location5, wp_wpl_location6, wp_wpl_location7, wp_wpl_locationtextsearch, wp_wpl_locationzips, wp_wpl_logs, wp_wpl_menus, wp_wpl_notifications, wp_wpl_properties, wp_wpl_property_types, wp_wpl_room_types, wp_wpl_setting_categories, wp_wpl_settings, wp_wpl_sort_options, wp_wpl_unit_types, wp_wpl_units, wp_wpl_user_group_types, wp_wpl_users
# Table Prefix: wp_
# Post Types: revision, acf-field, acf-field-group, agents, attachment, neighborhoods, page, post
# Protocol: http
# Multisite: false
# Subsite Export: false
# --------------------------------------------------------

/*!40101 SET NAMES utf8 */;

SET sql_mode='NO_AUTO_VALUE_ON_ZERO';



#
# Delete any existing table `wp_commentmeta`
#

DROP TABLE IF EXISTS `wp_commentmeta`;


#
# Table structure of table `wp_commentmeta`
#

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_commentmeta`
#

#
# End of data contents of table `wp_commentmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_comments`
#

DROP TABLE IF EXISTS `wp_comments`;


#
# Table structure of table `wp_comments`
#

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_comments`
#
INSERT INTO `wp_comments` ( `comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-06-28 02:28:23', '2018-06-28 02:28:23', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href="https://gravatar.com">Gravatar</a>.', 0, '1', '', '', 0, 0) ;

#
# End of data contents of table `wp_comments`
# --------------------------------------------------------



#
# Delete any existing table `wp_links`
#

DROP TABLE IF EXISTS `wp_links`;


#
# Table structure of table `wp_links`
#

CREATE TABLE `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_links`
#

#
# End of data contents of table `wp_links`
# --------------------------------------------------------



#
# Delete any existing table `wp_options`
#

DROP TABLE IF EXISTS `wp_options`;


#
# Table structure of table `wp_options`
#

CREATE TABLE `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB AUTO_INCREMENT=583 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_options`
#
INSERT INTO `wp_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://nervedevelopment.com/c21', 'yes'),
(2, 'home', 'http://nervedevelopment.com/c21', 'yes'),
(3, 'blogname', 'Century 21 SGR Chicago Real Estate', 'yes'),
(4, 'blogdescription', 'Find your dream home in Chicago', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'brooke.neace@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '1', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:127:{s:18:"(properties)/(.+)$";s:49:"index.php?pagename=$matches[1]&wpl_qs=$matches[2]";s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:21:"^index.php/wp-json/?$";s:22:"index.php?rest_route=/";s:24:"^index.php/wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:65:"neighborhoods_categories/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:63:"index.php?neighborhoods_categories=$matches[1]&feed=$matches[2]";s:60:"neighborhoods_categories/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:63:"index.php?neighborhoods_categories=$matches[1]&feed=$matches[2]";s:41:"neighborhoods_categories/([^/]+)/embed/?$";s:57:"index.php?neighborhoods_categories=$matches[1]&embed=true";s:53:"neighborhoods_categories/([^/]+)/page/?([0-9]{1,})/?$";s:64:"index.php?neighborhoods_categories=$matches[1]&paged=$matches[2]";s:35:"neighborhoods_categories/([^/]+)/?$";s:46:"index.php?neighborhoods_categories=$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:23:"category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:20:"tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:21:"type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:34:"agents/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:44:"agents/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:64:"agents/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"agents/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"agents/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:40:"agents/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:23:"agents/([^/]+)/embed/?$";s:39:"index.php?agents=$matches[1]&embed=true";s:27:"agents/([^/]+)/trackback/?$";s:33:"index.php?agents=$matches[1]&tb=1";s:35:"agents/([^/]+)/page/?([0-9]{1,})/?$";s:46:"index.php?agents=$matches[1]&paged=$matches[2]";s:42:"agents/([^/]+)/comment-page-([0-9]{1,})/?$";s:46:"index.php?agents=$matches[1]&cpage=$matches[2]";s:31:"agents/([^/]+)(?:/([0-9]+))?/?$";s:45:"index.php?agents=$matches[1]&page=$matches[2]";s:23:"agents/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:33:"agents/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:53:"agents/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:48:"agents/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:48:"agents/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:29:"agents/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:41:"neighborhoods/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:51:"neighborhoods/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:71:"neighborhoods/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:66:"neighborhoods/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:66:"neighborhoods/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:47:"neighborhoods/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:30:"neighborhoods/([^/]+)/embed/?$";s:46:"index.php?neighborhoods=$matches[1]&embed=true";s:34:"neighborhoods/([^/]+)/trackback/?$";s:40:"index.php?neighborhoods=$matches[1]&tb=1";s:42:"neighborhoods/([^/]+)/page/?([0-9]{1,})/?$";s:53:"index.php?neighborhoods=$matches[1]&paged=$matches[2]";s:49:"neighborhoods/([^/]+)/comment-page-([0-9]{1,})/?$";s:53:"index.php?neighborhoods=$matches[1]&cpage=$matches[2]";s:38:"neighborhoods/([^/]+)(?:/([0-9]+))?/?$";s:52:"index.php?neighborhoods=$matches[1]&page=$matches[2]";s:30:"neighborhoods/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:40:"neighborhoods/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:60:"neighborhoods/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:55:"neighborhoods/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:55:"neighborhoods/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:36:"neighborhoods/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:8:"embed/?$";s:21:"index.php?&embed=true";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:27:"comment-page-([0-9]{1,})/?$";s:38:"index.php?&page_id=5&cpage=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:17:"comments/embed/?$";s:21:"index.php?&embed=true";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:20:"search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:23:"author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";s:27:"[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:"[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:"[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"([^/]+)/embed/?$";s:37:"index.php?name=$matches[1]&embed=true";s:20:"([^/]+)/trackback/?$";s:31:"index.php?name=$matches[1]&tb=1";s:40:"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:35:"([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:28:"([^/]+)/page/?([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&paged=$matches[2]";s:35:"([^/]+)/comment-page-([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&cpage=$matches[2]";s:24:"([^/]+)(?:/([0-9]+))?/?$";s:43:"index.php?name=$matches[1]&page=$matches[2]";s:16:"[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:26:"[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:46:"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:22:"[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:3:{i:0;s:34:"advanced-custom-fields-pro/acf.php";i:1;s:40:"real-estate-listing-realtyna-wpl/WPL.php";i:2;s:31:"wp-migrate-db/wp-migrate-db.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'c21', 'yes'),
(41, 'stylesheet', 'c21', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:1:{s:40:"real-estate-listing-realtyna-wpl/WPL.php";a:2:{i:0;s:14:"wpl_extensions";i:1;s:13:"uninstall_wpl";}}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '2', 'yes'),
(84, 'page_on_front', '5', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'initial_db_version', '38590', 'yes'),
(93, 'wp_user_roles', 'a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:61:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}}', 'yes'),
(94, 'fresh_site', '0', 'yes'),
(95, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(97, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(99, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(100, 'sidebars_widgets', 'a:5:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}s:9:"sidebar-3";a:0:{}s:13:"array_version";i:3;}', 'yes') ;
INSERT INTO `wp_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(101, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(102, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(103, 'widget_media_audio', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(104, 'widget_media_image', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(105, 'widget_media_gallery', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(106, 'widget_media_video', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(107, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(108, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(109, 'widget_custom_html', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(110, 'cron', 'a:6:{i:1541377704;a:1:{s:34:"wp_privacy_delete_old_export_files";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:6:"hourly";s:4:"args";a:0:{}s:8:"interval";i:3600;}}}i:1541379801;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1541383207;a:1:{s:27:"cron-dsidxpress-flush-cache";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1541384904;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1541463423;a:2:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}s:25:"delete_expired_transients";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(111, 'theme_mods_twentyseventeen', 'a:2:{s:18:"custom_css_post_id";i:-1;s:16:"sidebars_widgets";a:2:{s:4:"time";i:1535329160;s:4:"data";a:4:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}s:9:"sidebar-3";a:0:{}}}}', 'yes'),
(139, 'can_compress_scripts', '1', 'no'),
(150, 'current_theme', 'Twenty Seventeen/c21', 'yes'),
(151, 'theme_mods_c21', 'a:3:{i:0;b:0;s:18:"nav_menu_locations";a:0:{}s:18:"custom_css_post_id";i:-1;}', 'yes'),
(152, 'theme_switched', '', 'yes'),
(186, 'WPLANG', '', 'yes'),
(187, 'new_admin_email', 'brooke.neace@gmail.com', 'yes'),
(263, 'recently_activated', 'a:0:{}', 'yes'),
(272, 'wpmdb_usage', 'a:2:{s:6:"action";s:8:"savefile";s:4:"time";i:1541377175;}', 'no'),
(308, 'widget_dsidx-quicksearch', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(309, 'widget_dsidx-search', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(310, 'widget_dsidx-list-areas', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(311, 'widget_dsidx-listings', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(312, 'widget_dsidx-single-listing', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(318, 'widget_dsidx-recentstatus', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(319, 'widget_dsidx-slideshow', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(320, 'widget_dsidx-mapsearch', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(330, 'wpl_version', '4.3.2', 'yes'),
(331, 'widget_wpl_search_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(332, 'widget_wpl_carousel_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(333, 'widget_wpl_agents_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(334, 'widget_wpl_googlemap_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(451, 'acf_version', '5.6.2', 'yes'),
(508, 'category_children', 'a:0:{}', 'yes'),
(512, 'neighborhoods_categories_children', 'a:0:{}', 'yes'),
(533, 'options_office_locations_0_office_name', 'WEST LOOP OFFICE', 'no'),
(534, '_options_office_locations_0_office_name', 'field_5bdf44985de41', 'no'),
(535, 'options_office_locations_0_office_address', '1161 W. Madison St.\r\nChicago, IL 60607', 'no'),
(536, '_options_office_locations_0_office_address', 'field_5bdf44a25de42', 'no'),
(537, 'options_office_locations', '2', 'no'),
(538, '_options_office_locations', 'field_5bdf44845de40', 'no'),
(547, 'options_footer_content', '1823 S. Michigan Ave. Chicago IL 60616 312.326.2121 | Fax: 312.326.7911\r\n© 2018 CENTURY 21 S.G.R., Inc. CENTURY 21® and the CENTURY 21 Logo are registered service marks owned by CENTURY 21 Real Estate LLC.\r\nCENTURY 21 Real Estate LLC fully supports the principles of the Fair Housing Act and the Equal Opportunity Act.\r\nEach Office is independently Owned and Operated.', 'no'),
(548, '_options_footer_content', 'field_5bdf4419b7690', 'no'),
(566, 'options_office_locations_1_office_name', 'TEST', 'no'),
(567, '_options_office_locations_1_office_name', 'field_5bdf44985de41', 'no'),
(568, 'options_office_locations_1_office_address', 'Address 1\r\nCity, State ZIP', 'no'),
(569, '_options_office_locations_1_office_address', 'field_5bdf44a25de42', 'no') ;

#
# End of data contents of table `wp_options`
# --------------------------------------------------------



#
# Delete any existing table `wp_postmeta`
#

DROP TABLE IF EXISTS `wp_postmeta`;


#
# Table structure of table `wp_postmeta`
#

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_postmeta`
#
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_edit_last', '1'),
(4, 5, '_edit_lock', '1537664134:1'),
(5, 8, '_edit_last', '1'),
(6, 8, '_edit_lock', '1540669286:1'),
(7, 10, '_edit_last', '1'),
(8, 10, '_edit_lock', '1540667269:1'),
(9, 12, '_edit_last', '1'),
(10, 12, '_edit_lock', '1540669669:1'),
(11, 14, '_edit_last', '1'),
(12, 14, '_edit_lock', '1537038876:1'),
(13, 16, '_edit_last', '1'),
(14, 16, '_edit_lock', '1537038888:1'),
(15, 27, '_edit_last', '1'),
(16, 27, '_edit_lock', '1540683796:1'),
(29, 33, '_wp_attached_file', '2018/09/Armando_Chacon240x360C.jpg'),
(30, 33, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:240;s:6:"height";i:320;s:4:"file";s:34:"2018/09/Armando_Chacon240x360C.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:34:"Armando_Chacon240x360C-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:34:"Armando_Chacon240x360C-225x300.jpg";s:5:"width";i:225;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:32:"twentyseventeen-thumbnail-avatar";a:4:{s:4:"file";s:34:"Armando_Chacon240x360C-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"8";s:6:"credit";s:0:"";s:6:"camera";s:18:"Canon PowerShot G2";s:7:"caption";s:0:"";s:17:"created_timestamp";s:9:"315532944";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"21";s:3:"iso";s:2:"50";s:13:"shutter_speed";s:4:"0.01";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(31, 27, '_thumbnail_id', '33'),
(32, 27, '_wp_page_template', 'page-agent.php'),
(33, 8, '_wp_page_template', 'page-agent-all.php'),
(34, 10, '_wp_page_template', 'default'),
(35, 12, '_wp_page_template', 'default'),
(36, 41, '_edit_last', '1'),
(37, 41, '_edit_lock', '1541373812:1'),
(38, 43, '_edit_last', '1'),
(39, 43, '_edit_lock', '1541373127:1'),
(40, 43, 'reviews', '4'),
(41, 43, '_reviews', 'field_5bddd9c8c5a3d'),
(42, 43, 'additional_languages_spoken', 'a:1:{i:0;s:7:"Spanish";}'),
(43, 43, '_additional_languages_spoken', 'field_5bddda93f632d'),
(44, 47, '_edit_last', '1'),
(45, 47, 'reviews', ''),
(46, 47, '_reviews', 'field_5bddd9c8c5a3d'),
(47, 47, 'additional_languages_spoken', ''),
(48, 47, '_additional_languages_spoken', 'field_5bddda93f632d'),
(49, 47, '_edit_lock', '1541372135:1'),
(50, 48, '_edit_last', '1'),
(51, 48, '_edit_lock', '1541350200:1'),
(52, 50, '_edit_last', '1'),
(53, 50, '_edit_lock', '1541353255:1'),
(54, 51, '_edit_last', '1'),
(55, 51, '_edit_lock', '1541353247:1'),
(56, 52, '_edit_last', '1'),
(57, 52, '_edit_lock', '1541353240:1'),
(58, 43, '_wp_page_template', 'page-agent.php'),
(59, 43, 'phone', '(555) 555-5555'),
(60, 43, '_phone', 'field_5bdf333aa22eb'),
(61, 43, 'fax', '(555) 555-5555'),
(62, 43, '_fax', 'field_5bdf334545d6a'),
(63, 43, 'email', 'armando.chacon@century21.com'),
(64, 43, '_email', 'field_5bdf334eb3741'),
(65, 43, 'website', 'https://www.century21.com/CENTURY-21-S.G.R.,-Inc.-7231c/Armando-Chacon-317563a'),
(66, 43, '_website', 'field_5bdf3357f2923'),
(67, 43, 'lifetime_sales', '$xx,xxx,xxx'),
(68, 43, '_lifetime_sales', 'field_5bdf33640fcce'),
(69, 43, 'number_of_reviews', '105'),
(70, 43, '_number_of_reviews', 'field_5bdf3374ae5c5'),
(71, 43, 'reviews_0_review_content', 'Armando was incredibly patient with our needs and priorities. He has a wealth of business and financial experience that really helped us during the contract/offer phase. He was always prompt, responsive and attentive to our questions. I would highly recommend working with him not only for his experience and knowledge of the local market but also because of his professionalism and attention to detail. He made the entire process easier.'),
(72, 43, '_reviews_0_review_content', 'field_5bdddae9626b8'),
(73, 43, 'reviews_0_review_site', 'zillow'),
(74, 43, '_reviews_0_review_site', 'field_5bdddaf9ecef4'),
(75, 43, 'reviews_1_review_content', 'Armando was solid for us. He has great balance with getting the job done and moving forward while not being too pushy in the process. That\'s tough to do in this business. Very professional all the way around.'),
(76, 43, '_reviews_1_review_content', 'field_5bdddae9626b8'),
(77, 43, 'reviews_1_review_site', 'real-satisfied'),
(78, 43, '_reviews_1_review_site', 'field_5bdddaf9ecef4'),
(79, 43, '_thumbnail_id', '33'),
(80, 61, '_edit_last', '1'),
(81, 61, '_edit_lock', '1541361128:1'),
(82, 63, '_edit_last', '1'),
(83, 63, '_edit_lock', '1541359190:1'),
(84, 43, 'primary_office', '1161 W. Madison St.<br />\r\nChicago, IL 60607'),
(85, 43, '_primary_office', 'field_5bdf46f27e47e'),
(86, 43, 'last_updated_on', '20181027'),
(87, 43, '_last_updated_on', 'field_5bdf4b622c3b4'),
(88, 43, 'zillow_url', 'https://www.zillow.com/profile/Armando-Chacon/'),
(89, 43, '_zillow_url', 'field_5bdf4b412c3b2'),
(90, 43, 'real_satisfied_url', 'http://www.realsatisfied.com/Armando-Chacon'),
(91, 43, '_real_satisfied_url', 'field_5bdf4b4b2c3b3'),
(92, 43, 'reviews_2_review_content', 'Armando helped sell our house in the West Loop, then we set him up with our friends who bought in the West Loop, and then he helped us BUY in Wicker Park.  Armando\'s responsiveness was impressive.  He stayed VERY connected to our deals both before and after close.  He was very fast to respond.  He   was our personal Real Estate Siri agent on call and we loved him for it.  We were very grateful for his flexibility and patience especially when we changed scope mid-process.'),
(93, 43, '_reviews_2_review_content', 'field_5bdddae9626b8'),
(94, 43, 'reviews_2_review_site', 'zillow'),
(95, 43, '_reviews_2_review_site', 'field_5bdddaf9ecef4'),
(96, 43, 'reviews_3_review_content', 'Armando Chacon and his colleagues were very professional and sold my condo efficiently.'),
(97, 43, '_reviews_3_review_content', 'field_5bdddae9626b8'),
(98, 43, 'reviews_3_review_site', 'real-satisfied'),
(99, 43, '_reviews_3_review_site', 'field_5bdddaf9ecef4'),
(100, 47, '_wp_page_template', 'page-agent.php'),
(101, 47, 'phone', ''),
(102, 47, '_phone', 'field_5bdf333aa22eb'),
(103, 47, 'fax', ''),
(104, 47, '_fax', 'field_5bdf334545d6a'),
(105, 47, 'email', ''),
(106, 47, '_email', 'field_5bdf334eb3741'),
(107, 47, 'website', ''),
(108, 47, '_website', 'field_5bdf3357f2923'),
(109, 47, 'lifetime_sales', ''),
(110, 47, '_lifetime_sales', 'field_5bdf33640fcce'),
(111, 47, 'last_updated_on', ''),
(112, 47, '_last_updated_on', 'field_5bdf4b622c3b4') ;
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(113, 47, 'primary_office', 'Address 1<br />\r\nCity, State ZIP'),
(114, 47, '_primary_office', 'field_5bdf46f27e47e'),
(115, 47, 'number_of_reviews', ''),
(116, 47, '_number_of_reviews', 'field_5bdf3374ae5c5'),
(117, 47, 'zillow_url', ''),
(118, 47, '_zillow_url', 'field_5bdf4b412c3b2'),
(119, 47, 'real_satisfied_url', ''),
(120, 47, '_real_satisfied_url', 'field_5bdf4b4b2c3b3'),
(121, 43, 'primary_office_address', '1161 W. Madison St.<br />\r\nChicago, IL 60607'),
(122, 43, '_primary_office_address', 'field_5bdf46f27e47e'),
(123, 43, 'primary_office_name', '1161 W. Madison St.<br />\r\nChicago, IL 60607'),
(124, 43, '_primary_office_name', 'field_5bdf78be3c9f1'),
(125, 47, 'primary_office_address', 'Address 1<br />\r\nCity, State ZIP'),
(126, 47, '_primary_office_address', 'field_5bdf46f27e47e'),
(127, 47, 'primary_office_name', 'Address 1<br />\r\nCity, State ZIP'),
(128, 47, '_primary_office_name', 'field_5bdf78be3c9f1'),
(129, 47, '_wp_trash_meta_status', 'publish'),
(130, 47, '_wp_trash_meta_time', '1541372544'),
(131, 47, '_wp_desired_post_slug', 'test') ;

#
# End of data contents of table `wp_postmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_posts`
#

DROP TABLE IF EXISTS `wp_posts`;


#
# Table structure of table `wp_posts`
#

CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_posts`
#
INSERT INTO `wp_posts` ( `ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2018-06-28 02:28:23', '2018-06-28 02:28:23', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2018-06-28 02:28:23', '2018-06-28 02:28:23', '', 0, 'http://nervedevelopment.com/c21/?p=1', 0, 'post', '', 1),
(2, 1, '2018-06-28 02:28:23', '2018-06-28 02:28:23', 'This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href="http://nervedevelopment.com/c21/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2018-06-28 02:28:23', '2018-06-28 02:28:23', '', 0, 'http://nervedevelopment.com/c21/?page_id=2', 0, 'page', '', 0),
(3, 1, '2018-06-28 02:28:23', '2018-06-28 02:28:23', '<h2>Who we are</h2><p>Our website address is: http://nervedevelopment.com/c21.</p><h2>What personal data we collect and why we collect it</h2><h3>Comments</h3><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><h3>Media</h3><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><h3>Contact forms</h3><h3>Cookies</h3><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><p>If you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><h3>Embedded content from other websites</h3><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracing your interaction with the embedded content if you have an account and are logged in to that website.</p><h3>Analytics</h3><h2>Who we share your data with</h2><h2>How long we retain your data</h2><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><h2>What rights you have over your data</h2><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><h2>Where we send your data</h2><p>Visitor comments may be checked through an automated spam detection service.</p><h2>Your contact information</h2><h2>Additional information</h2><h3>How we protect your data</h3><h3>What data breach procedures we have in place</h3><h3>What third parties we receive data from</h3><h3>What automated decision making and/or profiling we do with user data</h3><h3>Industry regulatory disclosure requirements</h3>', 'Privacy Policy', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2018-06-28 02:28:23', '2018-06-28 02:28:23', '', 0, 'http://nervedevelopment.com/c21/?page_id=3', 0, 'page', '', 0),
(5, 1, '2018-08-31 01:04:29', '2018-08-31 01:04:29', '[WPL kind="0" sf_locationtextsearch="Chicago, IL" sf_unit_price="260" limit="12" wplorderby="p.mls_id" wplorder="DESC" wplcolumns="3"]', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2018-09-21 02:15:36', '2018-09-21 02:15:36', '', 0, 'http://nervedevelopment.com/c21/?page_id=5', 0, 'page', '', 0),
(6, 1, '2018-08-31 01:04:29', '2018-08-31 01:04:29', 'Text', 'Home', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2018-08-31 01:04:29', '2018-08-31 01:04:29', '', 5, 'http://nervedevelopment.com/c21/2018/08/31/5-revision-v1/', 0, 'revision', '', 0),
(8, 1, '2018-09-15 18:13:36', '2018-09-15 18:13:36', '', 'Agents', '', 'publish', 'closed', 'closed', '', 'agents', '', '', '2018-10-27 19:43:40', '2018-10-27 19:43:40', '', 0, 'http://nervedevelopment.com/c21/?page_id=8', 0, 'page', '', 0),
(9, 1, '2018-09-15 18:13:36', '2018-09-15 18:13:36', '', 'Agents', '', 'inherit', 'closed', 'closed', '', '8-revision-v1', '', '', '2018-09-15 18:13:36', '2018-09-15 18:13:36', '', 8, 'http://nervedevelopment.com/c21/2018/09/15/8-revision-v1/', 0, 'revision', '', 0),
(10, 1, '2018-09-15 19:16:36', '2018-09-15 19:16:36', 'Diverse neighborhoods make Chicago real estate unique.   For this reason, it is critical to find an agent who have intimate knowledge of the more than 70 distinct neighborhoods that make up the great city of Chicago.  Each agent can offer you a wealth of information about the unique character and lifestyle of a neighborhood, and help you find the home that best suits your own unique real estate goals and requirements.  Learn more about specific Chicago neighborhoods by clicking the links below. You will find descriptions of each general neighborhood area, as well as information about communities within each neighborhood, plus neighborhood maps, amenities and restaurants and listings of featured properties.', 'Neighborhood Pricing', '', 'publish', 'closed', 'closed', '', 'neighborhood-pricing', '', '', '2018-10-27 19:10:11', '2018-10-27 19:10:11', '', 0, 'http://nervedevelopment.com/c21/?page_id=10', 0, 'page', '', 0),
(11, 1, '2018-09-15 19:16:36', '2018-09-15 19:16:36', '', 'Neighborhood Pricing', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2018-09-15 19:16:36', '2018-09-15 19:16:36', '', 10, 'http://nervedevelopment.com/c21/2018/09/15/10-revision-v1/', 0, 'revision', '', 0),
(12, 1, '2018-09-15 19:16:47', '2018-09-15 19:16:47', 'Century21 S.G.R., Inc. is one of the most successful real estate companies in the United States.  The Century21 S.G.R., Inc. team is made up of highly experienced and successful managers who have a proven track record of helping agents to reach their full potential. Century21 S.G.R., Inc. offers training and mentoring that is unmatched in the industry.', 'About Us', '', 'publish', 'closed', 'closed', '', 'about-us', '', '', '2018-10-27 19:50:11', '2018-10-27 19:50:11', '', 0, 'http://nervedevelopment.com/c21/?page_id=12', 0, 'page', '', 0),
(13, 1, '2018-09-15 19:16:47', '2018-09-15 19:16:47', '', 'About Us', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2018-09-15 19:16:47', '2018-09-15 19:16:47', '', 12, 'http://nervedevelopment.com/c21/2018/09/15/12-revision-v1/', 0, 'revision', '', 0),
(14, 1, '2018-09-15 19:16:58', '2018-09-15 19:16:58', '', 'Rental Package', '', 'publish', 'closed', 'closed', '', 'rental-package', '', '', '2018-09-15 19:16:58', '2018-09-15 19:16:58', '', 0, 'http://nervedevelopment.com/c21/?page_id=14', 0, 'page', '', 0),
(15, 1, '2018-09-15 19:16:58', '2018-09-15 19:16:58', '', 'Rental Package', '', 'inherit', 'closed', 'closed', '', '14-revision-v1', '', '', '2018-09-15 19:16:58', '2018-09-15 19:16:58', '', 14, 'http://nervedevelopment.com/c21/2018/09/15/14-revision-v1/', 0, 'revision', '', 0),
(16, 1, '2018-09-15 19:17:10', '2018-09-15 19:17:10', '', 'Agent Login', '', 'publish', 'closed', 'closed', '', 'agent-login', '', '', '2018-09-15 19:17:10', '2018-09-15 19:17:10', '', 0, 'http://nervedevelopment.com/c21/?page_id=16', 0, 'page', '', 0),
(17, 1, '2018-09-15 19:17:10', '2018-09-15 19:17:10', '', 'Agent Login', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2018-09-15 19:17:10', '2018-09-15 19:17:10', '', 16, 'http://nervedevelopment.com/c21/2018/09/15/16-revision-v1/', 0, 'revision', '', 0),
(18, 1, '2018-09-21 02:04:17', '2018-09-21 02:04:17', '[WPL]', 'Properties', '', 'publish', 'closed', 'closed', '', 'properties', '', '', '2018-09-21 02:04:17', '2018-09-21 02:04:17', '', 0, 'http://nervedevelopment.com/c21/properties/', 0, 'page', '', 0),
(21, 1, '2018-09-21 02:04:17', '2018-09-21 02:04:17', '[WPL sf_select_listing="9"]', 'For Sale', '', 'publish', 'closed', 'closed', '', 'for-sale', '', '', '2018-09-21 02:04:17', '2018-09-21 02:04:17', '', 0, 'http://nervedevelopment.com/c21/for-sale/', 0, 'page', '', 0),
(23, 1, '2018-09-21 02:04:17', '2018-09-21 02:04:17', '[WPL sf_select_listing="10"]', 'For Rent', '', 'publish', 'closed', 'closed', '', 'for-rent', '', '', '2018-09-21 02:04:17', '2018-09-21 02:04:17', '', 0, 'http://nervedevelopment.com/c21/for-rent/', 0, 'page', '', 0),
(25, 1, '2018-09-21 02:04:17', '2018-09-21 02:04:17', '[WPL sf_select_listing="12"]', 'Vacation Rental', '', 'publish', 'closed', 'closed', '', 'vacation-rental', '', '', '2018-09-21 02:04:17', '2018-09-21 02:04:17', '', 0, 'http://nervedevelopment.com/c21/vacation-rental/', 0, 'page', '', 0),
(26, 1, '2018-09-21 02:15:36', '2018-09-21 02:15:36', '[WPL kind="0" sf_locationtextsearch="Chicago, IL" sf_unit_price="260" limit="12" wplorderby="p.mls_id" wplorder="DESC" wplcolumns="3"]', 'Home', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2018-09-21 02:15:36', '2018-09-21 02:15:36', '', 5, 'http://nervedevelopment.com/c21/2018/09/21/5-revision-v1/', 0, 'revision', '', 0),
(27, 1, '2018-09-22 20:44:45', '2018-09-22 20:44:45', 'A lifelong Chicagoan, Armando received his Bachelor of Science in Accounting from the University of Illinois at Chicago. Real Estate has been one of his passions for the last 10 years. Prior to joining Century 21 S.G.R., Armando was the Director of Finance at Jones Lang LaSalle, an international commercial real estate firm. Before Jones Lang LaSalle, he managed a 70-property, 3,500,000 square foot real estate portfolio for Com Ed. Looking for a more personal connection to real estate, he joined Century 21 S.G.R. in 2004 to pursue residential sales, which he thought would allow him to make more of a difference in people’s lives.\r\n\r\nArmando brings his complex business background, analytical skills, passion, and positive energy to his clients. His love for the city and urban living is evident in his knowledge of emerging neighborhoods and their attributes. He sees obstacles as an opportunity to demonstrate his value and always places his clients’ best interests first. His problem solving and analytical skills are evident during each phase of buying and selling a home.\r\n\r\nHe attributes his success to high ethical principles, having a plan, and willingness to constantly make adjustments along the way.\r\n\r\nArmando’s other passions are his family, baseball, jazz/blues, traveling, beach volleyball, and working as a controller for Operation Homelink, a non-for-profit organization devoted to providing free computers to the families of our soldiers serving around the world.', 'Armando Chacon', '', 'publish', 'closed', 'closed', '', 'armando-chacon', '', '', '2018-10-27 23:45:25', '2018-10-27 23:45:25', '', 8, 'http://nervedevelopment.com/c21/?page_id=27', 0, 'page', '', 0),
(28, 1, '2018-09-22 20:44:45', '2018-09-22 20:44:45', '', 'Armando Chacon', '', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2018-09-22 20:44:45', '2018-09-22 20:44:45', '', 27, 'http://nervedevelopment.com/c21/2018/09/22/27-revision-v1/', 0, 'revision', '', 0),
(33, 1, '2018-09-23 00:39:22', '2018-09-23 00:39:22', '', 'Armando_Chacon240x360C', '', 'inherit', 'open', 'closed', '', 'armando_chacon240x360c', '', '', '2018-09-23 00:39:22', '2018-09-23 00:39:22', '', 27, 'http://nervedevelopment.com/c21/wp-content/uploads/2018/09/Armando_Chacon240x360C.jpg', 0, 'attachment', 'image/jpeg', 0),
(34, 1, '2018-09-23 00:42:01', '2018-09-23 00:42:01', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\nBut I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?', 'Armando Chacon', '', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2018-09-23 00:42:01', '2018-09-23 00:42:01', '', 27, 'http://nervedevelopment.com/c21/2018/09/23/27-revision-v1/', 0, 'revision', '', 0),
(36, 1, '2018-10-27 19:10:11', '2018-10-27 19:10:11', 'Diverse neighborhoods make Chicago real estate unique.   For this reason, it is critical to find an agent who have intimate knowledge of the more than 70 distinct neighborhoods that make up the great city of Chicago.  Each agent can offer you a wealth of information about the unique character and lifestyle of a neighborhood, and help you find the home that best suits your own unique real estate goals and requirements.  Learn more about specific Chicago neighborhoods by clicking the links below. You will find descriptions of each general neighborhood area, as well as information about communities within each neighborhood, plus neighborhood maps, amenities and restaurants and listings of featured properties.', 'Neighborhood Pricing', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2018-10-27 19:10:11', '2018-10-27 19:10:11', '', 10, 'http://nervedevelopment.com/c21/2018/10/27/10-revision-v1/', 0, 'revision', '', 0),
(37, 1, '2018-10-27 19:43:00', '2018-10-27 19:43:00', 'We are a multicultural real estate company.', 'Agents', '', 'inherit', 'closed', 'closed', '', '8-revision-v1', '', '', '2018-10-27 19:43:00', '2018-10-27 19:43:00', '', 8, 'http://nervedevelopment.com/c21/2018/10/27/8-revision-v1/', 0, 'revision', '', 0),
(38, 1, '2018-10-27 19:43:40', '2018-10-27 19:43:40', '', 'Agents', '', 'inherit', 'closed', 'closed', '', '8-revision-v1', '', '', '2018-10-27 19:43:40', '2018-10-27 19:43:40', '', 8, 'http://nervedevelopment.com/c21/2018/10/27/8-revision-v1/', 0, 'revision', '', 0),
(39, 1, '2018-10-27 19:50:11', '2018-10-27 19:50:11', 'Century21 S.G.R., Inc. is one of the most successful real estate companies in the United States.  The Century21 S.G.R., Inc. team is made up of highly experienced and successful managers who have a proven track record of helping agents to reach their full potential. Century21 S.G.R., Inc. offers training and mentoring that is unmatched in the industry.', 'About Us', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2018-10-27 19:50:11', '2018-10-27 19:50:11', '', 12, 'http://nervedevelopment.com/c21/2018/10/27/12-revision-v1/', 0, 'revision', '', 0),
(40, 1, '2018-10-27 23:45:25', '2018-10-27 23:45:25', 'A lifelong Chicagoan, Armando received his Bachelor of Science in Accounting from the University of Illinois at Chicago. Real Estate has been one of his passions for the last 10 years. Prior to joining Century 21 S.G.R., Armando was the Director of Finance at Jones Lang LaSalle, an international commercial real estate firm. Before Jones Lang LaSalle, he managed a 70-property, 3,500,000 square foot real estate portfolio for Com Ed. Looking for a more personal connection to real estate, he joined Century 21 S.G.R. in 2004 to pursue residential sales, which he thought would allow him to make more of a difference in people’s lives.\r\n\r\nArmando brings his complex business background, analytical skills, passion, and positive energy to his clients. His love for the city and urban living is evident in his knowledge of emerging neighborhoods and their attributes. He sees obstacles as an opportunity to demonstrate his value and always places his clients’ best interests first. His problem solving and analytical skills are evident during each phase of buying and selling a home.\r\n\r\nHe attributes his success to high ethical principles, having a plan, and willingness to constantly make adjustments along the way.\r\n\r\nArmando’s other passions are his family, baseball, jazz/blues, traveling, beach volleyball, and working as a controller for Operation Homelink, a non-for-profit organization devoted to providing free computers to the families of our soldiers serving around the world.', 'Armando Chacon', '', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2018-10-27 23:45:25', '2018-10-27 23:45:25', '', 27, 'http://nervedevelopment.com/c21/2018/10/27/27-revision-v1/', 0, 'revision', '', 0),
(41, 1, '2018-11-03 17:24:17', '2018-11-03 17:24:17', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:6:"agents";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'Agents', 'agents', 'publish', 'closed', 'closed', '', 'group_5bddd9b5e5503', '', '', '2018-11-04 22:56:14', '2018-11-04 22:56:14', '', 0, 'http://nervedevelopment.com/c21/?post_type=acf-field-group&#038;p=41', 0, 'acf-field-group', '', 0),
(42, 1, '2018-11-03 17:24:45', '2018-11-03 17:24:45', 'a:10:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"collapsed";s:0:"";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:3:"row";s:12:"button_label";s:0:"";}', 'Reviews', 'reviews', 'publish', 'closed', 'closed', '', 'field_5bddd9c8c5a3d', '', '', '2018-11-04 19:41:51', '2018-11-04 19:41:51', '', 41, 'http://nervedevelopment.com/c21/?post_type=acf-field&#038;p=42', 11, 'acf-field', '', 0),
(43, 1, '2018-11-03 17:27:28', '2018-11-03 17:27:28', 'A lifelong Chicagoan, Armando received his Bachelor of Science in Accounting from the University of Illinois at Chicago. Real Estate has been one of his passions for the last 10 years. Prior to joining Century 21 S.G.R., Armando was the Director of Finance at Jones Lang LaSalle, an international commercial real estate firm. Before Jones Lang LaSalle, he managed a 70-property, 3,500,000 square foot real estate portfolio for Com Ed. Looking for a more personal connection to real estate, he joined Century 21 S.G.R. in 2004 to pursue residential sales, which he thought would allow him to make more of a difference in people’s lives.\r\n\r\nArmando brings his complex business background, analytical skills, passion, and positive energy to his clients. His love for the city and urban living is evident in his knowledge of emerging neighborhoods and their attributes. He sees obstacles as an opportunity to demonstrate his value and always places his clients’ best interests first. His problem solving and analytical skills are evident during each phase of buying and selling a home.\r\n\r\nHe attributes his success to high ethical principles, having a plan, and willingness to constantly make adjustments along the way.\r\n\r\nArmando’s other passions are his family, baseball, jazz/blues, traveling, beach volleyball, and working as a controller for Operation Homelink, a non-for-profit organization devoted to providing free computers to the families of our soldiers serving around the world.', 'ARMANDO CHACON', '', 'publish', 'closed', 'closed', '', 'armando-chacon', '', '', '2018-11-04 23:05:25', '2018-11-04 23:05:25', '', 0, 'http://nervedevelopment.com/c21/?post_type=agents&#038;p=43', 0, 'agents', '', 0),
(44, 1, '2018-11-03 17:29:06', '2018-11-03 17:29:06', 'a:12:{s:4:"type";s:8:"checkbox";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"choices";a:4:{s:7:"Spanish";s:7:"Spanish";s:6:"French";s:6:"French";s:4:"Thai";s:4:"Thai";s:5:"Greek";s:5:"Greek";}s:12:"allow_custom";i:0;s:11:"save_custom";i:0;s:13:"default_value";a:0:{}s:6:"layout";s:8:"vertical";s:6:"toggle";i:0;s:13:"return_format";s:5:"value";}', 'Additional Languages Spoken', 'additional_languages_spoken', 'publish', 'closed', 'closed', '', 'field_5bddda93f632d', '', '', '2018-11-04 19:41:51', '2018-11-04 19:41:51', '', 41, 'http://nervedevelopment.com/c21/?post_type=acf-field&#038;p=44', 7, 'acf-field', '', 0),
(45, 1, '2018-11-03 17:29:25', '2018-11-03 17:29:25', 'a:10:{s:4:"type";s:8:"textarea";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:9:"maxlength";s:0:"";s:4:"rows";s:0:"";s:9:"new_lines";s:0:"";}', 'Review Content', 'review_content', 'publish', 'closed', 'closed', '', 'field_5bdddae9626b8', '', '', '2018-11-04 18:03:26', '2018-11-04 18:03:26', '', 42, 'http://nervedevelopment.com/c21/?post_type=acf-field&#038;p=45', 0, 'acf-field', '', 0),
(46, 1, '2018-11-03 17:29:57', '2018-11-03 17:29:57', 'a:12:{s:4:"type";s:5:"radio";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"choices";a:2:{s:6:"zillow";s:6:"zillow";s:14:"real-satisfied";s:14:"real-satisfied";}s:10:"allow_null";i:0;s:12:"other_choice";i:0;s:17:"save_other_choice";i:0;s:13:"default_value";s:0:"";s:6:"layout";s:8:"vertical";s:13:"return_format";s:5:"value";}', 'Review Site', 'review_site', 'publish', 'closed', 'closed', '', 'field_5bdddaf9ecef4', '', '', '2018-11-04 20:56:26', '2018-11-04 20:56:26', '', 42, 'http://nervedevelopment.com/c21/?post_type=acf-field&#038;p=46', 1, 'acf-field', '', 0),
(47, 1, '2018-11-03 17:32:44', '2018-11-03 17:32:44', '', 'test', '', 'trash', 'closed', 'closed', '', 'test__trashed', '', '', '2018-11-04 23:02:24', '2018-11-04 23:02:24', '', 0, 'http://nervedevelopment.com/c21/?post_type=agents&#038;p=47', 0, 'agents', '', 0),
(48, 1, '2018-11-03 17:49:06', '2018-11-03 17:49:06', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Gold Coast', '', 'publish', 'open', 'open', '', 'gold-coast', '', '', '2018-11-04 16:27:03', '2018-11-04 16:27:03', '', 0, 'http://nervedevelopment.com/c21/?post_type=neighborhoods&#038;p=48', 0, 'neighborhoods', '', 0),
(49, 1, '2018-11-04 16:19:31', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-11-04 16:19:31', '0000-00-00 00:00:00', '', 0, 'http://nervedevelopment.com/c21/?p=49', 0, 'post', '', 0),
(50, 1, '2018-11-04 16:27:39', '2018-11-04 16:27:39', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'South Loop', '', 'publish', 'open', 'open', '', 'south-loop', '', '', '2018-11-04 17:43:17', '2018-11-04 17:43:17', '', 0, 'http://nervedevelopment.com/c21/?post_type=neighborhoods&#038;p=50', 0, 'neighborhoods', '', 0),
(51, 1, '2018-11-04 16:28:25', '2018-11-04 16:28:25', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Pilsen', '', 'publish', 'open', 'open', '', 'pilsen', '', '', '2018-11-04 17:43:10', '2018-11-04 17:43:10', '', 0, 'http://nervedevelopment.com/c21/?post_type=neighborhoods&#038;p=51', 0, 'neighborhoods', '', 0),
(52, 1, '2018-11-04 17:15:52', '2018-11-04 17:15:52', '', 'Lincoln Park', '', 'publish', 'closed', 'open', '', 'lincoln-park', '', '', '2018-11-04 17:43:03', '2018-11-04 17:43:03', '', 0, 'http://nervedevelopment.com/c21/?post_type=neighborhoods&#038;p=52', 0, 'neighborhoods', '', 0),
(53, 1, '2018-11-04 17:58:25', '2018-11-04 17:58:25', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Phone', 'phone', 'publish', 'closed', 'closed', '', 'field_5bdf333aa22eb', '', '', '2018-11-04 17:58:33', '2018-11-04 17:58:33', '', 41, 'http://nervedevelopment.com/c21/?post_type=acf-field&#038;p=53', 0, 'acf-field', '', 0),
(54, 1, '2018-11-04 17:58:33', '2018-11-04 17:58:33', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Fax', 'fax', 'publish', 'closed', 'closed', '', 'field_5bdf334545d6a', '', '', '2018-11-04 17:58:43', '2018-11-04 17:58:43', '', 41, 'http://nervedevelopment.com/c21/?post_type=acf-field&#038;p=54', 1, 'acf-field', '', 0),
(55, 1, '2018-11-04 17:58:43', '2018-11-04 17:58:43', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Email', 'email', 'publish', 'closed', 'closed', '', 'field_5bdf334eb3741', '', '', '2018-11-04 17:58:53', '2018-11-04 17:58:53', '', 41, 'http://nervedevelopment.com/c21/?post_type=acf-field&#038;p=55', 2, 'acf-field', '', 0),
(56, 1, '2018-11-04 17:58:53', '2018-11-04 17:58:53', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Website', 'website', 'publish', 'closed', 'closed', '', 'field_5bdf3357f2923', '', '', '2018-11-04 17:59:06', '2018-11-04 17:59:06', '', 41, 'http://nervedevelopment.com/c21/?post_type=acf-field&#038;p=56', 3, 'acf-field', '', 0),
(57, 1, '2018-11-04 17:59:06', '2018-11-04 17:59:06', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Lifetime Sales', 'lifetime_sales', 'publish', 'closed', 'closed', '', 'field_5bdf33640fcce', '', '', '2018-11-04 17:59:41', '2018-11-04 17:59:41', '', 41, 'http://nervedevelopment.com/c21/?post_type=acf-field&#038;p=57', 4, 'acf-field', '', 0),
(58, 1, '2018-11-04 17:59:41', '2018-11-04 17:59:41', 'a:12:{s:4:"type";s:6:"number";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:3:"min";s:0:"";s:3:"max";s:0:"";s:4:"step";s:0:"";}', 'Number of reviews', 'number_of_reviews', 'publish', 'closed', 'closed', '', 'field_5bdf3374ae5c5', '', '', '2018-11-04 19:41:51', '2018-11-04 19:41:51', '', 41, 'http://nervedevelopment.com/c21/?post_type=acf-field&#038;p=58', 8, 'acf-field', '', 0),
(59, 1, '2018-11-04 19:04:51', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-11-04 19:04:51', '0000-00-00 00:00:00', '', 0, 'http://nervedevelopment.com/c21/?post_type=acf-field-group&p=59', 0, 'acf-field-group', '', 0),
(60, 1, '2018-11-04 19:05:13', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-11-04 19:05:13', '0000-00-00 00:00:00', '', 0, 'http://nervedevelopment.com/c21/?post_type=acf-field-group&p=60', 0, 'acf-field-group', '', 0),
(61, 1, '2018-11-04 19:10:40', '2018-11-04 19:10:40', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:12:"options_page";s:8:"operator";s:2:"==";s:5:"value";s:18:"acf-options-footer";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'Footer text', 'footer-text', 'publish', 'closed', 'closed', '', 'group_5bdf441255b75', '', '', '2018-11-04 19:13:15', '2018-11-04 19:13:15', '', 0, 'http://nervedevelopment.com/c21/?post_type=acf-field-group&#038;p=61', 0, 'acf-field-group', '', 0),
(62, 1, '2018-11-04 19:10:40', '2018-11-04 19:10:40', 'a:10:{s:4:"type";s:8:"textarea";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:9:"maxlength";s:0:"";s:4:"rows";s:0:"";s:9:"new_lines";s:7:"wpautop";}', 'Footer Content', 'footer_content', 'publish', 'closed', 'closed', '', 'field_5bdf4419b7690', '', '', '2018-11-04 19:13:15', '2018-11-04 19:13:15', '', 61, 'http://nervedevelopment.com/c21/?post_type=acf-field&#038;p=62', 0, 'acf-field', '', 0),
(63, 1, '2018-11-04 19:12:55', '2018-11-04 19:12:55', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:12:"options_page";s:8:"operator";s:2:"==";s:5:"value";s:19:"acf-options-offices";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'Offices', 'offices', 'publish', 'closed', 'closed', '', 'group_5bdf447c00a02', '', '', '2018-11-04 19:12:55', '2018-11-04 19:12:55', '', 0, 'http://nervedevelopment.com/c21/?post_type=acf-field-group&#038;p=63', 0, 'acf-field-group', '', 0),
(64, 1, '2018-11-04 19:12:55', '2018-11-04 19:12:55', 'a:10:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"collapsed";s:0:"";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"table";s:12:"button_label";s:0:"";}', 'Office locations', 'office_locations', 'publish', 'closed', 'closed', '', 'field_5bdf44845de40', '', '', '2018-11-04 19:12:55', '2018-11-04 19:12:55', '', 63, 'http://nervedevelopment.com/c21/?post_type=acf-field&p=64', 0, 'acf-field', '', 0),
(65, 1, '2018-11-04 19:12:55', '2018-11-04 19:12:55', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Office Name', 'office_name', 'publish', 'closed', 'closed', '', 'field_5bdf44985de41', '', '', '2018-11-04 19:12:55', '2018-11-04 19:12:55', '', 64, 'http://nervedevelopment.com/c21/?post_type=acf-field&p=65', 0, 'acf-field', '', 0),
(66, 1, '2018-11-04 19:12:55', '2018-11-04 19:12:55', 'a:10:{s:4:"type";s:8:"textarea";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:9:"maxlength";s:0:"";s:4:"rows";s:0:"";s:9:"new_lines";s:2:"br";}', 'Office Address', 'office_address', 'publish', 'closed', 'closed', '', 'field_5bdf44a25de42', '', '', '2018-11-04 19:12:55', '2018-11-04 19:12:55', '', 64, 'http://nervedevelopment.com/c21/?post_type=acf-field&p=66', 1, 'acf-field', '', 0),
(67, 1, '2018-11-04 19:23:10', '2018-11-04 19:23:10', 'a:13:{s:4:"type";s:6:"select";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"choices";a:4:{s:25:"1161 W. Madison St.<br />";s:25:"1161 W. Madison St.<br />";s:17:"Chicago, IL 60607";s:16:"WEST LOOP OFFICE";s:15:"Address 1<br />";s:15:"Address 1<br />";s:15:"City, State ZIP";s:4:"TEST";}s:13:"default_value";a:0:{}s:10:"allow_null";i:0;s:8:"multiple";i:0;s:2:"ui";i:0;s:4:"ajax";i:0;s:13:"return_format";s:5:"value";s:11:"placeholder";s:0:"";}', 'Primary Office Address', 'primary_office_address', 'publish', 'closed', 'closed', '', 'field_5bdf46f27e47e', '', '', '2018-11-04 22:56:14', '2018-11-04 22:56:14', '', 41, 'http://nervedevelopment.com/c21/?post_type=acf-field&#038;p=67', 6, 'acf-field', '', 0),
(68, 1, '2018-11-04 19:41:51', '2018-11-04 19:41:51', 'a:8:{s:4:"type";s:11:"date_picker";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:14:"display_format";s:5:"m/d/Y";s:13:"return_format";s:5:"m/d/Y";s:9:"first_day";i:1;}', 'Last Updated on', 'last_updated_on', 'publish', 'closed', 'closed', '', 'field_5bdf4b622c3b4', '', '', '2018-11-04 19:48:02', '2018-11-04 19:48:02', '', 41, 'http://nervedevelopment.com/c21/?post_type=acf-field&#038;p=68', 5, 'acf-field', '', 0),
(69, 1, '2018-11-04 19:41:51', '2018-11-04 19:41:51', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Zillow URL', 'zillow_url', 'publish', 'closed', 'closed', '', 'field_5bdf4b412c3b2', '', '', '2018-11-04 19:41:51', '2018-11-04 19:41:51', '', 41, 'http://nervedevelopment.com/c21/?post_type=acf-field&p=69', 9, 'acf-field', '', 0),
(70, 1, '2018-11-04 19:41:51', '2018-11-04 19:41:51', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Real Satisfied URL', 'real_satisfied_url', 'publish', 'closed', 'closed', '', 'field_5bdf4b4b2c3b3', '', '', '2018-11-04 19:41:51', '2018-11-04 19:41:51', '', 41, 'http://nervedevelopment.com/c21/?post_type=acf-field&p=70', 10, 'acf-field', '', 0),
(71, 1, '2018-11-04 22:56:14', '2018-11-04 22:56:14', 'a:13:{s:4:"type";s:6:"select";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"choices";a:0:{}s:13:"default_value";a:0:{}s:10:"allow_null";i:0;s:8:"multiple";i:0;s:2:"ui";i:0;s:4:"ajax";i:0;s:13:"return_format";s:5:"label";s:11:"placeholder";s:0:"";}', 'Primary Office Name', 'primary_office_name', 'publish', 'closed', 'closed', '', 'field_5bdf78be3c9f1', '', '', '2018-11-04 22:56:14', '2018-11-04 22:56:14', '', 41, 'http://nervedevelopment.com/c21/?post_type=acf-field&p=71', 12, 'acf-field', '', 0) ;

#
# End of data contents of table `wp_posts`
# --------------------------------------------------------



#
# Delete any existing table `wp_term_relationships`
#

DROP TABLE IF EXISTS `wp_term_relationships`;


#
# Table structure of table `wp_term_relationships`
#

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_term_relationships`
#
INSERT INTO `wp_term_relationships` ( `object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(48, 2, 0),
(50, 4, 0),
(50, 10, 0),
(51, 5, 0),
(51, 10, 0),
(52, 2, 0),
(52, 10, 0) ;

#
# End of data contents of table `wp_term_relationships`
# --------------------------------------------------------



#
# Delete any existing table `wp_term_taxonomy`
#

DROP TABLE IF EXISTS `wp_term_taxonomy`;


#
# Table structure of table `wp_term_taxonomy`
#

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_term_taxonomy`
#
INSERT INTO `wp_term_taxonomy` ( `term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'neighborhoods_categories', '', 0, 2),
(3, 3, 'neighborhoods_categories', '', 0, 0),
(4, 4, 'neighborhoods_categories', '', 0, 1),
(5, 5, 'neighborhoods_categories', '', 0, 1),
(6, 6, 'neighborhoods_categories', '', 0, 0),
(7, 7, 'neighborhoods_categories', '', 0, 0),
(8, 8, 'neighborhoods_categories', '', 0, 0),
(9, 9, 'neighborhoods_categories', '', 0, 0),
(10, 10, 'neighborhoods_categories', '', 0, 3) ;

#
# End of data contents of table `wp_term_taxonomy`
# --------------------------------------------------------



#
# Delete any existing table `wp_termmeta`
#

DROP TABLE IF EXISTS `wp_termmeta`;


#
# Table structure of table `wp_termmeta`
#

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_termmeta`
#

#
# End of data contents of table `wp_termmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_terms`
#

DROP TABLE IF EXISTS `wp_terms`;


#
# Table structure of table `wp_terms`
#

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_terms`
#
INSERT INTO `wp_terms` ( `term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'North', 'north', 0),
(3, 'Northwest', 'northwest', 0),
(4, 'South', 'south', 0),
(5, 'Southwest', 'southwest', 0),
(6, 'Suburbs - North', 'suburbs-north', 0),
(7, 'Suburbs – Northwest', 'suburbs-northwest', 0),
(8, 'Suburbs – South', 'suburbs-south', 0),
(9, 'Suburbs – Southwest', 'suburbs-southwest', 0),
(10, 'Featured', 'featured', 0) ;

#
# End of data contents of table `wp_terms`
# --------------------------------------------------------



#
# Delete any existing table `wp_usermeta`
#

DROP TABLE IF EXISTS `wp_usermeta`;


#
# Table structure of table `wp_usermeta`
#

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_usermeta`
#
INSERT INTO `wp_usermeta` ( `umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy'),
(15, 1, 'show_welcome_panel', '0'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '49'),
(18, 1, 'session_tokens', 'a:1:{s:64:"c10fa1a84ffb2c3a636511af30e3c5412d5c7ffd8654e9226996580c57788ef6";a:4:{s:10:"expiration";i:1541436731;s:2:"ip";s:3:"::1";s:2:"ua";s:120:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36";s:5:"login";i:1541263931;}}'),
(19, 1, 'wp_user-settings', 'libraryContent=browse'),
(20, 1, 'wp_user-settings-time', '1537663161'),
(21, 1, 'closedpostboxes_agents', 'a:3:{i:0;s:13:"trackbacksdiv";i:1;s:16:"commentstatusdiv";i:2;s:11:"commentsdiv";}'),
(22, 1, 'metaboxhidden_agents', 'a:1:{i:0;s:7:"slugdiv";}'),
(23, 1, 'acf_user_settings', 'a:1:{s:15:"show_field_keys";s:1:"1";}') ;

#
# End of data contents of table `wp_usermeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_users`
#

DROP TABLE IF EXISTS `wp_users`;


#
# Table structure of table `wp_users`
#

CREATE TABLE `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_users`
#
INSERT INTO `wp_users` ( `ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$B6XmJAl7WCBHk1vB4nRdwMCAwlIoVQ.', 'admin', 'brooke.neace@gmail.com', '', '2018-06-28 02:28:23', '', 0, 'admin') ;

#
# End of data contents of table `wp_users`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_activities`
#

DROP TABLE IF EXISTS `wp_wpl_activities`;


#
# Table structure of table `wp_wpl_activities`
#

CREATE TABLE `wp_wpl_activities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `activity` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `position` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  `index` float(5,2) NOT NULL DEFAULT '99.00',
  `params` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `show_title` tinyint(4) NOT NULL DEFAULT '1',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `association_type` tinyint(4) NOT NULL DEFAULT '1',
  `associations` text CHARACTER SET utf8,
  `client` tinyint(4) NOT NULL DEFAULT '2',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_activities`
#
INSERT INTO `wp_wpl_activities` ( `id`, `activity`, `position`, `enabled`, `index`, `params`, `show_title`, `title`, `association_type`, `associations`, `client`) VALUES
(2, 'listing_links', 'plisting_position2', 1, '0.03', '{"facebook":"1","google_plus":"1","twitter":"1","pinterest":"1"}', 0, '', 1, NULL, 2),
(3, 'listing_gallery:pshow', 'pshow_gallery', 1, '0.05', '{"image_width":"1600","image_height":"420","image_class":"","rewrite":"0","watermark":"0"}', 0, '', 1, NULL, 2),
(4, 'listing_links:pshow', 'pshow_position2', 1, '0.04', '{"facebook":"1","google_plus":"1","twitter":"1","pinterest":"1"}', 0, '', 1, NULL, 2),
(5, 'property_manager_actions', 'pmanager_position2', 1, '2.00', '', 0, '', 1, NULL, 2),
(6, 'listing_gallery:pmanager', 'pmanager_position3', 1, '0.12', '', 0, '', 1, NULL, 2),
(7, 'agent_info', 'pshow_position2', 1, '0.06', '', 1, 'Agent info', 1, NULL, 2),
(8, 'listing_rooms', 'pshow_position2', 1, '0.07', '', 1, 'Property Rooms', 1, NULL, 2),
(9, 'listing_attachments', 'pshow_position2', 1, '0.08', '', 1, 'Attachments', 1, NULL, 2),
(10, 'qrcode:default', 'pshow_qr_code', 1, '0.10', '{"picture_width":"90","picture_height":"90","outer_margin":"2","size":"4"}', 1, 'QR Code', 1, NULL, 2),
(11, 'listing_videos', 'pshow_video', 0, '0.14', '', 0, '', 1, NULL, 2),
(12, 'agent_info:profileshow', 'profile_show_position1', 1, '0.02', '', 1, 'Agent info', 1, NULL, 2),
(13, 'listing_gallery:simple', 'wpl_property_listing_image', 1, '0.01', '{"image_width":"285","image_height":"200","image_class":"","rewrite":"0","watermark":"0"}', 0, '', 1, NULL, 2),
(14, 'googlemap', 'plisting_position1', 1, '0.11', '', 0, '', 1, NULL, 2),
(15, 'googlemap:pshow', 'pshow_googlemap', 1, '1.00', '{"default_lt":"38.685516","default_ln":"-101.073324","default_zoom":"4"}', 0, '', 1, NULL, 2),
(23, 'listing_contact', 'pshow_position2', 1, '99.00', '', 1, 'Contact Agent', 1, '', 2),
(24, 'user_contact', 'profile_show_position1', 0, '99.00', '{"top_comment":""}', 1, 'Contact', 1, '', 2),
(46, 'property_stats', 'pmanager_position2', 0, '99.00', '{"contacts":"1","including_in_listing":"1","view_parent":"1","visit":"1"}', 1, 'Property Stats', 1, '', 2) ;

#
# End of data contents of table `wp_wpl_activities`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_addons`
#

DROP TABLE IF EXISTS `wp_wpl_addons`;


#
# Table structure of table `wp_wpl_addons`
#

CREATE TABLE `wp_wpl_addons` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `version` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `addon_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `params` text CHARACTER SET utf8,
  `update` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `update_key` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `support_key` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `updatable` tinyint(4) NOT NULL DEFAULT '1',
  `message` text CHARACTER SET utf8,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_addons`
#
INSERT INTO `wp_wpl_addons` ( `id`, `name`, `version`, `addon_name`, `params`, `update`, `update_key`, `support_key`, `updatable`, `message`) VALUES
(45, 'IDX', '1.0.0', 'idx', '', '', '', '', 1, '') ;

#
# End of data contents of table `wp_wpl_addons`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_cronjobs`
#

DROP TABLE IF EXISTS `wp_wpl_cronjobs`;


#
# Table structure of table `wp_wpl_cronjobs`
#

CREATE TABLE `wp_wpl_cronjobs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `cronjob_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `period` float(6,2) NOT NULL,
  `class_location` varchar(255) CHARACTER SET utf8 NOT NULL,
  `class_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `function_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `params` text CHARACTER SET utf8 NOT NULL,
  `enabled` tinyint(4) NOT NULL,
  `latest_run` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_cronjobs`
#
INSERT INTO `wp_wpl_cronjobs` ( `id`, `cronjob_name`, `period`, `class_location`, `class_name`, `function_name`, `params`, `enabled`, `latest_run`) VALUES
(1, 'Location Text Update', '24.00', 'libraries.locations', 'wpl_locations', 'update_locationtextsearch_data', '', 1, '2014-02-10 08:14:11'),
(2, 'Remove Expired tmp Directories', '24.00', 'global', 'wpl_global', 'delete_expired_tmp', '', 1, '2014-02-10 08:09:10'),
(3, 'Check All Updates', '24.00', 'global', 'wpl_global', 'check_all_update', '', 1, '2014-04-05 13:19:29'),
(5, 'Maintenance', '24.00', 'global', 'wpl_global', 'execute_maintenance_job', '', 1, '2014-12-31 11:54:17'),
(9, 'Auto-Update Exchange Rates', '24.00', 'libraries.units', 'wpl_units', 'auto_update_rates', '', 1, '2016-03-01 00:00:00'),
(32, 'Sort Option Indexes', '120.00', 'libraries.sort_options', 'wpl_sort_options', 'sort_options_add_indexes', '', 1, '2017-06-06 12:39:35'),
(33, 'Tags Indexes', '120.00', 'global', 'wpl_global', 'tags_add_indexes', '', 1, '2017-06-06 12:40:35'),
(36, 'Remove Duplicated Indexes', '240.00', 'global', 'wpl_global', 'remove_duplicated_indexes', '', 1, '2018-01-01 05:00:00') ;

#
# End of data contents of table `wp_wpl_cronjobs`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_dbcat`
#

DROP TABLE IF EXISTS `wp_wpl_dbcat`;


#
# Table structure of table `wp_wpl_dbcat`
#

CREATE TABLE `wp_wpl_dbcat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kind` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=property, 1=complex',
  `name` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `index` float NOT NULL DEFAULT '99',
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  `pshow` tinyint(4) NOT NULL DEFAULT '1',
  `pdf` tinyint(4) NOT NULL DEFAULT '0',
  `searchmod` tinyint(4) NOT NULL DEFAULT '1',
  `prefix` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `listing_specific` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `property_type_specific` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `params` text CHARACTER SET utf8,
  PRIMARY KEY (`id`),
  KEY `kind` (`kind`,`enabled`),
  KEY `kind_2` (`kind`,`enabled`),
  KEY `kind_3` (`kind`,`enabled`),
  KEY `kind_4` (`kind`,`enabled`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_dbcat`
#
INSERT INTO `wp_wpl_dbcat` ( `id`, `kind`, `name`, `index`, `enabled`, `pshow`, `pdf`, `searchmod`, `prefix`, `listing_specific`, `property_type_specific`, `params`) VALUES
(1, 0, 'Basic Details', '1', 1, 1, 1, 1, 'b', '', '', NULL),
(2, 0, 'Address Map', '2', 1, 1, 1, 1, 'ad', '', '', NULL),
(3, 0, 'Image Gallery', '3', 1, 0, 0, 1, 'gal', '', '', NULL),
(4, 0, 'Features', '1.5', 1, 1, 1, 1, 'f', '', '', NULL),
(5, 0, 'Appliances', '1.7', 1, 1, 1, 1, 'app', '', '', NULL),
(6, 0, 'Neighborhood', '2.5', 1, 1, 1, 1, 'n', '', '', NULL),
(7, 0, 'Video', '7', 1, 0, 0, 0, 'v', '', '', NULL),
(9, 0, 'Attachments', '9', 1, 0, 0, 0, 'att', '', '', NULL),
(10, 2, 'Basic Details', '1', 1, 1, 1, 1, 'b', '', '', NULL),
(11, 0, 'Specialties', '10', 1, 0, 0, 1, 'sp', '', '', NULL) ;

#
# End of data contents of table `wp_wpl_dbcat`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_dbst`
#

DROP TABLE IF EXISTS `wp_wpl_dbst`;


#
# Table structure of table `wp_wpl_dbst`
#

CREATE TABLE `wp_wpl_dbst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kind` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=property, 1=complex',
  `mandatory` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=no,1=yes,2=always,3=never',
  `name` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `type` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `options` text CHARACTER SET utf8,
  `enabled` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=no,1=yes,2=always',
  `pshow` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '1' COMMENT 'To show in propertyshow or not',
  `pdf` tinyint(4) NOT NULL DEFAULT '0',
  `plisting` tinyint(4) NOT NULL DEFAULT '0',
  `searchmod` tinyint(4) NOT NULL DEFAULT '0',
  `editable` tinyint(4) NOT NULL DEFAULT '1',
  `deletable` tinyint(4) NOT NULL DEFAULT '1',
  `index` float(9,4) NOT NULL DEFAULT '99.0000',
  `css` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `style` text CHARACTER SET utf8,
  `specificable` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=no,1=yes',
  `listing_specific` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `property_type_specific` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `user_specific` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `accesses` text CHARACTER SET utf8,
  `accesses_message` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `table_name` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT 'table which the data is stored to',
  `table_column` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT 'column of table which datat is stored to',
  `category` int(11) NOT NULL DEFAULT '1' COMMENT 'in propertywizard category',
  `rankable` tinyint(4) NOT NULL DEFAULT '1',
  `rank_point` int(11) NOT NULL DEFAULT '0',
  `comments` text CHARACTER SET utf8,
  `pwizard` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '1' COMMENT '0=no,1=yes,2=always',
  `text_search` tinyint(4) DEFAULT NULL,
  `params` text CHARACTER SET utf8,
  `flex` tinyint(4) NOT NULL DEFAULT '1',
  `sortable` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=not sortable,1=sortable,2=always',
  `field_specific` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `kind` (`kind`,`enabled`),
  KEY `plisting` (`plisting`),
  KEY `kind_2` (`kind`,`enabled`),
  KEY `pshow` (`pshow`),
  KEY `plisting_2` (`plisting`),
  KEY `pshow_2` (`pshow`),
  KEY `kind_3` (`kind`,`enabled`),
  KEY `plisting_3` (`plisting`),
  KEY `pshow_3` (`pshow`),
  KEY `kind_4` (`kind`,`enabled`),
  KEY `plisting_4` (`plisting`),
  KEY `pshow_4` (`pshow`)
) ENGINE=InnoDB AUTO_INCREMENT=919 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_dbst`
#
INSERT INTO `wp_wpl_dbst` ( `id`, `kind`, `mandatory`, `name`, `type`, `options`, `enabled`, `pshow`, `pdf`, `plisting`, `searchmod`, `editable`, `deletable`, `index`, `css`, `style`, `specificable`, `listing_specific`, `property_type_specific`, `user_specific`, `accesses`, `accesses_message`, `table_name`, `table_column`, `category`, `rankable`, `rank_point`, `comments`, `pwizard`, `text_search`, `params`, `flex`, `sortable`, `field_specific`) VALUES
(1, 0, 0, 'Listing Setting', 'separator', '', 1, '0', 0, 0, 1, 1, 0, '1.0100', '', 'font-weight:bold', 0, '', '', NULL, NULL, NULL, '', '', 1, 1, 0, '', '1', 1, '', 1, 0, NULL),
(2, 0, 2, 'Listing Type', 'listings', '', 2, '1', 1, 1, 1, 1, 0, '1.0300', '', '', 0, '', '', NULL, NULL, NULL, 'wpl_properties', 'listing', 1, 0, 0, '', '1', 1, '', 1, 1, NULL),
(3, 0, 2, 'Property Type', 'property_types', '', 2, '1', 1, 1, 1, 1, 0, '1.0200', '', '', 0, '', '', NULL, NULL, NULL, 'wpl_properties', 'property_type', 1, 0, 0, '', '1', 1, '', 1, 1, NULL),
(4, 0, 0, 'Ref ID', 'text', '', 1, '0', 0, 0, 1, 1, 0, '1.0500', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'ref_id', 1, 1, 10, '', '1', 1, '', 1, 1, NULL),
(5, 0, 3, 'Listing ID', 'text', '{"readonly":"1"}', 1, '1', 1, 1, 1, 1, 0, '1.0400', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'mls_id', 1, 0, 0, '', '1', 1, '', 1, 2, NULL),
(6, 0, 1, 'Price', 'price', '', 1, '1', 1, 1, 1, 1, 1, '1.0800', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'price', 1, 0, 0, '', '1', 1, '', 1, 1, NULL),
(7, 0, 1, 'View', 'select', '{"params":{"1":{"key":"1","enabled":"1","value":"Garden"},"2":{"key":"2","enabled":"1","value":"Street"},"3":{"key":"3","enabled":"1","value":"Sea"}}}', 1, '1', 1, 0, 1, 1, 1, '1.1000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'field_7', 1, 1, 0, '', '1', 1, '', 1, 1, NULL),
(8, 0, 1, 'Bedrooms', 'number', '', 1, '1', 1, 1, 1, 1, 1, '1.1100', '', '', 1, '', '6,7,', NULL, NULL, NULL, 'wpl_properties', 'bedrooms', 1, 1, 0, '', '1', 1, '', 1, 1, NULL),
(9, 0, 1, 'Bathrooms', 'number', '', 1, '1', 1, 1, 1, 1, 1, '1.1300', '', '', 1, '', '6,7,13,', NULL, NULL, NULL, 'wpl_properties', 'bathrooms', 1, 1, 0, '', '1', 1, '', 1, 1, NULL),
(10, 0, 1, 'Square Footage', 'area', '', 1, '1', 1, 1, 1, 1, 1, '1.1500', '', '', 1, '', '6,7,13,', NULL, NULL, NULL, 'wpl_properties', 'living_area', 1, 1, 0, '', '1', 1, '', 1, 1, NULL),
(11, 0, 0, 'Lot Area', 'area', '', 1, '1', 1, 1, 1, 1, 1, '1.1700', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'lot_area', 1, 1, 0, '', '1', 1, '', 1, 1, NULL),
(12, 0, 0, 'Year Built', 'number', '', 1, '1', 1, 0, 1, 1, 1, '1.1600', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'build_year', 1, 1, 0, '', '1', 1, '', 1, 1, NULL),
(13, 0, 1, 'Rooms', 'number', '', 1, '1', 1, 1, 1, 1, 1, '1.1200', '', '', 1, '', '13,', NULL, NULL, NULL, 'wpl_properties', 'rooms', 1, 1, 0, '', '1', 1, '', 1, 1, NULL),
(14, 0, 1, 'Price Type', 'select', '{"params":{"1":{"key":"30","enabled":"1","value":"Per Month"},"2":{"key":"7","enabled":"1","value":"Per Week"},"3":{"key":"365","enabled":"1","value":"Per Year"},"4":{"key":"1","enabled":"1","value":"Per Day"}}}', 1, '1', 1, 1, 1, 1, 1, '1.0700', '', '', 1, '10,12,', '', NULL, NULL, NULL, 'wpl_properties', 'price_period', 1, 1, 0, '', '1', 1, '', 1, 1, NULL),
(17, 0, 0, 'Half Bathrooms', 'number', '', 1, '1', 1, 1, 1, 1, 1, '1.1400', '', '', 1, '', '6,7,13,', NULL, NULL, NULL, 'wpl_properties', 'half_bathrooms', 1, 1, 0, '', '1', 1, '', 1, 1, NULL),
(18, 0, 0, 'Text Search', 'textsearch', '', 2, '0', 0, 0, 1, 0, 0, '1.2200', '', '', 0, '', '', NULL, NULL, NULL, 'wpl_properties', 'textsearch', 1, 0, 0, '', '0', 0, '', 1, 1, NULL),
(19, 0, 0, 'List Date', 'date', '', 1, '0', 0, 0, 1, 0, 0, '1.0900', '', '', 0, '', '', NULL, NULL, NULL, 'wpl_properties', 'add_date', 1, 0, 0, '', '0', 0, '', 1, 1, NULL),
(20, 0, 0, 'Alias / Permalink', 'text', '', 2, '0', 0, 0, 0, 1, 0, '1.0200', '', '', 0, '', '', '', NULL, NULL, 'wpl_properties', 'alias', 1, 0, 0, '', '0', 0, '', 1, 1, NULL),
(21, 0, 0, 'Location Text', 'text', '{"if_zero":"1","call_text":"Call"}', 2, '0', 0, 0, 0, 1, 0, '1.0210', '', '', 0, '', '', '', NULL, NULL, 'wpl_properties', 'location_text', 2, 0, 0, '', '0', 0, '', 1, 1, NULL),
(22, 0, 0, 'Category', 'ptcategory', '', 2, '0', 0, 0, 1, 0, 0, '1.0450', '', '', 0, '', '', '', NULL, NULL, NULL, NULL, 1, 0, 0, '', '0', 0, '', 0, 0, NULL),
(40, 0, 0, 'Address Location', 'separator', '', 1, '0', 0, 0, 1, 1, 0, '1.0000', '', 'font-weight:bold', 0, '', '', NULL, NULL, NULL, '', '', 2, 1, 0, '', '1', 1, '', 1, 0, NULL),
(41, 0, 2, 'Location', 'locations', '{"params":{"1":{"enabled":"1"},"2":{"enabled":"1"},"3":{"enabled":"1"},"4":{"enabled":"1"},"5":{"enabled":"1"},"6":{"enabled":"1"},"7":{"enabled":"1"},"zips":{"enabled":"1"}}}', 1, '1', 1, 1, 1, 1, 0, '2.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'locations', 2, 0, 0, '', '1', 1, '', 1, 1, NULL),
(42, 0, 0, 'Street', 'text', '', 1, '1', 1, 1, 1, 1, 1, '12.0000', 'class', 'style', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'field_42', 2, 1, 0, '', '1', 1, '', 1, 1, NULL),
(43, 0, 0, 'Postal Code', 'text', '', 1, '1', 1, 1, 1, 1, 1, '21.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'post_code', 2, 1, 30, '', '1', 1, '', 1, 1, NULL),
(45, 0, 0, 'Street Number', 'text', '', 1, '1', 1, 1, 1, 1, 1, '13.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'street_no', 2, 0, 0, NULL, '1', 1, '', 1, 1, NULL),
(50, 0, 0, 'Google Map', 'separator', '', 1, '0', 0, 0, 1, 1, 0, '48.0000', '', '', 0, '', '', NULL, NULL, NULL, '', '', 2, 1, 0, '', '1', 0, '', 1, 0, NULL),
(51, 0, 3, 'Longitude', 'text', '{"readonly":"1"}', 2, '1', 1, 0, 0, 1, 0, '49.0000', '', '', 0, '', '', NULL, NULL, NULL, 'wpl_properties', 'googlemap_ln', 2, 0, 0, '', '1', 0, '', 1, 1, NULL),
(52, 0, 3, 'Latitude', 'text', '{"readonly":"1"}', 2, '1', 1, 0, 0, 1, 0, '58.0000', '', '', 0, '', '', NULL, NULL, NULL, 'wpl_properties', 'googlemap_lt', 2, 0, 0, '', '1', 0, '', 1, 1, NULL),
(53, 0, 3, 'Map', 'googlemap', '', 1, '0', 0, 0, 0, 0, 0, '67.0000', '', '', 1, '', '', NULL, NULL, NULL, '', '', 2, 1, 50, '', '1', 0, '', 1, 0, NULL),
(54, 0, 0, 'Building Name', 'text', '', 1, '1', 1, 1, 1, 1, 1, '30.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'field_54', 2, 1, 30, '', '1', 1, '', 1, 1, NULL),
(55, 0, 0, 'Floor Number', 'number', '', 1, '1', 1, 1, 1, 1, 1, '39.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'field_55', 2, 1, 10, '', '1', 1, '', 1, 1, NULL),
(57, 0, 0, 'Street Suffix', 'text', NULL, 1, '1', 1, 1, 0, 1, 1, '14.0000', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 'wpl_properties', 'street_suffix', 2, 1, 0, NULL, '1', NULL, NULL, 1, 1, NULL),
(59, 0, 0, 'Zipcode', 'text', '', 1, '0', 0, 0, 1, 0, 0, '2.0600', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 'wpl_properties', 'zip_name', 2, 1, 0, NULL, '0', 1, NULL, 0, 1, NULL),
(91, 0, 0, 'Appliances', 'separator', '', 1, '0', 0, 0, 1, 1, 0, '1.0000', '', 'font-weight:bold', 0, '', '', NULL, NULL, NULL, '', '', 5, 1, 0, '', '1', 1, '', 1, 0, NULL),
(92, 0, 0, 'Neighborhood', 'separator', '', 1, '0', 0, 0, 1, 1, 0, '1.0000', '', 'font-weight:bold', 0, '', '', NULL, NULL, NULL, '', '', 6, 1, 0, '', '1', 1, '', 1, 0, NULL),
(100, 0, 3, 'Shopping Center', 'neighborhood', '', 1, '1', 1, 0, 1, 1, 1, '1.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'n_100', 6, 1, 10, '', '1', 1, '', 1, 1, NULL),
(101, 0, 3, 'Hospital', 'neighborhood', '', 1, '1', 1, 0, 1, 1, 1, '21.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'n_101', 6, 1, 10, '', '1', 1, '', 1, 1, NULL),
(102, 0, 3, 'Cinema', 'neighborhood', '', 1, '1', 1, 0, 1, 1, 1, '101.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'n_102', 6, 1, 0, '', '1', 1, '', 1, 1, NULL),
(103, 0, 3, 'Park', 'neighborhood', '', 1, '1', 1, 0, 1, 1, 1, '111.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'n_103', 6, 1, 10, '', '1', 1, '', 1, 1, NULL),
(105, 0, 3, 'Beach', 'neighborhood', '', 1, '1', 1, 0, 1, 1, 1, '81.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'n_105', 6, 1, 20, '', '1', 1, '', 1, 1, NULL),
(106, 0, 3, 'Coffee Shop', 'neighborhood', '', 1, '1', 1, 0, 1, 1, 1, '71.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'n_106', 6, 1, 0, '', '1', 1, '', 1, 1, NULL),
(107, 0, 3, 'Airport', 'neighborhood', '', 1, '1', 1, 0, 1, 1, 1, '61.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'n_107', 6, 1, 0, '', '1', 1, '', 1, 1, NULL),
(108, 0, 3, 'Bus Station', 'neighborhood', '', 1, '1', 1, 0, 1, 1, 1, '51.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'n_108', 6, 1, 0, '', '1', 1, '', 1, 1, NULL),
(109, 0, 3, 'Train Station', 'neighborhood', '', 1, '1', 1, 0, 1, 1, 1, '41.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'n_109', 6, 1, 0, '', '1', 1, '', 1, 1, NULL),
(110, 0, 3, 'School', 'neighborhood', '', 1, '1', 1, 0, 1, 1, 1, '121.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'n_110', 6, 1, 10, '', '1', 1, '', 1, 1, NULL),
(111, 0, 3, 'University', 'neighborhood', '', 1, '1', 1, 0, 1, 1, 1, '131.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'n_111', 6, 1, 0, '', '1', 1, '', 1, 1, NULL),
(112, 0, 3, 'Police Station', 'neighborhood', '', 1, '1', 1, 0, 1, 1, 1, '31.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'n_112', 6, 1, 10, '', '1', 1, '', 1, 1, NULL),
(113, 0, 3, 'Town Center', 'neighborhood', '', 1, '1', 1, 0, 1, 1, 1, '11.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'n_113', 6, 1, 20, '', '1', 1, '', 1, 1, NULL),
(114, 0, 3, 'Exhibition', 'neighborhood', '', 1, '1', 1, 0, 1, 1, 1, '151.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'n_114', 6, 1, 0, '', '1', 1, '', 1, 1, NULL),
(115, 0, 3, 'Tourist Site', 'neighborhood', '', 1, '1', 1, 0, 1, 1, 1, '141.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'n_115', 6, 1, 0, '', '1', 1, '', 1, 1, NULL),
(129, 0, 0, 'Features', 'separator', '', 1, '0', 0, 0, 1, 1, 0, '4.0000', '', 'font-weight:bold', 0, '', '', NULL, NULL, NULL, '', '', 4, 1, 0, '', '1', 1, '', 1, 0, NULL),
(130, 0, 3, 'Heating System', 'feature', '{"type":"single","values":[{"key":"1","value":"Split"},{"key":"2","value":"Heat Pump"},{"key":"3","value":"Central"}]}', 1, '1', 1, 0, 1, 1, 1, '4.0400', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_130', 4, 1, 5, '', '1', 1, '', 1, 1, NULL),
(131, 0, 3, 'Swimming Pool', 'feature', '{"type":"none"}', 1, '1', 1, 0, 1, 1, 1, '4.0200', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_131', 4, 1, 30, '', '1', 1, '', 1, 1, NULL),
(132, 0, 3, 'Jacuzzi', 'feature', '{"type":"none"}', 1, '1', 1, 0, 1, 1, 1, '4.0300', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_132', 4, 1, 10, '', '1', 1, '', 1, 1, NULL),
(133, 0, 3, 'Elevator', 'feature', '{"type":"none"}', 1, '1', 1, 0, 1, 1, 1, '4.0600', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_133', 4, 1, 5, '', '1', 1, '', 1, 1, NULL),
(134, 0, 3, 'Cooling System', 'feature', '{"type":"single","values":[{"key":"1","value":"Split"},{"key":"2","value":"Central"}]}', 1, '1', 1, 0, 1, 1, 1, '4.0500', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_134', 4, 1, 10, '', '1', 1, '', 1, 1, NULL),
(135, 0, 3, 'Garden', 'feature', '{"type":"none"}', 1, '1', 1, 0, 1, 1, 1, '4.0700', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_135', 4, 1, 0, '', '1', 1, '', 1, 1, NULL),
(136, 0, 3, 'Balcony', 'feature', '{"type":"none"}', 1, '1', 1, 1, 1, 1, 1, '4.0800', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_136', 4, 1, 5, '', '1', 1, '', 1, 1, NULL),
(137, 0, 3, 'Basement', 'feature', '{"type":"none"}', 1, '1', 1, 0, 1, 1, 1, '4.1000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_137', 4, 1, 0, '', '1', 1, '', 1, 1, NULL),
(138, 0, 3, 'Fence', 'feature', '{"type":"none"}', 1, '1', 1, 0, 1, 1, 1, '4.1100', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_138', 4, 1, 0, '', '1', 1, '', 1, 1, NULL),
(139, 0, 3, 'View', 'feature', '{"type":"none"}', 1, '1', 1, 0, 1, 1, 1, '4.1200', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_139', 4, 1, 0, '', '1', 1, '', 1, 1, NULL),
(140, 0, 3, 'Pet Policy', 'feature', '{"type":"single","values":[{"key":"1","value":"Allowed"},{"key":"2","value":"Not Allowed"}]}', 1, '1', 1, 0, 1, 1, 1, '4.1300', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_140', 4, 1, 0, '', '1', 1, '', 1, 1, NULL),
(141, 0, 3, 'Kitchen', 'feature', '{"type":"single","values":[{"key":"1","value":"Fully Equipped"},{"key":"2","value":"Semi Equipped"},{"key":"3","value":"Not Equipped"}]}', 1, '1', 1, 0, 1, 1, 1, '4.1400', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_141', 4, 1, 0, '', '1', 1, '', 1, 1, NULL),
(142, 0, 3, 'Steam', 'feature', '{"type":"none"}', 1, '1', 1, 0, 1, 1, 1, '4.1500', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_142', 4, 1, 0, '', '1', 1, '', 1, 1, NULL),
(143, 0, 3, 'Gymnasium', 'feature', '{"type":"none"}', 1, '1', 1, 0, 1, 1, 1, '4.1600', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_143', 4, 1, 0, '', '1', 1, '', 1, 1, NULL),
(144, 0, 3, 'Fireplace', 'feature', '{"type":"none"}', 1, '1', 1, 0, 1, 1, 1, '4.1700', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_144', 4, 1, 0, '', '1', 1, '', 1, 1, NULL),
(146, 0, 3, 'Patio', 'feature', '{"type":"none"}', 1, '1', 1, 0, 1, 1, 1, '4.1900', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_146', 4, 1, 0, '', '1', 1, '', 1, 1, NULL),
(147, 0, 3, 'Roof Deck', 'feature', '{"type":"none"}', 1, '1', 1, 0, 1, 1, 1, '4.2000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_147', 4, 1, 0, '', '1', 1, '', 1, 1, NULL),
(148, 0, 3, 'High Ceiling', 'feature', '{"type":"none"}', 1, '1', 1, 0, 1, 1, 1, '4.2100', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_148', 4, 1, 0, '', '1', 1, '', 1, 1, NULL),
(149, 0, 3, 'Storage', 'feature', '{"type":"none"}', 1, '1', 1, 0, 1, 1, 1, '4.2200', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_149', 4, 1, 0, '', '1', 1, '', 1, 1, NULL),
(150, 0, 3, 'Parking', 'feature', '{"type":"single","values":[{"key":"1","value":"1"},{"key":"2","value":"2"},{"key":"3","value":"3"},{"key":"4","value":"4"},{"key":"5","value":"5"},{"key":"6","value":"6"}]}', 1, '1', 1, 1, 1, 1, 1, '4.2300', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_150', 4, 1, 10, '', '1', 1, '', 1, 1, NULL),
(151, 0, 3, 'Furnished', 'feature', '{"type":"single","values":[{"key":"1","value":"Fully Furnished"},{"key":"2","value":"Semi Furnished"},{"key":"3","value":"Not Furnished"}]}', 1, '1', 1, 0, 1, 1, 1, '4.2400', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_151', 4, 1, 10, '', '1', 1, '', 1, 1, NULL),
(152, 0, 3, 'Security', 'feature', '{"type":"multiple","values":[{"key":"1","value":"Watchman"},{"key":"2","value":"Digicode"},{"key":"3","value":"Alarm"},{"key":"4","value":"Lock"},{"key":"5","value":"Cctv"},{"key":"6","value":"No"},{"key":"7","value":"Other"}]}', 1, '1', 1, 0, 1, 1, 1, '4.1800', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_152', 4, 1, 10, '', '1', 1, '', 1, 1, NULL),
(153, 0, 3, 'Refrigerator', 'feature', '{"type":"single","values":[{"key":"1","value":"Simple"},{"key":"2","value":"Side By Side"}]}', 1, '1', 1, 0, 1, 1, 1, '1.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_153', 5, 1, 5, '', '1', 1, '', 1, 1, NULL),
(154, 0, 3, 'Stove', 'feature', '{"type":"none"}', 1, '1', 1, 0, 1, 1, 1, '21.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_154', 5, 1, 5, '', '1', 1, '', 1, 1, NULL),
(155, 0, 3, 'Microwave', 'feature', '{"type":"none"}', 1, '1', 1, 0, 1, 1, 1, '11.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_155', 5, 1, 5, '', '1', 1, '', 1, 1, NULL),
(156, 0, 3, 'Washing Machine', 'feature', '{"type":"none"}', 1, '1', 1, 0, 1, 1, 1, '31.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_156', 5, 1, 5, '', '1', 1, '', 1, 1, NULL),
(157, 0, 3, 'TV', 'feature', '{"type":"none"}', 1, '1', 1, 0, 1, 1, 1, '41.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_157', 5, 1, 5, '', '1', 1, '', 1, 1, NULL),
(158, 0, 3, 'CD Player', 'feature', '{"type":"none"}', 1, '1', 1, 0, 1, 1, 1, '51.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_158', 5, 1, 5, '', '1', 1, '', 1, 1, NULL),
(159, 0, 3, 'Internet', 'feature', '{"type":"single","values":[{"key":"1","value":"Exclusive"},{"key":"2","value":"Inclusive"},{"key":"3","value":"Wireless"}]}', 1, '1', 1, 0, 1, 1, 1, '171.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_159', 5, 1, 5, '', '1', 1, '', 1, 1, NULL),
(160, 0, 3, 'Hair Dryer', 'feature', '{"type":"none"}', 1, '1', 1, 0, 1, 1, 1, '61.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_160', 5, 1, 5, '', '1', 1, '', 1, 1, NULL),
(161, 0, 3, 'Cleaning Service', 'feature', '{"type":"none"}', 1, '1', 1, 0, 1, 1, 1, '71.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_161', 5, 1, 5, '', '1', 1, '', 1, 1, NULL),
(162, 0, 3, 'Oven', 'feature', '{"type":"none"}', 1, '1', 1, 0, 1, 1, 1, '81.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_162', 5, 1, 5, '', '1', 1, '', 1, 1, NULL),
(163, 0, 3, 'Dishwasher', 'feature', '{"type":"none"}', 1, '1', 1, 0, 1, 1, 1, '91.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_163', 5, 1, 5, '', '1', 1, '', 1, 1, NULL),
(164, 0, 3, 'Dishes', 'feature', '{"type":"none"}', 1, '1', 1, 0, 1, 1, 1, '101.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_164', 5, 1, 5, '', '1', 1, '', 1, 1, NULL),
(165, 0, 3, 'Satellite', 'feature', '{"type":"none"}', 1, '1', 1, 0, 1, 1, 1, '111.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_165', 5, 1, 5, '', '1', 1, '', 1, 1, NULL),
(166, 0, 3, 'Telephone', 'feature', '{"type":"none"}', 1, '1', 1, 0, 1, 1, 1, '121.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_166', 5, 1, 5, '', '1', 1, '', 1, 1, NULL),
(167, 0, 3, 'Towels', 'feature', '{"type":"none"}', 1, '1', 1, 0, 1, 1, 1, '131.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_167', 5, 1, 5, '', '1', 1, '', 1, 1, NULL),
(168, 0, 3, 'Hot Tub', 'feature', '{"type":"none"}', 1, '1', 1, 0, 1, 1, 1, '151.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_168', 5, 1, 5, '', '1', 1, '', 1, 1, NULL),
(169, 0, 3, 'Iron', 'feature', '{"type":"none"}', 1, '1', 1, 0, 1, 1, 1, '161.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'f_169', 5, 1, 5, '', '1', 1, '', 1, 1, NULL),
(171, 0, 0, 'Rooms and Sizes', 'rooms', '', 1, '0', 0, 0, 0, 0, 1, '3.5000', '', '', 1, '', '', NULL, NULL, NULL, '', '', 4, 0, 0, '', '1', 0, '', 1, 0, NULL),
(300, 0, 3, 'Gallery', 'gallery', '{"ext_file":"gif,jpeg,png,jpg","file_size":"500"}', 1, '0', 0, 0, 1, 1, 0, '1.0000', '', '', 1, '', '', NULL, NULL, NULL, '', '', 3, 0, 0, '', '1', 0, '', 1, 0, NULL),
(301, 0, 0, 'Attachments', 'attachments', '{"ext_file":"pdf,doc,docx,zip,tar,rar,mp3,txt","file_size":"5000"}', 1, '0', 0, 0, 1, 1, 0, '1.0000', '', '', 0, '', '', NULL, NULL, NULL, '', '', 9, 0, 0, '', '1', 0, '', 1, 0, NULL),
(302, 0, 0, 'Details', 'separator', '', 1, '0', 0, 0, 1, 1, 0, '1.0600', '', 'font-weight:bold', 0, '', '', NULL, NULL, NULL, '', '', 1, 1, 0, '', '1', 1, '', 1, 0, NULL),
(308, 0, 0, 'Property Description', 'textarea', '{"advanced_editor":"1","rows":"7","cols":"41"}', 2, '0', 0, 0, 1, 1, 0, '1.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'field_308', 1, 1, 140, '', '1', 1, '', 1, 1, NULL),
(309, 0, 0, 'Description Meta Data', 'separator', '', 1, '0', 0, 0, 1, 1, 0, '1.1800', '', 'font-weight:bold', 0, '', '', NULL, NULL, NULL, '', '', 1, 1, 0, '', '1', 0, '', 1, 0, NULL),
(310, 0, 3, 'Meta Description', 'meta_desc', '{"advanced_editor":"0","rows":"6","cols":"60","readonly":"1"}', 1, '0', 0, 0, 1, 1, 1, '1.2000', '', '', 0, '', '', NULL, NULL, NULL, 'wpl_properties', 'meta_description', 1, 0, 0, '', '1', 0, '', 1, 1, NULL),
(311, 0, 3, 'Meta Keywords', 'meta_key', '{"advanced_editor":"0","rows":"6","cols":"60","readonly":"1"}', 1, '0', 0, 0, 1, 1, 1, '1.2100', '', '', 0, '', '', NULL, NULL, NULL, 'wpl_properties', 'meta_keywords', 1, 0, 0, '', '1', 0, '', 1, 1, NULL),
(312, 0, 3, 'Property Page Title', 'text', 'null', 2, '0', 0, 1, 1, 1, 0, '0.6000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'field_312', 1, 0, 0, '', '1', 1, '', 1, 1, NULL),
(313, 0, 3, 'Property Title', 'text', 'null', 2, '0', 0, 1, 1, 1, 0, '0.5000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'field_313', 1, 0, 0, '', '1', 1, '[]', 1, 1, NULL) ;
INSERT INTO `wp_wpl_dbst` ( `id`, `kind`, `mandatory`, `name`, `type`, `options`, `enabled`, `pshow`, `pdf`, `plisting`, `searchmod`, `editable`, `deletable`, `index`, `css`, `style`, `specificable`, `listing_specific`, `property_type_specific`, `user_specific`, `accesses`, `accesses_message`, `table_name`, `table_column`, `category`, `rankable`, `rank_point`, `comments`, `pwizard`, `text_search`, `params`, `flex`, `sortable`, `field_specific`) VALUES
(400, 0, 3, 'Featured', 'tag', '{"ribbon":"1","widget":"1","color":"29a9df","text_color":"ffffff","default_value":"0"}', 1, '1', 1, 1, 1, 1, 0, '2.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'sp_featured', 11, 1, 50, NULL, '1', 1, '', 1, 1, NULL),
(401, 0, 3, 'Hot Offer', 'tag', '{"ribbon":"1","widget":"1","color":"d21a10","text_color":"ffffff","default_value":"0"}', 1, '1', 1, 1, 1, 1, 0, '3.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'sp_hot', 11, 1, 50, NULL, '1', 1, '', 1, 1, NULL),
(402, 0, 3, 'Open House', 'tag', '{"ribbon":"1","widget":"1","color":"3cae2c","text_color":"ffffff","default_value":"0"}', 1, '1', 1, 1, 1, 1, 0, '4.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'sp_openhouse', 11, 1, 60, NULL, '1', 1, '', 1, 1, NULL),
(403, 0, 3, 'Foreclosure', 'tag', '{"ribbon":"1","widget":"1","color":"666666","text_color":"ffffff","default_value":"0"}', 1, '1', 1, 1, 1, 1, 0, '6.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', 'sp_forclosure', 11, 1, 60, NULL, '1', 1, '', 1, 1, NULL),
(567, 0, 0, 'Add On Video', 'addon_video', '{"ext_file":"avi,flv,mp4,swf,wmv","file_size":"100000"}', 1, '0', 0, 0, 1, 1, 0, '1.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_properties', '', 7, 1, 0, '', '1', 0, '', 1, 0, NULL),
(900, 2, 1, 'First Name', 'text', '', 1, '1', 1, 1, 1, 1, 0, '1.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_users', 'first_name', 10, 1, 0, NULL, '1', 1, '', 1, 1, NULL),
(901, 2, 1, 'Last Name', 'text', '', 1, '1', 1, 1, 1, 1, 0, '2.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_users', 'last_name', 10, 1, 0, NULL, '1', 1, '', 1, 1, NULL),
(902, 2, 0, 'Company Name', 'text', '', 1, '1', 1, 1, 1, 1, 0, '3.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_users', 'company_name', 10, 1, 0, NULL, '1', 1, '', 1, 1, NULL),
(903, 2, 0, 'Company Address', 'text', '', 1, '1', 1, 1, 1, 1, 0, '4.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_users', 'company_address', 10, 1, 0, NULL, '1', 1, '', 1, 1, NULL),
(904, 2, 0, 'Website', 'text', '', 1, '1', 1, 1, 1, 1, 1, '5.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_users', 'website', 10, 1, 0, NULL, '1', 1, '', 1, 1, NULL),
(905, 2, 0, 'Secondary Email', 'text', '', 1, '1', 1, 1, 1, 1, 0, '6.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_users', 'secondary_email', 10, 1, 0, NULL, '1', 1, '', 1, 1, NULL),
(906, 2, 0, 'Gender', 'select', '{"params":{"1":{"key":"1","enabled":"1","value":"Male"},"2":{"key":"2","enabled":"1","value":"Female"}}}', 1, '1', 1, 1, 1, 1, 0, '2.2000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_users', 'sex', 10, 1, 0, NULL, '1', 1, '', 1, 1, NULL),
(907, 2, 0, 'Tel', 'text', '', 1, '1', 1, 1, 1, 1, 1, '8.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_users', 'tel', 10, 1, 0, NULL, '1', 1, '', 1, 1, NULL),
(908, 2, 0, 'Fax', 'text', '', 1, '1', 1, 1, 1, 1, 1, '9.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_users', 'fax', 10, 1, 0, NULL, '1', 1, '', 1, 1, NULL),
(909, 2, 0, 'Mobile', 'text', '', 1, '1', 1, 1, 1, 1, 1, '10.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_users', 'mobile', 10, 1, 0, NULL, '1', 1, '', 1, 1, NULL),
(910, 2, 0, 'Location Text', 'text', '{"if_zero":"1","call_text":"Call"}', 2, '0', 0, 0, 0, 1, 0, '10.0550', '', '', 0, '', '', '', '', '', 'wpl_users', 'location_text', 10, 0, 0, '', '0', 0, '', 1, 1, NULL),
(911, 2, 0, 'Location', 'locations', '{"params":{"1":{"enabled":"1"},"2":{"enabled":"1"},"3":{"enabled":"1"},"4":{"enabled":"1"},"5":{"enabled":"1"},"6":{"enabled":"1"},"7":{"enabled":"1"},"zips":{"enabled":"1"}}}', 1, '1', 1, 1, 1, 1, 0, '11.0000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_users', 'locations', 10, 0, 0, '', '1', 1, '', 1, 2, NULL),
(912, 2, 0, 'Profile Picture', 'upload', '{"params":{"request_str":"?wpl_format=b:users:ajax&wpl_function=upload_file&file_name=[html_element_id]&item_id=[item_id]"},"preview":1,"remove_str":"wpl_format=b:users:ajax&wpl_function=delete_file&item_id=[item_id]"}', 1, '0', 0, 0, 0, 0, 0, '2.2500', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_users', 'profile_picture', 10, 0, 0, '', '1', 0, '', 1, 1, NULL),
(913, 2, 0, 'Company Logo', 'upload', '{"params":{"request_str":"?wpl_format=b:users:ajax&wpl_function=upload_file&file_name=[html_element_id]&item_id=[item_id]"},"preview":1,"remove_str":"wpl_format=b:users:ajax&wpl_function=delete_file&item_id=[item_id]"}', 1, '0', 0, 0, 0, 0, 0, '2.3000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_users', 'company_logo', 10, 0, 0, '', '1', 0, '', 1, 1, NULL),
(914, 2, 0, 'Email', 'text', '', 1, '1', 1, 1, 1, 1, 0, '5.5000', '', '', 1, '', '', NULL, NULL, NULL, 'wpl_users', 'main_email', 10, 1, 0, NULL, '1', 1, '', 1, 1, NULL),
(915, 2, 0, 'Personal Data', 'separator', 'null', 1, '0', 0, 0, 1, 1, 1, '10.0000', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 'wpl_users', NULL, 10, 1, 0, NULL, '1', 1, NULL, 1, 0, NULL),
(916, 2, 0, 'Company Data', 'separator', 'null', 1, '0', 0, 0, 1, 1, 1, '10.0600', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 'wpl_users', NULL, 10, 1, 0, NULL, '1', 1, NULL, 1, 0, NULL),
(917, 2, 0, 'Contact information', 'separator', 'null', 1, '0', 0, 0, 1, 1, 1, '10.1000', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 'wpl_users', NULL, 10, 1, 0, NULL, '1', 1, NULL, 1, 0, NULL),
(918, 2, 0, 'About', 'textarea', '{"advanced_editor":"0","rows":"7","cols":"41"}', 1, '1', 1, 1, 1, 1, 1, '10.0350', NULL, NULL, 1, '', '', '', NULL, NULL, 'wpl_users', 'about', 10, 1, 0, NULL, '1', 1, NULL, 1, 1, NULL) ;

#
# End of data contents of table `wp_wpl_dbst`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_dbst_types`
#

DROP TABLE IF EXISTS `wp_wpl_dbst_types`;


#
# Table structure of table `wp_wpl_dbst_types`
#

CREATE TABLE `wp_wpl_dbst_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kind` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '[0][1]',
  `type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `enabled` tinyint(4) NOT NULL COMMENT '0=no,1=yes,2=always',
  `index` float(5,2) NOT NULL DEFAULT '1.00',
  `queries_add` text CHARACTER SET utf8 NOT NULL,
  `queries_delete` text CHARACTER SET utf8 NOT NULL,
  `options` text CHARACTER SET utf8,
  PRIMARY KEY (`id`),
  KEY `kind` (`kind`,`enabled`),
  KEY `kind_2` (`kind`,`enabled`),
  KEY `kind_3` (`kind`,`enabled`),
  KEY `kind_4` (`kind`,`enabled`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_dbst_types`
#
INSERT INTO `wp_wpl_dbst_types` ( `id`, `kind`, `type`, `enabled`, `index`, `queries_add`, `queries_delete`, `options`) VALUES
(1, '[0][1][2][4]', 'text', 1, '1.00', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]` ADD `field_[FIELD_ID]` varchar(50) NULL; UPDATE `[TB_PREFIX]wpl_dbst` SET `table_name`=\'[TABLE_NAME]\', `table_column`=\'field_[FIELD_ID]\' WHERE id=[FIELD_ID];', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]`\nDROP `field_[FIELD_ID]`;', NULL),
(2, '[0][1][2][4]', 'number', 1, '1.00', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]` ADD `field_[FIELD_ID]` float NULL; UPDATE `[TB_PREFIX]wpl_dbst` SET `table_name`=\'[TABLE_NAME]\', `table_column`=\'field_[FIELD_ID]\' WHERE id=[FIELD_ID];', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]`\nDROP `field_[FIELD_ID]`;', NULL),
(3, '[0][1][2][4]', 'select', 1, '1.00', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]` ADD `field_[FIELD_ID]` int(11) NULL; UPDATE `[TB_PREFIX]wpl_dbst` SET `table_name`=\'[TABLE_NAME]\', `table_column`=\'field_[FIELD_ID]\' WHERE id=[FIELD_ID];', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]` DROP `field_[FIELD_ID]`;', NULL),
(4, '[0][1][4]', 'feature', 1, '1.00', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]` ADD `f_[FIELD_ID]_options` text NULL; ALTER TABLE `[TB_PREFIX][TABLE_NAME]` ADD `f_[FIELD_ID]` tinyint(4) NULL; UPDATE `[TB_PREFIX]wpl_dbst` SET `table_name`=\'[TABLE_NAME]\', `table_column`=\'f_[FIELD_ID]\' WHERE id=[FIELD_ID];', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]`\nDROP `f_[FIELD_ID]_options`;ALTER TABLE `[TB_PREFIX][TABLE_NAME]`\nDROP `f_[FIELD_ID]`;', NULL),
(5, '[0][1][2][4]', 'textarea', 1, '1.00', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]` ADD `field_[FIELD_ID]` text NULL; UPDATE `[TB_PREFIX]wpl_dbst` SET `table_name`=\'[TABLE_NAME]\', `table_column`=\'field_[FIELD_ID]\' WHERE id=[FIELD_ID];', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]` DROP `field_[FIELD_ID]`;', NULL),
(6, '[0][1][2][4]', 'separator', 1, '1.00', '', '', NULL),
(7, '[0][1][4]', 'neighborhood', 1, '1.00', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]` ADD `n_[FIELD_ID]` tinyint(4) NULL; ALTER TABLE `[TB_PREFIX][TABLE_NAME]` ADD `n_[FIELD_ID]_distance` int NULL; ALTER TABLE `[TB_PREFIX][TABLE_NAME]` ADD `n_[FIELD_ID]_distance_by` tinyint(4) NULL; UPDATE `[TB_PREFIX]wpl_dbst` SET `table_name`=\'[TABLE_NAME]\', `table_column`=\'n_[FIELD_ID]\' WHERE id=[FIELD_ID];', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]`\nDROP `n_[FIELD_ID]`;ALTER TABLE `[TB_PREFIX][TABLE_NAME]`\nDROP `n_[FIELD_ID]_distance`;ALTER TABLE `[TB_PREFIX][TABLE_NAME]`\nDROP `n_[FIELD_ID]_distance_by`;', NULL),
(8, '[0][1][4]', 'area', 1, '1.00', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]` ADD `field_[FIELD_ID]` double NOT NULL DEFAULT \'0\'; ALTER TABLE `[TB_PREFIX][TABLE_NAME]` ADD `field_[FIELD_ID]_si` double NOT NULL DEFAULT \'0\'; ALTER TABLE `[TB_PREFIX][TABLE_NAME]` ADD `field_[FIELD_ID]_unit` int NULL; UPDATE `[TB_PREFIX]wpl_dbst` SET `table_name`=\'[TABLE_NAME]\', `table_column`=\'field_[FIELD_ID]\' WHERE id=[FIELD_ID];', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]`\nDROP `field_[FIELD_ID]`;ALTER TABLE `[TB_PREFIX][TABLE_NAME]`\nDROP `field_[FIELD_ID]_unit`;ALTER TABLE `[TB_PREFIX][TABLE_NAME]`\nDROP `field_[FIELD_ID]_si`;', NULL),
(9, '[0][1][4]', 'length', 1, '1.00', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]` ADD `field_[FIELD_ID]` double NOT NULL DEFAULT \'0\'; ALTER TABLE `[TB_PREFIX][TABLE_NAME]` ADD `field_[FIELD_ID]_si` double NOT NULL DEFAULT \'0\'; ALTER TABLE `[TB_PREFIX][TABLE_NAME]` ADD `field_[FIELD_ID]_unit` int NULL; UPDATE `[TB_PREFIX]wpl_dbst` SET `table_name`=\'[TABLE_NAME]\', `table_column`=\'field_[FIELD_ID]\' WHERE id=[FIELD_ID];', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]`\nDROP `field_[FIELD_ID]`;ALTER TABLE `[TB_PREFIX][TABLE_NAME]`\nDROP `field_[FIELD_ID]_unit`;ALTER TABLE `[TB_PREFIX][TABLE_NAME]`\nDROP `field_[FIELD_ID]_si`;', NULL),
(10, '[0][1][4]', 'volume', 1, '1.00', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]` ADD `field_[FIELD_ID]` double NOT NULL DEFAULT \'0\'; ALTER TABLE `[TB_PREFIX][TABLE_NAME]` ADD `field_[FIELD_ID]_si` double NOT NULL DEFAULT \'0\'; ALTER TABLE `[TB_PREFIX][TABLE_NAME]` ADD `field_[FIELD_ID]_unit` int NULL; UPDATE `[TB_PREFIX]wpl_dbst` SET `table_name`=\'[TABLE_NAME]\', `table_column`=\'field_[FIELD_ID]\' WHERE id=[FIELD_ID];', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]`\nDROP `field_[FIELD_ID]`;ALTER TABLE `[TB_PREFIX][TABLE_NAME]`\nDROP `field_[FIELD_ID]_unit`;ALTER TABLE `[TB_PREFIX][TABLE_NAME]`\nDROP `field_[FIELD_ID]_si`;', NULL),
(11, '[0][1][2][4]', 'price', 1, '1.00', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]` ADD `field_[FIELD_ID]` double NOT NULL DEFAULT \'0\'; ALTER TABLE `[TB_PREFIX][TABLE_NAME]` ADD `field_[FIELD_ID]_si` double NOT NULL DEFAULT \'0\'; ALTER TABLE `[TB_PREFIX][TABLE_NAME]` ADD `field_[FIELD_ID]_unit` int NULL; UPDATE `[TB_PREFIX]wpl_dbst` SET `table_name`=\'[TABLE_NAME]\', `table_column`=\'field_[FIELD_ID]\' WHERE id=[FIELD_ID];', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]`\nDROP `field_[FIELD_ID]`;ALTER TABLE `[TB_PREFIX][TABLE_NAME]`\nDROP `field_[FIELD_ID]_unit`;ALTER TABLE `[TB_PREFIX][TABLE_NAME]`\nDROP `field_[FIELD_ID]_si`;', NULL),
(12, '[0][1][2][4]', 'date', 1, '1.00', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]` ADD `field_[FIELD_ID]` date NULL; UPDATE `[TB_PREFIX]wpl_dbst` SET `table_name`=\'[TABLE_NAME]\', `table_column`=\'field_[FIELD_ID]\' WHERE id=[FIELD_ID];', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]`\nDROP `field_[FIELD_ID]`;', NULL),
(13, '[0][1][2][4]', 'datetime', 1, '1.00', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]` ADD `field_[FIELD_ID]` datetime NULL; UPDATE `[TB_PREFIX]wpl_dbst` SET `table_name`=\'[TABLE_NAME]\', `table_column`=\'field_[FIELD_ID]\' WHERE id=[FIELD_ID];', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]`\nDROP `field_[FIELD_ID]`;', NULL),
(14, '[0][1][2][4]', 'url', 1, '1.00', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]` ADD `field_[FIELD_ID]` varchar(100) NULL; UPDATE `[TB_PREFIX]wpl_dbst` SET `table_name`=\'[TABLE_NAME]\', `table_column`=\'field_[FIELD_ID]\' WHERE id=[FIELD_ID];', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]`\r\nDROP `field_[FIELD_ID]`;', NULL),
(19, '[0][1][2][4]', 'boolean', 1, '19.00', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]` ADD `field_[FIELD_ID]` TINYINT( 4 ) NOT NULL DEFAULT \'[DEFAULT_VALUE]\'; UPDATE `[TB_PREFIX]wpl_dbst` SET `table_name`=\'[TABLE_NAME]\', `table_column`=\'field_[FIELD_ID]\' WHERE id=[FIELD_ID];', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]`\r\nDROP `field_[FIELD_ID]`;', NULL),
(20, '[0][1][2][4]', 'checkbox', 1, '20.00', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]` ADD `field_[FIELD_ID]` TINYINT( 4 ) NOT NULL DEFAULT \'0\'; UPDATE `[TB_PREFIX]wpl_dbst` SET `table_name`=\'[TABLE_NAME]\', `table_column`=\'field_[FIELD_ID]\' WHERE id=[FIELD_ID];', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]`\r\nDROP `field_[FIELD_ID]`;', NULL),
(24, '[0][1][2][4]', 'array', 1, '1.00', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]` ADD `field_[FIELD_ID]` text NULL; UPDATE `[TB_PREFIX]wpl_dbst` SET `table_name`=\'[TABLE_NAME]\', `table_column`=\'field_[FIELD_ID]\' WHERE id=[FIELD_ID];', 'ALTER TABLE `[TB_PREFIX][TABLE_NAME]` DROP `field_[FIELD_ID]`;', NULL) ;

#
# End of data contents of table `wp_wpl_dbst_types`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_events`
#

DROP TABLE IF EXISTS `wp_wpl_events`;


#
# Table structure of table `wp_wpl_events`
#

CREATE TABLE `wp_wpl_events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) CHARACTER SET utf8 NOT NULL,
  `trigger` varchar(100) CHARACTER SET utf8 NOT NULL,
  `class_location` varchar(200) CHARACTER SET utf8 NOT NULL,
  `class_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `function_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `params` text CHARACTER SET utf8 NOT NULL,
  `enabled` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `enabled` (`enabled`),
  KEY `enabled_2` (`enabled`),
  KEY `enabled_3` (`enabled`),
  KEY `enabled_4` (`enabled`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_events`
#
INSERT INTO `wp_wpl_events` ( `id`, `type`, `trigger`, `class_location`, `class_name`, `function_name`, `params`, `enabled`) VALUES
(4, 'notification', 'contact_agent', 'libraries.event_handlers.notifications', 'wpl_events_notifications', 'contact_agent', '', 1),
(5, 'notification', 'contact_profile', 'libraries.event_handlers.notifications', 'wpl_events_notifications', 'contact_profile', '', 1),
(39, 'notification', 'user_registered', 'libraries.event_handlers.notifications', 'wpl_events_notifications', 'user_registered', '', 1),
(40, 'notification', 'request_a_visit_send', 'libraries.event_handlers.notifications', 'wpl_events_notifications', 'request_a_visit', '', 1),
(41, 'notification', 'send_to_friend', 'libraries.event_handlers.notifications', 'wpl_events_notifications', 'send_to_friend', '', 1),
(42, 'notification', 'add_property', 'libraries.event_handlers.notifications', 'wpl_events_notifications', 'listing_create', '', 1),
(44, 'notification', 'schedule_tour', 'libraries.event_handlers.notifications', 'wpl_events_notifications', 'schedule_tour', '', 1),
(45, 'notification', 'wpl_adding_price_request', 'libraries.event_handlers.notifications', 'wpl_events_notifications', 'adding_price_request', '', 1) ;

#
# End of data contents of table `wp_wpl_events`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_extensions`
#

DROP TABLE IF EXISTS `wp_wpl_extensions`;


#
# Table structure of table `wp_wpl_extensions`
#

CREATE TABLE `wp_wpl_extensions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(20) CHARACTER SET latin1 NOT NULL,
  `title` varchar(200) CHARACTER SET latin1 NOT NULL,
  `parent` int(10) NOT NULL DEFAULT '0',
  `description` text CHARACTER SET latin1 NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  `param1` varchar(200) CHARACTER SET latin1 NOT NULL,
  `param2` text CHARACTER SET latin1 NOT NULL,
  `param3` varchar(200) CHARACTER SET latin1 NOT NULL,
  `param4` varchar(200) CHARACTER SET latin1 NOT NULL,
  `param5` varchar(200) CHARACTER SET latin1 NOT NULL,
  `params` text CHARACTER SET latin1 NOT NULL,
  `editable` tinyint(4) NOT NULL DEFAULT '1',
  `index` float(5,2) NOT NULL DEFAULT '99.99',
  `client` tinyint(4) NOT NULL DEFAULT '2',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=142 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_extensions`
#
INSERT INTO `wp_wpl_extensions` ( `id`, `type`, `title`, `parent`, `description`, `enabled`, `param1`, `param2`, `param3`, `param4`, `param5`, `params`, `editable`, `index`, `client`) VALUES
(1, 'action', 'Admin menu', 0, 'This extension generates admin menu', 1, 'admin_menu', 'wpl_extensions->wpl_admin_menus', '', '', '', '', 0, '99.99', 2),
(2, 'shortcode', 'property listing shortcode', 0, 'it used for showing property listing', 1, 'wpl_property_listings', 'wpl_controller->f:property_listing:display', '', '', '', '', 0, '99.99', 2),
(3, 'style', 'WPL backend stylesheet', 0, '', 1, 'wpl_backend_main_style', 'css/backend.css', '', '', '', '', 0, '99.99', 1),
(5, 'javascript', 'jQuery', 0, '', 1, 'jquery', '', '', '', '', '', 0, '0.00', 2),
(6, 'javascript', 'WPL backend javascript', 0, '', 1, 'wpl_backend_javascript', 'js/backend.min.js', '', '', '', '', 0, '100.10', 1),
(9, 'widget', 'WPL Search Widget', 0, '', 1, 'widgets.search.main', 'widgets_init', 'WPL_search_widget', '', '', '', 0, '99.99', 2),
(10, 'shortcode', 'Property Show shortcode', 0, 'it can be used for showing a specific listing', 1, 'wpl_property_show', 'wpl_controller->f:property_show:display', '', '', '', '', 0, '99.99', 2),
(11, 'service', 'SEF service', 0, 'For running SEF service', 1, 'template_redirect', 'sef->run', '9999', '', '', '', 0, '99.99', 2),
(12, 'shortcode', 'Profile Listing shortcode', 0, 'it can be used for showing a profile (Agent) listing', 1, 'wpl_profile_listing', 'wpl_controller->f:profile_listing:display', '', '', '', '', 0, '99.99', 2),
(13, 'style', 'WPL frontend stylesheet', 0, '', 1, 'wpl_frontend_main_style', 'css/frontend.css', '', '', '', '', 0, '99.99', 0),
(14, 'widget', 'WPL Carousel Widget', 0, '', 1, 'widgets.carousel.main', 'widgets_init', 'WPL_carousel_widget', '', '', '', 0, '99.99', 2),
(15, 'action', 'Admin bar menu', 0, 'This extension generates admin bar menu', 1, 'admin_bar_menu', 'wpl_extensions->wpl_admin_bar_menu', '999', '', '', '', 0, '99.99', 2),
(16, 'javascript', 'WPL frontend javascript', 0, '', 1, 'wpl_frontend_javascript', 'js/frontend.min.js', '', '', '', '', 0, '100.10', 0),
(18, 'service', 'IO service', 0, 'For running IO commands', 1, 'wp', 'io->run', '9999', '', '', '', 0, '99.99', 0),
(20, 'widget', 'WPL Agents Widget', 0, '', 1, 'widgets.agents.main', 'widgets_init', 'WPL_agents_widget', '', '', '', 0, '99.99', 2),
(21, 'sidebar', 'Property Show Bottom', 0, 'Appears on bottom of single property/property show page', 1, 'wpl-pshow-bottom', '', '', '', '', '', 0, '99.99', 2),
(22, 'sidebar', 'Profile Show Top', 0, 'Appears on top of agent show/profile show page', 1, 'wpl-profileshow-top', '', '', '', '', '', 0, '99.99', 2),
(24, 'sidebar', 'Property Listing Top', 0, 'Appears below of Google map in property listing page', 1, 'wpl-plisting-top', '', '', '', '', '', 0, '99.99', 2),
(26, 'sidebar', 'Profile Listing Top', 0, 'Appears in Profile listing page', 1, 'wpl-profile-listing-top', '', '', '', '', '', 0, '99.99', 2),
(27, 'sidebar', 'WPL Hidden', 0, 'Appears no where! Use it for widget short-codes.', 1, 'wpl-hidden', '', '', '', '', '', 0, '99.99', 2),
(31, 'shortcode', 'my profile shortcode', 0, 'it used for showing my profile', 1, 'wpl_my_profile', 'wpl_html->load_profile_wizard', '', '', '', '', 0, '99.99', 2),
(34, 'shortcode', 'Profile show shortcode', 0, 'it used for showing a profile', 1, 'wpl_profile_show', 'wpl_controller->f:profile_show:display', '', '', '', '', 0, '99.99', 2),
(36, 'service', 'Helps Service', 0, 'For running WPL Helps', 1, 'init', 'helps->run', '9999', '', '', '', 0, '99.99', 1),
(88, 'javascript', 'jQuery UI', 0, '', 1, 'jquery-ui-core', '', '', '1', '', '', 0, '88.00', 2),
(89, 'javascript', 'jQuery UI sortable', 0, '', 1, 'jquery-ui-sortable', '', '', '1', '', '', 0, '89.00', 2),
(99, 'style', 'Googlefont', 0, '', 1, 'wpl-google-font', '//fonts.googleapis.com/css?family=Droid+Serif|Open+Sans|Lato|BenchNine', '', '', '1', '', 0, '35.00', 0),
(102, 'javascript', 'customScrollBarJS', 0, '', 1, 'customScrollBarJS', 'js/libraries/wpl.jquery.mcustomscrollbar.min.js', '', '1', '', '', 0, '100.00', 1),
(107, 'shortcode', 'Widget shortcode', 0, 'For showing Widgets instance', 1, 'wpl_widget_instance', 'wpl_widget->load_widget_instance', '', '', '', '', 0, '99.99', 2),
(109, 'javascript', 'Handlebars', 0, '', 1, 'handlebars', 'js/libraries/wpl.handlebars.min.js', '', '', '', '', 0, '109.99', 2),
(116, 'javascript', 'Realtyna Framework', 0, '', 1, 'realtyna-framework', 'js/libraries/realtyna/realtyna.min.js', '', '1', '', '', 0, '200.00', 2),
(124, 'action', 'User Login', 0, 'Calls after user login', 1, 'wp_login', 'wpl_users->user_loggedin', '10', '2', '', '', 0, '99.99', 2),
(131, 'service', 'WPL Service', 0, 'For running WPL service', 1, 'init', 'wpl->run', '9999', '', '', '', 0, '99.99', 2),
(137, 'sidebar', 'Property Listing Bottom', 0, 'Appears in the bottom of property listing page', 1, 'wpl-plisting-bottom', '', '', '', '', '', 0, '99.99', 2),
(140, 'widget', 'WPL Google Maps Widget', 0, '', 1, 'widgets.googlemap.main', 'widgets_init', 'WPL_googlemap_widget', '', '', '', 0, '99.99', 2),
(141, 'style', 'Backend Googlefont', 0, '', 1, 'wpl-google-font-backend', '//fonts.googleapis.com/css?family=Droid+Serif|Open+Sans|Lato|Scada|Archivo+Narrow', '', '', '1', '', 0, '36.00', 1) ;

#
# End of data contents of table `wp_wpl_extensions`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_filters`
#

DROP TABLE IF EXISTS `wp_wpl_filters`;


#
# Table structure of table `wp_wpl_filters`
#

CREATE TABLE `wp_wpl_filters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) CHARACTER SET utf8 NOT NULL,
  `trigger` varchar(100) CHARACTER SET utf8 NOT NULL,
  `class_location` varchar(200) CHARACTER SET utf8 NOT NULL,
  `class_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `function_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `params` text CHARACTER SET utf8 NOT NULL,
  `enabled` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `enabled` (`enabled`),
  KEY `enabled_2` (`enabled`),
  KEY `enabled_3` (`enabled`),
  KEY `enabled_4` (`enabled`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_filters`
#

#
# End of data contents of table `wp_wpl_filters`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_item_categories`
#

DROP TABLE IF EXISTS `wp_wpl_item_categories`;


#
# Table structure of table `wp_wpl_item_categories`
#

CREATE TABLE `wp_wpl_item_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_type` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `category_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `index` float(5,2) NOT NULL DEFAULT '99.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_item_categories`
#
INSERT INTO `wp_wpl_item_categories` ( `id`, `item_type`, `category_name`, `index`) VALUES
(1, 'gallery', 'image', '99.00'),
(2, 'gallery', 'floorplan', '99.01'),
(3, 'attachment', 'attachment', '99.00'),
(4, 'addon_video', 'video', '99.00') ;

#
# End of data contents of table `wp_wpl_item_categories`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_items`
#

DROP TABLE IF EXISTS `wp_wpl_items`;


#
# Table structure of table `wp_wpl_items`
#

CREATE TABLE `wp_wpl_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_kind` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `parent_id` int(10) DEFAULT NULL,
  `item_type` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `item_cat` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `item_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `creation_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  `item_extra1` text CHARACTER SET utf8,
  `item_extra2` text CHARACTER SET utf8,
  `item_extra3` text CHARACTER SET utf8,
  `item_extra4` text CHARACTER SET utf8,
  `item_extra5` text CHARACTER SET utf8,
  `params` text CHARACTER SET utf8,
  `index` float(5,2) NOT NULL DEFAULT '99.00',
  PRIMARY KEY (`id`),
  KEY `parent_type` (`parent_kind`,`parent_id`),
  KEY `item_type` (`item_type`),
  KEY `item_cat` (`item_cat`),
  KEY `item_type_2` (`item_type`),
  KEY `item_cat_2` (`item_cat`),
  KEY `index` (`index`),
  KEY `index_2` (`index`),
  KEY `parent_id` (`parent_id`,`item_type`),
  KEY `parent_id_2` (`parent_id`,`item_type`),
  KEY `item_type_3` (`item_type`),
  KEY `item_cat_3` (`item_cat`),
  KEY `index_3` (`index`),
  KEY `parent_id_3` (`parent_id`,`item_type`),
  KEY `item_type_4` (`item_type`),
  KEY `item_cat_4` (`item_cat`),
  KEY `index_4` (`index`),
  KEY `parent_id_4` (`parent_id`,`item_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_items`
#

#
# End of data contents of table `wp_wpl_items`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_kinds`
#

DROP TABLE IF EXISTS `wp_wpl_kinds`;


#
# Table structure of table `wp_wpl_kinds`
#

CREATE TABLE `wp_wpl_kinds` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `table` varchar(100) CHARACTER SET utf8 NOT NULL,
  `plural` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `dbcat` tinyint(4) NOT NULL DEFAULT '1',
  `addon_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `map` varchar(10) CHARACTER SET utf8 DEFAULT 'marker',
  `index` float(6,3) NOT NULL DEFAULT '99.000',
  `params` text CHARACTER SET utf8,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_kinds`
#
INSERT INTO `wp_wpl_kinds` ( `id`, `name`, `table`, `plural`, `dbcat`, `addon_name`, `map`, `index`, `params`, `enabled`) VALUES
(0, 'Property', 'wpl_properties', 'Properties', 1, '', 'marker', '99.000', NULL, 1),
(2, 'User', 'wpl_users', 'Users', 0, 'membership', 'marker', '99.000', NULL, 1) ;

#
# End of data contents of table `wp_wpl_kinds`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_listing_types`
#

DROP TABLE IF EXISTS `wp_wpl_listing_types`;


#
# Table structure of table `wp_wpl_listing_types`
#

CREATE TABLE `wp_wpl_listing_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent` int(10) unsigned NOT NULL DEFAULT '0',
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  `editable` tinyint(4) NOT NULL DEFAULT '2',
  `index` float(5,2) NOT NULL DEFAULT '99.00',
  `gicon` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `caption_img` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`),
  KEY `parent_2` (`parent`),
  KEY `parent_3` (`parent`),
  KEY `parent_4` (`parent`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_listing_types`
#
INSERT INTO `wp_wpl_listing_types` ( `id`, `parent`, `enabled`, `editable`, `index`, `gicon`, `caption_img`, `name`) VALUES
(1, 0, 0, 1, '1.00', '', '', 'Sale'),
(2, 0, 0, 1, '2.00', '', '', 'Rental'),
(3, 0, 0, 1, '3.00', '', '', 'Vacational'),
(9, 1, 1, 2, '0.00', 'dot-blue.png', '', 'For Sale'),
(10, 2, 1, 2, '12.00', 'dot-green.png', '', 'For Rent'),
(12, 3, 1, 2, '19.00', 'dot-orange.png', '', 'Vacation Rental') ;

#
# End of data contents of table `wp_wpl_listing_types`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_location1`
#

DROP TABLE IF EXISTS `wp_wpl_location1`;


#
# Table structure of table `wp_wpl_location1`
#

CREATE TABLE `wp_wpl_location1` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `enabled` tinyint(4) DEFAULT NULL,
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `abbr` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `tax_percent` double DEFAULT NULL,
  `latitude` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `longitude` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `params` text CHARACTER SET utf8,
  PRIMARY KEY (`id`),
  KEY `enabled` (`enabled`),
  KEY `enabled_2` (`enabled`),
  KEY `enabled_3` (`enabled`),
  KEY `enabled_4` (`enabled`)
) ENGINE=InnoDB AUTO_INCREMENT=276 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_location1`
#
INSERT INTO `wp_wpl_location1` ( `id`, `enabled`, `name`, `abbr`, `tax_percent`, `latitude`, `longitude`, `params`) VALUES
(1, 0, 'Afghanistan', NULL, '0', '', '', ''),
(2, 0, 'albania', NULL, '0', '', '', ''),
(3, 0, 'Algeria', NULL, '0', '', '', ''),
(4, 0, 'American Samoa', NULL, '0', '', '', ''),
(5, 0, 'Andorra', NULL, '0', '', '', ''),
(6, 0, 'Angola', NULL, '0', '', '', ''),
(7, 0, 'Anguilla', NULL, '0', '', '', ''),
(8, 0, 'Antarctica', NULL, '0', '', '', ''),
(9, 0, 'Antigua and Barbuda', NULL, '0', '', '', ''),
(10, 0, 'Argentina', NULL, '0', '', '', ''),
(11, 0, 'Armenia', NULL, '0', '', '', ''),
(12, 0, 'Aruba', NULL, '0', '', '', ''),
(13, 0, 'Ashmore and Cartier', NULL, '0', '', '', ''),
(14, 0, 'Australia', NULL, '0', '', '', ''),
(15, 0, 'Austria', NULL, '0', '', '', ''),
(16, 0, 'Azerbaijan', NULL, '0', '', '', ''),
(17, 0, 'The Bahamas', NULL, '0', '', '', ''),
(18, 0, 'Bahrain', NULL, '0', '', '', ''),
(19, 0, 'Baker Island', NULL, '0', '', '', ''),
(20, 0, 'Bangladesh', NULL, '0', '', '', ''),
(21, 0, 'Barbados', NULL, '0', '', '', ''),
(22, 0, 'Bassas da India', NULL, '0', '', '', ''),
(23, 0, 'Belarus', NULL, '0', '', '', ''),
(24, 0, 'Belgium', NULL, '0', '', '', ''),
(25, 0, 'Belize', NULL, '0', '', '', ''),
(26, 0, 'Benin', NULL, '0', '', '', ''),
(27, 0, 'Bermuda', NULL, '0', '', '', ''),
(28, 0, 'Bhutan', NULL, '0', '', '', ''),
(29, 0, 'Bolivia', NULL, '0', '', '', ''),
(30, 0, 'Bosnia and Herzegovina', NULL, '0', '', '', ''),
(31, 0, 'Botswana', NULL, '0', '', '', ''),
(32, 0, 'Bouvet Island', NULL, '0', '', '', ''),
(33, 0, 'Brazil', NULL, '0', '', '', ''),
(34, 0, 'British Indian Ocean Territory', NULL, '0', '', '', ''),
(35, 0, 'British Virgin Islands', NULL, '0', '', '', ''),
(36, 0, 'Brunei Darussalam', NULL, '0', '', '', ''),
(37, 0, 'Bulgaria', NULL, '0', '', '', ''),
(38, 0, 'Burkina Faso', NULL, '0', '', '', ''),
(39, 0, 'Burma', NULL, '0', '', '', ''),
(40, 0, 'Burundi', NULL, '0', '', '', ''),
(41, 0, 'Cambodia', NULL, '0', '', '', ''),
(42, 0, 'Cameroon', NULL, '0', '', '', ''),
(43, 0, 'Canada', NULL, '0', '', '', ''),
(44, 0, 'Cape Verde', NULL, '0', '', '', ''),
(45, 0, 'Cayman Islands', NULL, '0', '', '', ''),
(46, 0, 'Central African Republic', NULL, '0', '', '', ''),
(47, 0, 'Chad', NULL, '0', '', '', ''),
(48, 0, 'Chile', NULL, '0', '', '', ''),
(49, 0, 'China', NULL, '0', '', '', ''),
(50, 0, 'Christmas Island', NULL, '0', '', '', ''),
(51, 0, 'Clipperton Island', NULL, '0', '', '', ''),
(52, 0, 'Cocos (Keeling) Islands', NULL, '0', '', '', ''),
(53, 0, 'Colombia', NULL, '0', '', '', ''),
(54, 0, 'Comoros', NULL, '0', '', '', ''),
(55, 0, 'Congo ,  Democratic Republic of the', NULL, '0', '', '', ''),
(56, 0, 'Congo ,  Republic of the', NULL, '0', '', '', ''),
(57, 0, 'Cook Islands', NULL, '0', '', '', ''),
(58, 0, 'Coral Sea Islands', NULL, '0', '', '', ''),
(59, 0, 'Costa Rica', NULL, '0', '', '', ''),
(60, 0, 'Cote d\'Ivoire', NULL, '0', '', '', ''),
(61, 0, 'Croatia', NULL, '0', '', '', ''),
(62, 0, 'Cuba', NULL, '0', '', '', ''),
(63, 0, 'Cyprus', NULL, '0', '', '', ''),
(64, 0, 'Czech Republic', NULL, '0', '', '', ''),
(65, 0, 'Denmark', NULL, '0', '', '', ''),
(66, 0, 'Djibouti', NULL, '0', '', '', ''),
(67, 0, 'Dominica', NULL, '0', '', '', ''),
(68, 0, 'Dominican Republic', NULL, '0', '', '', ''),
(69, 0, 'East Timor', NULL, '0', '', '', ''),
(70, 0, 'Ecuador', NULL, '0', '', '', ''),
(71, 0, 'Egypt', NULL, '0', '', '', ''),
(72, 0, 'El Salvador', NULL, '0', '', '', ''),
(73, 0, 'Equatorial Guinea', NULL, '0', '', '', ''),
(74, 0, 'Eritrea', NULL, '0', '', '', ''),
(75, 0, 'Estonia', NULL, '0', '', '', ''),
(76, 0, 'Ethiopia', NULL, '0', '', '', ''),
(77, 0, 'Europa Island', NULL, '0', '', '', ''),
(78, 0, 'Falkland Islands (Islas Malvinas)', NULL, '0', '', '', ''),
(79, 0, 'Faroe Islands', NULL, '0', '', '', ''),
(80, 0, 'Fiji', NULL, '0', '', '', ''),
(81, 0, 'Finland', NULL, '0', '', '', ''),
(82, 0, 'France', NULL, '0', '', '', ''),
(83, 0, 'France ,  Metropolitan', NULL, '0', '', '', ''),
(84, 0, 'French Guiana', NULL, '0', '', '', ''),
(85, 0, 'French Polynesia', NULL, '0', '', '', ''),
(86, 0, 'French Southern and Antarctic Lands', NULL, '0', '', '', ''),
(87, 0, 'Gabon', NULL, '0', '', '', ''),
(88, 0, 'The Gambia', NULL, '0', '', '', ''),
(89, 0, 'Gaza Strip', NULL, '0', '', '', ''),
(90, 0, 'Georgia', NULL, '0', '', '', ''),
(91, 0, 'Germany', NULL, '0', '', '', ''),
(92, 0, 'Ghana', NULL, '0', '', '', ''),
(93, 0, 'Gibraltar', NULL, '0', '', '', ''),
(94, 0, 'Glorioso Islands', NULL, '0', '', '', ''),
(95, 0, 'Greece', NULL, '0', '', '', ''),
(96, 0, 'Greenland', NULL, '0', '', '', ''),
(97, 0, 'Grenada', NULL, '0', '', '', ''),
(98, 0, 'Guadeloupe', NULL, '0', '', '', ''),
(99, 0, 'Guam', NULL, '0', '', '', ''),
(100, 0, 'Guatemala', NULL, '0', '', '', '') ;
INSERT INTO `wp_wpl_location1` ( `id`, `enabled`, `name`, `abbr`, `tax_percent`, `latitude`, `longitude`, `params`) VALUES
(101, 0, 'Guernsey', NULL, '0', '', '', ''),
(102, 0, 'Guinea', NULL, '0', '', '', ''),
(103, 0, 'Guinea-Bissau', NULL, '0', '', '', ''),
(104, 0, 'Guyana', NULL, '0', '', '', ''),
(105, 0, 'Haiti', NULL, '0', '', '', ''),
(106, 0, 'Heard Island and McDonald Islands', NULL, '0', '', '', ''),
(107, 0, 'Holy See (Vatican City)', NULL, '0', '', '', ''),
(108, 0, 'Honduras', NULL, '0', '', '', ''),
(109, 0, 'Hong Kong (SAR)', NULL, '0', '', '', ''),
(110, 0, 'Howland Island', NULL, '0', '', '', ''),
(111, 0, 'Hungary', NULL, '0', '', '', ''),
(112, 0, 'Iceland', NULL, '0', '', '', ''),
(113, 0, 'India', NULL, '0', '', '', ''),
(114, 0, 'Indonesia', NULL, '0', '', '', ''),
(115, 0, 'Iran', NULL, '0', '', '', ''),
(116, 0, 'Iraq', NULL, '0', '', '', ''),
(117, 0, 'Ireland', NULL, '0', '', '', ''),
(118, 0, 'Israel', NULL, '0', '', '', ''),
(119, 0, 'Italy', NULL, '0', '', '', ''),
(120, 0, 'Jamaica', NULL, '0', '', '', ''),
(121, 0, 'Jan Mayen', NULL, '0', '', '', ''),
(122, 0, 'Japan', NULL, '0', '', '', ''),
(123, 0, 'Jarvis Island', NULL, '0', '', '', ''),
(124, 0, 'Jersey', NULL, '0', '', '', ''),
(125, 0, 'Johnston Atoll', NULL, '0', '', '', ''),
(126, 0, 'Jordan', NULL, '0', '', '', ''),
(127, 0, 'Juan de Nova Island', NULL, '0', '', '', ''),
(128, 0, 'Kazakhstan', NULL, '0', '', '', ''),
(129, 0, 'Kenya', NULL, '0', '', '', ''),
(130, 0, 'Kingman Reef', NULL, '0', '', '', ''),
(131, 0, 'Kiribati', NULL, '0', '', '', ''),
(132, 0, 'Korea ,  North', NULL, '0', '', '', ''),
(133, 0, 'Korea ,  South', NULL, '0', '', '', ''),
(134, 0, 'Kuwait', NULL, '0', '', '', ''),
(135, 0, 'Kyrgyzstan', NULL, '0', '', '', ''),
(136, 0, 'Laos', NULL, '0', '', '', ''),
(137, 0, 'Latvia', NULL, '0', '', '', ''),
(138, 0, 'Lebanon', NULL, '0', '', '', ''),
(139, 0, 'Lesotho', NULL, '0', '', '', ''),
(140, 0, 'Liberia', NULL, '0', '', '', ''),
(141, 0, 'Libya', NULL, '0', '', '', ''),
(142, 0, 'Liechtenstein', NULL, '0', '', '', ''),
(143, 0, 'Lithuania', NULL, '0', '', '', ''),
(144, 0, 'Luxembourg', NULL, '0', '', '', ''),
(145, 0, 'Macao', NULL, '0', '', '', ''),
(146, 0, 'Macedonia ,  The Former Yugoslav Republic of', NULL, '0', '', '', ''),
(147, 0, 'Madagascar', NULL, '0', '', '', ''),
(148, 0, 'Malawi', NULL, '0', '', '', ''),
(149, 0, 'Malaysia', NULL, '0', '', '', ''),
(150, 0, 'Maldives', NULL, '0', '', '', ''),
(151, 0, 'Mali', NULL, '0', '', '', ''),
(152, 0, 'Malta', NULL, '0', '', '', ''),
(153, 0, 'Man ,  Isle of', NULL, '0', '', '', ''),
(154, 0, 'Marshall Islands', NULL, '0', '', '', ''),
(155, 0, 'Martinique', NULL, '0', '', '', ''),
(156, 0, 'Mauritania', NULL, '0', '', '', ''),
(157, 0, 'Mauritius', NULL, '0', '', '', ''),
(158, 0, 'Mayotte', NULL, '0', '', '', ''),
(159, 0, 'Mexico', NULL, '0', '', '', ''),
(160, 0, 'Micronesia ,  Federated States of', NULL, '0', '', '', ''),
(161, 0, 'Midway Islands', NULL, '0', '', '', ''),
(162, 0, 'Miscellaneous (French)', NULL, '0', '', '', ''),
(163, 0, 'Moldova', NULL, '0', '', '', ''),
(164, 0, 'Monaco', NULL, '0', '', '', ''),
(165, 0, 'Mongolia', NULL, '0', '', '', ''),
(166, 0, 'Montenegro', NULL, '0', '', '', ''),
(167, 0, 'Montserrat', NULL, '0', '', '', ''),
(168, 0, 'Morocco', NULL, '0', '', '', ''),
(169, 0, 'Mozambique', NULL, '0', '', '', ''),
(170, 0, 'Myanmar', NULL, '0', '', '', ''),
(171, 0, 'Namibia', NULL, '0', '', '', ''),
(172, 0, 'Nauru', NULL, '0', '', '', ''),
(173, 0, 'Navassa Island', NULL, '0', '', '', ''),
(174, 0, 'Nepal', NULL, '0', '', '', ''),
(175, 0, 'Netherlands', NULL, '0', '', '', ''),
(176, 0, 'Netherlands Antilles', NULL, '0', '', '', ''),
(177, 0, 'New Caledonia', NULL, '0', '', '', ''),
(178, 0, 'New Zealand', NULL, '0', '', '', ''),
(179, 0, 'Nicaragua', NULL, '0', '', '', ''),
(180, 0, 'Niger', NULL, '0', '', '', ''),
(181, 0, 'Nigeria', NULL, '0', '', '', ''),
(182, 0, 'Niue', NULL, '0', '', '', ''),
(183, 0, 'Norfolk Island', NULL, '0', '', '', ''),
(184, 0, 'Northern Mariana Islands', NULL, '0', '', '', ''),
(185, 0, 'Norway', NULL, '0', '', '', ''),
(186, 0, 'Oman', NULL, '0', '', '', ''),
(187, 0, 'Pakistan', NULL, '0', '', '', ''),
(188, 0, 'Palau', NULL, '0', '', '', ''),
(189, 0, 'Palmyra Atoll', NULL, '0', '', '', ''),
(190, 0, 'Panama', NULL, '0', '', '', ''),
(191, 0, 'Papua New Guinea', NULL, '0', '', '', ''),
(192, 0, 'Paracel Islands', NULL, '0', '', '', ''),
(193, 0, 'Paraguay', NULL, '0', '', '', ''),
(194, 0, 'Peru', NULL, '0', '', '', ''),
(195, 0, 'Philippines', NULL, '0', '', '', ''),
(196, 0, 'Pitcairn Islands', NULL, '0', '', '', ''),
(197, 0, 'Poland', NULL, '0', '', '', ''),
(198, 0, 'Portugal', NULL, '0', '', '', ''),
(199, 0, 'Puerto Rico', NULL, '0', '', '', ''),
(200, 0, 'Qatar', NULL, '0', '', '', '') ;
INSERT INTO `wp_wpl_location1` ( `id`, `enabled`, `name`, `abbr`, `tax_percent`, `latitude`, `longitude`, `params`) VALUES
(201, 0, 'R', NULL, '0', '', '', ''),
(202, 0, 'Romania', NULL, '0', '', '', ''),
(203, 0, 'Russia', NULL, '0', '', '', ''),
(204, 0, 'Rwanda', NULL, '0', '', '', ''),
(205, 0, 'Saint Helena', NULL, '0', '', '', ''),
(206, 0, 'Saint Kitts and Nevis', NULL, '0', '', '', ''),
(207, 0, 'Saint Lucia', NULL, '0', '', '', ''),
(208, 0, 'Saint Pierre and Miquelon', NULL, '0', '', '', ''),
(209, 0, 'Saint Vincent and the Grenadines', NULL, '0', '', '', ''),
(210, 0, 'Samoa', NULL, '0', '', '', ''),
(211, 0, 'San Marino', NULL, '0', '', '', ''),
(212, 0, 'S?o Tom', NULL, '0', '', '', ''),
(213, 0, 'Saudi Arabia', NULL, '0', '', '', ''),
(214, 0, 'Senegal', NULL, '0', '', '', ''),
(215, 0, 'Serbia', NULL, '0', '', '', ''),
(216, 0, 'Serbia and Montenegro', NULL, '0', '', '', ''),
(217, 0, 'Seychelles', NULL, '0', '', '', ''),
(218, 0, 'Sierra Leone', NULL, '0', '', '', ''),
(219, 0, 'Singapore', NULL, '0', '', '', ''),
(220, 0, 'Slovakia', NULL, '0', '', '', ''),
(221, 0, 'Slovenia', NULL, '0', '', '', ''),
(222, 0, 'Solomon Islands', NULL, '0', '', '', ''),
(223, 0, 'Somalia', NULL, '0', '', '', ''),
(224, 0, 'South Africa', NULL, '0', '', '', ''),
(225, 0, 'South Georgia and the South Sandwich Islands', NULL, '0', '', '', ''),
(226, 0, 'Spain', NULL, '0', '', '', ''),
(227, 0, 'Spratly Islands', NULL, '0', '', '', ''),
(228, 0, 'Sri Lanka', NULL, '0', '', '', ''),
(229, 0, 'Sudan', NULL, '0', '', '', ''),
(230, 0, 'Suriname', NULL, '0', '', '', ''),
(231, 0, 'Svalbard', NULL, '0', '', '', ''),
(232, 0, 'Swaziland', NULL, '0', '', '', ''),
(233, 0, 'Sweden', NULL, '0', '', '', ''),
(234, 0, 'Switzerland', NULL, '0', '', '', ''),
(235, 0, 'Syria', NULL, '0', '', '', ''),
(236, 0, 'Taiwan', NULL, '0', '', '', ''),
(237, 0, 'Tajikistan', NULL, '0', '', '', ''),
(238, 0, 'Tanzania', NULL, '0', '', '', ''),
(239, 0, 'Thailand', NULL, '0', '', '', ''),
(240, 0, 'Togo', NULL, '0', '', '', ''),
(241, 0, 'Tokelau', NULL, '0', '', '', ''),
(242, 0, 'Tonga', NULL, '0', '', '', ''),
(243, 0, 'Trinidad and Tobago', NULL, '0', '', '', ''),
(244, 0, 'Tromelin Island', NULL, '0', '', '', ''),
(245, 0, 'Tunisia', NULL, '0', '', '', ''),
(246, 0, 'Turkey', NULL, '0', '', '', ''),
(247, 0, 'Turkmenistan', NULL, '0', '', '', ''),
(248, 0, 'Turks and Caicos Islands', NULL, '0', '', '', ''),
(249, 0, 'Tuvalu', NULL, '0', '', '', ''),
(250, 0, 'Uganda', NULL, '0', '', '', ''),
(251, 0, 'Ukraine', NULL, '0', '', '', ''),
(252, 0, 'United Arab Emirates', NULL, '0', '', '', ''),
(253, 0, 'United Kingdom', NULL, '0', '', '', ''),
(254, 1, 'United States', 'US', '0', '', '', ''),
(255, 0, 'United States Minor Outlying Islands', NULL, '0', '', '', ''),
(256, 0, 'Uruguay', NULL, '0', '', '', ''),
(257, 0, 'Uzbekistan', NULL, '0', '', '', ''),
(258, 0, 'Vanuatu', NULL, '0', '', '', ''),
(259, 0, 'Venezuela', NULL, '0', '', '', ''),
(260, 0, 'Vietnam', NULL, '0', '', '', ''),
(261, 0, 'Virgin Islands', NULL, '0', '', '', ''),
(262, 0, 'Virgin Islands (UK)', NULL, '0', '', '', ''),
(263, 0, 'Virgin Islands (US)', NULL, '0', '', '', ''),
(264, 0, 'Wake Island', NULL, '0', '', '', ''),
(265, 0, 'Wallis and Futuna', NULL, '0', '', '', ''),
(266, 0, 'West Bank', NULL, '0', '', '', ''),
(267, 0, 'Western Sahara', NULL, '0', '', '', ''),
(268, 0, 'Western Samoa', NULL, '0', '', '', ''),
(269, 0, 'World', NULL, '0', '', '', ''),
(270, 0, 'Yemen', NULL, '0', '', '', ''),
(271, 0, 'Yugoslavia', NULL, '0', '', '', ''),
(272, 0, 'Zaire', NULL, '0', '', '', ''),
(273, 0, 'Zambia', NULL, '0', '', '', ''),
(274, 0, 'Zimbabwe', NULL, '0', '', '', ''),
(275, 0, 'Palestinian Territory ,  Occupied', NULL, '0', '', '', '') ;

#
# End of data contents of table `wp_wpl_location1`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_location2`
#

DROP TABLE IF EXISTS `wp_wpl_location2`;


#
# Table structure of table `wp_wpl_location2`
#

CREATE TABLE `wp_wpl_location2` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `abbr` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `tax_percent` double DEFAULT NULL,
  `latitude` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `longitude` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `params` text CHARACTER SET utf8,
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`)
) ENGINE=InnoDB AUTO_INCREMENT=6061 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_location2`
#
INSERT INTO `wp_wpl_location2` ( `id`, `parent`, `name`, `abbr`, `tax_percent`, `latitude`, `longitude`, `params`) VALUES
(6001, 254, 'Alabama', 'AL', '0', '', '', ''),
(6002, 254, 'Alaska', 'AK', '0', '', '', ''),
(6003, 254, 'American Samoa', 'AS', '0', '', '', ''),
(6004, 254, 'Arizona', 'AZ', '0', '', '', ''),
(6005, 254, 'Arkansas', 'AR', '0', '', '', ''),
(6006, 254, 'California', 'CA', '0', '', '', ''),
(6007, 254, 'Colorado', 'CO', '0', '', '', ''),
(6008, 254, 'Connecticut', 'CT', '0', '', '', ''),
(6009, 254, 'Delaware', 'DE', '0', '', '', ''),
(6010, 254, 'District Of Columbia', 'DC', '0', '', '', ''),
(6011, 254, 'Federated States of Micronesia', 'FM', '0', '', '', ''),
(6012, 254, 'Florida', 'FL', '0', '', '', ''),
(6013, 254, 'Georgia', 'GA', '0', '', '', ''),
(6014, 254, 'Guam', 'GU', '0', '', '', ''),
(6015, 254, 'Hawaii', 'HI', '0', '', '', ''),
(6016, 254, 'Idaho', 'ID', '0', '', '', ''),
(6017, 254, 'Illinois', 'IL', '0', '', '', ''),
(6018, 254, 'Indiana', 'IN', '0', '', '', ''),
(6019, 254, 'Iowa', 'IA', '0', '', '', ''),
(6020, 254, 'Kansas', 'KS', '0', '', '', ''),
(6021, 254, 'Kentucky', 'KY', '0', '', '', ''),
(6022, 254, 'Louisiana', 'LA', '0', '', '', ''),
(6023, 254, 'Maine', 'ME', '0', '', '', ''),
(6024, 254, 'Marshall Islands', 'MH', '0', '', '', ''),
(6025, 254, 'Maryland', 'MD', '0', '', '', ''),
(6026, 254, 'Massachusetts', 'MA', '0', '', '', ''),
(6027, 254, 'Michigan', 'MI', '0', '', '', ''),
(6028, 254, 'Minnesota', 'MN', '0', '', '', ''),
(6029, 254, 'Minor Outlying Islands', 'UM', '0', '', '', ''),
(6030, 254, 'Mississippi', 'MS', '0', '', '', ''),
(6031, 254, 'Missouri', 'MO', '0', '', '', ''),
(6032, 254, 'Montana', 'MT', '0', '', '', ''),
(6033, 254, 'Nebraska', 'NE', '0', '', '', ''),
(6034, 254, 'Nevada', 'NV', '0', '', '', ''),
(6035, 254, 'New Hampshire', 'NH', '0', '', '', ''),
(6036, 254, 'New Jersey', 'NJ', '0', '', '', ''),
(6037, 254, 'New Mexico', 'NM', '0', '', '', ''),
(6038, 254, 'New York', 'NY', '0', '', '', ''),
(6039, 254, 'North Carolina', 'NC', '0', '', '', ''),
(6040, 254, 'North Dakota', 'ND', '0', '', '', ''),
(6041, 254, 'Northern Mariana Islands', 'MP', '0', '', '', ''),
(6042, 254, 'Ohio', 'OH', '0', '', '', ''),
(6043, 254, 'Oklahoma', 'OK', '0', '', '', ''),
(6044, 254, 'Oregon', 'OR', '0', '', '', ''),
(6045, 254, 'Palau', 'PW', '0', '', '', ''),
(6046, 254, 'Pennsylvania', 'PA', '0', '', '', ''),
(6047, 254, 'Puerto Rico', 'PR', '0', '', '', ''),
(6048, 254, 'Rhode Island', 'RI', '0', '', '', ''),
(6049, 254, 'South Carolina', 'SC', '0', '', '', ''),
(6050, 254, 'South Dakota', 'SD', '0', '', '', ''),
(6051, 254, 'Tennessee', 'TN', '0', '', '', ''),
(6052, 254, 'Texas', 'TX', '0', '', '', ''),
(6053, 254, 'Utah', 'UT', '0', '', '', ''),
(6054, 254, 'Vermont', 'VT', '0', '', '', ''),
(6055, 254, 'Virgin Islands', 'VI', '0', '', '', ''),
(6056, 254, 'Virginia', 'VA', '0', '', '', ''),
(6057, 254, 'Washington', 'WA', '0', '', '', ''),
(6058, 254, 'West Virginia', 'WV', '0', '', '', ''),
(6059, 254, 'Wisconsin', 'WI', '0', '', '', ''),
(6060, 254, 'Wyoming', 'WY', '0', '', '', '') ;

#
# End of data contents of table `wp_wpl_location2`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_location3`
#

DROP TABLE IF EXISTS `wp_wpl_location3`;


#
# Table structure of table `wp_wpl_location3`
#

CREATE TABLE `wp_wpl_location3` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `abbr` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `tax_percent` double DEFAULT NULL,
  `latitude` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `longitude` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `params` text CHARACTER SET utf8,
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_location3`
#

#
# End of data contents of table `wp_wpl_location3`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_location4`
#

DROP TABLE IF EXISTS `wp_wpl_location4`;


#
# Table structure of table `wp_wpl_location4`
#

CREATE TABLE `wp_wpl_location4` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `abbr` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `tax_percent` double DEFAULT NULL,
  `latitude` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `longitude` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `params` text CHARACTER SET utf8,
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_location4`
#

#
# End of data contents of table `wp_wpl_location4`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_location5`
#

DROP TABLE IF EXISTS `wp_wpl_location5`;


#
# Table structure of table `wp_wpl_location5`
#

CREATE TABLE `wp_wpl_location5` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `abbr` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `tax_percent` double DEFAULT NULL,
  `latitude` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `longitude` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `params` text CHARACTER SET utf8,
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_location5`
#

#
# End of data contents of table `wp_wpl_location5`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_location6`
#

DROP TABLE IF EXISTS `wp_wpl_location6`;


#
# Table structure of table `wp_wpl_location6`
#

CREATE TABLE `wp_wpl_location6` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `abbr` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `tax_percent` double DEFAULT NULL,
  `latitude` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `longitude` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `params` text CHARACTER SET utf8,
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_location6`
#

#
# End of data contents of table `wp_wpl_location6`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_location7`
#

DROP TABLE IF EXISTS `wp_wpl_location7`;


#
# Table structure of table `wp_wpl_location7`
#

CREATE TABLE `wp_wpl_location7` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '1',
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `abbr` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `tax_percent` double DEFAULT NULL,
  `latitude` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `longitude` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `params` text CHARACTER SET utf8,
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_location7`
#

#
# End of data contents of table `wp_wpl_location7`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_locationtextsearch`
#

DROP TABLE IF EXISTS `wp_wpl_locationtextsearch`;


#
# Table structure of table `wp_wpl_locationtextsearch`
#

CREATE TABLE `wp_wpl_locationtextsearch` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `location_text` text CHARACTER SET utf8,
  `count` int(10) DEFAULT NULL,
  `counts` text CHARACTER SET utf8,
  PRIMARY KEY (`id`),
  KEY `location_text` (`location_text`(255)),
  KEY `location_text_2` (`location_text`(255)),
  KEY `location_text_3` (`location_text`(255)),
  KEY `location_text_4` (`location_text`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_locationtextsearch`
#

#
# End of data contents of table `wp_wpl_locationtextsearch`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_locationzips`
#

DROP TABLE IF EXISTS `wp_wpl_locationzips`;


#
# Table structure of table `wp_wpl_locationzips`
#

CREATE TABLE `wp_wpl_locationzips` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `median_income` int(11) DEFAULT NULL,
  `average_hvalue` int(11) DEFAULT NULL,
  `distance_to_downtown` int(11) DEFAULT NULL,
  `school_rating` tinyint(4) DEFAULT NULL,
  `tax_rate` int(11) DEFAULT NULL,
  `population` int(11) DEFAULT NULL,
  `boundary` text CHARACTER SET utf8,
  `color` varchar(6) CHARACTER SET utf8 DEFAULT '',
  `hcolor` varchar(6) CHARACTER SET utf8 DEFAULT '',
  `longitude` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `latitude` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `params` text CHARACTER SET utf8,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `parent` (`parent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_locationzips`
#

#
# End of data contents of table `wp_wpl_locationzips`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_logs`
#

DROP TABLE IF EXISTS `wp_wpl_logs`;


#
# Table structure of table `wp_wpl_logs`
#

CREATE TABLE `wp_wpl_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `addon_id` int(10) unsigned NOT NULL,
  `section` varchar(100) CHARACTER SET utf8 NOT NULL,
  `status` varchar(100) CHARACTER SET utf8 NOT NULL,
  `log_text` text CHARACTER SET utf8 NOT NULL,
  `log_date` datetime NOT NULL,
  `ip` varchar(50) CHARACTER SET utf8 NOT NULL,
  `priority` tinyint(4) NOT NULL,
  `params` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_logs`
#

#
# End of data contents of table `wp_wpl_logs`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_menus`
#

DROP TABLE IF EXISTS `wp_wpl_menus`;


#
# Table structure of table `wp_wpl_menus`
#

CREATE TABLE `wp_wpl_menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client` varchar(20) CHARACTER SET latin1 NOT NULL DEFAULT 'backend',
  `type` varchar(10) CHARACTER SET latin1 NOT NULL DEFAULT 'submenu',
  `parent` varchar(50) CHARACTER SET latin1 NOT NULL DEFAULT 'WPL_main_menu',
  `page_title` varchar(100) CHARACTER SET latin1 NOT NULL,
  `menu_title` varchar(100) CHARACTER SET latin1 NOT NULL,
  `capability` varchar(100) CHARACTER SET latin1 NOT NULL DEFAULT 'current',
  `menu_slug` varchar(100) CHARACTER SET latin1 NOT NULL,
  `function` varchar(100) CHARACTER SET latin1 NOT NULL,
  `separator` tinyint(4) NOT NULL DEFAULT '0',
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  `index` float(6,3) NOT NULL DEFAULT '1.000',
  `position` float NOT NULL,
  `dashboard` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=342 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_menus`
#
INSERT INTO `wp_wpl_menus` ( `id`, `client`, `type`, `parent`, `page_title`, `menu_title`, `capability`, `menu_slug`, `function`, `separator`, `enabled`, `index`, `position`, `dashboard`) VALUES
(1, 'backend', 'menu', '', 'WPL', 'WPL', 'admin', 'WPL_main_menu', 'b:wpl:admin_home', 0, 1, '1.000', '4.5', 1),
(2, 'backend', 'submenu', 'WPL_main_menu', 'Data Structure', 'Data Structure', 'admin', 'wpl_admin_data_structure', 'b:data_structure:home', 0, 1, '1.000', '0', 1),
(3, 'backend', 'submenu', 'WPL_main_menu', 'Settings', 'Settings', 'admin', 'wpl_admin_settings', 'b:settings:home', 0, 1, '2.000', '0', 1),
(4, 'backend', 'submenu', 'WPL_main_menu', 'User Manager', 'User Manager', 'admin', 'wpl_admin_user_manager', 'b:users:user_manager', 1, 1, '3.000', '0', 1),
(5, 'backend', 'submenu', 'WPL_main_menu', 'My Profile', 'My Profile', 'current', 'wpl_admin_profile', 'b:users:profile', 0, 1, '5.000', '0', 1),
(6, 'backend', 'submenu', 'WPL_main_menu', 'Add Listing', 'Add Listing', 'current', 'wpl_admin_add_listing', 'b:listing:wizard', 1, 1, '6.000', '0', 1),
(7, 'backend', 'submenu', 'WPL_main_menu', 'Listing Manager', 'Listing Manager', 'current', 'wpl_admin_listings', 'b:listings:manager', 0, 1, '7.000', '0', 1),
(8, 'backend', 'submenu', 'WPL_main_menu', 'Locations', 'Locations', 'admin', 'wpl_admin_locations', 'b:location_manager:home', 0, 1, '1.100', '0', 1),
(11, 'backend', 'submenu', 'WPL_main_menu', 'Flex', 'Flex', 'admin', 'wpl_admin_flex', 'b:flex:home', 0, 1, '1.050', '0', 1),
(12, 'backend', 'submenu', 'WPL_main_menu', 'Activity Manager', 'Activity Manager', 'admin', 'wpl_admin_activity', 'b:activity_manager:home', 0, 1, '2.010', '0', 0),
(13, 'backend', 'submenu', 'WPL_main_menu', 'Notifications', 'Notifications', 'admin', 'wpl_admin_notifications', 'b:notifications:home', 0, 1, '2.050', '0', 0),
(341, 'backend', 'submenu', 'WPL_main_menu', 'Organic IDX', 'Organic IDX', 'admin', 'wpl_addon_idx', 'b:addon_idx:home', 1, 1, '341.000', '0', 1) ;

#
# End of data contents of table `wp_wpl_menus`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_notifications`
#

DROP TABLE IF EXISTS `wp_wpl_notifications`;


#
# Table structure of table `wp_wpl_notifications`
#

CREATE TABLE `wp_wpl_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `template` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `additional_memberships` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `additional_users` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `additional_emails` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `options` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `params` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_notifications`
#
INSERT INTO `wp_wpl_notifications` ( `id`, `description`, `template`, `subject`, `additional_memberships`, `additional_users`, `additional_emails`, `options`, `params`, `enabled`) VALUES
(2, 'Contact to listing agent from listing page', 'contact_agent', 'New Contact', '', '', '', NULL, '', 1),
(3, 'Contact to agent from profile page', 'contact_profile', 'New Profile Contact', '', '', '', NULL, '', 1),
(5, 'Sends after registration process.', 'user_registered', 'Your Account has been registered.', '', '', '', NULL, '', 1),
(6, 'Send to friend', 'send_to_friend', 'Send to friend', '', '', '', NULL, '', 1),
(7, 'Request a visit', 'request_a_visit', 'Request a visit', '', '', '', NULL, '', 1),
(8, 'When a listing creates', 'listing_create', 'New listing', '', '', '', NULL, '', 0),
(9, 'Schedule a Tour', 'schedule_tour', 'New Tour Schedule Request', '', '', '', NULL, '', 0),
(10, 'Adding Price Request notification', 'adding_price_request', 'A new price request', '', '', '', NULL, NULL, 1) ;

#
# End of data contents of table `wp_wpl_notifications`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_properties`
#

DROP TABLE IF EXISTS `wp_wpl_properties`;


#
# Table structure of table `wp_wpl_properties`
#

CREATE TABLE `wp_wpl_properties` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `kind` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '0=property,1=complex',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `mls_id` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `mls_id_num` bigint(20) DEFAULT NULL,
  `parent` int(11) unsigned NOT NULL COMMENT 'Parent',
  `pic_numb` mediumint(9) NOT NULL DEFAULT '0',
  `att_numb` mediumint(9) NOT NULL DEFAULT '0',
  `sent_numb` mediumint(9) NOT NULL DEFAULT '0',
  `contact_numb` mediumint(9) NOT NULL DEFAULT '0',
  `inc_in_listings_numb` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `user_id` smallint(6) NOT NULL DEFAULT '0',
  `listing` int(11) NOT NULL DEFAULT '0',
  `property_type` int(11) NOT NULL DEFAULT '0',
  `location1_id` int(11) NOT NULL DEFAULT '0',
  `location2_id` int(11) NOT NULL DEFAULT '0',
  `location3_id` int(11) NOT NULL DEFAULT '0',
  `location4_id` int(11) NOT NULL DEFAULT '0',
  `location5_id` int(11) NOT NULL DEFAULT '0',
  `location6_id` int(11) NOT NULL DEFAULT '0',
  `location7_id` int(11) NOT NULL DEFAULT '0',
  `location1_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `location2_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `location3_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `location4_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `location5_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `location6_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `location7_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `show_address` tinyint(4) NOT NULL DEFAULT '1',
  `view` tinyint(4) NOT NULL DEFAULT '0',
  `price` double NOT NULL DEFAULT '0',
  `price_unit` int(11) NOT NULL DEFAULT '0',
  `price_si` double NOT NULL DEFAULT '0',
  `price_period` smallint(6) NOT NULL DEFAULT '0',
  `bedrooms` float NOT NULL DEFAULT '0',
  `rooms` float NOT NULL DEFAULT '0',
  `bathrooms` float NOT NULL DEFAULT '0',
  `living_area` float NOT NULL DEFAULT '0',
  `living_area_unit` int(11) NOT NULL DEFAULT '0',
  `living_area_si` float NOT NULL DEFAULT '0',
  `lot_area` float NOT NULL DEFAULT '0',
  `lot_area_unit` int(11) NOT NULL DEFAULT '0',
  `lot_area_si` float NOT NULL DEFAULT '0',
  `parkings` float NOT NULL DEFAULT '0',
  `googlemap_lt` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `googlemap_ln` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `finalized` tinyint(4) NOT NULL DEFAULT '0',
  `add_date` datetime DEFAULT NULL,
  `last_finalize_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expire_days` int(11) NOT NULL DEFAULT '0',
  `confirmed` tinyint(4) NOT NULL DEFAULT '0',
  `expired` tinyint(4) NOT NULL DEFAULT '0',
  `visit_time` mediumint(9) NOT NULL DEFAULT '0',
  `visit_date` datetime DEFAULT NULL,
  `build_year` int(11) NOT NULL DEFAULT '0',
  `zip_id` int(11) NOT NULL DEFAULT '0',
  `zip_name` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `last_modified_time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `post_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `meta_description` text CHARACTER SET utf8,
  `meta_description_manual` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `meta_keywords` text CHARACTER SET utf8,
  `meta_keywords_manual` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `street` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `street_no` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `sp_featured` tinyint(4) NOT NULL DEFAULT '0',
  `sp_hot` tinyint(4) NOT NULL DEFAULT '0',
  `sp_openhouse` tinyint(4) NOT NULL DEFAULT '0',
  `sp_forclosure` tinyint(4) NOT NULL DEFAULT '0',
  `textsearch` text CHARACTER SET utf8,
  `property_title` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `location_text` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `vids_numb` int(10) NOT NULL DEFAULT '0',
  `field_42` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `field_312` varchar(70) CHARACTER SET utf8 DEFAULT NULL,
  `field_313` varchar(70) CHARACTER SET utf8 DEFAULT NULL,
  `field_308` text CHARACTER SET utf8,
  `field_7` int(11) NOT NULL DEFAULT '0',
  `n_100` tinyint(4) DEFAULT NULL,
  `n_100_distance` int(11) DEFAULT NULL,
  `n_100_distance_by` tinyint(4) DEFAULT NULL,
  `n_101` tinyint(4) DEFAULT NULL,
  `n_101_distance` int(11) DEFAULT NULL,
  `n_101_distance_by` tinyint(4) DEFAULT NULL,
  `n_102` tinyint(4) DEFAULT NULL,
  `n_102_distance` int(11) DEFAULT NULL,
  `n_102_distance_by` tinyint(4) DEFAULT NULL,
  `n_103` tinyint(4) DEFAULT NULL,
  `n_103_distance` int(11) DEFAULT NULL,
  `n_103_distance_by` tinyint(4) DEFAULT NULL,
  `n_105` tinyint(4) DEFAULT NULL,
  `n_105_distance` int(11) DEFAULT NULL,
  `n_105_distance_by` tinyint(4) DEFAULT NULL,
  `n_106` tinyint(4) DEFAULT NULL,
  `n_106_distance` int(11) DEFAULT NULL,
  `n_106_distance_by` tinyint(4) DEFAULT NULL,
  `n_107` tinyint(4) DEFAULT NULL,
  `n_107_distance` int(11) DEFAULT NULL,
  `n_107_distance_by` tinyint(4) DEFAULT NULL,
  `n_108` tinyint(4) DEFAULT NULL,
  `n_108_distance` int(11) DEFAULT NULL,
  `n_108_distance_by` tinyint(4) DEFAULT NULL,
  `n_109` tinyint(4) DEFAULT NULL,
  `n_109_distance` int(11) DEFAULT NULL,
  `n_109_distance_by` tinyint(4) DEFAULT NULL,
  `n_110` tinyint(4) DEFAULT NULL,
  `n_110_distance` int(11) DEFAULT NULL,
  `n_110_distance_by` tinyint(4) DEFAULT NULL,
  `n_111` tinyint(4) DEFAULT NULL,
  `n_111_distance` int(11) DEFAULT NULL,
  `n_111_distance_by` tinyint(4) DEFAULT NULL,
  `n_112` tinyint(4) DEFAULT NULL,
  `n_112_distance` int(11) DEFAULT NULL,
  `n_112_distance_by` tinyint(4) DEFAULT NULL,
  `n_113` tinyint(4) DEFAULT NULL,
  `n_113_distance` int(11) DEFAULT NULL,
  `n_113_distance_by` tinyint(4) DEFAULT NULL,
  `n_114` tinyint(4) DEFAULT NULL,
  `n_114_distance` int(11) DEFAULT NULL,
  `n_114_distance_by` tinyint(4) DEFAULT NULL,
  `n_115` tinyint(4) DEFAULT NULL,
  `n_115_distance` int(11) DEFAULT NULL,
  `n_115_distance_by` tinyint(4) DEFAULT NULL,
  `f_130_options` text CHARACTER SET utf8,
  `f_130` tinyint(4) DEFAULT NULL,
  `f_131_options` text CHARACTER SET utf8,
  `f_131` tinyint(4) DEFAULT NULL,
  `f_132_options` text CHARACTER SET utf8,
  `f_132` tinyint(4) DEFAULT NULL,
  `f_133_options` text CHARACTER SET utf8,
  `f_133` tinyint(4) DEFAULT NULL,
  `f_134_options` text CHARACTER SET utf8,
  `f_134` tinyint(4) DEFAULT NULL,
  `f_135_options` text CHARACTER SET utf8,
  `f_135` tinyint(4) DEFAULT NULL,
  `f_136_options` text CHARACTER SET utf8,
  `f_136` tinyint(4) DEFAULT NULL,
  `f_137_options` text CHARACTER SET utf8,
  `f_137` tinyint(4) DEFAULT NULL,
  `f_138_options` text CHARACTER SET utf8,
  `f_138` tinyint(4) DEFAULT NULL,
  `f_139_options` text CHARACTER SET utf8,
  `f_139` tinyint(4) DEFAULT NULL,
  `f_140_options` text CHARACTER SET utf8,
  `f_140` tinyint(4) DEFAULT NULL,
  `f_141_options` text CHARACTER SET utf8,
  `f_141` tinyint(4) DEFAULT NULL,
  `f_142_options` text CHARACTER SET utf8,
  `f_142` tinyint(4) DEFAULT NULL,
  `f_143_options` text CHARACTER SET utf8,
  `f_143` tinyint(4) DEFAULT NULL,
  `f_144_options` text CHARACTER SET utf8,
  `f_144` tinyint(4) DEFAULT NULL,
  `f_146_options` text CHARACTER SET utf8,
  `f_146` tinyint(4) DEFAULT NULL,
  `f_147_options` text CHARACTER SET utf8,
  `f_147` tinyint(4) DEFAULT NULL,
  `f_148_options` text CHARACTER SET utf8,
  `f_148` tinyint(4) DEFAULT NULL,
  `f_149_options` text CHARACTER SET utf8,
  `f_149` tinyint(4) DEFAULT NULL,
  `f_150_options` text CHARACTER SET utf8,
  `f_150` tinyint(4) DEFAULT NULL,
  `f_151_options` text CHARACTER SET utf8,
  `f_151` tinyint(4) DEFAULT NULL,
  `f_152_options` text CHARACTER SET utf8,
  `f_152` tinyint(4) DEFAULT NULL,
  `f_153_options` text CHARACTER SET utf8,
  `f_153` tinyint(4) DEFAULT NULL,
  `f_154_options` text CHARACTER SET utf8,
  `f_154` tinyint(4) DEFAULT NULL,
  `f_155_options` text CHARACTER SET utf8,
  `f_155` tinyint(4) DEFAULT NULL,
  `f_156_options` text CHARACTER SET utf8,
  `f_156` tinyint(4) DEFAULT NULL,
  `f_157_options` text CHARACTER SET utf8,
  `f_157` tinyint(4) DEFAULT NULL,
  `f_158_options` text CHARACTER SET utf8,
  `f_158` tinyint(4) DEFAULT NULL,
  `f_159_options` text CHARACTER SET utf8,
  `f_159` tinyint(4) DEFAULT NULL,
  `f_160_options` text CHARACTER SET utf8,
  `f_160` tinyint(4) DEFAULT NULL,
  `f_161_options` text CHARACTER SET utf8,
  `f_161` tinyint(4) DEFAULT NULL,
  `f_162_options` text CHARACTER SET utf8,
  `f_162` tinyint(4) DEFAULT NULL,
  `f_163_options` text CHARACTER SET utf8,
  `f_163` tinyint(4) DEFAULT NULL,
  `f_164_options` text CHARACTER SET utf8,
  `f_164` tinyint(4) DEFAULT NULL,
  `f_165_options` text CHARACTER SET utf8,
  `f_165` tinyint(4) DEFAULT NULL,
  `f_166_options` text CHARACTER SET utf8,
  `f_166` tinyint(4) DEFAULT NULL,
  `f_167_options` text CHARACTER SET utf8,
  `f_167` tinyint(4) DEFAULT NULL,
  `f_168_options` text CHARACTER SET utf8,
  `f_168` tinyint(4) DEFAULT NULL,
  `f_169_options` text CHARACTER SET utf8,
  `f_169` tinyint(4) DEFAULT NULL,
  `ref_id` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `field_54` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `half_bathrooms` float NOT NULL DEFAULT '0',
  `field_55` float NOT NULL DEFAULT '0',
  `rendered` text CHARACTER SET utf8,
  `alias` text CHARACTER SET utf8,
  `source` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT 'wpl',
  `last_sync_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `street_suffix` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`property_type`),
  KEY `state_id` (`location2_id`),
  KEY `country_id` (`location1_id`),
  KEY `listing_type_id` (`listing`),
  KEY `bedrooms` (`bedrooms`),
  KEY `bathrooms` (`bathrooms`),
  KEY `last_modified_time_stamp` (`last_modified_time_stamp`),
  KEY `mls_id` (`mls_id`),
  KEY `kind` (`kind`,`deleted`,`finalized`,`confirmed`,`expired`),
  KEY `kind_2` (`kind`,`deleted`,`finalized`,`confirmed`,`expired`),
  KEY `user_id` (`user_id`),
  KEY `user_id_2` (`user_id`),
  KEY `googlemap_lt` (`googlemap_lt`,`googlemap_ln`),
  KEY `googlemap_lt_2` (`googlemap_lt`,`googlemap_ln`),
  KEY `show_address` (`show_address`),
  KEY `show_address_2` (`show_address`),
  KEY `price_si` (`price_si`),
  KEY `price_si_2` (`price_si`),
  KEY `add_date` (`add_date`),
  KEY `add_date_2` (`add_date`),
  KEY `ref_id` (`ref_id`),
  KEY `ref_id_2` (`ref_id`),
  KEY `mls_id_num` (`mls_id_num`),
  KEY `mls_id_num_2` (`mls_id_num`),
  KEY `inc_in_listings_numb` (`inc_in_listings_numb`),
  KEY `last_finalize_date` (`last_finalize_date`),
  KEY `last_finalize_date_2` (`last_finalize_date`),
  KEY `kind_3` (`kind`,`deleted`,`finalized`,`confirmed`,`expired`),
  KEY `user_id_3` (`user_id`),
  KEY `googlemap_lt_3` (`googlemap_lt`,`googlemap_ln`),
  KEY `show_address_3` (`show_address`),
  KEY `price_si_3` (`price_si`),
  KEY `add_date_3` (`add_date`),
  KEY `ref_id_3` (`ref_id`),
  KEY `mls_id_num_3` (`mls_id_num`),
  KEY `last_finalize_date_3` (`last_finalize_date`),
  KEY `kind_4` (`kind`,`deleted`,`finalized`,`confirmed`,`expired`),
  KEY `user_id_4` (`user_id`),
  KEY `googlemap_lt_4` (`googlemap_lt`,`googlemap_ln`),
  KEY `show_address_4` (`show_address`),
  KEY `price_si_4` (`price_si`),
  KEY `add_date_4` (`add_date`),
  KEY `ref_id_4` (`ref_id`),
  KEY `mls_id_num_4` (`mls_id_num`),
  KEY `last_finalize_date_4` (`last_finalize_date`),
  FULLTEXT KEY `textsearch` (`textsearch`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_properties`
#

#
# End of data contents of table `wp_wpl_properties`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_property_types`
#

DROP TABLE IF EXISTS `wp_wpl_property_types`;


#
# Table structure of table `wp_wpl_property_types`
#

CREATE TABLE `wp_wpl_property_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent` int(11) NOT NULL DEFAULT '0',
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  `editable` tinyint(4) NOT NULL DEFAULT '1',
  `index` float(5,2) NOT NULL DEFAULT '99.00',
  `listing` int(11) NOT NULL DEFAULT '0',
  `name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `show_marker` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`),
  KEY `parent_2` (`parent`),
  KEY `parent_3` (`parent`),
  KEY `parent_4` (`parent`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_property_types`
#
INSERT INTO `wp_wpl_property_types` ( `id`, `parent`, `enabled`, `editable`, `index`, `listing`, `name`, `show_marker`) VALUES
(1, 0, 0, 2, '0.00', 0, 'Residential', 1),
(2, 0, 0, 2, '0.00', 0, 'Commercial', 1),
(3, 0, 0, 2, '0.00', 0, 'Land', 1),
(6, 1, 1, 2, '7.00', 0, 'Apartment', 1),
(7, 1, 1, 2, '32.00', 0, 'Villa', 1),
(13, 2, 1, 2, '6.00', 0, 'Office', 1),
(14, 3, 1, 2, '14.00', 0, 'Land', 1) ;

#
# End of data contents of table `wp_wpl_property_types`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_room_types`
#

DROP TABLE IF EXISTS `wp_wpl_room_types`;


#
# Table structure of table `wp_wpl_room_types`
#

CREATE TABLE `wp_wpl_room_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `index` float(5,2) NOT NULL DEFAULT '99.00',
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  `icon` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_room_types`
#
INSERT INTO `wp_wpl_room_types` ( `id`, `name`, `index`, `enabled`, `icon`) VALUES
(1, 'Master Bedroom', '7.00', 1, 'default.png'),
(2, 'Guest Room', '3.00', 1, 'default.png'),
(3, 'Living Room', '2.00', 1, 'default.png'),
(4, 'Walk in Closet', '8.00', 1, 'hanger.png'),
(5, 'Bathroom', '9.00', 1, 'shower.png'),
(6, 'Laundry room', '10.00', 1, 'cloth.png'),
(7, 'Kitchen', '5.00', 1, 'default.png'),
(8, 'Story room', '11.00', 1, 'default.png'),
(9, 'Study room', '12.00', 1, 'book.png'),
(20, 'Bedroom', '1.00', 1, 'bed.png'),
(21, 'Dining Room', '4.00', 1, 'spoon_fork.png'),
(22, 'Extra Room', '6.00', 1, 'default.png'),
(23, 'Family room', '14.00', 1, 'tv.png') ;

#
# End of data contents of table `wp_wpl_room_types`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_setting_categories`
#

DROP TABLE IF EXISTS `wp_wpl_setting_categories`;


#
# Table structure of table `wp_wpl_setting_categories`
#

CREATE TABLE `wp_wpl_setting_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `showable` tinyint(4) NOT NULL DEFAULT '1',
  `index` float(5,2) NOT NULL DEFAULT '99.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_setting_categories`
#
INSERT INTO `wp_wpl_setting_categories` ( `id`, `name`, `showable`, `index`) VALUES
(1, 'Global', 1, '99.00'),
(2, 'Gallery', 1, '99.00'),
(3, 'Locations', 1, '99.00'),
(4, 'SEO', 1, '99.00'),
(5, 'Notifications', 1, '99.00') ;

#
# End of data contents of table `wp_wpl_setting_categories`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_settings`
#

DROP TABLE IF EXISTS `wp_wpl_settings`;


#
# Table structure of table `wp_wpl_settings`
#

CREATE TABLE `wp_wpl_settings` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `setting_name` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `setting_value` text CHARACTER SET utf8,
  `showable` tinyint(4) NOT NULL DEFAULT '1',
  `category` int(10) NOT NULL DEFAULT '1',
  `type` varchar(100) CHARACTER SET latin1 NOT NULL DEFAULT 'text',
  `title` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `params` text CHARACTER SET utf8,
  `options` text CHARACTER SET utf8,
  `index` float(5,2) NOT NULL DEFAULT '99.00',
  PRIMARY KEY (`id`),
  KEY `setting_name` (`setting_name`),
  KEY `category` (`category`),
  KEY `setting_name_2` (`setting_name`),
  KEY `category_2` (`category`),
  KEY `setting_name_3` (`setting_name`),
  KEY `category_3` (`category`),
  KEY `setting_name_4` (`setting_name`),
  KEY `category_4` (`category`)
) ENGINE=InnoDB AUTO_INCREMENT=332 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_settings`
#
INSERT INTO `wp_wpl_settings` ( `id`, `setting_name`, `setting_value`, `showable`, `category`, `type`, `title`, `params`, `options`, `index`) VALUES
(1, 'zipcode_parent_level', '4', 1, 3, 'select', 'Zipcode parent level', '', '{"values":[{"key":1,"value":"Location1"},{"key":2,"value":"Location2"},{"key":3,"value":"Location3"},{"key":4,"value":"Location4"},{"key":5,"value":"Location5"},{"key":6,"value":"Location6"},{"key":7,"value":"Location7"}]}', '5.00'),
(2, 'cache', '1', 1, 1, 'select', 'Cache', '', '{"values":[{"key":0,"value":"Disabled"},{"key":1,"value":"Enabled"}]}', '0.10'),
(5, 'image_resize_method', '0', 1, 2, 'select', 'Image resize method', '', '{"values":[{"key":0,"value":"Resize"},{"key":1,"value":"Crop Resize"},{"key":2,"value":"Crop Resize (Center)"}]}', '2.00'),
(6, 'default_resize_width', '500', 1, 2, 'text', 'Default resize width', '', '', '3.00'),
(7, 'default_resize_height', '450', 1, 2, 'text', 'Default resize height', '', '', '4.00'),
(8, 'watermark_status', '0', 1, 2, 'select', 'Watermark', '', '{"values":[{"key":0,"value":"No"},{"key":1,"value":"Yes"}]}', '5.00'),
(9, 'watermark_position', 'center', 1, 2, 'select', 'Watermark Position ', '', '{"values":[{"key":"center","value":"Center"},{"key":"top","value":"Top"},{"key":"left","value":"Left"},{"key":"right","value":"Right"},{"key":"bottom","value":"Bottom"},{"key":"top-right","value":"Top-Right"},{"key":"top-left","value":"Top-Left"},{"key":"bottom-right","value":"Bottom-Right"},{"key":"bottom-left","value":"Bottom-Left"}]}', '6.00'),
(10, 'watermark_opacity', '40', 1, 2, 'text', 'Watermark Opacity', '', '', '7.00'),
(11, 'watermark_url', '', 1, 2, 'upload', 'Watermark Logo', '{"html_element_id":"wpl_watermark_uploader","request_str":"admin.php?wpl_format=b:settings:ajax&wpl_function=save_watermark_image"}', '', '8.00'),
(12, 'video_uploader', '0', 1, 1, 'checkbox', 'Video uploader', '', '', '50.00'),
(13, 'location1_keyword', 'Country', 1, 3, 'text', 'Location1 keyword', '', '', '99.00'),
(14, 'location2_keyword', 'State', 1, 3, 'text', 'Location2 keyword', '', '', '99.00'),
(15, 'location3_keyword', 'County', 1, 3, 'text', 'Location3 keyword', '', '', '99.00'),
(16, 'location4_keyword', 'City', 1, 3, 'text', 'Location4 keyword', '', '', '99.00'),
(17, 'location5_keyword', '', 1, 3, 'text', 'Location5 keyword', '', '', '99.00'),
(18, 'location6_keyword', '', 1, 3, 'text', 'Location6 keyword', '', '', '99.00'),
(19, 'location7_keyword', '', 1, 3, 'text', 'Location7 keyword', '', '', '99.00'),
(20, 'locationzips_keyword', 'Zipcode', 1, 3, 'text', 'Zipcode Keyword', '', '', '99.00'),
(21, 'default_page_size', '12', 1, 1, 'text', 'Default page size', '', '{"show_shortcode":1,"shortcode_key":"limit"}', '99.00'),
(22, 'page_sizes', '6,12,18,24,30,100', 1, 1, 'text', 'Page size options', '', '', '51.00'),
(23, 'default_orderby', 'p.mls_id', 1, 1, 'sort_option', 'Default sort option', '', '{"show_shortcode":1,"shortcode_key":"wplorderby","kind":0}', '99.00'),
(24, 'default_order', 'DESC', 1, 1, 'select', 'Default sort type', '', '{"show_shortcode":1,"shortcode_key":"wplorder","values":[{"key":"ASC","value":"Ascending"},{"key":"DESC","value":"Descending"}]}', '99.00'),
(25, 'main_permalink', '18', 1, 4, 'wppages', 'Main Permalink', '', '{"show_empty":"1"}', '1.00'),
(26, 'sef_main_separator', '/', 0, 4, 'text', 'SEF separator', '', '', '2.00'),
(27, 'main_date_format', 'd/m/Y:dd/mm/yy', 1, 1, 'select', 'Date Format', '', '{"values":[{"key":"Y-m-d:yy-mm-dd","value":"2013-10-19"},{"key":"Y\\/m\\/d:yy\\/mm\\/dd","value":"2013\\/10\\/19"},{"key":"Y.m.d:yy.mm.dd","value":"2013.10.19"},{"key":"m-d-Y:mm-dd-yy","value":"10-19-2013"},{"key":"m\\/d\\/Y:mm\\/dd\\/yy","value":"10\\/19\\/2013"},{"key":"m.d.Y:mm.dd.yy","value":"10.19.2013"},{"key":"d-m-Y:dd-mm-yy","value":"19-10-2013"},{"key":"d\\/m\\/Y:dd\\/mm\\/yy","value":"19\\/10\\/2013"},{"key":"d.m.Y:dd.mm.yy","value":"19.10.2013"}]}', '52.00'),
(28, 'location_method', '1', 1, 3, 'select', 'Location Method', '{"tooltip":"Location Text is recommended."}', '{"values":[{"key":1,"value":"Location Text"},{"key":2,"value":"Location Database"}]}', '6.00'),
(29, 'default_profile_orderby', 'p.first_name', 1, 1, 'sort_option', 'Profile sort option', '', '{"show_shortcode":1,"shortcode_key":"wplorderby","kind":2}', '110.00'),
(30, 'default_profile_order', 'DESC', 1, 1, 'select', 'Profile sort type', '', '{"show_shortcode":1,"shortcode_key":"wplorder","values":[{"key":"ASC","value":"Ascending"},{"key":"DESC","value":"Descending"}]}', '111.00'),
(31, 'default_profile_page_size', '12', 1, 1, 'text', 'Profile page size', '', '{"show_shortcode":1,"shortcode_key":"limit"}', '109.00'),
(33, 'io_status', '1', 1, 1, 'select', 'I/O API', '', '{"values":[{"key":0,"value":"Disabled"},{"key":1,"value":"Enabled"}]}', '117.00'),
(34, 'io_public_key', 'U7hdbv673YhdjplzzX7wU7hdbv673YhdjplzzX7w', 1, 1, 'text', 'API key', '{"readonly":"readonly"}', '', '118.00'),
(35, 'io_private_key', 'Eft76bdh0o2uyhJkbG3T', 1, 1, 'text', 'API secret', '{"readonly":"readonly"}', '', '119.00'),
(36, 'realtyna_username', NULL, 0, 1, 'text', '', '', '', '99.00'),
(37, 'realtyna_password', NULL, 0, 1, 'text', '', '', '', '99.00'),
(38, 'realtyna_verified', '0', 0, 1, 'text', '', '', '', '99.00'),
(50, 'backend_listing_target_page', NULL, 1, 4, 'wppages', 'Backend Listing Target', '{"tooltip":"Used for backend views"}', '{"show_empty":1}', '3.00'),
(51, 'log', '0', 1, 1, 'select', 'WPL log', NULL, '{"values":[{"key":0,"value":"Disabled"},{"key":1,"value":"Enabled"}]}', '53.00'),
(53, 'wpl_sender_email', '', 1, 5, 'text', 'Sender email', NULL, '', '121.00'),
(54, 'wpl_sender_name', '', 1, 5, 'text', 'Sender name', NULL, '', '122.00'),
(55, 'property_location_pattern', '[street_no] [street] [street_suffix][glue] [location4_name][glue] [location2_name] [zip_name]', 1, 3, 'pattern', 'Property Pattern', NULL, '', '123.00'),
(56, 'user_location_pattern', '[location5_name][glue] [location4_name][glue] [location2_name] [zip_name]', 1, 3, 'pattern', 'User Pattern', NULL, '', '124.00'),
(63, 'location_separator1', '', 1, 3, 'separator', 'Location Keywords', '', '', '98.00'),
(64, 'location_separator2', '', 1, 3, 'separator', 'Location Patterns', '', '', '122.00'),
(65, 'location_separator3', '', 1, 3, 'separator', 'Locations', '', '', '4.50'),
(66, 'permalink_separator', '', 1, 4, 'separator', 'WPL Permalink', '', '', '0.90'),
(67, 'sender_separator', NULL, 1, 5, 'separator', 'Notification Sender', NULL, NULL, '120.50'),
(68, 'resize_separator', NULL, 1, 2, 'separator', 'Resize', NULL, NULL, '1.50'),
(69, 'watermark_separator', NULL, 1, 2, 'separator', 'Watermark', NULL, NULL, '4.50'),
(70, 'global_separator', NULL, 1, 1, 'separator', 'Global', NULL, NULL, '0.05'),
(71, 'listing_pages_separator', NULL, 1, 1, 'separator', 'Listings', NULL, NULL, '98.00'),
(72, 'users_separator', NULL, 1, 1, 'separator', 'Users', NULL, NULL, '107.00'),
(73, 'io_separator', NULL, 1, 1, 'separator', 'I/O Application', NULL, NULL, '116.00'),
(90, 'property_alias_pattern', '[property_type][glue][listing_type][glue][location][glue][rooms][glue][bedrooms][glue][bathrooms][glue][price]', 1, 4, 'pattern', 'Listing Link Pattern', '', '', '4.00'),
(107, 'js_default_path', 'js/libraries', 0, 1, 'text', 'JS Default Path', '', '', '106.00'),
(126, 'google_api_key', 'AIzaSyAsKjUqrVxWKmIgWikIRJr1bsllnkKXeow', 1, 1, 'text', 'Google API key (Client Side)', NULL, NULL, '55.00'),
(141, 'location_suffix_prefix', 'County, Avenue, Ave, Boulevard, Blvd, Highway, Hwy, Lane, Ln, Square, Sq, Street, St, Road, Rd, Drive, Dr, Place, Pl, Plaza, Crescent, Cresc, Court, Cou, Path. Pa, Gate, Gt, Terrace, Ter', 0, 3, 'text', 'Location Suffixes/Prefixes', '{"html_class":"long"}', '', '141.00'),
(166, 'txtimg_color1', '000000', 0, 1, 'text', 'Text to Image color', NULL, NULL, '166.00'),
(188, 'wizard_map_zoomlevel', '11', 1, 3, 'text', 'Wizard Map Zoom-level', '', '', '7.00'),
(200, 'autoupdate_exchange_rates', '0', 1, 1, 'select', 'Auto-update exchange rates', '', '{"values":[{"key":0,"value":"Disabled"},{"key":1,"value":"Enabled"}]}', '97.00'),
(203, 'wpl_theme_compatibility', '0', 1, 1, 'select', 'Theme Compatibility', '{"tooltip":"If enabled, WPL tries to make the frontend styles compatible with your theme. It works only for main style of some famous themes such as Bridge, Avada, X, Be etc."}', '{"values":[{"key":1,"value":"Enabled"},{"key":0,"value":"Disabled"}]}', '5.00'),
(204, 'property_page_title_pattern', '[property_type] [listing][glue] [rooms][glue] [bedrooms][glue] [bathrooms][glue] [price][glue] [mls_id]', 1, 4, 'pattern', 'Listing Page Title', NULL, NULL, '5.00'),
(218, 'seo_patterns_separator', '', 1, 4, 'separator', 'SEO Patterns', '', '', '3.50'),
(219, 'property_title_pattern', '[property_type] [listing]', 1, 4, 'pattern', 'Listing Title', NULL, NULL, '4.50'),
(220, 'meta_description_pattern', '[bedrooms] [str:Bedrooms:bedrooms] [rooms] [str:Rooms:rooms] [str:With:bathrooms] [bathrooms] [str:Bathrooms:bathrooms] [property_type] [listing_type] [field_54] [field_42] [str:On the:field_55] [field_55] [str:Floor:field_55] [str:In] [location]', 1, 4, 'pattern', 'Meta Description', NULL, NULL, '6.00'),
(221, 'meta_keywords_pattern', '[location][glue] [bedrooms] [str:Bedrooms:bedrooms][glue] [rooms] [str:Rooms:rooms][glue][bathrooms][str:Bathrooms:bathrooms][glue][property_type][glue][listing_type][glue][field_54][glue][field_42][glue][field_55][glue][listing_id]', 1, 4, 'pattern', 'Meta Keywords', NULL, NULL, '7.00'),
(222, 'seo_adv_patterns_separator', '', 1, 4, 'separator', 'Advanced Patterns', '', '', '22.20'),
(223, 'seo_patterns', '', 1, 4, 'seo_patterns', 'Advanced Patterns', '', '', '22.30'),
(234, 'listing_media_confirm', '0', 1, 1, 'select', 'Media Confirm', '{"tooltip":"It\'s for showing\\/hiding tick icon for images\\/attachments in Add\\/Edit Listing menu."}', '{"values":[{"key":0,"value":"Disabled"},{"key":1,"value":"Enabled"}]}', '97.50'),
(241, 'load_gmap_js_libraries', '1', 0, 1, 'select', 'Load Google Maps JS libraries', '', '{"values":[{"key":0,"value":"Disabled"},{"key":1,"value":"Enabled"}]}', '241.00'),
(242, 'multiple_marker_icon', 'multiple.png', 0, 1, 'text', 'Multiple Marker Icon', '', '', '242.00'),
(251, 'base_currency', 'USD', 1, 1, 'currency', 'Base Currency', '{"tooltip":"This is used for currency conversion."}', '', '96.00'),
(254, 'hide_invalid_markers', '1', 1, 3, 'select', 'Hide invalid markers', '{"tooltip":"Hide the marker from maps when the geo point is not available or it set\'s to 0,0."}', '{"values":[{"key":1,"value":"Yes"},{"key":0,"value":"No"}]}', '8.00'),
(257, 'googlemap_display_status', '1', 1, 1, 'select', 'Google Maps Display', '{"tooltip":"If you hided it, you can place a Google Maps widget into your desired sidebar. Then your website users can view the map using the Google Maps widget."}', '{"values":[{"key":1,"value":"Show By Default"},{"key":0,"value":"Hide By Default"}]}', '56.00'),
(258, 'wpl_cronjobs', '1', 0, 1, 'text', NULL, NULL, NULL, '99.00'),
(259, 'wpl_last_cpanel_cronjobs', '', 0, 1, 'text', NULL, NULL, NULL, '99.00'),
(274, 'microdata_separator', '', 1, 4, 'separator', 'Micordata', '', '', '274.00'),
(275, 'microdata', '0', 1, 4, 'select', 'Microdata(schema.org)', '', '{"values":[{"key":0,"value":"Disabled"},{"key":1,"value":"Enabled"}]}', '274.01'),
(277, 'google_serverside_api_key', NULL, 1, 1, 'text', 'Google API key (Server Side)', '{"tooltip":"Please note that for the Server Side API Key, you should not set any referrer restrictions (or IP restrictions)."}', NULL, '55.05'),
(293, 'advanced_markers', '{"status":"0","listing_types":{"9":"#29a9df","10":"#cccc24","12":"#d326d6"},"property_types":{"13":"income.svg","6":"commercial.svg","14":"land.svg","7":"apartment.svg"}}', 1, 1, 'advanced_markers', '', NULL, NULL, '240.70'),
(294, 'advanced_markers_separator', NULL, 1, 1, 'separator', 'Advanced Markers', NULL, NULL, '240.60'),
(295, 'geolocation_listings', '1', 1, 1, 'checkbox', 'Geolocation Listings', '{"tooltip":"Show listings based on users current geolocation."}', NULL, '100.10'),
(297, 'show_plisting_visits', '0', 1, 1, 'checkbox', 'Show Listing Visits', '{"tooltip":"If enabled, total property visits will show on the search results pages."}', NULL, '101.00'),
(298, 'gre_separator', NULL, 1, 1, 'separator', 'Google Recaptcha', '', '', '250.05'),
(299, 'gre_enable', '0', 1, 1, 'select', 'Google Recaptcha', NULL, '{"values":[{"key":"0","value":"Disable"},{"key":"1","value":"Enable"}]}', '250.10'),
(300, 'gre_site_key', NULL, 1, 1, 'text', 'Site Key', NULL, '', '250.15'),
(301, 'gre_secret_key', NULL, 1, 1, 'text', 'Secret Key', NULL, '', '250.20'),
(302, 'gre_user_contact_activity', '0', 1, 1, 'checkbox', 'User Contact', NULL, '', '250.25'),
(303, 'gre_listing_contact_activity', '0', 1, 1, 'checkbox', 'Listing Contact', NULL, '', '250.30'),
(304, 'gre_widget_contact_activity', '0', 1, 1, 'checkbox', 'Favorites Widget', NULL, '', '250.35'),
(310, 'gre_report_listing', '0', 1, 1, 'checkbox', 'Report Listing', NULL, '', '250.40'),
(311, 'gre_send_to_friend', '0', 1, 1, 'checkbox', 'Send to Friend', NULL, '', '250.50'),
(312, 'gre_request_visit', '0', 1, 1, 'checkbox', 'Request a Visit', NULL, '', '250.45'),
(321, 'listing_404_redirect', '0', 1, 1, 'checkbox', 'Not Found Redirection', '{"tooltip":"Redirect the user to property listing page if property not found!"}', '', '102.00'),
(331, 'profile_email_type', '0', 1, 1, 'select', 'Profile email type', '', '{"values":[{"key":"0","value":"Image"},{"key":"1","value":"Text"}]}', '111.00') ;

#
# End of data contents of table `wp_wpl_settings`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_sort_options`
#

DROP TABLE IF EXISTS `wp_wpl_sort_options`;


#
# Table structure of table `wp_wpl_sort_options`
#

CREATE TABLE `wp_wpl_sort_options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kind` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '[0][1]',
  `name` text CHARACTER SET utf8,
  `asc_label` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `asc_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `desc_label` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `desc_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `field_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  `index` float(5,2) NOT NULL DEFAULT '99.00',
  PRIMARY KEY (`id`),
  KEY `kind` (`kind`,`enabled`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_sort_options`
#
INSERT INTO `wp_wpl_sort_options` ( `id`, `kind`, `name`, `asc_label`, `asc_enabled`, `desc_label`, `desc_enabled`, `field_name`, `enabled`, `index`) VALUES
(1, '[0][1]', 'Listing ID', NULL, 1, NULL, 1, 'p.mls_id', 1, '4.00'),
(2, '[0][1]', 'Price', NULL, 1, NULL, 1, 'p.price_si', 1, '6.00'),
(3, '[0][1]', 'Built up Area', NULL, 1, NULL, 1, 'p.living_area_si', 1, '5.00'),
(4, '[0][1]', 'Bedrooms', NULL, 1, NULL, 1, 'p.bedrooms', 0, '9.00'),
(6, '[0][1]', 'Pictures', NULL, 1, NULL, 1, 'p.pic_numb', 1, '10.00'),
(7, '[0][1]', 'Listing', NULL, 1, NULL, 1, 'p.listing', 0, '12.00'),
(8, '[0][1]', 'Property Type', NULL, 1, NULL, 1, 'p.property_type', 0, '13.00'),
(9, '[0][1]', 'Add date', NULL, 1, NULL, 1, 'p.add_date', 1, '11.00'),
(10, '[0][1]', 'View', NULL, 1, NULL, 1, 'p.view', 0, '15.00'),
(11, '[0][1]', 'Lot Area', NULL, 1, NULL, 1, 'p.lot_area_si', 0, '16.00'),
(12, '[0][1]', 'Zipcode', NULL, 1, NULL, 1, 'p.zip_name', 0, '17.00'),
(13, '[0][1]', 'Featured', NULL, 1, NULL, 1, 'p.sp_featured', 1, '14.00'),
(31, '[2]', 'Name', NULL, 1, NULL, 1, 'p.first_name', 1, '99.00'),
(32, '[2]', 'Country', NULL, 1, NULL, 1, 'p.location1_name', 1, '99.00'),
(33, '[2]', 'State', NULL, 1, NULL, 1, 'p.location2_name', 0, '99.00') ;

#
# End of data contents of table `wp_wpl_sort_options`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_unit_types`
#

DROP TABLE IF EXISTS `wp_wpl_unit_types`;


#
# Table structure of table `wp_wpl_unit_types`
#

CREATE TABLE `wp_wpl_unit_types` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_unit_types`
#
INSERT INTO `wp_wpl_unit_types` ( `id`, `name`) VALUES
(1, 'Length'),
(2, 'Area'),
(3, 'Volume'),
(4, 'Currency') ;

#
# End of data contents of table `wp_wpl_unit_types`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_units`
#

DROP TABLE IF EXISTS `wp_wpl_units`;


#
# Table structure of table `wp_wpl_units`
#

CREATE TABLE `wp_wpl_units` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '2' COMMENT '1=length,2=area,3=volume 4=price',
  `enabled` tinyint(4) NOT NULL DEFAULT '0',
  `tosi` double NOT NULL DEFAULT '0',
  `index` int(11) NOT NULL DEFAULT '999',
  `extra` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'iso code',
  `extra2` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'currency name',
  `extra3` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'icon',
  `extra4` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `seperator` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `d_seperator` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `after_before` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=before,1=after',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=271 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_units`
#
INSERT INTO `wp_wpl_units` ( `id`, `name`, `type`, `enabled`, `tosi`, `index`, `extra`, `extra2`, `extra3`, `extra4`, `seperator`, `d_seperator`, `after_before`) VALUES
(1, 'Sqft', 2, 1, '0.093', 1, '', '', '', '', '', '', 0),
(2, 'm&sup2;', 2, 1, '1', 2, '', '', '', '', '', '', 0),
(3, 'Acre', 2, 0, '4046.873', 3, '', '', '', '', '', '', 0),
(4, 'km&sup2;', 2, 0, '1000000', 4, '', '', '', '', '', '', 0),
(5, 'sq mi', 2, 0, '2589988', 6, '', '', '', '', '', '', 0),
(6, 'sq yd', 2, 0, '0.836', 5, '', '', '', '', '', '', 0),
(7, 'Hectare', 2, 0, '10000', 7, '', '', '', '', '', '', 0),
(11, 'm', 1, 1, '1', 0, '', '', '', '', '', '', 0),
(12, 'ft', 1, 1, '0.305', 0, '', '', '', '', '', '', 0),
(13, 'km', 1, 1, '1000', 2, '', '', '', '', '', '', 0),
(14, 'mi', 1, 1, '1609.344', 5, '', '', '', '', '', '', 0),
(15, 'yd', 1, 0, '0.9144', 6, '', '', '', '', '', '', 0),
(16, 'cm', 1, 0, '0.01', 7, '', '', '', '', '', '', 0),
(17, 'in', 1, 0, '0.0254', 0, '', '', '', '', '', '', 0),
(21, 'm&sup3;', 3, 0, '1', 1, '', '', '', '', '', '', 0),
(100, 'ლ', 4, 0, '0.5977', 9, 'GEL', 'Georgian Lari', 'Abkhazia', 'Tetri', '', '', 0),
(101, '؋', 4, 0, '0.0184', 11, 'AFN', 'Afghan Afghani', 'Afghanistan', 'Pul', '', '', 0),
(102, 'L', 4, 0, '0.0094', 12, 'ALL', 'Albania Lek', 'Albanian', 'Qintar', '', '', 0),
(103, '£', 4, 0, '87.9', 13, 'GBP', 'Alderney Pound', 'Alderney', 'Penny', '', '', 0),
(104, 'د.ج', 4, 0, '0.0126', 14, 'DZD', 'Algerian Dinar', 'Algeria', 'Centime', '', '', 0),
(105, 'Kz', 4, 0, '0.0104', 15, 'AOA', 'Angolan Kwanza', 'Angola', 'Cêntimo', '', '', 0),
(106, '$', 4, 0, '0.1886', 16, 'ARS', 'Argentine Peso', 'Argentina', 'Centavo', '', '', 0),
(107, 'դր.', 4, 0, '0.0024', 17, 'AMD', 'Armenian Dram', 'Armenia', 'Luma', '', '', 0),
(108, 'Afl.', 4, 0, '0.5587', 18, 'AWG', 'Aruban Florin', 'Aruba', 'Cent', '', '', 0),
(109, '$, A$ or AU$', 4, 0, '0.9435', 6, 'AUD', 'Australian Dollar', 'Australia', 'Cent', '', '', 0),
(110, 'm, ман. or man.', 4, 0, '1.2742', 19, 'AZN', 'Azerbaijani Manat', 'Azerbaijan', 'qəpik', '', '', 0),
(111, 'B$', 4, 0, '1', 20, 'BSD', 'Bahamian dollar', 'Bahamas', 'Cent', '', '', 0),
(112, '.د.ب (Arabic), or BD (Latin),', 4, 0, '2.652', 21, 'BHD', 'Bahraini Dollar', 'Bahrain', 'Fils', '', '', 0),
(113, '৳, ৲', 4, 0, '0.0129', 22, 'BDT', 'Bangladeshi Taka', 'Bangladesh', 'Poisha', '', '', 0),
(114, 'Bds$', 4, 0, '0.5', 23, 'BBD', 'Barbadian Dollar', 'Barbados', 'Cent', '', '', 0),
(115, 'Br', 4, 0, '0.0001', 24, 'BYR', 'Belarusian Ruble', 'Belarus', 'Kapeyka', '', '', 0),
(116, 'BZ$', 4, 0, '0.5', 25, 'BZD', 'Belize Dollar', 'Belize', 'Cent', '', '', 0),
(117, 'BD$', 4, 0, '1', 26, 'BMD', 'Bermudian Dollar', 'Bermuda', 'Cent', '', '', 0),
(118, 'Nu. or Ch.', 4, 0, '0.0173', 27, 'BTN', 'Bhutanese Ngultrum', 'Bhutan', 'Chhertum', '', '', 0),
(119, 'Bs.', 4, 0, '0.1447', 28, 'BOB', 'Bolivian Boliviano', 'Bolivia', 'Centavo', '', '', 0),
(120, 'KM (Latin), or КМ (Cyrillic),', 4, 0, '0.6733', 29, 'BAM', 'Bosnia and Herzegovina Marka', 'Bosnia and Herzegovina', 'fening (Latin), or фенинг (Cyrillic),', '', '', 0),
(121, 'P', 4, 0, '0.1175', 30, 'BWP', 'Botswana Pula', 'Botswana', 'thebe', '', '', 0),
(122, 'R$ or Ɽ', 4, 0, '0.4678', 31, 'BRL', 'Brazillian Real', 'Brazil', 'Centavo', '', '', 0),
(123, 'B$', 4, 0, '0.7969', 32, 'BND', 'Brunei Dollar', 'Sultanate of Brunei', 'Sen', '', '', 0),
(124, 'лв', 4, 0, '0.676', 33, 'BGN', 'Bulgarian Lev', 'Bulgaria', 'Stotinka', '', '', 0),
(125, 'FBu', 4, 0, '0.0006', 34, 'BIF', 'Burundian Franc', 'Burundi', 'Centime', '', '', 0),
(126, '៛', 4, 0, '0.0003', 35, 'KHR', 'Cambodian Riel', 'Cambodia', '1/10 Kak', '', '', 0),
(127, 'C$', 4, 0, '0.9805', 5, 'CAD', 'Canadian Dollar', 'Canada', 'Cent', '', '', 0),
(128, 'Esc or $', 4, 0, '0.012', 36, 'CVE', 'Cap Werdean Escudo', 'Cape Verde', 'Centavo', '', '', 0),
(129, '$', 4, 0, '1.2195', 37, 'KYD', 'Cayman Islands Dollar', 'Caymen Islands', 'Cent', '', '', 0),
(130, 'FCFA, c', 4, 0, '0.002', 38, 'XAF', 'Central African CFA franc', 'Central Africa', 'Centime', '', '', 0),
(131, '$', 4, 0, '0.002', 39, 'CLP', 'Chilean Peso', 'Chile', 'Centavo', '', '', 0),
(132, '$', 4, 0, '0.0005', 40, 'COP', 'Colombian Peso', 'Republic of Colombia', 'Centavo', '', '', 0),
(133, 'CF', 4, 0, '0.0027', 41, 'KMF', 'Comorian Franc', 'Comoros', 'Centime', '', '', 0),
(134, 'Fr', 4, 0, '0.0011', 42, 'CDF', 'Congolese Franc', 'Democratic Republic of Congo', 'Centime', '', '', 0),
(135, '$ or c', 4, 0, '87.6', 43, 'NZD', 'Cook Islands Dollar', 'Cook Islands', 'Cent', '', '', 0),
(136, '₡', 4, 0, '0.002', 44, 'CRC', 'Costa Rican colón', 'Costa Rica', 'Céntimo', '', '', 0),
(137, 'kn or lp', 4, 0, '0.1714', 45, 'HRK', 'Croatian Kuna', 'Croatia', 'Lipa', '', '', 0),
(138, '$, CUC or CUC$', 4, 0, '87.6', 46, 'CUC', 'Cuban Convertible Peso', 'Cuba', 'Centavo', '', '', 0),
(139, '$ or $MN', 4, 0, '1', 47, 'CUP', 'Cuban Peso', 'Cuba', 'Centavo', '', '', 0),
(140, 'K�? or h', 4, 0, '0.0505', 48, 'CZK', 'Czech Koruna', 'Czech Republic', 'Haléř', '', '', 0),
(141, 'kr', 4, 0, '0.1743', 49, 'DKK', 'Danish Krone', 'Denmark', 'øre or kroner', '', '', 0),
(142, 'Fdj', 4, 0, '0.0056', 50, 'DJF', 'Djiboutian Franc', 'Djibouti', 'Centime', '', '', 0),
(143, 'RD$', 4, 0, '0.0243', 51, 'DOP', 'Dominican Peso', 'Dominican Republic', 'Centavo', '', '', 0),
(144, '$', 4, 0, '0.3704', 52, 'XCD', 'East Caribbean Dollar', 'East Caribbean', 'Cent', '', '', 0),
(145, '$', 4, 0, '1', 10, 'USD', 'Ecuadorian Centavo Coins', 'Ecuador', 'Cent', '', '', 0),
(146, '£, LE, or ج.م or Pt.', 4, 0, '0.1431', 53, 'EGP', 'Egyptian Pound', 'Egypt', 'qirsh or', '', '', 0),
(147, 'Nfk', 4, 0, '0.0888', 54, 'ERN', 'Eritrean Nakfa', 'Eritrea', 'Cent', '', '', 0),
(148, 'KR', 4, 0, '0', 55, 'EEK', 'Estonian Kroon', 'Estonia', 'Sent', '', '', 0),
(149, 'Br', 4, 0, '0.0536', 56, 'ETB', 'Ethiopian Birr', 'Ethiopia', 'Santim', '', '', 0),
(150, '€', 4, 1, '1.3634', 2, 'EUR', 'Euro', 'European-Union', 'Euro', ',', '', 0),
(151, '£ or p', 4, 0, '1.5193', 57, 'FKP', 'Flakland Islands Pound', 'Falkland Islands', 'Penny', '', '', 0),
(152, 'kr', 4, 0, '0.1743', 58, 'DKK', 'Faroese Króna', 'Faroe Islands', 'Oyra', '', '', 0),
(153, 'FJ$', 4, 0, '0.5433', 59, 'FJD', 'Fijian Dollar', 'Fiji', 'Cent', '', '', 0),
(154, 'D', 4, 0, '0.0295', 60, 'GMD', 'Gambian Dalasi', 'Gambia', 'Butut', '', '', 0),
(155, 'ლ', 4, 0, '0.6091', 61, 'GEL', 'Georgian Lari', 'Georgia', 'Tetri', '', '', 0),
(156, 'GH₵', 4, 0, '0.5007', 62, 'GHS', 'Ghanaian Cedi', 'Ghana', 'Pesewa', '', '', 0),
(157, '£ or p', 4, 0, '1.5194', 63, 'GIP', 'Gibraltar Pound', 'Gibraltar', 'Penny', '', '', 0),
(158, 'Q', 4, 0, '0.1283', 64, 'GTQ', 'Guatemalan Quetzal', 'Guatemala', 'Centavo', '', '', 0),
(159, '£ or p', 4, 0, '87.6', 65, 'GBP', 'Guernsey Pound', 'Guernsey', 'Penny', '', '', 0),
(160, 'FG or Fr/GFr', 4, 0, '0.0001', 66, 'GNF', 'Guinean Franc', 'Guinea', 'Centime', '', '', 0),
(161, '$', 4, 0, '0.0049', 67, 'GYD', 'Guyanese Dollar', 'Guyana', 'Cent', '', '', 0),
(162, 'G', 4, 0, '0.0236', 68, 'HTG', 'Haitian Gourde', 'Haiti', 'Centime', '', '', 0),
(163, 'L', 4, 0, '0.0494', 69, 'HNL', 'Hondyran Lempira', 'Honduras', 'Centavo', '', '', 0),
(164, '£ or HK$', 4, 0, '0.1288', 70, 'HKD', 'Hong Kong Dollar', 'Hong Kong', 'Cent', '', '', 0),
(165, 'Ft', 4, 0, '0.0044', 71, 'HUF', 'Hungarian Forint', 'Hungary', 'Fillér', '', '', 0),
(166, 'kr, �?kr', 4, 0, '0.0081', 72, 'ISK', 'Icelandic Króna', 'Iceland', 'Eyrir', '', '', 0),
(167, '', 4, 0, '0.0177', 73, 'INR', 'Indian Rupee', 'India', 'Paisa', '', '', 0),
(168, 'Rp', 4, 0, '0.0001', 74, 'IDR', 'Indonesian Rupiah', 'Indonesia', 'Sen', '', '', 0),
(169, '﷼', 4, 0, '0.0001', 75, 'IRR', 'Iranian Rial', 'Iran', 'Toman or Dinar', '', '', 0),
(170, 'ع.د', 4, 0, '0.0009', 76, 'IQD', 'Iraqi Dinar', 'Iraq', 'Fils', '', '', 0),
(171, '₪', 4, 0, '0.2711', 77, 'ILS', 'Israeli New Sheqel', 'Israel', 'Agora', '', '', 0),
(172, '$', 4, 0, '0.0101', 78, 'JMD', 'Jamaican Dollar', 'Jamaica', 'Cent', '', '', 0),
(173, '¥', 4, 0, '0.0102', 7, 'JPY', 'Japanese Yen', 'Japan', 'Sen or Rin', '', '', 0),
(174, '£ or p', 4, 0, '87.6', 79, 'GBP', 'Jersey Pound', 'Jersey', 'Penny', '', '', 0),
(175, 'د.ا', 4, 0, '1.4104', 80, 'JOD', 'Jordanian Dinar', 'Jordan', 'Dirham', '', '', 0),
(176, '', 4, 0, '0.0066', 81, 'KZT', 'Kazakhstani tenge', 'Kazakhstan', 'tiyn (тиын),', '', '', 0),
(177, 'KSh', 4, 0, '0.0117', 82, 'KES', 'Kenyan Shilling', 'Kenya', 'Cent', '', '', 0),
(178, '$ or ¢', 4, 0, '87.6', 83, 'AUD', 'Kiribati Dollar', 'Kiribati', 'Cent', '', '', 0),
(179, 'د.ك', 4, 0, '3.4955', 84, 'KWD', 'Kuwaiti Dinar', 'Kuwait', 'Fils', '', '', 0),
(180, 'N/A', 4, 0, '0.0207', 85, 'KGS', 'Kyrgyzstani som', 'Kyrgyz Republic', 'Tyiyn', '', '', 0),
(181, '₭ or ₭N', 4, 0, '0.0001', 86, 'LAK', 'Lao Kip', 'Laos', 'Att', '', '', 0),
(182, 'Ls or s', 4, 0, '1.8505', 87, 'LVL', 'Latvian Lats', 'Lativa', 'Santims', '', '', 0),
(183, 'ل.ل', 4, 0, '0.0007', 88, 'LBP', 'Lebanese Pound', 'Lebanon', 'Piastre', '', '', 0),
(184, 'L or M', 4, 0, '0.0994', 89, 'LSL', 'Lesotho Loti', 'Lesotho', 'Sente', '', '', 0) ;
INSERT INTO `wp_wpl_units` ( `id`, `name`, `type`, `enabled`, `tosi`, `index`, `extra`, `extra2`, `extra3`, `extra4`, `seperator`, `d_seperator`, `after_before`) VALUES
(185, 'L$', 4, 0, '0.0133', 90, 'LRD', 'Liberian Dollar', 'Liberia', 'Cent', '', '', 0),
(186, 'LD and ل.د', 4, 0, '0.78', 91, 'LYD', 'Libyan Dinar', 'Libya', 'Dirham', '', '', 0),
(187, 'Lt', 4, 0, '0.376', 92, 'LTL', 'Lithuanian Litas', 'Lithuania', 'Centas', '', '', 0),
(188, 'MOP$', 4, 0, '0.1251', 93, 'MOP', 'Macanese Pataca', 'Macau', 'Avo', '', '', 0),
(189, 'ден', 4, 0, '0.0208', 94, 'MKD', 'Macedonian Denar', 'Republic of Macedonia', 'Deni', '', '', 0),
(190, 'N/A', 4, 0, '0.0005', 95, 'MGA', 'Malagasy Ariary', 'Madagascar', 'Iraimbilanja', '', '', 0),
(191, 'MK', 4, 0, '0.003', 96, 'MWK', 'Malawian Kwacha', 'Malawi', 'Tambala', '', '', 0),
(192, 'RM', 4, 0, '0.3227', 97, 'MYR', 'Malaysian Ringgit', 'Malaysia', 'Sen', '', '', 0),
(193, 'Rf, MRf, or .ރ', 4, 0, '0.0649', 98, 'MVR', 'Maldivian Rufiyaa', 'Maldives', 'Laari', '', '', 0),
(194, '£ or p', 4, 0, '87.6', 99, 'GBP', 'Manx Pound', 'Isle Of Man', 'Penny', '', '', 0),
(195, 'UM', 4, 0, '0.0034', 100, 'MRO', 'Mauritanian Ouguiya', 'Mauritania', 'Khoums', '', '', 0),
(196, 'Rs', 4, 0, '0.032', 101, 'MUR', 'Mauritian Rupee', 'Mauritius', 'Cent', '', '', 0),
(197, '$ or Mex$ / ¢', 4, 0, '0.078', 102, 'MXN', 'Mexican Peso', 'Mexico', 'Centavo', '', '', 0),
(198, 'L', 4, 0, '0.08', 103, 'MDL', 'Moldovan Leu', 'Moldova', 'Ban', '', '', 0),
(199, '₮', 4, 0, '0.0007', 104, 'MNT', 'Mongolian Tögrög', 'Mongolia', 'Möngö(мөнгө),', '', '', 0),
(200, 'د.م', 4, 0, '0.1168', 105, 'MAD', 'Moroccan Dirham', 'Morocco', 'Santim', '', '', 0),
(201, 'MTn', 4, 0, '0.0336', 106, 'MZN', 'Mozambican Metical', 'Mozambique', 'Centavo', '', '', 0),
(202, 'K', 4, 0, '0.0011', 107, 'MMK', 'Myanma Kyat', 'Myanmar/Burma', 'Pya', '', '', 0),
(203, 'N$', 4, 0, '0.0993', 108, 'NAD', 'Namibian Dollar', 'Namibia', 'Cent', '', '', 0),
(204, 'Rs', 4, 0, '0.0111', 109, 'NPR', 'Nepalese Rupee', 'Nepal', 'Paisa', '', '', 0),
(205, 'NAƒ, NAf, ƒ, or f', 4, 0, '0.5587', 110, 'ANG', 'Netherlands Antillean Guilder', 'Netherlands Antilles', 'Cent', '', '', 0),
(206, '$ or NT$', 4, 0, '0.0333', 111, 'TWD', 'New Taiwan Dollar', 'New Taiwan', 'Cent', '', '', 0),
(207, '$ or c', 4, 0, '0.7947', 112, 'NZD', 'New Zealand Dollar', 'New Zealand', 'Cent', '', '', 0),
(208, 'C$', 4, 0, '0.0402', 113, 'NIO', 'Nicaraguan Córdoba', 'Nicaragua', 'Centavo', '', '', 0),
(209, '₦', 4, 0, '0.0063', 114, 'NGN', 'Nigerian Naira', 'Nigeria', 'Kobo', '', '', 0),
(210, '₩', 4, 0, '0.0011', 115, 'KPW', 'North Korean Won', 'North Korea', 'Ch�?n', '', '', 0),
(211, 'kr', 4, 0, '0.1703', 116, 'NOK', 'Norweigian Krone', 'Norway', 'øre', '', '', 0),
(212, 'ر.ع.', 4, 0, '2.5998', 117, 'OMR', 'Omani Rial', 'Oman', 'Baisa', '', '', 0),
(213, 'Rs', 4, 0, '0.0102', 118, 'PKR', 'Pakistani Rupee', 'Pakistan', 'Paisa', '', '', 0),
(214, 'B/. or B', 4, 0, '1', 119, 'PAB', 'Panamanian Blaboa', 'Panama', 'Centesimo', '', '', 0),
(215, 'K', 4, 0, '0.4401', 120, 'PGK', 'Papua New Guinea Kina', 'Papua New Guinea', 'Toea', '', '', 0),
(216, '₲', 4, 0, '0.0002', 121, 'PYG', 'Paraguayan Guarani', 'Paraguay', 'Centimo', '', '', 0),
(217, 'S/.', 4, 0, '0.3655', 122, 'PEN', 'Peruvian Nuevo Sol', 'Peru', 'Centimo', '', '', 0),
(218, '', 4, 0, '0.0237', 123, 'PHP', 'Philippine Peso', 'Philippines', 'Sentimo', '', '', 0),
(219, 'zł or gr', 4, 0, '0.304', 124, 'PLN', 'Polish złoty', 'Poland', 'Grosz', '', '', 0),
(220, '£', 4, 1, '1.6412', 3, 'GBP', 'Pound Sterling', 'England', 'Penny', ',', '', 0),
(221, 'QR or ر.ق', 4, 0, '0.2746', 125, 'QAR', 'Qatari Riyal', 'Qatar', 'Dirham', '', '', 0),
(222, '¥', 4, 0, '0.1629', 126, 'CNY', 'Renminbi', 'PRC', 'jiǎo (角', '', '', 0),
(223, 'L', 4, 0, '0.2954', 127, 'RON', 'Romanian Leu', 'Romania', 'Bani', '', '', 0),
(224, 'руб. / Р. / р. or', 4, 0, '0.0313', 128, 'RUB', 'Russian Ruble', 'Russia', 'Kopek', '', '', 0),
(225, 'RF', 4, 0, '0.0016', 129, 'RWF', 'Rwandan Franc', 'Rwanda', 'Centime', '', '', 0),
(226, '£', 4, 0, '1.5194', 130, 'SHP', 'Saint Helena Pound', 'Saint Helena', 'Penny', '', '', 0),
(227, '₡', 4, 0, '0.1143', 131, 'SVC', 'Salvadorian Colón', 'El Salvador', 'Centavo', '', '', 0),
(228, 'WS$', 4, 0, '0.4364', 132, 'WST', 'Samoan Tala', 'Samoa', 'Sene', '', '', 0),
(229, 'ر.س (Arabic),, SR (Latin),', 4, 0, '0.2666', 133, 'SAR', 'Saudi Riyal', 'Saudi Arabia', 'Halala', '', '', 0),
(230, 'РСД and RSD', 4, 0, '0.0115', 134, 'RSD', 'Serbian Dinar', 'Serbia', 'Para', '', '', 0),
(231, 'SR or SRe', 4, 0, '0.0847', 135, 'SCR', 'Seychellois Rupee', 'Seychelles', 'Cent', '', '', 0),
(232, 'Le', 4, 0, '0.0002', 136, 'SLL', 'Sierra Leonean Leone', 'Sierra Leone', 'Cent', '', '', 0),
(233, 'S$', 4, 0, '0.7908', 137, 'SGD', 'Singapore Dollar', 'Singapore', 'Cent', '', '', 0),
(234, 'SI$', 4, 0, '0.1391', 138, 'SBD', 'Solomon Islands Dollar', 'Solomon Islands', 'Cent', '', '', 0),
(235, 'So. Sh.', 4, 0, '0.0007', 139, 'SOS', 'Somali Shilling', 'Somalia', 'Cent', '', '', 0),
(236, 'Sl. Sh.', 4, 0, '0.0007', 140, 'SOS', 'Somaliland Shilling', 'Somaliland', 'Cent', '', '', 0),
(237, 'R or c', 4, 0, '0.0993', 141, 'ZAR', 'South African Rand', 'South Africa', 'Cent', '', '', 0),
(238, '₩', 4, 0, '0.0009', 142, 'KRW', 'South Korean Won', 'South Korea', 'Jeon', '', '', 0),
(239, 'Rs or SLRss/SLRs', 4, 0, '0.0079', 143, 'LKR', 'Sri Lankan Rupee', 'Sri Lanka', 'Cents', '', '', 0),
(240, '£', 4, 0, '0.2272', 144, 'SDG', 'Sudanese Pound', 'Sudan', 'Qirsh', '', '', 0),
(241, '$', 4, 0, '0.3042', 145, 'SRD', 'Surinamese Dollar', 'Suriname', 'Cent', '', '', 0),
(242, 'L or E', 4, 0, '0.0993', 146, 'SZL', 'Swazi Lilangeni', 'Swaziland', 'Cent', '', '', 0),
(243, 'kr, :-', 4, 0, '0.1521', 8, 'SEK', 'Swedish Krona', 'Sweden', 'öre', '', '', 0),
(244, 'CHF, Fr., SFr. (old),', 4, 0, '1.1024', 4, 'CHF', 'Swiss Franc', 'Switzerland', 'Centime', ',', '', 0),
(245, '£ or ل.س', 4, 0, '0.0103', 147, 'SYP', 'Syrian Pound', 'Syria', 'Piastre', '', '', 0),
(246, 'Db', 4, 0, '0.0001', 148, 'STD', 'São Tomé and Príncipe dobra', 'São Tomé and Príncipe', 'cêntimo', '', '', 0),
(247, 'SM', 4, 0, '0.2103', 149, 'TJS', 'Tajikistani Somoni', 'Tajikistan', 'Dirham', '', '', 0),
(248, 'x/y', 4, 0, '0.0006', 150, 'TZS', 'Tanzanian Shilling', 'Tanzania', 'Senti', '', '', 0),
(249, '฿', 4, 0, '0.0329', 151, 'THB', 'Thai Baht', 'Thailand', 'Satang', '', '', 0),
(250, 'T$ or PT - ¢', 4, 0, '0.5743', 152, 'TOP', 'Tongan Pa\'anga', 'Tonga', 'Seniti', '', '', 0),
(251, 'p.', 4, 0, '87.6', 153, 'PRB', 'Transnistrian Ruble', 'Transnistria', 'Kopecks', '', '', 0),
(252, '$', 4, 0, '0.1558', 154, 'TTD', 'Trinidad and Tobago Dollar', 'Trinidad and Tobago', 'Cent', '', '', 0),
(253, 'د.ت (Arabic), or DT (Latin),', 4, 0, '0.6085', 155, 'TND', 'Tunisian Dollar', 'Tunisia', 'Milim', '', '', 0),
(254, 'TL', 4, 0, '0.5333', 156, 'TRY', 'Turkish Lira', 'Turkey', 'kuruş', '', '', 0),
(255, 'm', 4, 0, '0.3506', 157, 'TMT', 'Turkmenistani manat', 'Turkmenistan', 'tennesi (tenge),', '', '', 0),
(256, '$ or ¢', 4, 0, '0', 158, 'TVD', 'Tuvaluan Dollar', 'Tivalu', 'Cent', '', '', 0),
(257, 'USh', 4, 0, '0.0004', 159, 'UGX', 'Ugandan Shilling', 'Uganda', 'Cent', '', '', 0),
(258, '₴', 4, 0, '0.1226', 160, 'UAH', 'Ukrainian hryvnia', 'Ukraine', 'kopiyka (копійка),', '', '', 0),
(259, 'د.إ', 4, 0, '0.2722', 161, 'AED', 'United Arab Emirates dirham', 'United Arab Emirates', 'fils', '', '', 0),
(260, '$', 4, 1, '1', 1, 'USD', 'United States dollar', 'United-States', 'Dime/ Cent /Mill', ',', '', 0),
(261, '$', 4, 0, '0.0496', 162, 'UYU', 'Uruguayan peso', 'Uruguay', 'centésimo', '', '', 0),
(262, 'N/A', 4, 0, '0.0005', 163, 'UZS', 'Uzbekistani som', 'Uzbekistan', 'tiyin', '', '', 0),
(263, 'Vt', 4, 0, '0.0106', 164, 'VUV', 'Vanuatu vatu', 'Vanuatu', 'none', '', '', 0),
(264, 'Bs. F', 4, 0, '0.1589', 165, 'VEF', 'Venezuelan bolívar', 'Venezuela', 'céntimo', '', '', 0),
(265, '₫', 4, 0, '0', 166, 'VND', 'Vietnamese đồng', 'Vietnam', 'hào or xu', '', '', 0),
(266, 'CFA or c', 4, 0, '0.002', 167, 'XOF', 'West African CFA franc', 'West Africa', 'centime', '', '', 0),
(267, '﷼', 4, 0, '0.0047', 168, 'YER', 'Yemeni Rial', 'Yemen', 'Fils', '', '', 0),
(268, 'ZK', 4, 0, '0.0002', 169, 'ZMK', 'Zambian Kwacha', 'Zambia', 'Ngwee', '', '', 0),
(269, '$', 4, 0, '0.0031', 170, 'ZWL', 'Zimbabwean Dollar', 'Zimbabwe', 'Cent', '', '', 0),
(270, '¥', 4, 0, '0.16', 999, 'CNY', 'Chinese Yuan Renminbi', 'china', 'Fen', NULL, NULL, 0) ;

#
# End of data contents of table `wp_wpl_units`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_user_group_types`
#

DROP TABLE IF EXISTS `wp_wpl_user_group_types`;


#
# Table structure of table `wp_wpl_user_group_types`
#

CREATE TABLE `wp_wpl_user_group_types` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `default_membership_id` int(10) NOT NULL DEFAULT '-1',
  `description` text CHARACTER SET utf8,
  `editable` tinyint(4) unsigned NOT NULL DEFAULT '1',
  `deletable` tinyint(4) unsigned NOT NULL DEFAULT '1',
  `index` float(5,2) NOT NULL DEFAULT '99.00',
  `params` text CHARACTER SET utf8,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_user_group_types`
#
INSERT INTO `wp_wpl_user_group_types` ( `id`, `name`, `default_membership_id`, `description`, `editable`, `deletable`, `index`, `params`, `enabled`) VALUES
(1, 'Agents', -1, NULL, 1, 0, '1.00', NULL, 1),
(2, 'Owners', -1, NULL, 1, 0, '2.00', NULL, 1),
(3, 'Guests', -1, NULL, 1, 0, '0.50', NULL, 1) ;

#
# End of data contents of table `wp_wpl_user_group_types`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpl_users`
#

DROP TABLE IF EXISTS `wp_wpl_users`;


#
# Table structure of table `wp_wpl_users`
#

CREATE TABLE `wp_wpl_users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `membership_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `membership_id` int(10) DEFAULT NULL,
  `membership_type` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `index` float(5,2) NOT NULL DEFAULT '99.00',
  `access_propertywizard` tinyint(4) NOT NULL DEFAULT '0',
  `access_propertyshow` tinyint(4) NOT NULL DEFAULT '1',
  `access_propertylisting` tinyint(4) NOT NULL DEFAULT '1',
  `access_profilewizard` tinyint(4) NOT NULL DEFAULT '1',
  `access_confirm` tinyint(4) NOT NULL DEFAULT '0',
  `access_propertymanager` tinyint(4) NOT NULL DEFAULT '0',
  `access_delete` tinyint(4) NOT NULL DEFAULT '0',
  `access_public_profile` tinyint(4) NOT NULL DEFAULT '1',
  `access_change_user` tinyint(4) NOT NULL DEFAULT '0',
  `access_receive_notifications` tinyint(4) NOT NULL DEFAULT '1',
  `first_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `about` text CHARACTER SET utf8,
  `company_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `company_address` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `website` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `main_email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `secondary_email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `sex` tinyint(4) DEFAULT NULL,
  `tel` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `fax` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `mobile` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `languages` text CHARACTER SET utf8,
  `location1_id` int(11) NOT NULL DEFAULT '0',
  `location2_id` int(11) NOT NULL DEFAULT '0',
  `location3_id` int(11) NOT NULL DEFAULT '0',
  `location4_id` int(11) NOT NULL DEFAULT '0',
  `location5_id` int(11) NOT NULL DEFAULT '0',
  `location6_id` int(11) NOT NULL DEFAULT '0',
  `location7_id` int(11) NOT NULL DEFAULT '0',
  `location1_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `location2_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `location3_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `location4_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `location5_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `location6_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `location7_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `show_address` tinyint(4) NOT NULL DEFAULT '1',
  `zip_id` int(11) NOT NULL DEFAULT '0',
  `zip_name` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `maccess_num_prop` int(10) DEFAULT NULL,
  `maccess_num_feat` int(10) DEFAULT NULL,
  `maccess_num_hot` int(10) DEFAULT NULL,
  `maccess_num_pic` int(10) DEFAULT NULL,
  `maccess_period` int(10) DEFAULT NULL,
  `maccess_price` double DEFAULT NULL,
  `maccess_price_unit` int(10) DEFAULT NULL,
  `maccess_lrestrict` int(4) DEFAULT NULL,
  `maccess_listings` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `maccess_ptrestrict` int(4) DEFAULT NULL,
  `maccess_property_types` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `maccess_renewable` int(4) DEFAULT NULL,
  `maccess_renewal_price` double DEFAULT NULL,
  `maccess_renewal_price_unit` int(11) DEFAULT NULL,
  `maccess_upgradable` int(4) DEFAULT NULL,
  `maccess_upgradable_to` text CHARACTER SET utf8,
  `maccess_short_description` text CHARACTER SET utf8,
  `maccess_long_description` text CHARACTER SET utf8,
  `maccess_wpl_color` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `expired` tinyint(4) NOT NULL DEFAULT '0',
  `expiry_date` datetime DEFAULT NULL,
  `textsearch` text CHARACTER SET utf8,
  `location_text` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `rendered` text CHARACTER SET utf8,
  `last_modified_time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `profile_picture` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `company_logo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `maccess_lrestrict_pshow` int(4) DEFAULT '0',
  `maccess_ptrestrict_pshow` int(4) DEFAULT '0',
  `maccess_listings_pshow` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `maccess_property_types_pshow` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `maccess_lrestrict_plisting` int(4) DEFAULT '0',
  `maccess_ptrestrict_plisting` int(4) DEFAULT '0',
  `maccess_listings_plisting` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `maccess_property_types_plisting` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `membership_id` (`membership_id`),
  KEY `expired` (`expired`,`expiry_date`),
  KEY `membership_id_2` (`membership_id`),
  KEY `expired_2` (`expired`,`expiry_date`),
  KEY `membership_id_3` (`membership_id`),
  KEY `expired_3` (`expired`,`expiry_date`),
  KEY `membership_id_4` (`membership_id`),
  KEY `expired_4` (`expired`,`expiry_date`),
  FULLTEXT KEY `textsearch` (`textsearch`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_wpl_users`
#
INSERT INTO `wp_wpl_users` ( `id`, `membership_name`, `membership_id`, `membership_type`, `index`, `access_propertywizard`, `access_propertyshow`, `access_propertylisting`, `access_profilewizard`, `access_confirm`, `access_propertymanager`, `access_delete`, `access_public_profile`, `access_change_user`, `access_receive_notifications`, `first_name`, `last_name`, `about`, `company_name`, `company_address`, `website`, `main_email`, `secondary_email`, `sex`, `tel`, `fax`, `mobile`, `languages`, `location1_id`, `location2_id`, `location3_id`, `location4_id`, `location5_id`, `location6_id`, `location7_id`, `location1_name`, `location2_name`, `location3_name`, `location4_name`, `location5_name`, `location6_name`, `location7_name`, `show_address`, `zip_id`, `zip_name`, `maccess_num_prop`, `maccess_num_feat`, `maccess_num_hot`, `maccess_num_pic`, `maccess_period`, `maccess_price`, `maccess_price_unit`, `maccess_lrestrict`, `maccess_listings`, `maccess_ptrestrict`, `maccess_property_types`, `maccess_renewable`, `maccess_renewal_price`, `maccess_renewal_price_unit`, `maccess_upgradable`, `maccess_upgradable_to`, `maccess_short_description`, `maccess_long_description`, `maccess_wpl_color`, `expired`, `expiry_date`, `textsearch`, `location_text`, `rendered`, `last_modified_time_stamp`, `profile_picture`, `company_logo`, `maccess_lrestrict_pshow`, `maccess_ptrestrict_pshow`, `maccess_listings_pshow`, `maccess_property_types_pshow`, `maccess_lrestrict_plisting`, `maccess_ptrestrict_plisting`, `maccess_listings_plisting`, `maccess_property_types_plisting`) VALUES
(-2, 'Guest', 0, '3', '99.00', 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, '', '', NULL, '', '', '', '', '', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', 1, 0, '', 0, 0, 0, 0, 0, '0', 0, 0, '', 0, '', 0, '0', 0, 0, '', NULL, NULL, NULL, 0, NULL, '', NULL, '', '2018-09-20 22:04:12', '', '', 0, 0, NULL, NULL, 0, 0, NULL, NULL),
(-1, 'Default', -1, '1', '99.00', 1, 1, 1, 1, 0, 1, 1, 0, 0, 1, '', '', NULL, '', '', '', '', '', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', 1, 0, '', -1, -1, -1, -1, 0, '0', 0, 0, '', 0, '', 0, '0', 0, 0, '', NULL, NULL, NULL, 0, NULL, '', NULL, '', '2018-09-20 22:04:05', '', '', 0, 0, NULL, NULL, 0, 0, NULL, NULL),
(0, 'Guest', -2, '3', '99.00', 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, '', '', NULL, '', '', '', '', '', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', 1, 0, '', 0, 0, 0, 0, 0, '0', 0, 0, '', 0, '', 0, '0', 0, 0, '', NULL, NULL, NULL, 0, NULL, '', NULL, '', '2018-09-20 22:04:12', '', '', 0, 0, NULL, NULL, 0, 0, NULL, NULL),
(1, 'Default', -1, '1', '99.00', 1, 1, 1, 1, 0, 1, 1, 0, 0, 1, '', '', NULL, '', '', '', 'brooke.neace@gmail.com', '', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', 1, 0, '', -1, -1, -1, -1, 0, '0', 0, 0, '', 0, '', 0, '0', 0, 0, '', '', '', NULL, 0, '0000-00-00 00:00:00', 'Email brooke.neace@gmail.com', '', '{"rendered":{"914":{"field_id":"914","type":"text","name":"Email","value":"brooke.neace@gmail.com"},"911":{"field_id":"911","type":"locations","name":"Location","value":"","settings":[]}},"materials":{"main_email":{"field_id":"914","type":"text","name":"Email","value":"brooke.neace@gmail.com"},"locations":{"field_id":"911","type":"locations","name":"Location","value":"","settings":[]}},"location_text":""}', '2018-09-20 22:04:17', '', '', 0, 0, NULL, NULL, 0, 0, NULL, NULL) ;

#
# End of data contents of table `wp_wpl_users`
# --------------------------------------------------------

#
# Add constraints back in and apply any alter data queries.
#

