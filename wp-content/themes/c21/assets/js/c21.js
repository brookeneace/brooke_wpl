jQuery(document).ready(function ($) {

	console.log('ready');
	
	  $('.home-backgrounds').slick({
	       arrows: false,
		   fade: true,
		   dots: false,
		   autoplay:true
      });
	
	$('ul.reviews').each(function() {
	    var $list = $(this);
	    $list.after('<div class="button-wrapper"><button class="more_less">More</button></div>')
	   $list.find('li:gt(1)').hide();
	});


	$('.more_less').click(function() {
	    var $btn = $(this)
	    $btn.parent().prev().find('li:gt(1)').slideToggle();    
	    $btn.text($btn.text() == 'More' ? 'Less' : 'More');    
	});
	
	$(function(){
      // bind change event to select
      $('#languages').on('change', function () {
          var url = $(this).val(); // get selected value
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
      });
    });
    
    $(function(){
      // bind change event to select
      $('#lastname').on('change', function () {
          var url = $(this).val(); // get selected value
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
      });
    });
    
    $('.sale-rent span').on('click', function(){
	    $(this).addClass('active').siblings().removeClass('active'); 
	    $('.home-form').removeClass('show-form'); 
		var $form = $(this).attr('data-form');
		console.log($form);
		$('.home-form.'+$form+'').addClass('show-form'); 
    });
    
   
$('#sf2_textsearch_textsearch, #sf3_textsearch_textsearch, #sf5_textsearch_textsearch').attr('placeholder', 'Enter a neighborhood, city, address or Zip code');

});