<?php
/**
 * Template Name: All Agents
 */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			
			

			
			
			<div id="agent-wrap">
				<?php
				while ( have_posts() ) : the_post();
	
					get_template_part( 'template-parts/page/content', 'page-allagents' );
	

				endwhile; // End of the loop.
				?>
			</div>
			
			<div id="agents-home" class="agents-interior">
				
				
			
			<div class="agents-wrap">
				<div class="wrap">
					<ul class="agents">
						<?php
								
								// Build the meta query based on the selected options

						        // The Arguments
						        $args = array(
						            'post_type' => 'agents', 
						            'showposts' => 100,
                                    'orderby'=>'title','order'=>'ASC'								 			           
						        );  
						
						        // The Query
						        $the_query = new WP_Query( $args ); ?>
						
						        <?php
						
						        // If we have the posts...
						        if ( $the_query->have_posts() ) : ?>
						
						        <!-- Start the loop the loop --> 
						            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						
						                <li><a href="<?php the_permalink(); ?>">
							                <div class="agent-pic">
								                <?php  if(has_post_thumbnail()){
									                  the_post_thumbnail();
									             } ?>
								<div class="agent-hover"><h4>View Profile</h4></div>
								</div>
								<h3><?php the_title(); ?></h3></a>
								<?php if( get_field('number_of_reviews') ): ?><p style="margin-bottom: 0;"><?php the_field('number_of_reviews'); ?> REVIEWS</p><?php endif; ?>
								<p><?php the_field('primary_office_name'); ?></p>
								<a href="tel:<?php the_field('phone'); ?>"><i class="fas fa-phone"></i></a>
								<a href="mailto:<?php the_field('email'); ?>"><i class="fas fa-envelope"></i></a>
								<?php if( get_field('website') ): ?>
									<a href="<?php the_field('website'); ?>"><i class="fas fa-laptop"></i></a>
								<?php endif; ?>
								<p><span><?php echo get_the_term_list( $post->ID, 'languages', 'Additional Languages Spoken: ', ', ', '' ); ?></span></p>
								
								</li>
						
						            <?php endwhile; endif; // end of the loop. ?>
						
						        <?php wp_reset_postdata(); ?>						
					</ul>
					
				

					
					
				</div>
			</div>
			
			
		</div>
			
			
			

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<?php get_footer();
