<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');
?>
<div>
	<div class="wpl_rev_container">
        <h4>MLS add-on v3.7.0 (<span class="wpl_log_date">01/30/2019</span> <span class="wpl_log_revision">revision 4438</span>)</h4>
        -- Added maximum search results option.<br>
        -- Applied some speed improvements.

        <h4>MLS add-on v3.6.0 (<span class="wpl_log_date">10/17/2018</span> <span class="wpl_log_revision">revision 4326</span>)</h4>
        -- Added MLS timezone support for hourly updates.<br>
        -- Added CREA DestinationID support.<br>
        -- Fixed some issues.

		<h4>MLS add-on v3.5.0 (<span class="wpl_log_date">08/06/2018</span> <span class="wpl_log_revision">revision 4238</span>)</h4>
        -- Updated PHRETS library to 2.6.1.<br>
        -- Improved PHP7.2 Compatibility.<br>
        -- Performance Improvements.<br>
        -- Fixed some issues.

        <h4>MLS add-on v3.4.5 (<span class="wpl_log_date">07/25/2018</span> <span class="wpl_log_revision">revision 4214</span>)</h4>
        -- Improved mapping page.<br>
        -- Fixed some issues.

        <h4>MLS add-on v3.4.0 (<span class="wpl_log_date">04/25/2018</span> <span class="wpl_log_revision">revision 4089</span>)</h4>
        -- Added MLS Price Estimator Widget.

        <h4>MLS add-on v3.2.0 (<span class="wpl_log_date">02/05/2018</span> <span class="wpl_log_revision">revision 3951</span>)</h4>
        -- Added basic authentication option to the MLS options.<br>
        -- Improved cronjob funcyions.<br>
        -- Fixed some incompatibility with PHP 7.<br>
        -- Fixed some issues.

        <h4>MLS add-on v3.1.0 (<span class="wpl_log_date">08/06/2017</span> <span class="wpl_log_revision">revision 3503</span>)</h4>
        -- Updated PHRETS library to latest version.<br>
        -- Fixed some issues.
        
        <h4>MLS add-on v3.0.0 (<span class="wpl_log_date">07/10/2017</span> <span class="wpl_log_revision">revision 3421</span>)</h4>
        -- Added ListTrack Integration.<br>
        -- Added MLS Disclaimer widget.<br>
        -- Added "White Space Trim" feature for images that come from MLS.<br>
        -- Optimized MLS Import system to import new listings faster.<br>
        -- Optimized MLS Purge system to purge expired listings faster.<br>
        -- Updated PHRETS library to latest version.<br>
        -- Fixed some issues.
        
        <h4>MLS add-on v2.7.0 (<span class="wpl_log_date">02/01/2017</span> <span class="wpl_log_revision">revision 2950</span>)</h4>
        -- Added Get GEO cordinates option to get the latitude and longitude of each property in the MLS addon.<br>
        -- Added "Value to Value" mapping option.<br>
        -- Added the purge listing functionality into a separate cronjob.<br>
        -- Added a new version of PHRETS library.
        
        <h4>MLS add-on v2.5.0 (<span class="wpl_log_date">07/13/2016</span> <span class="wpl_log_revision">revision 2455</span>)</h4>
        -- Developed compatibility with Neighborhood and Complex addons.<br>
        -- Added an option for removing thumbnails after update.
        
        <h4>MLS add-on v1.0.0</h4>
        -- MLS add-on v1.0.0 is released.
    </div>
</div>