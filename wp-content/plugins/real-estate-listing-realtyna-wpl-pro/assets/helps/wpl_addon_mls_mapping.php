<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');

/** Define Tabs **/
$tabs = array();
$tabs['tabs'] = array();

$content  = '<h3>'.__('MLS Add-on / Mapping', 'real-estate-listing-realtyna-wpl').'</h3><p>'.__("You should map MLS/RETS fields to WPL fields here. This step is very important. Please note the following items during mapping: ", 'real-estate-listing-realtyna-wpl').'</p>';
$content .= '<ul>';
$content .= '<li>'.__('Map the source fields to existing WPL fields.', 'real-estate-listing-realtyna-wpl').'</li>';
$content .= '<li>'.__('Avoid creating new fields using "Auto Create" unless there really is no related field in WPL for those fields.', 'real-estate-listing-realtyna-wpl').'</li>';
$content .= '<li>'.__("It's better not to map fields with no data or sample data. It can make your website messy with unnecessary information.", 'real-estate-listing-realtyna-wpl').'</li>';
$content .= '<li>'.__("WPL's most important fields are: listing type, property type, price, price unit, price type (for rental properties), listing ID, bedrooms, bathrooms, rooms, view, property description, built up area, lot area, images, location information, property title (if exists). Map these fields with carefully.", 'real-estate-listing-realtyna-wpl').'</li>';
$content .= '<li>'.__("Mapping some fields may need to be manually tailored based on customizations you may have on your WPL, other Add-ons.", 'real-estate-listing-realtyna-wpl').'</li>';
$content .= '<li>'.__("It's highly recommended to don't map the fields that you don't need.", 'real-estate-listing-realtyna-wpl').'</li>';
$content .= '</ul>';

$tabs['tabs'][] = array('id'=>'wpl_contextual_help_tab_int', 'content'=>$content, 'title'=>__('Introduction', 'real-estate-listing-realtyna-wpl'));

$articles  = '';
$articles .= '<li><a href="https://support.realtyna.com/index.php?/Default/Knowledgebase/Article/View/559/" target="_blank">'.__("Where can I find help with the WPL MLS Add-on?", 'real-estate-listing-realtyna-wpl').'</a></li>';
$articles .= '<li><a href="https://support.realtyna.com/index.php?/Default/Knowledgebase/Article/View/614/" target="_blank">'.__("How do I include MLS/Importer Add-on fields into the search widget??", 'real-estate-listing-realtyna-wpl').'</a></li>';

$content = '<h3>'.__('Related KB Articles', 'real-estate-listing-realtyna-wpl').'</h3><p>'.__('Here you will find KB articles with information related to this page. You can come back to this section to find an answer to any questions that may come up.', 'real-estate-listing-realtyna-wpl').'</p><p><ul>'.$articles.'</ul></p>';
$tabs['tabs'][] = array('id'=>'wpl_contextual_help_tab_kb', 'content'=>$content, 'title'=>__('KB Articles', 'real-estate-listing-realtyna-wpl'));

// Hide Tour button
$tabs['sidebar'] = array('content'=>'');

return $tabs;