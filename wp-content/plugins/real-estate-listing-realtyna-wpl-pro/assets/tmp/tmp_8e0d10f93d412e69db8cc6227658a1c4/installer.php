<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');

class wpl_installer
{
	var $message;
	var $method = 'update';
	var $addon_id = 3;
	var $addon_name = 'PRO Addon';
	var $required_core_version = '4.3.2'; // Previous Package Version
	var $path;
	
	/** for update packages **/
	var $version_from = '4.3.2';
	var $version_to = '4.4.0';
	
	/**
	* method to run the script
	*
	* @return void
	*/
	function run()
	{
		/** run script preflight **/
		if(!$this->preflight()) return false;
		
		$method = $this->method ? $this->method : 'install';
		if($method == 'install') $this->install();
		elseif($method == 'update') $this->update();
		
		/** run script postflight **/
		$this->postflight();
		
		$this->message = $this->addon_name.' successfully '.($this->method == 'install' ? 'installed.' : 'updated.');
		return true;
	}
	
	/**
	* method to run before an install method
	*
	* @return boolean
	*/
	function preflight()
	{
		/** check core min version **/
		if($this->required_core_version)
		{
			$core_version = wpl_global::wpl_version();
			if(version_compare($core_version, $this->required_core_version, '<'))
			{
				$this->error = 'Error: WPL version is low! please update your WPL to '.$this->required_core_version.' version, then try to '.$this->method.' this addon.';
				return false;
			}
		}
		
		if($this->method == 'install')
		{
			/** check installation of addon **/
			$result = wpl_db::select("SELECT id FROM `#__wpl_addons` WHERE `id`='".$this->addon_id."'", 'loadObject');
			
			if($result)
			{
				$this->error = 'Error: This addon is already installed.';
				return false;
			}
		}
		
		if($this->method == 'update')
		{
			/** check installation of addon **/
			$addon = wpl_db::select("SELECT * FROM `#__wpl_addons` WHERE `id`='".$this->addon_id."'", 'loadObject');
			
			if(version_compare($addon->version, $this->version_from, '<'))
			{
				$this->error = 'Error: Addon version is low! please update your Addon to version'.$this->version_from.' first and then try again.';
				return false;
			}
			
			if(version_compare($addon->version, $this->version_to, '>='))
			{
				$this->error = 'Error: Your Addon is updated! There is no need to update it again!';
				return false;
			}
		}
			
		return true;
	}
	
	/**
	* method to install the addon
	*
	* @return void
	*/
	function install()
	{
		/** run queries **/
		if(wpl_file::exists($this->path.'temporary_dir'.DS.'queries.sql')) wpl_global::do_file_queries($this->path.'temporary_dir'.DS.'queries.sql', true);
		
		/** copy files **/
        if(wpl_folder::exists($this->path.'temporary_dir'.DS.'files')) $res = wpl_folder::copy($this->path.'temporary_dir'.DS.'files', WPL_ABSPATH, '', true);
	}
	
	/**
	* method to update the addon
	*
	* @return void
	*/
	function update()
	{
		/** run queries **/
		if(wpl_file::exists($this->path.'temporary_dir'.DS.'queries.sql')) wpl_global::do_file_queries($this->path.'temporary_dir'.DS.'queries.sql', true);
        
		/** copy files **/
        if(wpl_folder::exists($this->path.'temporary_dir'.DS.'files')) $res = wpl_folder::copy($this->path.'temporary_dir'.DS.'files', WPL_ABSPATH, '', true);

        // Remove Zipcode Option if not enabled
        $assign_listings_by_zipcode = wpl_global::get_setting('assign_agents_by_zipcode', 1);
        if(!$assign_listings_by_zipcode)
        {
        	wpl_db::q("DELETE FROM `#__wpl_settings` WHERE `id`='309'");
        	wpl_db::q("DELETE FROM `#__wpl_events` WHERE `id`='47'");
        }
	}
	
	/**
	* method to run after an install method
	*
	* @return void
	*/
	function postflight()
	{
		/** delete install tmp directory **/
		wpl_folder::delete($this->path);
		
		if($this->method == 'update')
		{
			/** update version of addon **/
			wpl_db::q("UPDATE `#__wpl_addons` SET `version`='".$this->version_to."' WHERE `id`='".$this->addon_id."'");
		
			/** clear update message of addon **/
			wpl_db::q("UPDATE `#__wpl_addons` SET `message`='' WHERE `id`='".$this->addon_id."'");
		}
	}
}