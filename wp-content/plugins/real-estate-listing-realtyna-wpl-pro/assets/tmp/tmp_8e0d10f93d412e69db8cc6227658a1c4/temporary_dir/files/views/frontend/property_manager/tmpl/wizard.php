<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');

$this->_wpl_import($this->tpl_path.'.scripts.wizard.js', true, true);
$this->_wpl_import($this->tpl_path.'.scripts.wizard.css', true, true);
if(wpl_global::check_addon('membership')) $dashboard = wpl_addon_membership::URL('dashboard');
?>
<div class="wpl_view_container">
    <?php if(wpl_global::check_addon('membership')): ?>
        <div class="wpl-pwizard-back-wrapp">
    	    <input class="wpl-button button-1 wpl-pwizard-back" type="button" value="<?php echo __('Return to Dashboard', 'real-estate-listing-realtyna-wpl'); ?>" onclick="window.location.href = '<?php echo $dashboard; ?>'" />
        </div>
    <?php endif; ?>
    <?php /** loading Property Wizard page **/ echo wpl_html::load_add_edit_listing($this->instance); ?>
</div>