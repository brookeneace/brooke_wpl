ALTER TABLE `#__wpl_sort_options` ADD `default_order` VARCHAR(5) NOT NULL DEFAULT 'DESC' AFTER `name`;
UPDATE `#__wpl_sort_options` SET `default_order`='ASC' WHERE `field_name` IN ('p.first_name','p.location1_name','p.location2_name','ptype_adv','ltype_adv');

UPDATE `#__wpl_addons` SET `version`='4.4.0' WHERE `id`='3';