<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');

/**
 * Favorites Widget Shortcode for Divi Builder
 * @author Howard <howard@realtyna.com>
 * @package WPL PRO
 */
class wpl_page_builders_divi_widget_favorites extends ET_Builder_Module
{
    public $vb_support = 'on';
    
    public function init()
    {
        $this->name = __('WPL Favorites Widget', 'real-estate-listing-realtyna-wpl');
        $this->slug = 'et_pb_wpl_widget_favorites';
		$this->fields_defaults = array();

        // Global WPL Settings
		$this->settings = wpl_global::get_settings();
	}

    public function get_fields()
    {
        $favorites = new wpl_favorites_widget();

        // Module Fields
        $fields = array();

        $fields['title'] = array(
            'label'           => esc_html__('Title', 'real-estate-listing-realtyna-wpl'),
            'type'            => 'text',
            'option_category' => 'basic_option',
            'description'     => esc_html__('The widget title', 'real-estate-listing-realtyna-wpl'),
        );

        $widget_layouts = $favorites->get_layouts('favorites');

        $widget_layouts_options = array();
        foreach($widget_layouts as $widget_layout) $widget_layouts_options[str_replace('.php', '', $widget_layout)] = esc_html__(ucfirst(str_replace('.php', '', $widget_layout)), 'real-estate-listing-realtyna-wpl');

        $fields['tpl'] = array(
            'label'           => esc_html__('Layout', 'real-estate-listing-realtyna-wpl'),
            'type'            => 'select',
            'option_category' => 'basic_option',
            'options'         => $widget_layouts_options,
        );

        $pages = wpl_global::get_wp_pages();

        $pages_options = array();
        $pages_options[''] = '-----';

        foreach($pages as $page) $pages_options[$page->ID] = esc_html__($page->post_title, 'real-estate-listing-realtyna-wpl');

        $fields['wpltarget'] = array(
            'label'           => esc_html__('Target Page', 'real-estate-listing-realtyna-wpl'),
            'type'            => 'select',
            'option_category' => 'basic_option',
            'options'         => $pages_options,
        );

        $fields['max_char_title'] = array(
            'label'           => esc_html__('Maximum title character', 'real-estate-listing-realtyna-wpl'),
            'type'            => 'text',
            'option_category' => 'basic_option',
        );

        $fields['image_width'] = array(
            'label'           => esc_html__('Image Width', 'real-estate-listing-realtyna-wpl'),
            'type'            => 'text',
            'option_category' => 'basic_option',
        );

        $fields['image_height'] = array(
            'label'           => esc_html__('Image Height', 'real-estate-listing-realtyna-wpl'),
            'type'            => 'text',
            'option_category' => 'basic_option',
        );

        $fields['css_class'] = array(
            'label'           => esc_html__('CSS Class', 'real-estate-listing-realtyna-wpl'),
            'type'            => 'text',
            'option_category' => 'basic_option',
        );

        $fields['show_contact_form'] = array(
            'label'           => esc_html__('Show Contact', 'real-estate-listing-realtyna-wpl'),
            'type'            => 'select',
            'option_category' => 'basic_option',
            'options'         => array(
                '1' => esc_html__('Yes', 'real-estate-listing-realtyna-wpl'),
                '0' => esc_html__('No', 'real-estate-listing-realtyna-wpl'),
            ),
        );

        $fields['show_compare'] = array(
            'label'           => esc_html__('Show Compare', 'real-estate-listing-realtyna-wpl'),
            'type'            => 'select',
            'option_category' => 'basic_option',
            'options'         => array(
                '1' => esc_html__('Yes', 'real-estate-listing-realtyna-wpl'),
                '0' => esc_html__('No', 'real-estate-listing-realtyna-wpl'),
            ),
        );

		return $fields;
	}

    public function render($atts, $content = NULL, $function_name = NULL)
    {
        ob_start();

        $googlemap = new wpl_favorites_widget();
        $googlemap->widget(array(
            'before_widget'=>'',
            'after_widget'=>'',
            'before_title'=>'',
            'after_title'=>'',
        ),
        array
        (
            'title'=>isset($atts['title']) ? $atts['title'] : '',
            'layout'=>isset($atts['tpl']) ? $atts['tpl'] : '',
            'wpltarget'=>isset($atts['wpltarget']) ? $atts['wpltarget'] : '',
            'data'=>array(
                'max_char_title'=>isset($atts['max_char_title']) ? $atts['max_char_title'] : 15,
                'image_width'=>isset($atts['image_width']) ? $atts['image_width'] : 32,
                'image_height'=>isset($atts['image_height']) ? $atts['image_height'] : 32,
                'css_class'=>isset($atts['css_class']) ? $atts['css_class'] : '',
                'show_contact_form'=>isset($atts['show_contact_form']) ? $atts['show_contact_form'] : 1,
                'show_compare'=>isset($atts['show_compare']) ? $atts['show_compare'] : 1,
            )
        ));

        return ob_get_clean();
    }
}