<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');

/**
 * User Links Shortcode for Divi Builder
 * @author Howard <howard@realtyna.com>
 * @package WPL PRO
 */
class wpl_page_builders_divi_user_links extends ET_Builder_Module
{
    public $slug       = 'et_pb_wpl_user_links';
    public $vb_support = 'on';

    public function init()
    {
        $this->name = __('WPL User Links', 'real-estate-listing-realtyna-wpl');
        $this->slug = 'et_pb_wpl_user_links';
		$this->fields_defaults = array();

        // Global WPL Settings
		$this->settings = wpl_global::get_settings();
	}

    public function get_fields()
    {
        // Module Fields
        $fields = array();

		return $fields;
	}

    public function render($atts, $content = NULL, $function_name = NULL)
    {
        $shortcode_atts = '';
        foreach($atts as $key=>$value)
        {
            if(trim($value) == '' or $value == '-1') continue;

            $shortcode_atts .= $key.'="'.$value.'" ';
        }

        return do_shortcode('[wpl_user_links'.(trim($shortcode_atts) ? ' '.trim($shortcode_atts) : '').']');
    }
}