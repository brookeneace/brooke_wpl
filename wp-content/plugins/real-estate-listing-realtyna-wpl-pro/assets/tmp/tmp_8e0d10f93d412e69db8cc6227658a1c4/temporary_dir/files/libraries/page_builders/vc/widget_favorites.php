<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');

_wpl_import('widgets.favorites.main');

/**
 * Favorites Widget Shortcode for VC
 * @author Howard <howard@realtyna.com>
 * @package WPL PRO
 */
class wpl_page_builders_vc_widget_favorites
{
    public $settings;

    public function __construct()
    {
        // Global WPL Settings
		$this->settings = wpl_global::get_settings();
        
        // VC Widget Shortcode
        add_shortcode('wpl_vc_favorites_widget', array($this, 'shortcode_callback'));
        
        vc_map(array
        (
            'name' => __('WPL Favorites Widget', 'real-estate-listing-realtyna-wpl'),
            //'custom_markup' => '<strong>'.__('WPL Favorites Widget', 'real-estate-listing-realtyna-wpl').'</strong>',
            'description' => __('WPL Carousel Widget', 'real-estate-listing-realtyna-wpl'),
            'base' => 'wpl_vc_favorites_widget',
            'class' => '',
            'controls' => 'full',
            'icon' => 'wpb-wpl-icon',
            'category' => __('WPL', 'real-estate-listing-realtyna-wpl'),
            'params' => $this->get_fields()
        ));
	}
    
    public function get_fields()
    {
        $favorites = new wpl_favorites_widget();
        
        // Module Fields
        $fields = array();
        
        $fields[] = array(
            'heading'         => esc_html__('Title', 'real-estate-listing-realtyna-wpl'),
            'type'            => 'textfield',
            'holder'          => 'div',
            'class'           => '',
            'param_name'      => 'title',
            'value'           => '',
            'admin_label'     => true,
            'description'     => esc_html__('The widget title', 'real-estate-listing-realtyna-wpl'),
        );
        
        $widget_layouts = $favorites->get_layouts('favorites');
        
        $widget_layouts_options = array();
        foreach($widget_layouts as $widget_layout) $widget_layouts_options[esc_html__(ucfirst(str_replace('.php', '', $widget_layout)), 'real-estate-listing-realtyna-wpl')] = str_replace('.php', '', $widget_layout);
        
        $fields[] = array(
            'heading'         => esc_html__('Layout', 'real-estate-listing-realtyna-wpl'),
            'type'            => 'dropdown',
            'holder'          => 'div',
            'class'           => '',
            'param_name'      => 'tpl',
            'value'           => $widget_layouts_options,
            'std'             => '',
            'description'     => esc_html__('Layout of the page', 'real-estate-listing-realtyna-wpl'),
        );
        
        $pages = wpl_global::get_wp_pages();
        
        $pages_options = array();
        $pages_options['-----'] = '';
        
        foreach($pages as $page) $pages_options[esc_html__($page->post_title, 'real-estate-listing-realtyna-wpl')] = $page->ID;
        
        $fields[] = array(
            'heading'         => esc_html__('Target Page', 'real-estate-listing-realtyna-wpl'),
            'type'            => 'dropdown',
            'value'           => $pages_options,
            'holder'          => 'div',
            'class'           => '',
            'param_name'      => 'wpltarget',
            'std'             => '',
        );
        
        $fields[] = array(
            'heading'         => esc_html__('Maximum title character', 'real-estate-listing-realtyna-wpl'),
            'type'            => 'textfield',
            'holder'          => 'div',
            'class'           => '',
            'param_name'      => 'max_char_title',
            'value'           => '15',
        );
        
        $fields[] = array(
            'heading'         => esc_html__('Image Width', 'real-estate-listing-realtyna-wpl'),
            'type'            => 'textfield',
            'holder'          => 'div',
            'class'           => '',
            'param_name'      => 'image_width',
            'value'           => '32',
        );
        
        $fields[] = array(
            'heading'         => esc_html__('Image Height', 'real-estate-listing-realtyna-wpl'),
            'type'            => 'textfield',
            'holder'          => 'div',
            'class'           => '',
            'param_name'      => 'image_height',
            'value'           => '32',
        );
        
        $fields[] = array(
            'heading'         => esc_html__('CSS Class', 'real-estate-listing-realtyna-wpl'),
            'type'            => 'textfield',
            'holder'          => 'div',
            'class'           => '',
            'param_name'      => 'css_class',
            'value'           => '',
        );
        
        $fields[] = array(
            'heading'         => esc_html__('Show Contact', 'real-estate-listing-realtyna-wpl'),
            'type'            => 'dropdown',
            'holder'          => 'div',
            'class'           => '',
            'param_name'      => 'show_contact_form',
            'value'           => array(
                esc_html__('Yes', 'real-estate-listing-realtyna-wpl') => '1',
                esc_html__('No', 'real-estate-listing-realtyna-wpl') => '0',
            ),
            'std'             => '',
        );
        
        $fields[] = array(
            'heading'         => esc_html__('Show Compare', 'real-estate-listing-realtyna-wpl'),
            'type'            => 'dropdown',
            'holder'          => 'div',
            'class'           => '',
            'param_name'      => 'show_compare',
            'value'           => array(
                esc_html__('Yes', 'real-estate-listing-realtyna-wpl') => '1',
                esc_html__('No', 'real-estate-listing-realtyna-wpl') => '0',
            ),
            'std'             => '',
        );
        
		return $fields;
	}
    
    public function shortcode_callback($atts)
    {
        ob_start();

        $favorites = new wpl_favorites_widget();
        $favorites->widget(array(
            'before_widget'=>'',
            'after_widget'=>'',
            'before_title'=>'',
            'after_title'=>'',
        ),
        array
        (
            'title'=>isset($atts['title']) ? $atts['title'] : '',
            'layout'=>isset($atts['tpl']) ? $atts['tpl'] : '',
            'wpltarget'=>isset($atts['wpltarget']) ? $atts['wpltarget'] : '',
            'data'=>array(
                'max_char_title'=>isset($atts['max_char_title']) ? $atts['max_char_title'] : 15,
                'image_width'=>isset($atts['image_width']) ? $atts['image_width'] : 32,
                'image_height'=>isset($atts['image_height']) ? $atts['image_height'] : 32,
                'css_class'=>isset($atts['css_class']) ? $atts['css_class'] : '',
                'show_contact_form'=>isset($atts['show_contact_form']) ? $atts['show_contact_form'] : 1,
                'show_compare'=>isset($atts['show_compare']) ? $atts['show_compare'] : 1,
            )
        ));
        
        return ob_get_clean();
    }
}