import React from 'react'


export default class WidgetUnitSwitcher extends React.Component {

  static slug = 'et_pb_wpl_widget_unit_switcher'

  state = {
    options: [
      {
        id: 0,
        name: 'm',
        value: 'm',
        selected: false
      },
      {
        id: 1,
        name: 'ft',
        value: 'ft',
        selected: false
      },
      {
        id: 2,
        name: 'km',
        value: 'km',
        selected: false
      },
      {
        id: 3,
        name: 'mi',
        value: 'mi',
        selected: true
      }
    ]
  }

  render () {
    return (
      <div className='wpl_unit_switcher_activity' id='wpl_unit_switcher_activity'>
        <select
          id='wpl_unit_switcher_activity'
          autoComplete='off'
          defaultValue={ this.state.options[0].name }
        >

          {
            this.state.options.map(item =>
              <option
                key={item.id}
                value={ item.value } >{ item.name }
              </option>
            )
          }

        </select>
      </div>
    )
  }
}
