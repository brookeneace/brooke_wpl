<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');

/**
 * User Links Shortcode for Elementor
 * @author Howard <howard@realtyna.com>
 * @package WPL PRO
 */
class wpl_page_builders_elementor_user_links extends \Elementor\Widget_Base
{
    public function get_name()
    {
        return 'wpl_user_links';
    }

    public function get_title()
    {
        return __('WPL User Links', 'real-estate-listing-realtyna-wpl');
    }

    public function get_icon()
    {
        return 'fa fa-link';
    }

    public function get_categories()
    {
        return array('wpl');
    }

    protected function _register_controls()
    {
    }

    protected function render()
    {
        echo do_shortcode('[wpl_user_links]');
    }
}