<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');

$this->_wpl_import($this->tpl_path.'.scripts.css');
$this->_wpl_import($this->tpl_path.'.scripts.js');
?>

<div class="wrap wpl-wp pmanager-wp wpl_view_container">
    <header>
        <div id="icon-pstats" class="icon48"></div>
        <h2><?php echo __('Listing Stats', 'real-estate-listing-realtyna-wpl'); ?></h2>
    </header>
    <div class="wpl_property_stats_list"><div class="wpl_show_message"></div></div>
    <div class="pstats-cnt">
        
        <?php if($this->blogs): ?>
            <div class="">
                <label for="wpl-property-stats-blogs"><?php echo __('View stats for', 'real-estate-listing-realtyna-wpl'); ?>: </label>
                <select id="wpl-property-stats-blogs" onchange="wpl_change_blog(this.value)">
                    <option value="0"><?php echo __('All Sites', 'real-estate-listing-realtyna-wpl'); ?></option>
                    <?php foreach ($this->blogs as $blog): ?>
                        <option value="<?php echo $blog->blog_id; ?>" <?php echo $this->current_blog_id == $blog->blog_id ? 'selected' : ''; ?>><?php echo $blog->domain; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        <?php endif; ?>

        <?php if(isset($this->pagination->max_page) and $this->pagination->max_page > 1): ?>
            <div class="pagination-wp">
                <?php echo $this->pagination->show(); ?>
            </div>
        <?php endif; ?>

        <div class="pwizard-panel">
            <div class="pwizard-section">
                <div class="wpl-backend-table">
                    <div class="tr th table-header">
                        <div class="td"><?php echo __('Listing ID', 'real-estate-listing-realtyna-wpl'); ?></div>
                        <div class="td"><?php echo __('Listing Title', 'real-estate-listing-realtyna-wpl'); ?></div>
                        <div class="td"><?php echo __('Listing Address', 'real-estate-listing-realtyna-wpl'); ?></div>
                        <div class="td"><?php echo __('Property Visits', 'real-estate-listing-realtyna-wpl'); ?></div>
                        <div class="td"><?php echo __('Contacts', 'real-estate-listing-realtyna-wpl'); ?></div>
                        <div class="td"><?php echo __('Including in Listings', 'real-estate-listing-realtyna-wpl'); ?></div>
                        <?php if(wpl_global::check_addon('complex')): ?><div class="td"><?php echo __('Views in Complex Page', 'real-estate-listing-realtyna-wpl'); ?></div><?php endif; ?>
                    </div>
                    <?php foreach($this->stats as $stat): ?>
                        <div class="tr">
                            <div class="td">
                                <span><?php echo $stat['id']; ?></span>
                            </div>
                            <div class="td">
                                <span><a href="<?php echo $stat['link']; ?>" target="_blank"><?php echo $stat['title']; ?></a></span>
                            </div>
                            <div class="td">
                                <span><?php echo $stat['location']; ?></span>
                            </div>
                            <div class="td">
                                <span><?php echo $stat['visits'] ? $stat['visits'] : 0; ?></span>
                            </div>
                            <div class="td">
                                <span><?php echo $stat['contacts']; ?></span>
                            </div>
                            <div class="td">
                                <span><?php echo $stat['inc_listings']; ?></span>
                            </div>
                            <?php if(wpl_global::check_addon('complex')): ?>
                            <div class="td">
                                <span><?php echo $stat['parent_views']; ?></span>
                            </div>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>

        <?php if(isset($this->pagination->max_page) and $this->pagination->max_page > 1): ?>
        <div class="pagination-wp">
            <?php echo $this->pagination->show(); ?>
        </div>
        <?php endif; ?>
    </div>

    <?php if(wpl_global::check_addon('crm')): ?>
        <header>
            <div id="icon-pstats" class="icon48"></div>
            <h2><?php echo __('CRM Stats', 'real-estate-listing-realtyna-wpl'); ?></h2>
        </header>
        <div class="wpl_crm_stats_list"><div class="wpl_show_message"></div></div>
        <div class="pstats-cnt">
            <div class="pwizard-panel">
                <div class="pwizard-section">
                    <div class="wpl-backend-table">
                        <div class="tr th table-header">
                            <div class="td"><?php echo __('Blog ID', 'real-estate-listing-realtyna-wpl'); ?></div>
                            <div class="td"><?php echo __('Blog Name', 'real-estate-listing-realtyna-wpl'); ?></div>
                            <div class="td"><?php echo __('CRM Requests', 'real-estate-listing-realtyna-wpl'); ?></div>
                        </div>
                        <?php foreach($this->crm_stats as $stat): ?>
                            <div class="tr">
                                <div class="td">
                                    <span><?php echo $stat['id']; ?></span>
                                </div>
                                <div class="td">
                                    <span><a href="<?php echo $stat['link']; ?>"><?php echo $stat['title']; ?></a></span>
                                </div>
                                <div class="td">
                                    <span><?php echo $stat['requests']; ?></span>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <footer>
        <div class="logo"></div>
    </footer>
</div>
