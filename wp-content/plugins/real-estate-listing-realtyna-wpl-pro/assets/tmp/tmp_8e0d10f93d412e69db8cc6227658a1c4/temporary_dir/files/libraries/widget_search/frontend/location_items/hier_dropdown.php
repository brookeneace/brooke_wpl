<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');

if($show == 'hier_dropdown' and !$done_this)
{
    $levels = explode(',', $field['extoption']);
    if(count($levels) == 1 and trim($levels[0]) == '') $levels = array(2, 3);

    $load_locations = true;
    $previous_level = NULL;
    $previous_level_value = NULL;

    $i = 0;
    foreach($levels as $level)
    {
        $level = trim($level);
        if(!is_numeric($level)) continue;
        
        /** current values **/
        $current_value = stripslashes(wpl_request::getVar('sf_text_location'.$level.'_name', ''));
        if($current_value or $previous_level_value) $load_locations = true;
        
        if($load_locations) $locations = wpl_db::select("SELECT `location".$level."_name` FROM `#__wpl_properties` WHERE `finalized`='1' AND `expired`='0' AND `confirmed`='1' AND `deleted`='0' AND `location".$level."_name`!=''".($previous_level_value ? " AND `location".$previous_level."_name`='$previous_level_value'" : "")." GROUP BY `location".$level."_name` ORDER BY `location".$level."_name` ASC", 'loadColumn');
        else $locations = array();

        // Next Level
        $next_level = isset($levels[$i+1]) ? $levels[$i+1] : NULL;
        
        $html .= '<label for="sf'.$widget_id.'_text_location'.$level.'_name">'.__($location_settings['location'.$level.'_keyword'], 'real-estate-listing-realtyna-wpl').'</label>
        <select name="sf'.$widget_id.'_text_location'.$level.'_name" id="sf'.$widget_id.'_text_location'.$level.'_name" class="wpl'.$widget_id.'_search_widget_field_'.$field['id'].'_select" data-level="'.$level.'" data-next-level="'.$next_level.'">
            <option value="" '.($current_value == '' ? 'selected="selected"' : '').'>'.__($location_settings['location'.$level.'_keyword'], 'real-estate-listing-realtyna-wpl').'</option>';

        if(count($locations))
        {
            $uniques = array();
            foreach($locations as $location)
            {
                $location = trim(stripslashes($location));
                if(isset($uniques[$location])) continue;

                $uniques[$location] = true;

                $html .= '<option value="'.$location.'" '.($current_value == $location ? 'selected="selected"' : '').'>'.__($location, 'real-estate-listing-realtyna-wpl').'</option>';
            }
        }
        
        $html .= '</select>';

        $load_locations = false;
        $previous_level = $level;
        $previous_level_value = $current_value;
        $i++;
    }

    wpl_html::set_footer('<script type="text/javascript">
    (function($){$(function()
    {
        $(".wpl'.$widget_id.'_search_widget_field_'.$field['id'].'_select").on("change", function()
        {
            var level = $(this).data("level");
            var next_level = $(this).data("next-level");
            var parent = $(this).val();
            
            // Dont run if there is no next level
            if(!next_level) return false;
            
            // Empty Next Levels
            for(var i = next_level; i <= 7; i++) $("#sf'.$widget_id.'_text_location"+i+"_name").find("option").not(":first").remove();
            $(".wpl'.$widget_id.'_search_widget_field_'.$field['id'].'_select").trigger("chosen:updated");
            
            $.ajax(
            {
                type: "GET",
                url: "'.wpl_global::get_wp_site_url().'?wpl_format=f:property_listing:ajax_pro&wpl_function=get_locations_options&level="+level+"&next_level="+next_level+"&parent="+parent+"&kind='.(isset($this->kind) ? $this->kind : '').'",
                dataType: "json",
                success: function(response)
                {
                    $("#sf'.$widget_id.'_text_location"+next_level+"_name").html(response.html);
                    $(".wpl'.$widget_id.'_search_widget_field_'.$field['id'].'_select").trigger("chosen:updated");
                },
                error: function(response)
                {
                }
            });
        });
    });})(jQuery);
	</script>');
	
	$done_this = true;
}