<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');
_wpl_import('libraries.notifications.notifications');

/**
 * WPL PRO event handler functions
 * @author Howard <howard@realtyna.com>
 */
class wpl_events_pro
{
    /**
     * Sends email to agent for contacting
     * @author Howard <howard@realtyna.com>
     * @static
     * @param array $params
     * @return boolean
     */
    public static function send_favorite_contact_form($params)
    {
        $replacements = $params[0];
        $notification_data = wpl_notifications::get_notification(1);

        /** Email notification is enabled **/
        if($notification_data['enabled'])
        {
            $notification = new wpl_notifications('email');
            $notification->prepare(1, $replacements);

            $pids = wpl_addon_pro::favorite_get_pids();

            foreach($pids as $property_id)
            {
                $property = wpl_property::get_property_raw_data($property_id);
                $user = wpl_users::get_user($property['user_id']);

                $property_title = wpl_property::update_property_title($property);
                $replacements['listing_id'] = '<a href="'.wpl_property::get_property_link(NULL, $property_id).'">'.$property['mls_id'].' ('.$property_title.')</a>';

                $notification->replacements = $notification->set_replacements($replacements);
                $notification->rendered_content = $notification->render_notification_content();
                $notification->recipients = $notification->set_recipients(array($user->data->user_email));

                $notification->send();
            }
        }
        
        /** SMS notification is enabled **/
        if(wpl_global::check_addon('sms') and $notification_data['sms_enabled'])
        {
            $notification = new wpl_notifications('sms');
            $notification->prepare(1, $replacements);
            
            $pids = wpl_addon_pro::favorite_get_pids();
            
            foreach($pids as $property_id)
            {
                $property = wpl_property::get_property_raw_data($property_id);
                $user = $notification->sms->wpl_get_user_data('wpl.`mobile`', "AND wpl.`id`='".$property['user_id']."'", 'loadObject');

                $property_title = wpl_property::update_property_title($property);
                $replacements['listing_id'] = '<a href="'.wpl_property::get_property_link(NULL, $property_id).'">'.$property['mls_id'].' ('.$property_title.')</a>';

                $notification->replacements = $notification->set_replacements($replacements);
                $notification->rendered_content = $notification->render_notification_content();
                $notification->recipients = $notification->set_recipients(array($user->mobile));

                $notification->send();
            }
        }
        
        return true;
    }
    
    /**
     * Sends abuse report to website admin
     * @author Howard <howard@realtyna.com>
     * @static
     * @param array $params
     * @return boolean
     */
    public static function send_abuse_report($params)
    {
        $replacements = $params[0];
        $notification_data = wpl_notifications::get_notification(4);

        /** Email notification is enabled **/
        if($notification_data['enabled'])
        {
            $notification = new wpl_notifications('email');
            $notification->prepare(4, $replacements);

            $property_id = $replacements['property_id'];
            $property = wpl_property::get_property_raw_data($property_id);
            $user = wpl_users::get_user($property['user_id']);

            $property_title = wpl_property::update_property_title($property);
            $replacements['listing_id'] = '<a href="'.wpl_property::get_property_link(NULL, $property_id).'">'.$property['mls_id'] .' ('.$property_title.')</a>';

            $details = '';
            foreach($replacements as $key=>$value)
            {
                if(in_array($key, array('property_id', 'listing_id')) or trim($value) == '') continue;
                $details .= '<strong>'.$key.': </strong><span>'.$value.'</span><br />';
            }

            $replacements['report_details'] = $details;

            $notification->replacements = $notification->set_replacements($replacements);
            $notification->rendered_content = $notification->render_notification_content();
            $notification->recipients = $notification->set_recipients(array($user->data->user_email, wpl_global::get_admin_id()));

            $notification->send();
        }
        
        /** SMS notification is enabled **/
        if(wpl_global::check_addon('sms') and $notification_data['sms_enabled'])
        {
            $notification = new wpl_notifications('sms');
            $notification->prepare(4, $replacements);
            
            $property_id = $replacements['property_id'];
            $property = wpl_property::get_property_raw_data($property_id);
            
            $user = $notification->sms->wpl_get_user_data('wpl.`mobile`', "AND wpl.`id`='".$property['user_id']."'", 'loadObject');

            $property_title = wpl_property::update_property_title($property);
            $replacements['listing_id'] = '<a href="'.wpl_property::get_property_link(NULL, $property_id).'">'.$property['mls_id'] .' ('.$property_title.')</a>';

            $details = '';
            foreach($replacements as $key=>$value)
            {
                if(in_array($key, array('property_id', 'listing_id')) or trim($value) == '') continue;
                $details .= '<strong>'.$key.': </strong><span>'.$value.'</span><br />';
            }

            $replacements['report_details'] = $details;

            $notification->replacements = $notification->set_replacements($replacements);
            $notification->rendered_content = $notification->render_notification_content();
            $notification->recipients = $notification->set_recipients(array($user->mobile, wpl_global::get_admin_id()));

            $notification->send();
            
        }
        
        return true;
    }

    /**
     * Remove favorites of purged property
     * @author Steve A. <steve@realtyna.com>
     * @static
     * @param array $params
     * @return boolean
     */
    public static function remove_favorites($params)
    {
        $replacements = $params[0];
        $property_id = $replacements['property_id'];

        wpl_db::q("DELETE FROM `#__wpl_addon_pro_favorites` WHERE `property_id` = '$property_id'");
        
        return true;
    }

    /**
     * Log property field update
     * @author Steve A. <steve@realtyna.com>
     * @static
     * @param array $params
     * @return boolean
     */
    public static function log_property_field_edit($params)
    {
        $log = wpl_global::get_setting('log', 1);
        if(!$log) return false;

        _wpl_import('libraries.logs');

        $id = $params[0]['id'];
        $table_column = $params[0]['table_column'];
        $value = $params[0]['value'];

        $mls_id = wpl_db::select("SELECT `mls_id` FROM `#__wpl_properties` WHERE `id` = '$id'", 'loadResult');
        $field = wpl_flex::get_field_by_column($table_column, 0);
        $value = wpl_property::render_field($value, $field->id)['value'];

        $log_text = wpl_db::escape("Field '{$field->name}' of property #{$mls_id} was changed to '{$value}'.");
        wpl_logs::add($log_text, 'Property');

        return true;
    }

    /**
     * Assign a Listing to an Agent based on Zipcode
     * @author Steve A. <steve@realtyna.com>
     * @static
     * @param array $params
     * @return boolean
     */
    public static function assign_agent_by_zipcode($params)
    {
        // Feature is Disabled
        if(!wpl_global::get_setting('assign_agents_by_zipcode')) return false;

        $pid = $params[0];
        $zipcode = wpl_property::get_property_field('zip_name', $pid);

        // Zipcode is not set
        if(!trim($zipcode)) return false;

        $users = wpl_users::get_wpl_users("AND (wpl.`zip_name` = '{$zipcode}' OR wpl.`zip_name` LIKE '%,{$zipcode}' OR wpl.`zip_name` LIKE '{$zipcode},%' OR wpl.`zip_name` LIKE '%,{$zipcode},%')");

        // No User Found
        if(!is_array($users) or (is_array($users) and !count($users))) return false;

        foreach($users as $user)
        {
            wpl_db::q("UPDATE `#__wpl_properties` SET `user_id` = '".$user->id."' WHERE `id` = '$pid'");
            break;
        }

        return true;
    }

    public function listing_about_expire($params)
    {
        $replacements = $params[0];
        $notification_data = wpl_notifications::get_notification(11);

        // Email notification is enabled
        if($notification_data['enabled'])
        {
            $notification = new wpl_notifications('email');
            $notification->prepare(11, $replacements);

            $property_id = $params[0]['property_id'];
            $property = wpl_property::get_property_raw_data($property_id);

            $user = wpl_users::get_user($property['user_id']);
            if(wpl_global::check_addon('membership') && $user->wpl_data->maccess_direct_contact == 0)
                $user = wpl_users::get_user($user->wpl_data->maccess_direct_contact_user_id);

            $property_title = wpl_property::update_property_title($property);

            $replacements['name'] = isset($user->data->wpl_data) ? $user->data->wpl_data->first_name : $user->data->display_name;
            $replacements['listing_id'] = '<a href="'.wpl_property::get_property_link(NULL, $property_id).'">'.$property['mls_id'].' ('.$property_title.')</a>';
            $replacements['location'] = $property['location_text'];
            $replacements['days'] = $params[0]['days'];

            $notification->replacements = $notification->set_replacements($replacements);
            $notification->rendered_content = $notification->render_notification_content();
            $notification->recipients = $notification->set_recipients(array($user->data->user_email));
            $notification->send();
        }

        // SMS notification is enabled
        if(wpl_global::check_addon('sms') and $notification_data['sms_enabled'])
        {
            $notification = new wpl_notifications('sms');
            $notification->prepare(11, $replacements);

            $property_id = $params[0]['property_id'];
            $property = wpl_property::get_property_raw_data($property_id);

            $user = wpl_users::get_user($property['user_id']);
            if(wpl_global::check_addon('membership') && $user->wpl_data->maccess_direct_contact == 0)
                $user = wpl_users::get_user($user->wpl_data->maccess_direct_contact_user_id);

            $user = $notification->sms->wpl_get_user_data('*', "AND wpl.`id`='".$user->data->ID."'", 'loadObject');

            $replacements['name'] = isset($user->first_name) ? $user->first_name : $user->display_name;
            $replacements['listing_id'] = $property['mls_id'];
            $replacements['location'] = $property['location_text'];
            $replacements['days'] = $params[0]['days'];

            $notification->replacements = $notification->set_replacements($replacements);
            $notification->rendered_content = $notification->render_notification_content();
            $notification->recipients = $notification->set_recipients(array($user->mobile));
            $notification->send();
        }

        return true;
    }
}