import React from 'react'

export default class WidgetFavorites extends React.Component {

  static slug = 'et_pb_wpl_widget_favorites'

  constructor (props) {
    super(props)

    this.state = {
      widgetId: 2,
      favoriteLitings: [
        {
          id: 0,
          href: '#propertyShow',
          image: null,
          title: 'Loft For Sale'
        },
        {
          id: 1,
          href: '#propertyShow',
          image: null,
          title: 'Loft For Rent'
        },
        {
          id: 2,
          href: '#propertyShow',
          image: null,
          title: 'Loft For Sale'
        }
      ]
    }
  }

  render () {
    return (
      <div id='wpl_favorite_listings_cnt' className='wpl_favorite_listings'>
        <div className='wpl_favorite_widget_title'>
          Favorites
          <div id='wpl_favorites_count' className='badge'>{ this.state.favoriteLitings.length }</div>
        </div>
        <ul id='wpl_favorites_items' className='wpl_favorites_items'>
          {
            this.state.favoriteLitings.map((item, index) => (
              <li id={`wpl_favorites_item${this.state.widgetId}`}>
                <a href={item.href} target='_blank'>
                    {
                        item.image ?
                          <img
                            class="wpl_favorite_item_image"
                            src={item.image}
                          /> : <div class="no_image_box" />
                    }
                    <span class='wpl_favorite_item_title'>{ item.title }</span>
                </a>
                <span className='wpl_favorite_item_remove'>x</span>
              </li>
            ))
          }
        </ul>
      </div>
    )
  }
}
