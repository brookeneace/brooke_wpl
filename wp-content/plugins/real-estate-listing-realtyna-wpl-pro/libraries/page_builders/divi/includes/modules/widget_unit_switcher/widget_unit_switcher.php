<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');

_wpl_import('widgets.unit_switcher.main');

/**
 * Unit Switcher Widget Shortcode for Divi Builder
 * @author Howard <howard@realtyna.com>
 * @package WPL PRO
 */
class wpl_page_builders_divi_widget_unit_switcher extends ET_Builder_Module
{
    public $vb_support = 'on';
    
    public function init()
    {
        $this->name = __('WPL Unit Switcher Widget', 'real-estate-listing-realtyna-wpl');
        $this->slug = 'et_pb_wpl_widget_unit_switcher';

		$this->fields_defaults = array();

        // Global WPL Settings
		$this->settings = wpl_global::get_settings();
	}

    public function get_fields()
    {
        // Module Fields
        $fields = array();

        $fields['title'] = array(
            'label'           => esc_html__('Title', 'real-estate-listing-realtyna-wpl'),
            'type'            => 'text',
            'option_category' => 'basic_option',
            'description'     => esc_html__('The widget title', 'real-estate-listing-realtyna-wpl'),
        );

        $widget_layouts = wpl_global::get_layouts('unit_switcher', array('message.php'), 'activities');

        $widget_layouts_options = array();
        foreach($widget_layouts as $widget_layout) $widget_layouts_options[str_replace('.php', '', $widget_layout)] = esc_html__(ucfirst(str_replace('.php', '', $widget_layout)), 'real-estate-listing-realtyna-wpl');

        $fields['tpl'] = array(
            'label'           => esc_html__('Layout', 'real-estate-listing-realtyna-wpl'),
            'type'            => 'select',
            'option_category' => 'basic_option',
            'options'         => $widget_layouts_options,
        );

        $unit_types = wpl_units::get_unit_types();

        $unit_types_options = array();
        foreach($unit_types as $unit_type) $unit_types_options[$unit_type['id']] = esc_html__($unit_type['name'], 'real-estate-listing-realtyna-wpl');

        $fields['unit_type'] = array(
            'label'           => esc_html__('Unit Types', 'real-estate-listing-realtyna-wpl'),
            'type'            => 'select',
            'option_category' => 'basic_option',
            'options'         => $unit_types_options,
        );

        $fields['css_class'] = array(
            'label'           => esc_html__('CSS Class', 'real-estate-listing-realtyna-wpl'),
            'type'            => 'text',
            'option_category' => 'basic_option',
        );

		return $fields;
	}

    public function render($atts, $content = NULL, $function_name = NULL)
    {
        ob_start();

        $unit_switcher = new wpl_unit_switcher_widget();
        $unit_switcher->widget(array(
            'before_widget'=>'',
            'after_widget'=>'',
            'before_title'=>'',
            'after_title'=>'',
        ),
        array
        (
            'title'=>isset($atts['title']) ? $atts['title'] : '',
            'layout'=>isset($atts['tpl']) ? $atts['tpl'] : '',
            'data'=>array(
                'unit_type'=>isset($atts['unit_type']) ? $atts['unit_type'] : 4,
                'css_class'=>isset($atts['css_class']) ? $atts['css_class'] : '',
            )
        ));

        return ob_get_clean();
    }
}