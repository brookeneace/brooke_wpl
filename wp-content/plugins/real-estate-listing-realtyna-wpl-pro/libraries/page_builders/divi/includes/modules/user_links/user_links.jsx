import React from 'react'

export default class UserLinks extends React.Component {
  static slug = 'et_pb_wpl_user_links'
  render () {
    return (
      <div className='wpl_links_widget_container'>
        <div className='wpl-login-box'>
          <ul>
            <li>
              <a href='#'>
                <i className='wpl-font-logout' aria-hiddent='true'></i>
                Logout
              </a>
            </li>
            <li>
              <a href='#'>
                <i className='wpl-font-compare2' aria-hiddent='true'></i>
                Compare
              </a>
            </li>
            <li>
              <a href='#'>
                <i className='wpl-font-favorite' aria-hiddent='true'></i>
                Favorites
              </a>
            </li>
          </ul>
        </div>
      </div>
    )
  }
}
