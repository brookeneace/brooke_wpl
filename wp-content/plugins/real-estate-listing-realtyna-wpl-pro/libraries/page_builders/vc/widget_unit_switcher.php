<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');

_wpl_import('widgets.unit_switcher.main');

/**
 * Unit Switcher Widget Shortcode for VC
 * @author Howard <howard@realtyna.com>
 * @package WPL PRO
 */
class wpl_page_builders_vc_widget_unit_switcher
{
    public $settings;

    public function __construct()
    {
        // Global WPL Settings
		$this->settings = wpl_global::get_settings();
        
        // VC Widget Shortcode
        add_shortcode('wpl_vc_unit_switcher', array($this, 'shortcode_callback'));
        
        vc_map(array
        (
            'name' => __('WPL Unit Switcher Widget', 'real-estate-listing-realtyna-wpl'),
            //'custom_markup' => '<strong>'.__('WPL Unit Switcher Widget', 'real-estate-listing-realtyna-wpl').'</strong>',
            'description' => __('WPL Unit Switcher Widget', 'real-estate-listing-realtyna-wpl'),
            'base' => 'wpl_vc_unit_switcher',
            'class' => '',
            'controls' => 'full',
            'icon' => 'wpb-wpl-icon',
            'category' => __('WPL', 'real-estate-listing-realtyna-wpl'),
            'params' => $this->get_fields()
        ));
	}
    
    public function get_fields()
    {
        // Module Fields
        $fields = array();
        
        $fields[] = array(
            'heading'         => esc_html__('Title', 'real-estate-listing-realtyna-wpl'),
            'type'            => 'textfield',
            'holder'          => 'div',
            'class'           => '',
            'param_name'      => 'title',
            'value'           => '',
            'description'     => esc_html__('The widget title', 'real-estate-listing-realtyna-wpl'),
        );
        
        $widget_layouts = wpl_global::get_layouts('unit_switcher', array('message.php'), 'activities');
        
        $widget_layouts_options = array();
        foreach($widget_layouts as $widget_layout) $widget_layouts_options[esc_html__(ucfirst($widget_layout), 'real-estate-listing-realtyna-wpl')] = $widget_layout;
        
        $fields[] = array(
            'heading'         => esc_html__('Layout', 'real-estate-listing-realtyna-wpl'),
            'type'            => 'dropdown',
            'holder'          => 'div',
            'class'           => '',
            'param_name'      => 'tpl',
            'value'           => $widget_layouts_options,
            'std'             => '',
            'description'     => esc_html__('Layout of the page', 'real-estate-listing-realtyna-wpl'),
        );
        
        $unit_types = wpl_units::get_unit_types();
        
        $unit_types_options = array();
        foreach($unit_types as $unit_type) $unit_types_options[esc_html__($unit_type['name'], 'real-estate-listing-realtyna-wpl')] = $unit_type['id'];
        
        $fields[] = array(
            'heading'         => esc_html__('Unit Types', 'real-estate-listing-realtyna-wpl'),
            'type'            => 'dropdown',
            'holder'          => 'div',
            'class'           => '',
            'param_name'      => 'unit_type',
            'value'           => $unit_types_options,
            'admin_label'     => true,
        );
        
        $fields[] = array(
            'heading'         => esc_html__('CSS Class', 'real-estate-listing-realtyna-wpl'),
            'type'            => 'textfield',
            'holder'          => 'div',
            'class'           => '',
            'param_name'      => 'css_class',
            'value'           => '',
        );
        
		return $fields;
	}
    
    public function shortcode_callback($atts)
    {
        ob_start();
        
        $unit_switcher = new wpl_unit_switcher_widget();
        $unit_switcher->widget(array(
            'before_widget'=>'',
            'after_widget'=>'',
            'before_title'=>'',
            'after_title'=>'',
        ),
        array
        (
            'title'=>isset($atts['title']) ? $atts['title'] : '',
            'layout'=>isset($atts['tpl']) ? $atts['tpl'] : '',
            'data'=>array(
                'unit_type'=>isset($atts['unit_type']) ? $atts['unit_type'] : 4,
                'css_class'=>isset($atts['css_class']) ? $atts['css_class'] : '',
            )
        ));
        
        return ob_get_clean();
    }
}