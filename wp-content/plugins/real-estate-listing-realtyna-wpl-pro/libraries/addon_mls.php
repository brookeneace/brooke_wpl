<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');

_wpl_import('libraries.addon_mls.mapper');
_wpl_import('libraries.logs');

/**
 * MLS addon Library
 * @author Howard <howard@realtyna.com>
 * @package MLS Add-on
 */
class wpl_addon_mls
{
	var $rets;
	var $tmp_path;
	var $agent_username = 'PHRETS/1.0.1';
	var $rets_version = 'RETS/1.5';
	var $search;
	var $results = array();
	var $agent_password;
	var $mls_auth_method = 'digest';
    
	/**
		Developed by : Howard
		Inputs : void
		Outputs : void
		Date : 2013-12-09
		Description: Constructor method
	**/
	public function __construct($mls_server_id = NULL)
	{
		/** settings **/
		@ini_set('memory_limit', '-1');
        @ini_set('max_execution_time', 0);
        @set_time_limit(0);
		
		$this->mls_server_id = $mls_server_id;
		$this->rets = $this->get_RETS();
		$this->tmp_path = wpl_global::init_tmp_folder();
		
		if($this->mls_server_id)
		{
			$this->mls_server_data = $this->get_servers($this->mls_server_id);
			$this->login_url = $this->mls_server_data->url;
			$this->username = $this->mls_server_data->username;
			$this->password = $this->mls_server_data->password;
			$this->rets_version = $this->mls_server_data->rets_version;
			$this->agent_username = $this->mls_server_data->agent_username;
			$this->agent_password = $this->mls_server_data->agent_password;
			$this->mls_auth_method = $this->mls_server_data->mls_auth_method;
		}
		else return;
	}
	
	/**
		Developed by : Howard
		Inputs : mls_server_id
		Outputs : mls servers
		Date : 2013-12-09
	**/
	public static function get_servers($mls_server_id = NULL)
	{
		$query = "SELECT * FROM `#__wpl_addon_mls` WHERE 1 ".($mls_server_id ? "AND `id`='$mls_server_id'" : "")." ORDER BY `id` ASC";
		
		$output = $mls_server_id ? 'loadObject' : 'loadObjectList';
		return wpl_db::select($query, $output);
	}
	
	/**
		Developed by : Howard
		Inputs : mls_query_id
		Outputs : mls queries
		Date : 2013-12-14
	**/
	public static function get_mls_queries($mls_query_id = NULL)
	{
		$query = "SELECT * FROM `#__wpl_addon_mls_queries` WHERE 1 ".($mls_query_id ? "AND `id`='$mls_query_id'" : "")." ORDER BY `id` ASC";
		
		$output = $mls_query_id ? 'loadObject' : 'loadObjectList';
		return wpl_db::select($query, $output);
	}
	
	/**
		Developed by : Howard
		Inputs : mls_server_id
		Outputs : void
		Date : 2013-12-09
	**/
	public static function remove_server($mls_server_id = NULL)
	{
		wpl_db::delete('wpl_addon_mls', $mls_server_id);
        
        /** trigger event **/
		wpl_global::event_handler('mls_server_removed', array('id'=>$mls_server_id));
	}
	
	/**
		Developed by : Howard
		Inputs : mls_server_id
		Outputs : array result
		Date : 2013-12-09
	**/
	public function test_connection()
	{
		$rets_connection = $this->connect();
        $this->rets = $this->get_RETS();
        return $rets_connection;
	}
	
	/**
		Developed by : Howard
		Inputs : mls_server_id, condition
		Outputs : mls fields
		Date : 2013-12-10
	**/
	public static function get_fields($mls_server_id = NULL, $condition = '')
	{
		if(trim($condition) == '')
		{
			$condition = "";
			if(trim($mls_server_id) != '') $condition .= "AND `mls_server_id`='$mls_server_id' ";
		}
		
		$query = "SELECT * FROM `#__wpl_addon_mls_mappings` WHERE 1 ".$condition;
		return wpl_db::select($query, 'loadObjectList');
	}
	
	/**
		Developed by : Howard
		Inputs : field_id
		Outputs : field data
		Date : 2013-12-10
	**/
	public static function get_field($field_id = NULL)
	{
		$query = "SELECT * FROM `#__wpl_addon_mls_mappings` WHERE 1 AND `id`='$field_id'";
		return wpl_db::select($query, 'loadObject');
	}
	
	/**
		Developed by : Howard
		Inputs : mls_server_id, condition
		Outputs : mls classes
		Date : 2013-12-10
	**/
	public static function get_classes($mls_server_id = NULL)
	{
		$query = "SELECT * FROM `#__wpl_addon_mls_mappings` WHERE 1 AND `mls_server_id`='$mls_server_id' GROUP BY `mls_class_id`";
		$results = wpl_db::select($query, 'loadObjectList');
		
		$classes = array();
		foreach($results as $result)
		{
			$classes[$result->mls_class_id] = $result->mls_class_name;
		}
		
		return $classes;
	}
	
	/**
		Developed by : Howard
		Inputs : field_id, wpl_field id
		Outputs : boolean
		Date : 2013-12-10
	**/
	public static function save_mapping($field_id, $wpl_field_id, $custom1 = '', $value_mapping_status = 0, $value_mappings = '')
	{
		/** map the field itself **/
		$value_mappings = wpl_db::escape($value_mappings);
		$query = "UPDATE `#__wpl_addon_mls_mappings` SET `wpl_field_id`='$wpl_field_id', `custom1`='$custom1', `value_mapping_status`='$value_mapping_status', `value_mappings`='$value_mappings' WHERE `id`='$field_id'";
		wpl_db::q($query);
		
		/** map same fields on other mls classes if they're not mapped **/
		$field_data = wpl_addon_mls::get_field($field_id);
		$mls_data = wpl_addon_mls::get_servers($field_data->mls_server_id);

		if(!$mls_data->map_per_class)
		{
			$query = "UPDATE `#__wpl_addon_mls_mappings` SET `wpl_field_id`='$wpl_field_id', `custom1`='$custom1', `value_mapping_status`='$value_mapping_status', `value_mappings`='$value_mappings' WHERE `mls_server_id`='".$field_data->mls_server_id."' AND `field_id`='".$field_data->field_id."' AND `field_type`='".$field_data->field_type."' AND `wpl_field_id`=''";
			wpl_db::q($query);
		}
		
        /** trigger event **/
		wpl_global::event_handler('mls_mapping_saved', array('field_id'=>$field_id, 'wpl_field_id'=>$wpl_field_id, 'custom1'=>$custom1));
        
		return true;
	}
	
	/**
	* Developed by : Matthew
	* Inputs : void
	* Outputs : connection
	* Date : 2017-05-02
	**/
	public function connect($use_post_method = 0)
	{
		//Set new connection
		\PHRETS\Http\Client::set(new \GuzzleHttp\Client);

		$this->rets->setLoginUrl($this->login_url);
		$this->rets->setUsername($this->username);
		$this->rets->setPassword($this->password);
		$this->rets->setRetsVersion($this->rets_version);

		$this->rets->setUserAgent($this->agent_username);
		$this->rets->setUserAgentPassword($this->agent_password);
		$this->rets->setHttpAuthenticationMethod($this->mls_auth_method);

		if($use_post_method) $this->rets->setOption('use_post_method', true);

		//Disable the ability to automatically handle redirects sent by the server. Default is false (follow redirects)
		// $this->rets->setOption('disable_follow_location', true);

		try{
			$this->rets = new \PHRETS\Session($this->rets);
			return $this->rets->Login();
		}
		catch(Exception $e)
		{
			wpl_global::event_handler('wpl_mls_connection_log', array('error_code'=>$e->getCode(), 'error_message'=>wpl_db::escape($e->getMessage())));
			return array('status'=>0, 'message'=>__('MLS Error: ', 'real-estate-listing-realtyna-wpl').$e->getMessage());
		}
	}
	
	/**  Deprecated
    Developed by : Howard
	Inputs : void
	Outputs : search result
	Date : 2013-12-12
	**/
	public function search($limit = 10, $query = '')
	{
		$this->search = $this->rets->SearchQuery($this->mls_server_data->resource, $this->mls_class_id, $query, array('Limit'=>$limit, 'Count'=>1));
		return $this->search;
	}

	/**
		Developed by : Howard
		Inputs : void
		Outputs : search result
		Date : 2013-12-12
	**/
	public function results()
	{
		$this->results = array();
		while($result = $this->rets->FetchRow($this->search)) $this->results[] = $result;
		
		return $this->results;
	}
	
	/**
		Developed by : Howard
		Inputs : void
		Outputs : PHRETS object
		Date : 2013-12-09
	**/
	public static function get_RETS()
	{
		/** import PHRETS library **/
		_wpl_import('libraries.addon_mls.phrets2.vendor.autoload');

		/** Initialize Object **/
		return new \PHRETS\Configuration;
	}
	
	/**
		Developed by : Howard
		Inputs : void
		Outputs : void
		Date : 2013-12-09
	**/
	public function import_basic_data()
	{
		$mls_server = $this->mls_server_data;

		$this->connect($mls_server->mls_use_post);

		try{ $mls_classes = $this->rets->GetClassesMetadata($mls_server->resource); }
		catch(Exception $e) { wpl_global::event_handler('wpl_mls_connection_log', array('error_code'=>$e->getCode(), 'error_message'=>wpl_db::escape($e->getMessage()))); }
        
        $mls_info = !empty($mls_server->mls_info) ? json_decode($mls_server->mls_info, true) : '';
        
        if(!isset($mls_info['objects']) or empty($mls_info['objects']))
        {
			if (!is_array($mls_info)) $mls_info = array();

	        $objects = array();

	        try{ $objects = $this->rets->GetObjectMetadata($mls_server->resource);}
	        catch(Exception $e) {
		        wpl_global::event_handler('wpl_mls_connection_log', array('error_code'=>$e->getCode(), 'error_message'=>wpl_db::escape($e->getMessage())));
	        }
            foreach($objects as $object) $mls_info['objects'][] = array('ObjectType'=>$object['ObjectType'], 'Description'=>$object['Description']);
        }
        
        if(!isset($mls_info['classes']) or empty($mls_info['classes']))
        {
            foreach($mls_classes as $mls_class) $mls_info['classes'][] = array('ClassName'=>$mls_class['ClassName'], 'Description'=>$mls_class['Description']);
        }
        
        $mls_info = json_encode($mls_info);
        $query = "UPDATE `#__wpl_addon_mls` SET `mls_info`='".wpl_db::escape($mls_info)."' WHERE `id`='".$this->mls_server_id."'";
        wpl_db::q($query);

		$imported_classes = array();

		/********
		* importing mls fields
		********/
		foreach($mls_classes as $mls_class)
		{
			$mls_class_id = $mls_class['ClassName'];
			$mls_class_name = trim($mls_class['Description']) != '' ? $mls_class['Description'] : $mls_class['ClassName'];

			$fields = $this->rets->GetTableMetadata($mls_server->resource, $mls_class_id);
            
            $imported_unique_field = false;
			$query_field = '';
			$found_query_field = false;

			foreach($fields as $field)
			{
				$field_id = wpl_db::escape($field['SystemName']);
				$field_name = wpl_db::escape($field['LongName']);
				$field_type = $field['DataType'];
                $field_lookupName = wpl_db::escape($field['LookupName']);
                $field_Searchable = $field['Searchable'];

				$date_field_array = array('date','datetime');
				$lower_field_type = strtolower($field_type);
				$lower_field_id = strtolower($field_id);
				$lower_field_name = strtolower($field_name);

				// Find a date field to make a query
				if(in_array($lower_field_type, $date_field_array) AND !$found_query_field AND
                      ((
                       stristr($lower_field_name, 'update') or
		               stristr($lower_field_name, 'modifi') or
                       stristr($lower_field_name, 'status') or
		               stristr($lower_field_name, 'last')
		              ) OR (
						stristr($lower_field_id, 'update') or
						stristr($lower_field_id, 'modifi') or
						stristr($lower_field_id, 'status') or
						stristr($lower_field_id, 'last') )) )
				{
					$query_field = $field_id.'||'.$lower_field_type;
					$found_query_field = true;
				}

				// Check if a field is unique
                if($field['Unique'] and !$imported_unique_field)
                {
                    $query = "SELECT `mls_unique_field` FROM `#__wpl_addon_mls` WHERE `id`='".$this->mls_server_id."'";
		            $unique_field = wpl_db::select($query, 'loadResult');
                    if(!empty($unique_field) and $unique_field != 'mls_id') $imported_unique_field = true;

                    if(!$imported_unique_field) wpl_db::update('wpl_addon_mls', array('mls_unique_field'=>$field_id), 'id', $this->mls_server_id);
                }

				// If already existed a field
				$num_query = "SELECT COUNT(id) FROM `#__wpl_addon_mls_mappings` WHERE `mls_server_id`='{$this->mls_server_id}' AND `mls_class_id`='$mls_class_id' AND `field_id`='$field_id'";
				if(wpl_db::num($num_query)) continue;

				// Insert new MLS field
				$query = "INSERT INTO `#__wpl_addon_mls_mappings` (`mls_server_id`,`field_id`,`field_name`,`field_type`,`field_sample_data`,`mls_class_id`,`mls_class_name`,`mls_field_lookup_name`,`mls_field_searchable`) VALUES ('{$this->mls_server_id}','$field_id','$field_name','$field_type','','$mls_class_id','$mls_class_name','$field_lookupName','$field_Searchable')";
				wpl_db::q($query);

                //Adding the values for each MLS field
                $this->add_mls_field_values($mls_server, $field_lookupName, $field_id, $mls_class_id);
			}

			// get imported fields of a class
			$imported_classes[$mls_class_id] = $mls_class_name;
            
			/********
			* importing sample data
			********/
			$sample_query = trim($mls_server->mls_sample_query) ? $mls_server->mls_sample_query : '*';

			if('*' === $sample_query)
			{
				$exp_query_field = explode('||', $query_field);
				$date_query = date('Y-m-d', strtotime('-1 month'));

				$render_date = array(
					'date'=>'('.$exp_query_field[0].'='.$date_query.'+)',
					'datetime'=>'('.$exp_query_field[0].'='.$date_query.'T00:00:00+)',
				);

				$sample_query = '('.$render_date[$exp_query_field[1]].')';
			}

			// Making a row in query table
			$this->create_default_query($this->mls_server_id, $mls_class_id, $mls_class_name, $sample_query);

			try { $listings = $this->rets->Search($mls_server->resource, $mls_class_id, $sample_query, array('Limit'=>10)); }
			catch (Exception $e)
			{
				wpl_global::event_handler('wpl_mls_connection_log', array('error_code'=>$e->getCode(), 'error_message'=>wpl_db::escape($e->getMessage())));
				continue;
			}

			$listing_to_array = array();

			foreach($listings as $listing) $listing_to_array[] = $listing->toArray();

			$sample_values = array();
			for($i=0; $i<count($listing_to_array); $i++)
			{
				if(!$listings) continue;
				if(is_array($listing_to_array[$i]) || is_object($listing_to_array[$i])) foreach($listing_to_array[$i] as $key=>$value) $sample_values[$key][] = $value;
			}

			foreach($sample_values as $key=>$values)
			{
				$values_str = '';
				$values = array_unique($values);
				foreach($values as $value) if(trim($value)) $values_str .= '['.trim($value).']';
				
				$query = "UPDATE `#__wpl_addon_mls_mappings` SET `field_sample_data`='".wpl_db::escape($values_str)."' WHERE `field_id`='".$key."' AND `mls_server_id`='".$this->mls_server_id."' AND `mls_class_id`='".$mls_class_id."' AND `field_sample_data`=''";
				wpl_db::q($query);
			}
		}

		return $imported_classes;
	}
	
	/**
		Developed by : Howard
		Inputs : mls_field_id
		Outputs : boolean
		Date : 2013-12-10
	**/
	public static function create_field($mls_field_id = NULL, $dbst_type = NULL)
	{
		$mls_field_data = wpl_addon_mls::get_field($mls_field_id);
		
		$category = wpl_addon_mls::get_wpl_mls_category();
		if(!trim($dbst_type)) $dbst_type = wpl_addon_mls::detect_wpl_field_type($mls_field_data->field_type);
        
		$dbst_kind = wpl_addon_mls::get_wpl_mls_kind();
		$dbst_id = wpl_flex::create_default_dbst(NULL, 0); #WPL field Created
		
		$q = '';
		$q .= "`kind`='".$dbst_kind."', `name`='".$mls_field_data->field_name."', `type`='".$dbst_type."', `category`='".$category."'";
		
		$query = "UPDATE `#__wpl_dbst` SET ".$q." WHERE `id`='$dbst_id'";
		wpl_db::q($query, 'update');
		
		/** run queries **/
		wpl_flex::run_dbst_type_queries($dbst_id, $dbst_type, $dbst_kind, 'add');
		
		/** Save mapping **/
		wpl_addon_mls::save_mapping($mls_field_id, $dbst_id);
		
		/** trigger event **/
		wpl_global::event_handler('dbst_modified', array('id'=>$dbst_id, 'mode'=>'add', 'kind'=>$dbst_kind, 'type'=>$dbst_type));
		
		return true;
	}
	
	/**
		Developed by : Howard
		Inputs : void
		Outputs : dbcat id of MLS addon
		Date : 2013-12-10
	**/
	public static function get_wpl_mls_category()
	{
		return 31;
	}
	
	/**
		Developed by : Howard
		Inputs : void
		Outputs : kind id of MLS addon
		Date : 2013-12-10
	**/
	public static function get_wpl_mls_kind()
	{
		return 0;
	}
	
	/**
		Developed by : Howard
		Inputs : void
		Outputs : dbcat id of MLS addon
		Date : 2013-12-10
	**/
	public static function detect_wpl_field_type($mls_type)
	{
		$mls_type = strtolower($mls_type);
		$wpl_types = array('int'=>'number', 'character'=>'text', 'small'=>'number', 'datetime'=>'datetime', 'date'=>'date', 'decimal'=>'number');
		
		if(isset($wpl_types[$mls_type])) return $wpl_types[$mls_type];
		else return 'text';
	}
	
	#TODO
	/**
		Developed by : Howard
		Inputs : void
		Outputs : void
		Date : 2013-12-15
	**/
	public static function sync()
	{
		
	}
	
	/**
		Developed by : Howard
		Inputs : mls listings object, query id
		Outputs : mapped data
		Date : 2013-12-16
	**/
	public function map($mls_listings, $mls_query_id)
	{
		/** global vars **/
		global $mls_query;
		global $mls_server;
		global $mls_fields;
		global $wpl_fields;
		global $mls_sorted_fields;
		
		/** force to array **/
		$mls_listings = (array) $mls_listings;
		
		/** first validation **/
		if(trim($mls_query_id) == '') return false;
		
		/** including mapper classes **/
		$path = WPL_ABSPATH.'libraries'.DS.'addon_mls'.DS.'fields'.DS;
		
		$files = wpl_folder::files($path);
		foreach($files as $file)
		{
			/** get overrode file **/
			$overrode_path = $path.'overrides'.DS.$file;
			
			if(wpl_file::exists($overrode_path)) include_once $overrode_path;
			else include_once $path.$file;
		}
		
		$mls_query = wpl_addon_mls::get_mls_queries($mls_query_id);
		$mls_server = $this->mls_server_data;
		
		$mls_fields = wpl_addon_mls::get_fields('', " AND `mls_server_id`='{$mls_query->mls_server_id}' AND `mls_class_id`='{$mls_query->mls_class_id}' AND `wpl_field_id`!=''");
		$wpl_fields = (array) wpl_flex::get_fields();
		
        /** Get Mapper Object **/
        $mapper = new wpl_addon_mls_mapper($this->rets);
        
		$mls_sorted_fields = array();
		foreach($mls_fields as $mls_field) $mls_sorted_fields[$mls_field->field_id] = (array) $mls_field;
		
		$field_options = array();
		$field_options_checker = array();
		$mapped = array();
		
		foreach($mls_listings as $mls_listing)
		{
			$wpl_listing = array();
			
			/** run before mapping function and set the default values **/
			$default_values = $mapper->before_mapping($mls_listing, $mls_query_id, $mls_listings);
			$wpl_listing = array_merge($wpl_listing, $default_values);
			
			foreach($mls_listing as $mls_field_id=>$mls_value)
			{
				$wpl_field_id = isset($mls_sorted_fields[$mls_field_id]['wpl_field_id']) ? $mls_sorted_fields[$mls_field_id]['wpl_field_id'] : NULL;
				if(!trim($wpl_field_id)) continue;
				
				$wpl_field = (array) $wpl_fields[$wpl_field_id];
				$mls_field = $mls_sorted_fields[$mls_field_id];
				
				// pre populate options
				if (!isset($field_options[$wpl_field_id]) && isset($wpl_field['options']))
				{
					$field_options[$wpl_field_id] = json_decode($wpl_field['options'], true);
					
					// initialize field options if null from db
					if (!is_array($field_options[$wpl_field_id])) $field_options[$wpl_field_id] = array();
					
					// count all params for select and values for feature
					$count_values = $field_options[$wpl_field_id]['values'] ? count($field_options[$wpl_field_id]['values']) : 0;
					$count_params = $field_options[$wpl_field_id]['params'] ? count($field_options[$wpl_field_id]['params']) : 0;

					$field_options_checker[$wpl_field_id] = $count_values + $count_params;
				}
                
                // Value Mapping
                if(isset($mls_field['value_mapping_status']) and $mls_field['value_mapping_status'])
                {
                    $value_mappings = json_decode($mls_field['value_mappings'], true);
	                $lower_mls_value = strtolower($mls_value);
                    $mls_value = (isset($value_mappings[$lower_mls_value]) and trim($value_mappings[$lower_mls_value])) ? $value_mappings[$lower_mls_value] : $mls_value;
                }
                
				$arr = array();
				$arr['mls_value'] = $mls_value;
				$arr['mls_field'] = $mls_field;
				$arr['wpl_field'] = $wpl_field;
				
				/** edit values **/
				$class_name = 'wpl_mls_'.$wpl_field['type'].'_map';
				
				$return = array();
				if(class_exists($class_name)) $class_obj = new $class_name();
				else $class_obj = new wpl_addon_mls_mapper($this->rets);
				
				$return = $class_obj->map($wpl_field, $mls_value, $mls_listing, $mls_field, $field_options);
			
				if(isset($return['value']) and is_array($return['value']))
                {
                    $i = 0;
                    foreach($return['value'] as $value)
                    {
                        $arr['wpl_value'] = isset($value) ? $value : $mls_value;
                        $wpl_table_column = isset($return['table_column'][$i]) ? $return['table_column'][$i] : $wpl_field['table_column'];
                        $i++;
                        
                        /** skip to next field **/
                        if(!trim($wpl_table_column)) continue;

                        $arr['wpl_table_column'] = $wpl_table_column;
                        $wpl_listing[$wpl_table_column] = $arr;
                    }
				}
				else
				{
					$arr['wpl_value'] = isset($return['value']) ? $return['value'] : $mls_value;
					$wpl_table_column = isset($return['table_column']) ? $return['table_column'] : $wpl_field['table_column'];
					$arr['wpl_table_column'] = $wpl_table_column;
				
					$wpl_listing[$wpl_table_column] = $arr;
				}
			}
			
			/** run after mapping function **/
			$wpl_listing = $mapper->after_mapping($wpl_listing, $mls_query_id, $mls_listing);
			
			/** add listing to the final results **/
			$mapped[] = $wpl_listing;
		}
		
		/** save options **/
		foreach($field_options as $wpl_field_key => $select_options) 
		{
			/** only save if count is changed **/
			$count_values = $select_options['values'] ? count($select_options['values']) : 0 ;
			$count_params = $select_options['params'] ? count($select_options['params']) : 0 ;
			$after_count = $count_values + $count_params;

			if ($field_options_checker[$wpl_field_key] != $after_count)
				wpl_flex::update('wpl_dbst', $wpl_field_key, 'options', wpl_db::escape(json_encode($select_options)));
		}
		
		return $mapped;
	}
	
	/**
		Developed by : Howard
		Inputs : mls_query_id
		Outputs : select text
		Date : 2013-12-16
	**/
	public static function generate_selects_query($mls_query_id)
	{
		/** first validation **/
		if(trim($mls_query_id) == '') return false;
		
		$mls_query = wpl_addon_mls::get_mls_queries($mls_query_id);
		$mappings = wpl_addon_mls::get_fields('', " AND `mls_server_id`='{$mls_query->mls_server_id}' AND `mls_class_id`='{$mls_query->mls_class_id}' AND `wpl_field_id`!=''");
		
		$selects = '';
		foreach($mappings as $mapping)
		{
			$selects .= $mapping->field_id.', ';
		}
		
		$selects = trim($selects, ', ');
		wpl_db::set('wpl_addon_mls_queries', $mls_query_id, 'selects', $selects);
		
		return $selects;
	}
	
	/**
		Developed by : Howard
		Inputs : mapped_data, mls_query_id and wpl_unique_table_column
		Outputs : wpl property ids
		Date : 2013-12-17
	**/
	public static function import_mapped_data($mapped_data, $mls_query_id = NULL, $wpl_unique_table_column = NULL, $mls_query = '')
	{
		$wpl_unique_field = wpl_addon_mls::get_wpl_unique_field($mls_query_id);
		$unique_table_column = $wpl_unique_table_column ? $wpl_unique_table_column : $wpl_unique_field['table_column'];
		$row_query = wpl_addon_mls::get_mls_queries($mls_query_id);
		$mls_category = $row_query->mls_class_name;
		
		//Logs
		if($mls_query)
		{
			$section = 'Cron Job';
		}
		else
		{
			$section = 'Backend Import';
			$mls_query = $row_query->query;	
		}
		
        $log_params = array();
		$log_params['Query'] = $mls_query;
		$log_params['Category'] = $mls_category;
		$log_params['Section'] = $section;
		$log_params['User_id'] = $row_query->default_user_id;
        
		return wpl_property::import($mapped_data, $unique_table_column, '', 'mls', false, $log_params);
	}
	
	/**
		Developed by : Howard
		Inputs : mls_query_id, mls_server_id
		Outputs : wpl unique field name
		Date : 2013-12-17
		Description : Use this function for converting mls usnique field to WPl unique field for example "ListingID" to "mls_id"
	**/
	public static function get_wpl_unique_field($mls_query_id, $mls_server_id = '')
	{
		if(!$mls_server_id)
		{
			$mls_query = wpl_addon_mls::get_mls_queries($mls_query_id);
			$mls_server = wpl_addon_mls::get_servers($mls_query->mls_server_id);
			
			$query = "SELECT `wpl_field_id` FROM `#__wpl_addon_mls_mappings` WHERE 1 AND `mls_server_id`='{$mls_query->mls_server_id}' AND `mls_class_id`='{$mls_query->mls_class_id}' AND `field_id`='{$mls_server->mls_unique_field}' AND `wpl_field_id`!=''";
		}
		else
		{
			$mls_server = wpl_addon_mls::get_servers($mls_server_id);
			
			$query = "SELECT `wpl_field_id` FROM `#__wpl_addon_mls_mappings` WHERE 1 AND `mls_server_id`='$mls_server_id' AND `field_id`='{$mls_server->mls_unique_field}' AND `wpl_field_id`!='' LIMIT 1";
		}
		
		$wpl_field_id = wpl_db::select($query, 'loadResult');
		$wpl_field = wpl_flex::get_field($wpl_field_id);
		
		return array('wpl_field_id'=>$wpl_field_id, 'table_column'=>$wpl_field->table_column);
	}
	
	/**
		Developed by : Howard
		Inputs : property_ids, mls_server_id, import_limit, force_to_update
		Outputs : count of images array
		Date : 2013-12-17
		Description : Use this function for saving image of some properties
	**/
	public function import_properties_images($property_ids, $mls_server_id, $import_limit = -1, $force_to_update = false)
	{
		$results = array();
		foreach($property_ids as $property_id)
		{
			$results[$property_id] = $this->import_property_images($property_id, $mls_server_id, $import_limit, $force_to_update);
		}
		
		return $results;
	}
	
	/**
		Developed by : Howard
		Inputs : property_id, mls_server_id, import_limit, force_to_update
		Outputs : count images
		Date : 2013-12-17
		Description : Use this function for saving image of one property
	**/
	public function import_property_images($property_id, $mls_server_id, $import_limit = -1, $force_to_update = true)
	{
        if($this->mls_server_data->custom_image_resource != null)
		{
			$this->custom_import_property_images($property_id, $mls_server_id, $import_limit, $force_to_update);
			return;
		}
        
		/** remove current images **/
		$query = "SELECT * FROM `#__wpl_items` WHERE 1 AND `parent_id`='$property_id' AND `parent_kind`='0' AND `item_type`='gallery' AND (`item_extra3`='mls' OR `item_name` LIKE '%MLS_external%') ORDER BY `index` ASC";
		$galleries = wpl_db::select($query, 'loadObjectList');
		
		/** skip if force to update is false and images already imported **/
		if(!$force_to_update and count($galleries)) return count($galleries);

		$unique_filed = $this->get_wpl_unique_field('', $mls_server_id);
		$listing_id = wpl_db::get($unique_filed['table_column'], 'wpl_properties', 'id', $property_id);

		if(0 === $listing_id or empty($listing_id)) return;
		
        $image_location = $this->mls_server_data->image_location;

		try
		{
		    $images = $this->rets->GetObject($this->mls_server_data->resource, $this->mls_server_data->image_resource, $listing_id, '*', $this->mls_server_data->image_location);
		}
		catch(Exception $e)
		{
		    wpl_logs::add($e->getMessage() . __(' For listing #'.$listing_id, 'wpl'), 'MLS Import Images', $status = 1, '', $addon_id = '1', 3);
		}
		
		if(!empty($this->mls_server_data->alt_image_resource))
		{
		    $image_location = $this->mls_server_data->alt_image_location;
		    try
		    {
		        $alt_images = $this->rets->GetObject($this->mls_server_data->resource, $this->mls_server_data->alt_image_resource, $listing_id, '*', $this->mls_server_data->alt_image_location);
		    }
		    catch(Exception $e)
		    {
		        wpl_logs::add($e->getMessage() . __(' For listing #'.$listing_id, 'wpl'), 'MLS Import Alt. Images', $status = 1, '', $addon_id = '1', 3);
		    }
		}
		
		foreach($images as $image)
		{
            if($image->isError())
            {
                $error = $image->getError();
                wpl_logs::add($error->getMessage() . __(' For listing #'.$listing_id.'. Using Alt Image Object', 'real-estate-listing-realtyna-wpl'), 'MLS Import Images', $status = 1, '', $addon_id = '1', 3);
                $images = $alt_images;
                break;
            }
		}
		
		$property_folder = wpl_items::get_path($property_id);
        
        /** Deleting property thumbnails */
        $clear_property_thumbnails = wpl_global::get_setting('clear_thumbnails_after_update', 1);
        if($clear_property_thumbnails) wpl_property::clear_property_thumbnails($property_id);
        
		$i = 1;
		if(count($images) > 0)
		{
			foreach($galleries as $gallery) wpl_items::delete_file($gallery->item_name, $property_id, 0);

            $image_trim = wpl_global::get_setting('mls_image_trim');
            
			// Skip the first image, if the associated field is enabled.
			if(count($images) > 1)
			{
				$skip_first_image = isset($this->mls_server_data->skip_first_image) ? $this->mls_server_data->skip_first_image : 0;
				if($skip_first_image) unset($images[0]);
			}

			foreach($images as $image)
			{
                if($image->isError())
                {
	                $error = $image->getError();
	                wpl_logs::add($error->getMessage() . __(' For listing #'.$listing_id, 'real-estate-listing-realtyna-wpl'), 'MLS Import Images', $status = 1, '', $addon_id = '1', 3);
	                continue;
                }

				if($import_limit != -1 and $i > $import_limit) break;

				$image_content_type = $image->getContentType();
				$content_type = !empty($image_content_type) ? strtolower($image_content_type) : 'image/jpeg';
				
                $ex_ct = explode(';', $content_type);
                $content_type = $ex_ct[0];
        
				if($content_type == 'image/jpeg' or $content_type == 'image/jpg' or $content_type == 'image/pjpeg') $extension = 'jpg';
				elseif($content_type == 'image/png') $extension = 'png';
				elseif($content_type == 'image/gif') $extension = 'gif';

				if(!$extension)
				{
					if($image_location)
					{
						$image_location = $image->getLocation();
						$ext_image = explode('/', $image_location);
						$ext_image_type = explode('.', end($ext_image));
						$array_ext = array('jpg','png','gif');
						$extension = in_array(strtolower($ext_image_type[1]), $array_ext) ? $ext_image_type[1] : NULL;

						if(!$extension)
						{
							$url_headers = get_headers($image_location, 1);
							$array_types = array('image/jpeg'=>'jpg', 'image/jpg'=>'jpg', 'image/pjpeg'=>'jpg', 'image/png'=>'png', 'image/gif'=>'gif');

							if(is_array($url_headers['Content-Type']))
							{
								$images_type_keys = array_keys($array_types);
								foreach($url_headers['Content-Type'] as $content_type)
								{
									if(in_array(strtolower($content_type), $images_type_keys)) $extension = $array_types[strtolower($content_type)];
								}
							}
							else $extension = $array_types[strtolower($url_headers['Content-Type'])];
						}
					}
				}

				if($extension)
				{
					$extension = strtolower($extension);
                    $image_path = '';
                    $image_object_id = $image->getObjectId();
	                $index = !empty($image_object_id) ? $image_object_id : $i;
						
                    // External Image
                    if($image_location)
                    {
	                    $image_location = $image->getLocation();

	                    if($this->mls_server_data->download_external_image)
	                    {
		                    $item_name = 'MLS'.$i.'.'.$extension;
		                    $content = file_get_contents($image_location);
                            
                            $image_path = $property_folder .DS. $item_name;
		                    wpl_file::write($image_path, $content);
                            
		                    wpl_items::save(array('parent_kind'=>'0', 'parent_id'=>$property_id, 'item_type'=>'gallery', 'item_cat'=>'image', 'item_name'=>$item_name, 'item_extra3'=>'mls', 'index'=>$index));
	                    }
	                    else
	                    {
		                    $item_name = 'MLS_external'.$i.'.'.$extension;
		                    wpl_items::save(array('parent_kind'=>'0', 'parent_id'=>$property_id, 'item_type'=>'gallery', 'item_cat'=>'external', 'item_name'=>$item_name, 'item_extra3'=>$image_location, 'index'=>$index));
	                    }
                    }
                    else
                    {
                        $item_name = 'MLS'.$i.'.'.$extension;
	                    $content = $image->getContent();
                        
                        $image_path = $property_folder .DS. $item_name;
                        wpl_file::write($image_path, $content);
                        
                        wpl_items::save(array('parent_kind'=>'0', 'parent_id'=>$property_id, 'item_type'=>'gallery', 'item_cat'=>'image', 'item_name'=>$item_name, 'item_extra3'=>'mls', 'index'=>$index));
                    }
                    
                    // Trim White Spaces if enabled and needed
                    if($image_trim and trim($image_path))
                    {
                        wpl_images::trim_white_spaces($image_path, $image_path);
                    }
                    
					$i++;
				}
			}
		}
		
		return count($images);
	}
	
	/**
		Developed by : Howard
		Inputs : property_id
		Outputs : void
		Date : 2013-12-18
		Description : Use this function for finalizing the properties after import
	**/
	public function finalize($property_id, $mode = 'edit')
	{
		$user_id = wpl_property::get_property_user($property_id);
		
        /** Get Mapper Object **/
        $mapper = new wpl_addon_mls_mapper($this->rets);
        
		/** run MLS before finalize function **/
		$mapper->before_finalize($property_id, $mode, $user_id);
		
		/** run WPL finalize function **/
		wpl_property::finalize($property_id, $mode, $user_id);
		
		/** run MLS after finalize function **/
		$mapper->after_finalize($property_id, $mode, $user_id);
	}
	
	/**
     * Returns WPL selected fields of MLS
     * @author Natan <natan@realtyna.com>
     * @static
     * @param int $mls_server_id, varchar $mls_class_id
     * @return array
    */
    public static function get_wpl_selected_fields($mls_server_id, $mls_class_id)
    {
        $query = "SELECT `wpl_field_id` FROM `#__wpl_addon_mls_mappings` WHERE 1 AND `mls_server_id`='$mls_server_id' AND `mls_class_id`='$mls_class_id' AND `wpl_field_id`!=''";
		$fields = wpl_db::select($query, 'loadAssocList');
        
        $wpl_fields = array();
        foreach($fields as $field)
        {
            $wpl_fields[] = $field['wpl_field_id'];
        }
        
        return $wpl_fields;
    }
	
	/**
		Developed by : Natan
		Inputs : Added unique_ids, Updated unique_ids, Params
		Outputs : void
		Date : 2014-09-28
		Description : Use this function for Logging after import
	**/
	public static function log($added, $updated, $log_params)
	{
		$log = '';
		if(count($added) > 0) $log .= implode(', ', $added).(count($added) > 1 ? ' were ':' was ').'added AND ';
		else $log .= 'Nothing added AND ';
        
		if(count($updated) > 0) $log .= implode(', ', $updated).(count($updated) > 1 ? ' were ':' was ').'updated.';
		else $log .= 'Nothing updated.';
		
		$params='Query: '.$log_params['Query'].', MLS Class Name: '.$log_params['Category'];
		wpl_logs::add($log, $log_params['Section'], 1, $log_params['User_id'], 1, 3, $params);
	}
    
    /**
	 * Use this function for saving image of one property from other MLS resource
	 * @author Matthew N. <matthew@realtyna.com>
	 * @param  int  $property_id
	 * @param  int  $mls_server_id
	 * @param  int $import_limit
	 * @param  boolean $force_to_update
	 * @return integer count of images
	 */
	public function custom_import_property_images($property_id, $mls_server_id, $import_limit = -1, $force_to_update = false)
	{
		$custom_recource = trim($this->mls_server_data->custom_image_resource);
		if(!stristr($custom_recource, ':')) return false;

		$resuorce_fields = explode(':', $custom_recource);

		/** remove current images **/
		$query = "SELECT * FROM `#__wpl_items` WHERE 1 AND `parent_id`='$property_id' AND `parent_kind`='0' AND `item_type`='gallery' AND (`item_extra3`='mls' OR `item_cat`='external') ORDER BY `index` ASC";
		$galleries = wpl_db::select($query, 'loadObjectList');

		/** skip if force to update is false and images already imported **/
		if(!$force_to_update) return count($galleries);

		foreach($galleries as $gallery) wpl_items::delete_file($gallery->item_name, $property_id, 0);
        
        /** Deleting property thumbnails */
        $clear_property_thumbnails = wpl_global::get_setting('clear_thumbnails_after_update', 1);
        if($clear_property_thumbnails) wpl_property::clear_property_thumbnails($property_id);

		$unique_filed = $this->get_wpl_unique_field('', $mls_server_id);
		$listing_id = wpl_db::get($unique_filed['table_column'], 'wpl_properties', 'id', $property_id);
		
		/**
		 * $resource_fields[0] = MLS photo resource
		 * $resource_fields[1] = MLS photo class
		 * $resource_fields[2] = MLS photo listing id field
		 * $resource_fields[3] = Additional Parameter/MLSlistings
		 * @var [type]
		 */
		$query = "($resuorce_fields[2]=$listing_id)";
		if(isset($resuorce_fields[3])) $query ="($query,$resuorce_fields[3])";

		try
        {
        	$search_photos = $this->rets->Search($resuorce_fields[0], $resuorce_fields[1], $query);
		}
        
        catch(Exception $e)
        {
        	echo $e->getMessage; 
        }
		$property_folder = wpl_items::get_path($property_id);

		$i = 1;
        $cmh = curl_multi_init();
		$images = array();
        
		foreach($search_photos as $photos_object)
		{
			$photos_object = $photos_object->toArray();
            
			// Skip the first image, if the associated field is enabled.
			if($i == 1)
			{
				$skip_first_image = isset($this->mls_server_data->skip_first_image) ? $this->mls_server_data->skip_first_image : 0;

				if($skip_first_image)
				{
					$i++;
					continue;
				}
			}

			if($import_limit != -1 and $i > $import_limit) break;

			$url = '';
			if(isset($photos_object['MediaURL'])) $url = $photos_object['MediaURL'];
			else
			{
				$validation = "/^[^\?]+\.(jpg|jpeg|gif|png)(?:\?|$)/i";
				foreach($photos_object as $value)
				{
					if(preg_match($validation, $value)) $url = $value;
				}
			}

            if(!isset($photos_object['FileExtension']))
			{
				$ext = explode('.', $url);
				$photos_object['FileExtension'] = '.'.end($ext);
			}

			$extension = '';

			if(stristr($photos_object['FileExtension'], 'jp')) $extension = 'jpg';
			elseif(stristr($photos_object['FileExtension'], 'png')) $extension = 'png';
			elseif(stristr($photos_object['FileExtension'], 'gif')) $extension = 'gif';

			if(isset($photos_object['place'])) $index = $photos_object['place'].'.00';
			elseif(isset($photos_object['index'])) $index = $photos_object['index'].'.00';
			else $index = $i.'.00';
			
			if($extension != '')
			{
				if($this->mls_server_data->download_external_image)
				{
	                $ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_HEADER, false);
					curl_setopt($ch, CURLOPT_AUTOREFERER, true);
					curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
					curl_setopt($ch, CURLOPT_TIMEOUT, 120);
					curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					
					curl_multi_add_handle($cmh, $ch);
					
					$images[] = array('ch'=>$ch, 'index'=>$index, 'item_name'=>'MLS'.$i.'.'.$extension);
				}
				else
				{
					$images[] = array('ch'=>$url, 'index'=>$index, 'item_name'=>'MLS'.$i.'.'.$extension);
				}
				
				$i++;
			}
		}
        
        if($this->mls_server_data->download_external_image)
		{
	        $running = NULL;
			
			do
			{
				curl_multi_exec($cmh, $running);
			}
			while($running > 0);
		}

		$imported_images = array();

		foreach($images as $image)
		{
			if(in_array($image['item_name'], $imported_images)) continue;
			$image_data = array();
			$ch = $image['ch'];
			$item_name = $image['item_name'];
			$index = $image['index'];
            
            if($this->mls_server_data->download_external_image)
            {
				$content = curl_multi_getcontent($ch);
	            if(curl_getinfo($ch, CURLINFO_HTTP_CODE) != 200) continue;
				
				wpl_file::write($property_folder .DS. $item_name, $content);
				$image_data = array(
					'parent_kind'	=> '0',
					'parent_id'		=> $property_id,
					'item_type'		=> 'gallery',
					'item_cat'		=> 'image',
					'item_name'		=> $item_name,
					'item_extra3'	=> 'mls',
					'index'			=> $index
				);					
				curl_multi_remove_handle($cmh, $ch);
				curl_close($ch);
			}
			else
			{
				$image_data = array(
					'parent_kind'	=> '0',
					'parent_id'		=> $property_id,
					'item_type'		=> 'gallery',
					'item_cat'		=> 'external',
					'item_name'		=> $item_name,
					'item_extra3'	=> $ch,
					'index'			=> $index
				);	
			}
			wpl_items::save($image_data);

		}
		
		curl_multi_close($cmh);
		return count($search_photos);
	}
    
    /**
     * Export Mapping for a file
     * @author Matthew  <matthew@realtyna.com>
     * @static
     * @param  string $format File Format
     * @return object 		  Settings File
     */
    public static function export_mapping($mls_class_id, $format = 'json')
    {
        $query_select_fields = "SELECT `mapping`.`id` as `mapping_id`,`mapping`.`field_id`, `mapping`.`wpl_field_id`, `mapping`.`mls_class_id`, `mapping`.`custom1`, `dbst`.`id` AS `dbst_id`, `dbst`.`kind`, `dbst`.`name`, `dbst`.`options`, `dbst`.`table_column`, `dbst`.`type`, `dbst`.`category`, `dbst`.`searchmod` FROM `#__wpl_dbst` AS `dbst` INNER JOIN `#__wpl_addon_mls_mappings` AS `mapping` ON `dbst`.`id`=`mapping`.`wpl_field_id` WHERE `mapping`.`mls_class_id`='$mls_class_id'";
        $fields = wpl_db::select($query_select_fields, 'loadObjectList');
        
        $generate_fields = array();
        foreach($fields as $field) $generate_fields[$field->mapping_id] = $field;
        
    	if($format == 'json') return json_encode($generate_fields);
    	elseif($format == 'xml')
    	{
    		$xml = new SimpleXMLElement('<wpl_mls_mapping/>');
    		foreach($generate_fields as $k=>$v) $xml->addChild($k, htmlspecialchars($v));
            
		    return $xml->asXML();
    	}
    	else return NULL;
    }
    
    /**
     * Import Settings from a file
     * @author Matthew  <matthew@realtyna.com>
     * @static
     * @param  string  $file Settings File
     * @return boolean		 Result
     */
    public static function import_mapping($file, $mls_server_id)
    {
    	$content = wpl_file::read($file);
    	$ext = wpl_file::getExt($file);

    	if($ext == 'json')
    	{
    		$mappings = json_decode($content);
	    	if(!$mappings) return false;
    	}
    	elseif($ext == 'xml')
    	{
    		$mappings = simplexml_load_string($content);
			if(!$mappings) return false;
            
			$mappings = (array) $mappings;
    	}
    	else return false;

    	foreach($mappings as $id=>$value)
        {
            $mls_field_id = $value->field_id;
            $mls_field_class_id = $value->mls_class_id;
            $mls_field_custom1 = $value->custom1;
            $wpl_field_name = $value->name;
            $wpl_field_id = $value->dbst_id;
            $wpl_field_type = $value->type;
            $wpl_field_options = $value->options;
            $wpl_field_kind = $value->kind;
            $wpl_field_category = $value->category;
            $wpl_field_table_column = $value->table_column;
            $wpl_field_table_searchmod = isset($value->searchmod) ? $value->searchmod : 0;

            $check_exist_wpl_field = wpl_flex::get_field($wpl_field_id);

            if(!empty($check_exist_wpl_field) and $wpl_field_id < 3000)
            {
                $wpl_field_array_update = array(
                    'name'=>$wpl_field_name,
                    'type'=>$wpl_field_type,
                    'options'=>$wpl_field_options,
                    'kind'=>$wpl_field_kind,
                    'category'=>$wpl_field_category,
                    'searchmod'=>$wpl_field_table_searchmod
                );
                wpl_db::update('wpl_dbst', $wpl_field_array_update, 'id', $wpl_field_id);
            }
            else
            {
	            $wpl_field_id = self::find_mapped_field_by_field_id($mls_field_id, $mls_server_id);

	            if(!$wpl_field_id)
	            {
		            $wpl_field_id = wpl_flex::create_default_dbst(0, $wpl_field_table_searchmod);
		            wpl_flex::run_dbst_type_queries($wpl_field_id, $wpl_field_type, $wpl_field_kind, 'add');

		            $wpl_field_array_update = array(
				            'name'=>$wpl_field_name,
				            'options'=>$wpl_field_options,
				            'category'=>$wpl_field_category,
				            'type'=>$wpl_field_type
		            );
		            wpl_db::update('wpl_dbst', $wpl_field_array_update, 'id', $wpl_field_id);
	            }
            }

	        wpl_db::q("UPDATE `#__wpl_addon_mls_mappings` SET `wpl_field_id`='{$wpl_field_id}',`custom1`='{$mls_field_custom1}' WHERE `mls_server_id`='{$mls_server_id}' AND `mls_class_id`='{$mls_field_class_id}' AND `field_id`='{$mls_field_id}'",'update');
        }

    	return true;
    }
    
    /**
     * @author Matthew <matthew@realtyna.com>
     * @param array $mls_server
     * @param string $field_lookupName
     * @param string/int $field_id
     * @param int $mls_class_id
     * @return type
     */
    public function add_mls_field_values($mls_server, $field_lookupName, $field_id, $mls_class_id)
    {
        if(empty($field_lookupName)) return;

        try
        {
	        $field_values = $this->rets->GetLookupValues($mls_server->resource, $field_lookupName);
        }
        catch(Exception $e) {return;}

        if(empty($field_values)) $field_values = array();

	    $values = array();
	    foreach($field_values as $field_value) $values[] = array('MetadataEntryID'=>$field_value['MetadataEntryID'],'Value'=>$field_value['Value'],'ShortValue'=>$field_value['ShortValue'],'LongValue'=>$field_value['LongValue']);

        $values_str = json_encode($values);

        $query = "UPDATE `#__wpl_addon_mls_mappings` SET `mls_field_values`='".wpl_db::escape($values_str)."' WHERE `field_id`='".$field_id."' AND `mls_server_id`='".$this->mls_server_id."' AND `mls_class_id`='".$mls_class_id."' ";
        wpl_db::q($query);
    }
    
    /**
     * Create a defult query and generate relevant Status Query!
     * @author Matthew <matthew@realtyna.com>
     * @return echo json
     */
    public function create_default_query($mls_server_id, $mls_class_id, $mls_class_name, $generate_query = null)
    {
        if(!$mls_class_id) return;
        
        $query = "SELECT `id` FROM `#__wpl_addon_mls_queries` WHERE `mls_class_id` = '$mls_class_id' and `mls_server_id`='{$mls_server_id}'";
        $exist_mls_class = wpl_db::select($query, 'loadResult');
        
        if($exist_mls_class) return json_encode(array('success'=>0, 'message'=>__('This category already exists!', 'real-estate-listing-realtyna-wpl')));

        if(!$generate_query) $generate_query = $this->generate_mls_class_query($mls_class_id);
        $insert_query = "INSERT INTO `#__wpl_addon_mls_queries` (`mls_server_id`,`mls_class_id`,`mls_class_name`,`default_user_id`,`images`,`enabled`,`last_sync_date`,`query`,`limit`,`import_limit`) VALUES ('$mls_server_id','$mls_class_id','$mls_class_name','1','-1','1','2016-01-01','$generate_query',10,10)";
        $id = wpl_db::q($insert_query, 'insert');
        
        if(!empty($id) and $id != 0) return json_encode(array('success'=>1, 'message'=>__('The query was added successfully!', 'real-estate-listing-realtyna-wpl')));
        else return json_encode(array('success'=>0, 'message'=>__('An error occurred! Please, try again.', 'real-estate-listing-realtyna-wpl')));
    }
    
    /**
     * Generate a default query/criteria for created MLS query
     * @author Matthew <matthew@realtyna.com>
     * @param int $mls_class_id
     * @return string
     */
    private function generate_mls_class_query($mls_class_id)
    {
        if(empty($mls_class_id)) return;
        
        $query = "SELECT `field_id`,`mls_field_values` FROM `#__wpl_addon_mls_mappings` WHERE `mls_class_id`='$mls_class_id' AND `field_name` LIKE '%status%' AND `mls_field_searchable`=1";
        
        $result = wpl_db::select($query, 'loadAssoc');
        
        if(!empty($result))
        {
	        $values = !empty($result['mls_field_values']) ? json_decode($result['mls_field_values'], true) : array();
            
            $status_query = '';
            $active_values = array('act','active');
            
            foreach($values as $value)
            {
                if(in_array(strtolower($value['LongValue']), $active_values))
                {
                    $status_query = '(';
                    $status_query .= $result['field_id'].'=|'.$value['Value'];
                    $status_query .= '),';
                    break;
                }
            }
            $status_query = rtrim($status_query, ',');
            $main_query = '('.$status_query.')';
            
            return $main_query;
        }
        else return;
    }

	/**
	 * Generate count days on website
	 * @author Matthew <matthew@realtyna.com>
	 * @param int $pid
	 * @param string $added_date_field
	 * @return int
	 */
	public function listing_count_days($pid, $added_date_field = null)
	{
		if(null === $added_date_field) $added_date_field = 'add_date';

		$added_date_value = wpl_db::select("SELECT `{$added_date_field}` FROM `#__wpl_properties` WHERE `id`='{$pid}'", 'loadResult');

		$now = time(); // or your date as well
		$added_date_value = strtotime($added_date_value);
		$datediff = $now - $added_date_value;

		return floor($datediff / (60 * 60 * 24));
	}

	/**
	 * get the column_name of the dbst data.
	 * @string $mls_field_id
	 *
	 * @return bool|string
	 */
	public static function get_mls_field_data($mls_field_id)
	{
		if($mls_field_id == NULL OR !$mls_field_id) return false;

		$query = "SELECT `mapping`.`wpl_field_id`, `mapping`.`custom1`, `dbst`.`id` AS `dbst_id`, `dbst`.`name`, `dbst`.`table_column` FROM `#__wpl_dbst` AS `dbst` INNER JOIN `#__wpl_addon_mls_mappings` AS `mapping` ON `dbst`.`id`=`mapping`.`wpl_field_id` WHERE `mapping`.`field_id`='{$mls_field_id}' LIMIT 1";
		$field = wpl_db::select($query, 'loadAssoc');

		if($field['dbst_id'] == '41')
		{
			if($field['custom1'] == 'zip') $field['table_column'] = 'zip_name';
			else $field['table_column'] = 'location'.$field['custom1'].'_name';
		}

		if(!$field) return false;
		else return $field['table_column'];
	}

	/**
	 * Import mapping files if any classes are imported from connection wizard
	 * @author Matthew <matthew@realtyna.com>
	 * $imported_classes (array)
	 * @return JSON
	 */
	public function import_mapping_files_connection_wizard($imported_classes = null, $mls_name=null)
	{
		if(!$imported_classes) return array('success'=>0, 'message'=>__('There is no imported classes value!', 'real-estate-listing-realtyna-wpl'));

		if(!$mls_name) $mls_name = 'mls_mapping_file';

		$mls_server = $this->mls_server_data;
		$tmp_directory = wpl_global::init_tmp_folder();
		$tmp_extract_directory = wpl_global::init_tmp_folder();

		$mls_url = parse_url($mls_server->url);
		$mls_website_name = $mls_url['host'];

		$mapping_file_link = 'http://billing.realtyna.com/io/files/mappings/'.$mls_website_name.'.zip';

		$file = fopen($tmp_directory.$mls_name.'.zip', 'w+');

		try {
			$buffer = wpl_global::get_web_page($mapping_file_link);
			fwrite($file, $buffer);
			fclose($file);
		}
		catch(Exception $e) {
			return array('success'=>0, 'message'=>__($e->getMessage(), 'real-estate-listing-realtyna-wpl'));
		}

		wpl_global::zip_extract($tmp_directory.$mls_name.'.zip', $tmp_extract_directory);

		$array_mapping_files = array();
		$files = glob($tmp_extract_directory . '*.json');
		foreach($files as $class_files)
		{
			$exp_class_file = explode('/', $class_files);
			$class_id = end($exp_class_file);

			$clear_class_id = str_replace('mapping_', '', $class_id);
			$clear_class_id = str_replace(array('.json', '.xml'), '', $clear_class_id);

			$array_mapping_files[$clear_class_id] = $class_files;
		}

		foreach($imported_classes as $class_id => $class_name)
		{
			if(isset($array_mapping_files[$class_id])) self::import_mapping($array_mapping_files[$class_id], $mls_server->id);
		}

		return array('success'=>1, 'message'=>__('The mapping files are imported successfully!', 'real-estate-listing-realtyna-wpl'));
	}

	/** Return a user_id by MLS Agent ID
	 * @author Matthew N. <matthew@realtyna.com>
	 * @param $agent_id
	 *
	 * @return bool|mixed
	 */
	public static function mls_agent_mapping_get_user_id($agent_id)
	{
		$query = "SELECT `id` FROM `#__wpl_users` WHERE `mls_agent_id`='{$agent_id}' OR `mls_agent_id` LIKE '%,{$agent_id},%'";
		$user_id = wpl_db::select($query, 'loadResult');

		if($user_id and !empty($user_id)) return $user_id;
		else return false;
	}

	/**
	 * Find relevant ID using MLS Field Name to protect duplicate mapping
	 * @author Matthew <matthew@realtyna.com>
	 * @param $mls_field_id
	 * @return array|boolean
	 */
	public static function find_mapped_field_by_field_id($mls_field_id, $mls_server_id)
	{
		if(empty($mls_field_id) or empty($mls_server_id)) return false;

		$mls_field_id = strtolower($mls_field_id);
		$select_mls_field_name = wpl_db::select("SELECT `field_name` FROM `#__wpl_addon_mls_mappings` WHERE `mls_server_id`='{$mls_server_id}' AND LOWER(`field_id`)='{$mls_field_id}' AND `wpl_field_id`!='' LIMIT 1", 'loadResult');

		if(!empty($select_mls_field_name))
		{
			$select_mls_field_name = strtolower($select_mls_field_name);
			$find_mapped_id = wpl_db::select("SELECT `wpl_field_id` FROM `#__wpl_addon_mls_mappings` WHERE `mls_server_id`='{$mls_server_id}' AND LOWER(`field_name`)='{$select_mls_field_name}' AND `wpl_field_id`!='' LIMIT 1", 'loadResult');
			return $find_mapped_id;
		}

		return false;
	}

	/**
	 * Import RETS configurations based on JSON file
	**/
	public static function import_configuration($config_file, $server_id)
	{
		$content = wpl_file::read($config_file);
    	$ext = wpl_file::getExt($config_file);

    	if($ext == 'json')
    	{
    		$configs = json_decode($content);
	    	if(!$configs) return false;
    	}
    	else return false;

    	$mls_connection_config = array(
    				'mls_name'				=> $configs->mls_name,
                    'resource'				=> $configs->resource,
                    'params'				=> $configs->params,
                    'image_resource'		=> $configs->image_resource,
                    'image_location'		=> $configs->image_location,
                    'custom_image_resource'	=> $configs->custom_image_resource,
                    'mls_use_post'			=> $configs->mls_use_post,
                    'skip_first_image'		=> $configs->skip_first_image,
                    'mls_auth_method'		=> $configs->mls_auth_method,
                    'support_offset'		=> $configs->support_offset,
                    'connect_per_class'		=> isset($configs->connect_per_class) ? $configs->connect_per_class : 0,
                    'map_per_class'			=> isset($configs->map_per_class) ? $configs->map_per_class : 0
                );
    	
        wpl_db::update('wpl_addon_mls', $mls_connection_config, 'id', $server_id);
    	return true;
	}

	/**
	 * Estimate Hosting space/inode and port requirements
	**/
	public static function get_requirements()
	{
		$mls_servers = wpl_addon_mls::get_servers();
		$mls_queries = wpl_addon_mls::get_mls_queries();

        foreach ($mls_servers as $server)
        {
            $total = 0;
            $result[$server->mls_name] = array();
            $port = '';
            $consumed_space = 0;
            $total_images = 0;
            $current_images = 0;
            $directory = '';
            $total_listings = 0;

            $port = parse_url($server->url, PHP_URL_PORT);

            if(!empty($port) and $port != '80' and $port != '443') $result[$server->mls_name]['port'] = $port;

            if($server->image_location and !$server->download_external_image)
            {
            	$result[$server->mls_name]['desc'] = 'Images will be loaded via URLs. So file usage will be approximately the same as total listings number and also, less than 10 GB hosting space will be enough.';
            	$result[$server->mls_name]['image'] = 0;
            	
            	continue;
            }
            
            $properties = wpl_db::select("SELECT `id`,`pic_numb` from `#__wpl_properties` WHERE `mls_server_id` = {$server->id}",'loadObjectList');

            if(!count($properties)) continue;
            
            foreach ($properties as $property)
            {
                $size = 0;
                $directory = wpl_items::get_path($property->id);
                
                foreach (glob(rtrim($directory, '/').'/*', GLOB_NOSORT) as $file)
                {
                    $size += is_file($file) ? filesize($file) : 0;
                }

                $consumed_space += $size;
                $current_images += $property->pic_numb;
            }

            $kb_per_img = floor($consumed_space / $current_images / 1024);
            $avg_img = floor($current_images / count($properties));

            foreach ($mls_queries as $mls_query)
            {
                if($mls_query->mls_server_id == $server->id )
                {
                    $options = json_decode($mls_query->query_options);
                    $total_listings += $options->total;
                }
            }

            $total_images = $total_listings * $avg_img;
            $total_space = $total_listings * $avg_img * $kb_per_img;
            
            $result[$server->mls_name]['image'] = $total_images;
            $result[$server->mls_name]['space'] = $total_space; 
        }

        return $result;
	}
}