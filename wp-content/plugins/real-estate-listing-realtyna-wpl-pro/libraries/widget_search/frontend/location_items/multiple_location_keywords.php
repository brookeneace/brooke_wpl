<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');

if($show == 'mullocationkeys' and !$done_this)
{
    /** current values **/
    $current_value = stripslashes(wpl_request::getVar('sf_mullocationkeys', ''));

    $html .= '<label for="sf'.$widget_id.'_mullocationkeys">'.__($placeholder, 'real-estate-listing-realtyna-wpl').'</label>
    <input type="text" class="wpl_search_widget_location_textsearch" value="'.$current_value.'" id="sf'.$widget_id.'_mullocationkeys" name="sf'.$widget_id.'_mullocationkeys" placeholder="'.__($placeholder, 'real-estate-listing-realtyna-wpl').'">';
	
	$done_this = true;
}