<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');

_wpl_import('libraries.addon_mls');

/**
 * MLS service
 * @author Howard <howard@realtyna.com>
 * @date 12/21/2013
 * @package MLS Add-on
 */
class wpl_service_addon_mls
{
    /**
     * Service runner
     * @author Howard <howard@realtyna.com>
     * @return void
     */
    public function run()
    {
		if(wpl_request::getVar('rets_cron_job') == 1)
		{
			if(wpl_request::getVar('rets_cron_job_type') == 'offline') $this->run_offline();
			elseif(wpl_request::getVar('rets_cron_job_type') == 'crea') $this->run_crea_offline();
	    	else $this->run_online();
		}
		elseif(wpl_request::getVar('rets_import_cron_job') == 1) $this->import_data();
		elseif(wpl_request::getVar('rets_import_images_cron_job') == 1) $this->import_images();
		elseif(wpl_request::getVar('rets_purge_cron_job') == 1) $this->purge_listing();
		elseif(wpl_request::getVar('firewall_test') == 1) $this->firewall_test();
		elseif(wpl_request::getVar('sync_mls_listing') == 1) $this->sync_mls_listing();
		elseif(wpl_request::getVar('mls_stats') == 1) $this->mls_stats();
		elseif(wpl_request::getVar('purge_older_listings') == 1) $this->purge_older_listings();
		elseif(wpl_request::getVar('openhouse_tags_status') == 1) $this->openhouse_tags_status();
		elseif(wpl_request::getVar('wpl_update_fields')) $this->update_fields(wpl_request::getVar('wpl_update_fields'));

        // WPL Settings
        $this->settings = wpl_global::get_settings();
        
        // Run the ListTrack Integration when it's enabled and the code is placed
        if(isset($this->settings['mls_listtrack_integration']) and $this->settings['mls_listtrack_integration'] and isset($this->settings['mls_listtrack_code']) and trim($this->settings['mls_listtrack_code']))
        {
            add_action('template_redirect', array($this, 'listtrack'), '99999');
        }
    }

    /**
     * Online RETS import process
     * @author Howard <howard@realtyna.com>
     * @return void
     */
	public function run_online()
	{
		$ids = wpl_request::getVar('rets_query_ids');

		$query = "SELECT * FROM `#__wpl_addon_mls_queries` WHERE (`last_sync_date`='0000-00-00' OR DATE_ADD(`last_sync_date`, INTERVAL `sync_period` DAY)<'".date("Y-m-d H:i:s")."') AND `enabled`>='1'";
		if(is_numeric(str_replace(',', '', $ids))) $query .= " AND `id` IN ({$ids})";
        
		$mls_queries = wpl_db::select($query);
		$rets_objects = array();
		$connection = 0;
		$server_id = 0;
		$connect_per_class = 0;
        
		foreach($mls_queries as $mls_query)
		{
			/** load rets object **/
			if($rets_objects[$mls_query->mls_server_id] and ($server_id == $mls_query->mls_server_id) and !$connect_per_class) $wplrets = $rets_objects[$mls_query->mls_server_id];
			else $wplrets = new wpl_addon_mls($mls_query->mls_server_id);
			
			if(trim($wplrets->mls_server_data->mls_unique_field) == '') continue;

			$connect_per_class = $wplrets->mls_server_data->connect_per_class;
			
			/** set to rets objects **/
			$rets_objects[$mls_query->mls_server_id] = $wplrets;
			
			/** connect **/
			if((!$connection) or ($server_id != $mls_query->mls_server_id) or $connect_per_class) 
			{
				$connection = $wplrets->connect($wplrets->mls_server_data->mls_use_post);
				$server_id = $mls_query->mls_server_id;
			}

			if(!empty($mls_query->selects)) $options = array('Count'=>1, 'Select'=>$mls_query->selects, 'Limit'=>$mls_query->limit);
			else $options = array('Count'=>1, 'Limit'=>$mls_query->limit);

			// support_offset per MLS
			if(isset($wplrets->mls_server_data->support_offset) and $wplrets->mls_server_data->support_offset == 1) $options['Offset'] = $mls_query->offset;
			
			/** set query **/
			$mls_query_string = $mls_query->query;
			$date1 = ($mls_query->last_sync_date == '0000-00-00' ? date('Y-m-01') : $mls_query->last_sync_date);
			$date2 = date('Y-m-d', strtotime('+'.$mls_query->sync_period.' day', strtotime($date1)));

			$params = !empty($mls_query->params) ? json_decode($mls_query->params) : array();

			/** hourly updates, sample: {"pattern":"DTT","last":"15:44:47","tz":"MDT"} **/
			if($mls_query->query_options == 'hourly' and isset($params->pattern))
			{
				if($params->pattern == 'DTT') 
				{
					$mls_query_string = str_replace('date+', $date1.'T'.$params->last.'+', $mls_query_string);
					$query_all_listings = str_replace('date+', $mls_query->start_purge_date.'+', $mls_query->query);
				}
			}
			else
			{
				if(stristr($mls_query_string, 'date+t')) $mls_query_string = str_replace('date+t', $date1.'T00:00:00+', $mls_query_string);
				elseif(strstr($mls_query_string, 'dateT') != '')
				{
					$query_all_listings = str_replace('dateT', $mls_query->start_purge_date.'T00:00:00-'.date('Y-m-d').'T00:00:00', $mls_query->query);
					$mls_query_string = str_replace('dateT', $date1.'T00:00:00-'.$date2.'T', $mls_query_string);
					$mls_query_string = str_replace($date2.'T00:00:00+', $date2.'T00:00:00', $mls_query_string);
				}
				else
				{
					$mls_query_string = str_replace('date+', $date1.'-'.$date2, $mls_query_string);
					$query_all_listings = str_replace('date+', $mls_query->start_purge_date.'+', $mls_query->query);
				}
			}

			try{
				$listings = $wplrets->rets->Search($wplrets->mls_server_data->resource, $mls_query->mls_class_id, $mls_query_string, $options);
				$count = $listings->getTotalResultsCount();
			}
			catch (Exception $e)
			{
				wpl_global::event_handler('wpl_mls_connection_log', array('error_code'=>$e->getCode(), 'error_message'=>wpl_db::escape($e->getMessage())));
				continue;
			}

			if(!$count) continue;
			
			$results = array();
			foreach($listings as $row) $results[$row[$wplrets->mls_server_data->mls_unique_field]] = $row->toArray();

			/** map data **/
			$mapped = $wplrets->map($results, $mls_query->id);
			
			/** import properties **/
			$pids = $wplrets->import_mapped_data($mapped, $mls_query->id, '', $mls_query_string);
			
            $force_to_image_update = $wplrets->mls_server_data->mls_photo_update;
            
			/** download images **/
			if(trim($mls_query->images)) $wplrets->import_properties_images($pids, $mls_query->mls_server_id, $mls_query->images, $force_to_image_update);
			
			/** finalizing properties **/
			foreach($pids as $pid) $wplrets->finalize($pid, (wpl_property::is_finalized($pid) ? 'edit' : 'add'));
			
			/** update **/
			if($count <= $mls_query->limit+$mls_query->offset) self::mls_query_is_done($mls_query->id, $date2, 0, $params);
			else self::update_query($mls_query->id, ($mls_query->offset+$mls_query->limit));

			/** Find Expired listings for purge function **/
			if(date('Y-m-d H:i:s', strtotime('+'.$mls_query->purge_period.' hours', strtotime($mls_query->last_purge_date))) < date('Y-m-d H:i:s'))
			{
			    $params = !empty($mls_query->params) ? json_decode($mls_query->params) : array();
			    $default_offset = isset($params->default_offset) ? $params->default_offset : 0;
                $result = self::fetch_class_listings($wplrets, $mls_query->mls_class_id, $query_all_listings, $default_offset);
                $wpl_unique_field = wpl_addon_mls::get_wpl_unique_field($mls_query->id, $mls_query->mls_server_id);
				$current_properties = wpl_db::select("SELECT count(`id`) FROM `#__wpl_properties` WHERE `source`='mls' AND `mls_query_id` = {$mls_query->id}", 'loadResult');
                $count_properties = ($current_properties * 60) / 100;
                $mls_properties = count($result);
        	
        	    if($mls_properties > $count_properties)
			    {
				    $result = implode("','", $result);
				    $query_delete = "SELECT `id` from `#__wpl_properties` WHERE `".$wpl_unique_field['table_column']."` NOT IN('{$result}') AND `mls_query_id` = {$mls_query->id}";
		            $expired_properties = wpl_db::select($query_delete);

		            foreach($expired_properties as $expired_property) wpl_property::purge($expired_property->id);
			    }
			
			    wpl_db::q("UPDATE `#__wpl_addon_mls_queries` SET `last_purge_date` = NOW() WHERE `id` = '{$mls_query->id}'");
			    wpl_global::event_handler('wpl_mls_purged', array('mls_class_name'=>$mls_query->mls_class_name));
			}
		}
        
        /** Delete expired temporary directories **/
        wpl_global::delete_expired_tmp();
        
        /** cPanel cronjob **/
        exit();
	}
	
	/**
	 * Offline RETS import process
	 * @author Steve A. <steve@realtyna.com>
	 * @return void
	 */
	public function run_offline()
	{
		$ids = wpl_request::getVar('rets_query_ids');

		$query = "SELECT * FROM `#__wpl_addon_mls_queries` WHERE `enabled`>='1'";
		if(is_numeric(str_replace(',', '', $ids))) $query .= " AND `id` IN ({$ids})";

		$mls_queries = wpl_db::select($query);
		$rets_objects = array();
		$connection = 0;
        $server_id = 0;
        $connect_per_class = 0;
		
		foreach($mls_queries as $mls_query)
		{
			/** load rets object **/
			if($rets_objects[$mls_query->mls_server_id] and ($server_id == $mls_query->mls_server_id) and !$connect_per_class) $wplrets = $rets_objects[$mls_query->mls_server_id];
			else $wplrets = new wpl_addon_mls($mls_query->mls_server_id);
			
			if(trim($wplrets->mls_server_data->mls_unique_field) == '') continue;

			$connect_per_class = $wplrets->mls_server_data->connect_per_class;
			
			/** set to rets objects **/
			$rets_objects[$mls_query->mls_server_id] = $wplrets;
			
			/** connect **/
			if((!$connection) or ($server_id != $mls_query->mls_server_id) or $connect_per_class) 
			{
				$connection = $wplrets->connect($wplrets->mls_server_data->mls_use_post);
				$server_id = $mls_query->mls_server_id;
			}
			
            $options = array('Count'=>1);

			// support_offset per MLS
			if(isset($wplrets->mls_server_data->support_offset) and $wplrets->mls_server_data->support_offset == 1) $options['Offset'] = $mls_query->offset;
            
			$today = date('Y-m-d H:i:s');
			
			/** set query **/
			$mls_query_string = $mls_query->query;
			$date = ($mls_query->last_sync_date == '0000-00-00' ? date('2015-01-01') : $mls_query->last_sync_date);
            $date2 = date('Y-m-d', strtotime('+'.$mls_query->sync_period.' day', strtotime($date)));

			$params = !empty($mls_query->params) ? json_decode($mls_query->params) : array();
			if(isset($params->raw_import_limit)) $options['Limit'] = $params->raw_import_limit;

			/** hourly updates, sample: {"pattern":"DTT","last":"15:44:47","tz":"MDT"} **/
			if($mls_query->query_options == 'hourly' and isset($params->pattern))
			{
				if($params->pattern == 'DTT') $mls_query_string = str_replace('date+', $date.'T'.$params->last.'+', $mls_query_string);
			}
			else
			{
				if(stristr($mls_query_string, 'date+s'))
				{
					if($date2 > $today) continue;
					
					$mls_query_string = str_replace('date+s', $date.'-'.$date2, $mls_query_string);
					$today = date('Y-m-d H:i:s', strtotime($date2));
				}
				elseif(stristr($mls_query_string, 'dateT00:00:00Z'))
				{
				    $mls_query_string = str_replace('dateT00:00:00Z', $date.'T00:00:00Z', $mls_query_string);
				}
				elseif(stristr($mls_query_string, 'date+t')) $mls_query_string = str_replace('date+t', $date.'T00:00:00+', $mls_query_string);
				elseif(stristr($mls_query_string, 'dateT'))
				{
					if($date2 > $today) continue;

					$mls_query_string = str_replace('dateT', $date.'T00:00:00-'.$date2.'T00:00:00', $mls_query_string);
					$mls_query_string = str_replace($date2.'T00:00:00+', $date2.'T00:00:00', $mls_query_string);

					$today = date('Y-m-d H:i:s', strtotime($date2));
				}
				else $mls_query_string = str_replace('date+', $date.'+', $mls_query_string);
			}
			
			try {
				$listings = $wplrets->rets->Search($wplrets->mls_server_data->resource, $mls_query->mls_class_id, $mls_query_string, $options);
				$count = $listings->getTotalResultsCount();
			}
			catch (Exception $e)
			{
				wpl_global::event_handler('wpl_mls_connection_log', array('error_code'=>$e->getCode(), 'error_message'=>wpl_db::escape($e->getMessage())));
				continue;
			}

			// example: params column = {"default_offset":"1"}
			$default_offset = isset($params->default_offset) ? $params->default_offset : 0;

			if(!$count and (stristr($mls_query->query, 'date+s') OR stristr($mls_query->query, 'dateT')))
			{
				self::mls_query_is_done($mls_query->id, $today, $default_offset, $params);
				continue;
			}

			$num = 0;
			$ids = array();

			foreach($listings as $row)
			{
				$row = $row->toArray();

				$unique_value = $row[$wplrets->mls_server_data->mls_unique_field];
				$exists = wpl_db::select("SELECT `unique_value` FROM `#__wpl_addon_mls_data` WHERE `unique_value` = '{$unique_value}' AND (`mls_server_id` != '0' AND `mls_server_id` = '{$mls_query->mls_server_id}')", 'loadResult');

				$data = base64_encode(json_encode($row));

				if(!empty($exists)) wpl_db::q("UPDATE `#__wpl_addon_mls_data` SET `content` = '{$data}', `date` = '{$today}', `imported` = 0 WHERE `unique_value` = '{$unique_value}' AND `mls_server_id` = '{$mls_query->mls_server_id}'");
				else wpl_db::q("INSERT INTO `#__wpl_addon_mls_data` (`mls_server_id`, `mls_query_id`, `unique_value`, `content`, `date`) VALUES ('{$mls_query->mls_server_id}', '{$mls_query->id}', '{$unique_value}', '{$data}', '{$today}')");

				$ids[] = $unique_value;
				$num++;
			}

			/** update **/
			if($count == 0 && $num == 0) self::mls_query_is_done($mls_query->id, $today, $default_offset, $params);
			elseif($count <= $num) self::mls_query_is_done($mls_query->id, $today, $mls_query->offset, $params);
			else
			{
				if(0 === $num) self::mls_query_is_done($mls_query->id, $today, $default_offset, $params);
				else self::update_query($mls_query->id, ($mls_query->offset+$num));
			}
		}
        
        /** Delete expired temporary directories **/
        wpl_global::delete_expired_tmp();

		/** cPanel cronjob **/
		exit();
	}

	/**
	 * Offline CREA import process
	 * @author Natan Y. <natan@realtyna.com>
	 * @return void
	 */
	public function run_crea_offline()
	{
		$ids = wpl_request::getVar('rets_query_ids');

		$query = "SELECT * FROM `#__wpl_addon_mls_queries` WHERE (`last_sync_date`='0000-00-00' OR DATE_ADD(`last_sync_date`, INTERVAL `sync_period` DAY)<'".date("Y-m-d H:i:s")."') AND `enabled`>='1'";
		if(is_numeric(str_replace(',', '', $ids))) $query .= " AND `id` IN ({$ids})";

		$mls_queries = wpl_db::select($query);
		$rets_objects = array();
		$connection = 0;
        $server_id = 0;
		
		foreach($mls_queries as $mls_query)
		{
			/** load rets object **/
			if($rets_objects[$mls_query->mls_server_id] and ($server_id == $mls_query->mls_server_id) and !$connect_per_class) $wplrets = $rets_objects[$mls_query->mls_server_id];
			else $wplrets = new wpl_addon_mls($mls_query->mls_server_id);
			
			if(trim($wplrets->mls_server_data->mls_unique_field) == '') continue;

			/** set to rets objects **/
			$rets_objects[$mls_query->mls_server_id] = $wplrets;
			
			/** connect **/
			if((!$connection) or ($server_id != $mls_query->mls_server_id) or $connect_per_class) 
			{
				$connection = $wplrets->connect($wplrets->mls_server_data->mls_use_post);
				$server_id = $mls_query->mls_server_id;
			}
			
            $options = array('Count'=>1);
            $today = date('Y-m-d H:i:s');

			// support_offset per MLS
			if(isset($wplrets->mls_server_data->support_offset) and $wplrets->mls_server_data->support_offset == 1) $options['Offset'] = $mls_query->offset;
            
			$params = !empty($mls_query->params) ? json_decode($mls_query->params) : array();
			if(isset($params->raw_import_limit)) $options['Limit'] = $params->raw_import_limit;
			
			try {
				$records = $wplrets->rets->Search($wplrets->mls_server_data->resource, $mls_query->mls_class_id, $mls_query->query, $options);
				$count = $records->getTotalResultsCount();
			}
			catch (Exception $e)
			{
				wpl_global::event_handler('wpl_mls_connection_log', array('error_code'=>$e->getCode(), 'error_message'=>wpl_db::escape($e->getMessage())));
				continue;
			}

			$ids = array();

			foreach($records as $row)
			{
				$row = $row->toArray();
				$ids[] = $row[$wplrets->mls_server_data->mls_unique_field];
			}

			$query_string = "(ID=".implode(',',$ids).")";
			$options = array("Limit"=>100, "Offset"=>1, "Count"=>1);

			try {
				$listings = $wplrets->rets->Search($wplrets->mls_server_data->resource, $mls_query->mls_class_id, $query_string, $options);
			}
			catch (Exception $e)
			{
				wpl_global::event_handler('wpl_mls_connection_log', array('error_code'=>$e->getCode(), 'error_message'=>wpl_db::escape($e->getMessage())));
				continue;
			}

			$num = 0;
			foreach($listings as $listing)
			{
				$listing = $listing->toArray();
				$unique_value = $listing[$wplrets->mls_server_data->mls_unique_field];
				$exists = wpl_db::select("SELECT `unique_value` FROM `#__wpl_addon_mls_data` WHERE `unique_value` = '{$unique_value}' AND (`mls_server_id` != '0' AND `mls_server_id` = '{$mls_query->mls_server_id}')", 'loadResult');

				$data = base64_encode(json_encode($listing));

				if(!empty($exists)) wpl_db::q("UPDATE `#__wpl_addon_mls_data` SET `content` = '{$data}', `date` = '{$today}', `imported` = 0 WHERE `unique_value` = '{$unique_value}' AND `mls_server_id` = '{$mls_query->mls_server_id}'");
				else wpl_db::q("INSERT INTO `#__wpl_addon_mls_data` (`mls_server_id`, `mls_query_id`, `unique_value`, `content`, `date`) VALUES ('{$mls_query->mls_server_id}', '{$mls_query->id}', '{$unique_value}', '{$data}', '{$today}')");
				$num++;
			}

			$total_imported = $mls_query->offset+$num;
			if($count < $total_imported) self::mls_query_is_done($mls_query->id, $today, 1, $params);
			else self::update_query($mls_query->id, $total_imported);
		}
        
        /** Delete expired temporary directories **/
        wpl_global::delete_expired_tmp();

		/** cPanel cronjob **/
		exit();
	}
    /**
     * Sets an MLS query to done.
     * @author Howard <howard@realtyna.com>
     * @static
     * @param int $query_id
     * @param date $last_sync_date
     */
	public static function mls_query_is_done($query_id, $last_sync_date, $offset, $params)
	{
		if(!empty($params) and isset($params->pattern))
		{
			$timestamp = time();
            $dt = new DateTime("now", new DateTimeZone($params->tz));
            $dt->setTimestamp($timestamp);
            $dt->modify("-5 minutes");
            
			if(date("H") == 0 and date("i") == 0) $params->last = "00:00:00";
			else $params->last=$dt->format('H:i:s');
			
			$query = "UPDATE `#__wpl_addon_mls_queries` SET `last_sync_date`='".$last_sync_date."', `offset`='".$offset."', `params`='".json_encode($params)."' WHERE `id`='$query_id'";
		}
		else $query = "UPDATE `#__wpl_addon_mls_queries` SET `last_sync_date`='".$last_sync_date."', `offset`='".$offset."' WHERE `id`='$query_id'";
		
		wpl_db::q($query);
	}
	
    /**
     * Updates MLS query
     * @author Howard <howard@realtyna.com>
     * @static
     * @param int $query_id
     * @param int $offset
     */
	public static function update_query($query_id, $offset)
	{
		$query = "UPDATE `#__wpl_addon_mls_queries` SET `offset`='".$offset."' WHERE `id`='".$query_id."'";
		wpl_db::q($query);
	}
	
	/**
     * Fetch all listing for each class based on offset MLS query
     * @author Natan <natan@realtyna.com>
     */
	public function fetch_class_listings($wplrets, $class_id, $query, $offset=0,$limit=0)
	{
		$results = array();

		if($limit) $options = array("Select" => $wplrets->mls_server_data->mls_unique_field, "Limit"=> $limit);
		else $options = array("Select" => $wplrets->mls_server_data->mls_unique_field);
		if(isset($wplrets->mls_server_data->support_offset) and $wplrets->mls_server_data->support_offset == 1) $options['Offset'] = $offset;

		$listings = $wplrets->rets->Search($wplrets->mls_server_data->resource, $class_id, $query, $options);
		
		foreach($listings as $row)
		{
			$row = $row->toArray();
			$results[] = $row[$wplrets->mls_server_data->mls_unique_field];
		}

		/* Checking if the Result count less than Total count, then fetching all Result */
		$count_all = $listings->getTotalResultsCount();
		$count_res = count($results);
		$limit = $count_res;

		if(!empty($results) and $count_all > $count_res)
		{
			$round = round($count_all / $count_res);
			for($i=0; $i <= $round; $i++)
			{
				if($count_all > count($results))
				{
					$offset += $count_res;

					$options = array("Select" => $wplrets->mls_server_data->mls_unique_field, 'Limit'=>$limit);
					if(isset($wplrets->mls_server_data->support_offset) and $wplrets->mls_server_data->support_offset == 1) $options['Offset'] = $offset;

					$search = $wplrets->rets->Search($wplrets->mls_server_data->resource, $class_id, $query, $options);

					foreach($search as $row)
					{
						$row = $row->toArray();
						$results[] = $row[$wplrets->mls_server_data->mls_unique_field];
					}
				}
				else continue;
			}
			$results = array_unique($results);
		}

		return $results;
	}
	
	/**
     * checking to grab all listing
     * @author Natan <natan@realtyna.com>
     */
	public function all_properties_retrieved($total_properties, $division_factor)
	{
		$fraction = $total_properties/$division_factor;
		return is_int($fraction);
	}

	/**
	 * Import properties from offline data
	 * @author Steve A. <steve@realtyna.com>
	 * @return void
	 */
	public function import_data()
	{
		$ids = wpl_request::getVar('rets_query_ids');

		$query = "SELECT * FROM `#__wpl_addon_mls_queries` WHERE `enabled`>='1'";
		
		if(is_numeric(str_replace(',', '', $ids))) $query .= " AND `id` IN ({$ids})";
        
		$mls_queries = wpl_db::select($query);
		$rets_objects = array();
		$connection = 0;
        $server_id = 0;
		
		foreach($mls_queries as $mls_query)
		{
			$query = "SELECT * FROM `#__wpl_addon_mls_data` WHERE `mls_query_id` = '{$mls_query->id}' AND `imported` = 0 ORDER BY `date` LIMIT {$mls_query->import_limit}";
			$mls_data = wpl_db::select($query);
			if(!$mls_data) continue;
			$results = array();
			$ids = array();

			foreach($mls_data as $data)
			{
				$results[$data->unique_value] = (array)json_decode(base64_decode($data->content));
				$ids[] = $data->id;
			}
			
			/** load rets object **/
			if($rets_objects[$mls_query->mls_server_id] and ($server_id == $mls_query->mls_server_id)) $wplrets = $rets_objects[$mls_query->mls_server_id];
			else $wplrets = new wpl_addon_mls($mls_query->mls_server_id);
			
			if(trim($wplrets->mls_server_data->mls_unique_field) == '') continue;
			
			/** set to rets objects **/
			$rets_objects[$mls_query->mls_server_id] = $wplrets;

			/** connect **/
			if((!$connection) or ($server_id != $mls_query->mls_server_id)) 
			{
				$connection = $wplrets->connect($wplrets->mls_server_data->mls_use_post);
				$server_id = $mls_query->mls_server_id;
			}
			
			/** map data **/
			$mapped = $wplrets->map($results, $mls_query->id);
			
			/** import properties **/
			$pids = $wplrets->import_mapped_data($mapped, $mls_query->id);
			
            $force_to_image_update = $wplrets->mls_server_data->mls_photo_update;
            
			/** download images **/
			if(trim($mls_query->images)) $wplrets->import_properties_images($pids, $mls_query->mls_server_id, $mls_query->images, $force_to_image_update);
			
			/** finalizing properties **/
			foreach($pids as $pid) $wplrets->finalize($pid, (wpl_property::is_finalized($pid) ? 'edit' : 'add'));
		
			/** update imported field **/
			wpl_db::q("UPDATE `#__wpl_addon_mls_data` SET `imported` = '1', `content` = NULL WHERE `id` IN ('".implode("','", $ids)."')");

			if(count($pids) > 0) wpl_events::trigger('wpl_mls_imported_listing', array('query_id'=>$mls_query->id, 'imported'=>count($pids)));
		}

		/** cPanel cronjob **/
		exit();
	}

	/**
	 * Download property images only
	 * @author Steve A. <steve@realtyna.com>
	 * @return void
	 */
	public function import_images()
	{
		$query = "SELECT * FROM `#__wpl_addon_mls_queries` WHERE `enabled`>='1'";
		$mls_queries = wpl_db::select($query);
		$rets_objects = array();
		$connection = 0;
        $server_id = 0;
		
		foreach($mls_queries as $mls_query)
		{
			$query = "SELECT `id` FROM `#__wpl_properties` WHERE `pic_numb` = '0' AND `mls_query_id` = '{$mls_query->id}' LIMIT {$mls_query->import_limit}";
			$pids = wpl_db::select($query, 'loadColumn');

			/** load rets object **/
			if($rets_objects[$mls_query->mls_server_id] and ($server_id == $mls_query->mls_server_id)) $wplrets = $rets_objects[$mls_query->mls_server_id];
			else $wplrets = new wpl_addon_mls($mls_query->mls_server_id);
			
			/** set to rets objects **/
			$rets_objects[$mls_query->mls_server_id] = $wplrets;

			/** connect **/
			if((!$connection) or ($server_id != $mls_query->mls_server_id)) 
			{
				$connection = $wplrets->connect($wplrets->mls_server_data->mls_use_post);
				$server_id = $mls_query->mls_server_id;
			}
			
			$force_to_image_update = $wplrets->mls_server_data->mls_photo_update;

			/** download images **/
			if(trim($mls_query->images)) $wplrets->import_properties_images($pids, $mls_query->mls_server_id, $mls_query->images, $force_to_image_update);

			/** finalizing properties **/
			foreach($pids as $pid) $wplrets->finalize($pid, (wpl_property::is_finalized($pid) ? 'edit' : 'add'));
		}

		/** cPanel cronjob **/
		exit();
	}

	/**
	 * Purge listing
	 * @author Matthew N. <matthew@realtyna.com>
	 * @return void
	 */
	public function purge_listing()
	{
	    /** Mark expired listings **/
	    $ids = wpl_request::getVar('rets_query_ids');
	    $query = "SELECT * FROM `#__wpl_addon_mls_queries` WHERE `enabled`>='1' and DATE_ADD(`last_purge_date`, INTERVAL `purge_period` HOUR)<'".date("Y-m-d H:i:s")."'";
	    if(is_numeric(str_replace(',', '', $ids))) $query .= " AND `id` IN ({$ids})";

		$mls_queries = wpl_db::select($query);
		$rets_objects = array();
		$connection = 0;
		$server_id = 0;
		$connect_per_class = 0;
		
		foreach($mls_queries as $mls_query)
		{
			/** load rets object **/
			if($rets_objects[$mls_query->mls_server_id] and ($server_id == $mls_query->mls_server_id) and !$connect_per_class) $wplrets = $rets_objects[$mls_query->mls_server_id];
			else $wplrets = new wpl_addon_mls($mls_query->mls_server_id);
			
			if(trim($wplrets->mls_server_data->mls_unique_field) == '') continue;

			$connect_per_class = $wplrets->mls_server_data->connect_per_class;

			/** set to rets objects **/
			$rets_objects[$mls_query->mls_server_id] = $wplrets;

			/** connect **/
			if((!$connection) or ($server_id != $mls_query->mls_server_id) or $connect_per_class) 
			{
				$connection = $wplrets->connect($wplrets->mls_server_data->mls_use_post);
				$server_id = $mls_query->mls_server_id;
				$wpl_unique_field = wpl_addon_mls::get_wpl_unique_field($mls_query->id, $mls_query->mls_server_id);
			}

			$date_now = date('Y-m-d');

			$params = !empty($mls_query->params) ? json_decode($mls_query->params) : array();
			$default_offset = isset($params->default_offset) ? $params->default_offset : 0;
			if(isset($params->raw_import_limit)) $options['Limit'] = $params->raw_import_limit;

			if(stristr($mls_query->query, 'date+s')) $mls_query_fetch_listings = str_replace('date+s', $mls_query->start_purge_date.'-'.$date_now, $mls_query->query);
			elseif(stristr($mls_query->query, 'date+t')) $mls_query_fetch_listings = str_replace('date+t', $mls_query->start_purge_date.'T00:00:00+', $mls_query->query);
			elseif(stristr($mls_query->query, 'dateT00:00:00Z')) $mls_query_fetch_listings ='(ID=*)';
			elseif(stristr($mls_query->query, 'dateT')) $mls_query_fetch_listings = str_replace('dateT', $mls_query->start_purge_date.'T00:00:00-'.$date_now.'T00:00:00', $mls_query->query);
			else $mls_query_fetch_listings = str_replace('date+', $mls_query->start_purge_date.'+', $mls_query->query);

			$result = self::fetch_class_listings($wplrets, $mls_query->mls_class_id, $mls_query_fetch_listings, $default_offset,$options['Limit']);
			$current_properties = wpl_db::select("SELECT count(`id`) FROM `#__wpl_properties` WHERE `source`='mls' AND `mls_query_id`='".$mls_query->id."'", 'loadResult');
            $count_properties = ($current_properties * 60) / 100;
            $mls_properties = count($result);
            
            wpl_db::set('wpl_addon_mls_queries', $mls_query->id, 'query_options', json_encode(array('total' => $mls_properties, 'updated_at' => $date_now)));

        	if($mls_properties > $count_properties)
			{
				$result = implode("','", $result);
                wpl_db::q("UPDATE `#__wpl_addon_mls_data` SET `purged` = 1 WHERE `mls_query_id` = '{$mls_query->id}' AND `unique_value` NOT IN('{$result}')");
			}
			
			wpl_db::q("UPDATE `#__wpl_addon_mls_queries` SET `last_purge_date` = NOW() WHERE `id` = '{$mls_query->id}'");
		}
	    
		/** Purge marked listings **/
		$query = "SELECT * FROM `#__wpl_addon_mls_queries` WHERE `enabled`>='1'";
		
		if(is_numeric(str_replace(',', '', $ids))) $query .= " AND `id` IN ({$ids})";
        
		$mls_queries = wpl_db::select($query);
        $server_id = 0;
		
		foreach($mls_queries as $mls_query)
		{
		    if($server_id != $mls_query->mls_server_id) 
			{
			    $wpl_unique_field = wpl_addon_mls::get_wpl_unique_field($mls_query->id, $mls_query->mls_server_id);	
				$server_id = $mls_query->mls_server_id;
			}
		    
	        $query = "SELECT `unique_value` FROM `#__wpl_addon_mls_data` WHERE `purged` = 1 and `mls_query_id` = '{$mls_query->id}' LIMIT {$mls_query->limit}";
		    $expired_ids = wpl_db::select($query);
		    
			$ex_ids_arr = array();
		    foreach($expired_ids as $expired_id) $ex_ids_arr[] = $expired_id->unique_value;
		    
			if (is_array($ex_ids_arr) && count($ex_ids_arr) > 0)
			{
				$expired_ids = implode("','", $ex_ids_arr);
				$query_delete = "SELECT `id` from `#__wpl_properties` WHERE `".$wpl_unique_field['table_column']."` IN('{$expired_ids}')";
				$expired_properties = wpl_db::select($query_delete);
				
				foreach($expired_properties as $expired_property) wpl_property::purge($expired_property->id);
				
				wpl_db::delete('wpl_addon_mls_data', '', "AND `mls_query_id` = '{$mls_query->id}' AND `unique_value` IN('{$expired_ids}')");
			}
			
			// Set purge log
			wpl_global::event_handler('wpl_mls_purged', array('mls_class_name'=>$mls_query->mls_class_name));
		}
		
		exit();
	}

	/**
	 * Firewall test to check the possible issue in relation to Ports: 80 & 6103
	 * @author Matthew N. <matthew@realtyna.com>
	 * @return print some text from FirewallTest()
	 */
	public function firewall_test()
	{
		_wpl_import('libraries.addon_mls.phrets');
		$rets = new phRETS();
		$rets->FirewallTest();

		/** cPanel cronjob **/
		exit();
	}

	/**
	 * Synchronizing the MLS listing with the clients' websites database
	 * @author Matthew N. <matthew@realtyna.com>
	 * @return void
	 */
	public function sync_mls_listing()
	{
		$ids = wpl_request::getVar('rets_query_ids');
		$limit_listing = wpl_request::getVar('limit_listing', 200);

		$query = "SELECT * FROM `#__wpl_addon_mls_queries` WHERE `enabled`>='1'";
		if(is_numeric(str_replace(',', '', $ids))) $query .= " AND `id` IN ({$ids})";

		$mls_queries = wpl_db::select($query);
		$rets_objects = array();
		$connection = 0;
		$server_id = 0;

		foreach($mls_queries as $mls_query)
		{
			/** load rets object **/
			if($rets_objects[$mls_query->mls_server_id] and ($server_id == $mls_query->mls_server_id)) $wplrets = $rets_objects[$mls_query->mls_server_id];
			else $wplrets = new wpl_addon_mls($mls_query->mls_server_id);

			if(trim($wplrets->mls_server_data->mls_unique_field) == '') continue;

			/** set to rets objects **/
			$rets_objects[$mls_query->mls_server_id] = $wplrets;

			/** connect **/
			if((!$connection) or ($server_id != $mls_query->mls_server_id))
			{
				$connection = $wplrets->connect($wplrets->mls_server_data->mls_use_post);
				$server_id = $mls_query->mls_server_id;
			}

			$options = array('Count'=>1);

			// support_offset per MLS
			if(isset($wplrets->mls_server_data->support_offset) and $wplrets->mls_server_data->support_offset == 1) $options['Offset'] = $mls_query->offset;

			$today = date('Y-m-d H:i:s');

			if(!empty($mls_query->missing_listings))
			{
				$missing_listings = explode(',',$mls_query->missing_listings);
				$select_missing_listings = array_slice($missing_listings, 0, $limit_listing);
				$import_status = $this->import_missing_listings($wplrets, $mls_query, $select_missing_listings);

				if($import_status) wpl_db::q("UPDATE `#__wpl_addon_mls_queries` SET `missing_listings`='".implode(',',array_slice($missing_listings, $limit_listing))."' WHERE `id`='{$mls_query->id}'");
				continue;
			}
			else if(strtotime($mls_query->missing_listings_last_check) >= strtotime('today')) continue;

			/** set query **/
			$mls_query_string = $mls_query->query;
			$date = ($mls_query->missing_listings_last_check == '0000-00-00' ? date('2017-01-01') : $mls_query->missing_listings_last_check);
			$date2 = date('Y-m-d');

			$params = !empty($mls_query->params) ? json_decode($mls_query->params) : array();
			$default_offset = isset($params->default_offset) ? $params->default_offset : 0;

			/** hourly updates, sample: {"pattern":"DTT","last":"15:44:47"} **/
			if($mls_query->query_options == 'hourly' and isset($params->pattern))
			{
				if($params->pattern == 'DTT') $mls_query_string = str_replace('date+', $date.'T'.$params->last.'+', $mls_query_string);
			}
			else
			{
				if(stristr($mls_query_string, 'date+s'))
				{
					if($date2 > $today) continue;

					$mls_query_string = str_replace('date+s', $date.'-'.$date2, $mls_query_string);
					$today = date('Y-m-d H:i:s', strtotime($date2));
				}
				elseif(stristr($mls_query_string, 'date+t')) $mls_query_string = str_replace('date+t', $date.'T00:00:00+', $mls_query_string);
				elseif(stristr($mls_query_string, 'dateT'))
				{
					if($date2 > $today) continue;

					$mls_query_string = str_replace('dateT', $date.'T00:00:00-'.$date2.'T00:00:00', $mls_query_string);
					$mls_query_string = str_replace($date2.'T00:00:00+', $date2.'T00:00:00', $mls_query_string);

				}
				else $mls_query_string = str_replace('date+', $date.'+', $mls_query_string);
			}

			$result = self::fetch_class_listings($wplrets, $mls_query->mls_class_id, $mls_query_string, $default_offset);
			$array_select_listing = array();
			$get_missing_listing = 0;

			if($result > 0)
			{
				$select_listing = wpl_db::select("SELECT `unique_value` FROM `#__wpl_addon_mls_data` WHERE `mls_query_id` = '{$mls_query->id}'", 'loadAssocList');
				foreach($select_listing as $listing) $array_select_listing[] = $listing['unique_value'];

				$get_missing_listing = array_diff($result,$array_select_listing);
			}

			$count_missing_listing = count($get_missing_listing);

			if($count_missing_listing > $limit_listing) wpl_db::q("UPDATE `#__wpl_addon_mls_queries` SET `missing_listings`='".implode(',',$get_missing_listing)."' WHERE `id`='{$mls_query->id}'");
			else if($count_missing_listing > 0) $this->import_missing_listings($wplrets, $mls_query, $get_missing_listing);

			wpl_db::q("UPDATE `#__wpl_addon_mls_queries` SET `missing_listings_last_check`='".date('Y-m-d')."' WHERE `id`='{$mls_query->id}'");
			sleep(2);
		}

		/** cPanel cronjob **/
		exit();
	}

	/**
	 * Import the missing listings
	 * @author Matthew N. <matthew@realtyna.com>
	 * @param $wplrets
	 * @param $mls_query
	 * @param $get_missing_listing
	 *
	 * @return bool
	 */
	public function import_missing_listings($wplrets, $mls_query, $get_missing_listing)
	{
		$query = '(('.$wplrets->mls_server_data->mls_unique_field.'='.implode(',',$get_missing_listing).'))';

		try
		{
			$listings = $wplrets->rets->Search($wplrets->mls_server_data->resource,$mls_query->mls_class_id,$query, array('Limit'=>'100000'));
		}
		catch (Exception $e)
		{
			wpl_global::event_handler('wpl_mls_connection_log', array('error_code'=>$e->getCode(), 'error_message'=>wpl_db::escape($e->getMessage())));
			return false;
		}

		$count = $listings->getTotalResultsCount();
		$today = date('Y-m-d H:i:s');

		foreach($listings as $row)
		{
			$row = $row->toArray();
			$unique_value = $row[$wplrets->mls_server_data->mls_unique_field];
			$data = base64_encode(json_encode($row));
			wpl_db::q("INSERT INTO `#__wpl_addon_mls_data` (`mls_server_id`, `mls_query_id`, `unique_value`, `content`, `date`) VALUES ('{$mls_query->mls_server_id}', '{$mls_query->id}', '{$unique_value}', '{$data}', '{$today}')");
		}
		return true;
	}

	/**
	 * Get a statistics of MLS data and the website data
	 * @author Matthew <matthew@realtyna.com>
	 * @return void
	 */
	public function mls_stats()
	{
		$ids = wpl_request::getVar('rets_server_ids');

		$query = "SELECT * FROM `#__wpl_addon_mls_queries` WHERE `enabled`>='1'";
		if(is_numeric(str_replace(',', '', $ids))) $query .= " AND `mls_server_id` IN ({$ids})";

		$mls_queries = wpl_db::select($query);
		$rets_objects = array();
		$connection = 0;
		$server_id = 0;

		$return = array();

		$count = 0;
		foreach($mls_queries as $mls_query)
		{
			/** load rets object **/
			if($rets_objects[$mls_query->mls_server_id] and ($server_id == $mls_query->mls_server_id)) $wplrets = $rets_objects[$mls_query->mls_server_id];
			else $wplrets = new wpl_addon_mls($mls_query->mls_server_id);

			if(trim($wplrets->mls_server_data->mls_unique_field) == '') continue;

			/** set to rets objects **/
			$rets_objects[$mls_query->mls_server_id] = $wplrets;

			/** connect **/
			if((!$connection) or ($server_id != $mls_query->mls_server_id))
			{
				$connection = $wplrets->connect($wplrets->mls_server_data->mls_use_post);
				$server_id = $mls_query->mls_server_id;
			}

			$return[$mls_query->mls_class_id]['class_name'] = $mls_query->mls_class_name;
			$return[$mls_query->mls_class_id]['class_server_id'] = $mls_query->mls_server_id;
			$return[$mls_query->mls_class_id]['class_last_sync_date'] = $mls_query->last_sync_date;

			$options = array('Count'=>1);

			// support_offset per MLS
			if(isset($wplrets->mls_server_data->support_offset) and $wplrets->mls_server_data->support_offset == 1) $options['Offset'] = $mls_query->offset;

			$date_now = date('Y-m-d');

			if(stristr($mls_query->query, 'date+s')) $mls_query_fetch_listings = str_replace('date+s', $mls_query->start_purge_date.'+', $mls_query->query);
			elseif(stristr($mls_query->query, 'date+t')) $mls_query_fetch_listings = str_replace('date+t', $mls_query->start_purge_date.'T00:00:00+', $mls_query->query);
			elseif(stristr($mls_query->query, 'dateT')) $mls_query_fetch_listings = str_replace('dateT', $mls_query->start_purge_date.'T00:00:00-'.$date_now.'T00:00:00', $mls_query->query);
			else $mls_query_fetch_listings = str_replace('date+', $mls_query->start_purge_date.'+', $mls_query->query);

			$listings = $wplrets->rets->Search($wplrets->mls_server_data->resource, $mls_query->mls_class_id, $mls_query_fetch_listings, array("Select" => $wplrets->mls_server_data->mls_unique_field, 'Limit'=>2));
			$count_all = $listings->getTotalResultsCount();

			$count += $count_all;

			$sample_data = '';

			foreach($listings as $row)
			{
				$row = $row->toArray();
				$sample_data .= $row[$wplrets->mls_server_data->mls_unique_field].',';
			}

			$return[$mls_query->mls_class_id]['mls_sample_data'] = trim($sample_data, ',');

			$return[$mls_query->mls_class_id]['mls_listing_counts'] = $count_all;

			$select_properties = wpl_db::select("SELECT count(`id`) FROM `#__wpl_properties` WHERE `source`='mls' AND `mls_query_id`='".$mls_query->id."'", 'loadResult');

			$return[$mls_query->mls_class_id]['count_all_listings_in_database'] = $select_properties;

			$yesterday = date('Y-m-d 00:00:00', strtotime('yesterday'));

			$select_last_updated_properties = wpl_db::select("SELECT count(`id`) FROM `#__wpl_properties` WHERE `source`='mls' AND `mls_query_id`='".$mls_query->id."' AND date(`last_sync_date`) > date('{$yesterday}')", 'loadResult');
			$return[$mls_query->mls_class_id]['count_last_updated_properties'] = $select_last_updated_properties;

			$select_last_added_properties = wpl_db::select("SELECT count(`id`) FROM `#__wpl_properties` WHERE `source`='mls' AND `mls_query_id`='".$mls_query->id."' AND date(`add_date`) > date('{$yesterday}')", 'loadResult');
			$return[$mls_query->mls_class_id]['count_last_added_properties'] = $select_last_added_properties;
		}

		$return['all_mls_listing_count'] = $count;

		echo '<pre>'.print_r($return, true);

		/** cPanel cronjob **/
		exit();
	}
    
    public function listtrack()
    {
        $wplview = wpl_request::getVar('wplview');
        $pid = wpl_request::getVar('pid', 0);
        
        // The ListTrack code should only apply in the property details page
        if($wplview != 'property_show' or !$pid) return;
        
        // Listing Data
        $data = wpl_db::select("SELECT `mls_id`, `zip_name` FROM `#__wpl_properties` WHERE `id`='$pid'", 'loadAssoc');
        
        // The listing data is not found!
        if(!isset($data['mls_id'])) return;
        
        // Add ListTrack JS
        $javascript = (object) array('param1'=>'listtrack', 'param2'=>'http'.(stristr(wpl_global::get_full_url(), 'https://') != '' ? 's' : '').'://code.listtrac.com/monitor.ashx?acct='.$this->settings['mls_listtrack_code'], 'param4'=>'1', 'external'=>true);
        wpl_extensions::import_javascript($javascript);
        
        // Add the JS codes to the property details page
        wpl_html::set_footer('<script type="text/javascript">
        jQuery(document).ready(function()
        {
            // ListTrack is defined correctly
            if(typeof(_LT) != "undefined" && _LT != null)
            {
                // Property View
                _LT._trackEvent(_eventType.view, "'.$data['mls_id'].'", "'.$data['zip_name'].'", null, null, "view");
                
                // Lead
                wplj(".wpl-contact-listing-btn .btn").on("click", function()
                {
                    _LT._trackEvent(_eventType.view, "'.$data['mls_id'].'", "'.$data['zip_name'].'", null, null, "lead");
                });
                
                // Favorite
                wplj(".favorite_link a").on("click", function()
                {
                    _LT._trackEvent(_eventType.view, "'.$data['mls_id'].'", "'.$data['zip_name'].'", null, null, "favorite");
                });
                
                // Print
                wplj(".pdf_link a").on("click", function()
                {
                    _LT._trackEvent(_eventType.view, "'.$data['mls_id'].'", "'.$data['zip_name'].'", null, null, "printListing");
                });
                
                // Driving Direction
                wplj(".wpl-map-get-direction-btn-cnt").on("click", function()
                {
                    _LT._trackEvent(_eventType.view, "'.$data['mls_id'].'", "'.$data['zip_name'].'", null, null, "drivingDirections");
                });
                
                // Gallery
                wplj(".wpl-gallery-pshow li").on("click", function()
                {
                    _LT._trackEvent(_eventType.view, "'.$data['mls_id'].'", "'.$data['zip_name'].'", null, null, "gallery");
                });
                
                // Share
                wplj(".facebook_link a, .google_plus_link a, .twitter_link a, .twitter_link a, .pinterest_link a, .linkedin_link a").on("click", function()
                {
                    _LT._trackEvent(_eventType.view, "'.$data['mls_id'].'", "'.$data['zip_name'].'", null, null, "share");
                });
            }
        });
        </script>');
    }

	/**
	 * Purge the older listings
	 */
	public function purge_older_listings()
	{
		$field = wpl_request::getVar('field');
		$date = wpl_request::getVar('date');

		if(!$field)
		{
			echo __("Please send a MLS field ID for this functionality like (field=mls_field_id).", 'real-estate-listing-realtyna-wpl');
			exit;
		}
		if(!$date)
		{
			echo __("Please send a date for this functionality like (date=2015-01-01).", 'real-estate-listing-realtyna-wpl');
			exit;
		}

		$field_column_name = wpl_addon_mls::get_mls_field_data($field);

		if($field_column_name)
		{
			$select_older_listings = wpl_db::select("SELECT `id` FROM `#__wpl_properties` WHERE date(`{$field_column_name}`) < date('{$date}') ", 'loadObjectList');

			foreach($select_older_listings as $select_older_listing) wpl_property::purge($select_older_listing->id);

			exit();
		}

		// If the MLS field didn't mapp yet.
		$this->purge_older_listings_by_mls($field, $date);
	}

	public function purge_older_listings_by_mls($field, $date)
	{
		$ids = wpl_request::getVar('rets_query_ids');

		$query = "SELECT * FROM `#__wpl_addon_mls_queries` WHERE `enabled`>='1'";
		if(is_numeric(str_replace(',', '', $ids))) $query .= " AND `id` IN ({$ids})";

		$mls_queries = wpl_db::select($query);
		$rets_objects = array();
		$connection = 0;
		$server_id = 0;

		$count_queries = count($mls_queries);

		foreach($mls_queries as $mls_query)
		{
			/** load rets object **/
			if($rets_objects[$mls_query->mls_server_id] and ($server_id == $mls_query->mls_server_id)) $wplrets = $rets_objects[$mls_query->mls_server_id];
			else $wplrets = new wpl_addon_mls($mls_query->mls_server_id);

			if(trim($wplrets->mls_server_data->mls_unique_field) == '') continue;

			/** set to rets objects **/
			$rets_objects[$mls_query->mls_server_id] = $wplrets;

			/** connect **/
			if((!$connection) or ($server_id != $mls_query->mls_server_id))
			{
				$connection = $wplrets->connect($wplrets->mls_server_data->mls_use_post);
				$server_id = $mls_query->mls_server_id;
			}

			$date_now = date('Y-m-d');

			// Remove Expired Listings
			$offset_checked = 0;

			$mls_query_fetch_listings = "(({$field}={$date}+))";

			$result = self::fetch_class_listings($wplrets, $mls_query->mls_class_id, $mls_query_fetch_listings, $offset_checked);
			$wpl_unique_field = wpl_addon_mls::get_wpl_unique_field($mls_query->id, $mls_query->mls_server_id);

			$count_result = count($result);

			if($count_result > 0)
			{
				$result = implode("','", $result);

				$where = "WHERE `mls_query_id`='".$mls_query->id."' AND `".$wpl_unique_field['table_column']."` NOT IN('{$result}');";
				$query_delete = "SELECT `id` from `#__wpl_properties` ".$where;
				$expired_properties = wpl_db::select($query_delete);

				foreach($expired_properties as $expired_property) wpl_property::purge($expired_property->id);
				wpl_db::delete('wpl_addon_mls_data', '', "AND `mls_query_id` = '{$mls_query->id}' AND `unique_value` NOT IN('{$result}')");
			}

			// Set purge log
			wpl_global::event_handler('wpl_mls_purged', array('mls_class_name'=>$mls_query->mls_class_name));

			// set a delay to get other results from the MLS server
			if($count_queries > 4) sleep(2);
			else sleep(4);
		}

		exit();
	}

	/**
	 * Check the Open House tags for the clients that have Open House customization.
	 * @author Matthew <matthew@realtyna.com>
	 * @return void
	 */
	public function openhouse_tags_status()
	{
		$listings = wpl_db::select("SELECT `id` FROM `#__wpl_properties` WHERE `sp_openhouse`='1'", 'loadAssocList');

		if(!empty($listings))
		{
			$disable_ids = array();

			foreach($listings as $listing)
			{
				$id = $listing['id'];
				$open_dates = wpl_db::select("SELECT * FROM `#__wpl_items` WHERE `parent_id`='{$id}' AND `item_type`='opendates'", 'loadObjectList');

				if(!empty($open_dates))
				{
					$valid_date = wpl_items::render_opendates($open_dates, true);
					if(empty($valid_date) or count($valid_date) == 0) $disable_ids[] = $id;
				}
			}
            
			if(count($disable_ids) > 0) wpl_db::q("UPDATE `#__wpl_properties` SET `sp_openhouse`='0' WHERE `id` IN (".implode(',', $disable_ids).")");
		}
        
		exit();
	}

	/**
	 * Update Properties table after full import process to fetch new values
	 * @author Natan <natan@realtyna.com>
	 * @return string
	 */
	public function update_fields($mls_fields)
	{
	    $mls_fields_array = explode(',', $mls_fields);
	    $mls_server_id = 0;
	    
	    //Get MLS Server ID and validations of the fields
	    foreach($mls_fields_array as $key=>$mls_field)
	    {
	        $data = wpl_db::select("SELECT `mls_server_id`,`wpl_field_id` FROM `#__wpl_addon_mls_mappings` WHERE `field_id`='$mls_field'", 'loadObject');
	        $new_mls_server_id = $data->mls_server_id;
	        
	        //Validations
	        if(!$new_mls_server_id)
	        {
	            echo "$mls_field couldn't be found<br>";
	            unset($mls_fields_array[$key]);
	            continue;
	        }
	        
	        if($mls_server_id and $mls_server_id != $new_mls_server_id)
	        {
	            echo "All the fields should be into the same MLS feed, $mls_field is skipped<br>";
	            unset($mls_fields_array[$key]);
	            $new_mls_server_id = $mls_server_id;
	            continue;
	        }
	       
	        $mls_server_id = $new_mls_server_id;
	        $wpl_field = wpl_flex::get_field($data->wpl_field_id);
	        
	        if(empty($wpl_field))
	        {
	            echo "$mls_field is not mapped yet.<br>";
	            unset($mls_fields_array[$key]);
	            continue;
	        }
	        
	        $wpl_fields_array[] = $wpl_field->table_column; 
	    }

	    //Cleaned fields list
	    $mls_fields = implode(',', $mls_fields_array);
        
	    $ids = wpl_request::getVar('rets_query_ids');

		$query = "SELECT * FROM `#__wpl_addon_mls_queries` WHERE `enabled`>='1'";
		if(is_numeric(str_replace(',', '', $ids))) $query .= " AND `id` IN ({$ids})";

		$mls_queries = wpl_db::select($query);
		$rets_objects = array();
		$connection = 0;
        $connect_per_class = 0;
        
		foreach($mls_queries as $mls_query)
		{
		    $selects = '';
		    
	        /** load rets object **/
			if($rets_objects[$mls_query->mls_server_id] and ($server_id == $mls_query->mls_server_id) and !$connect_per_class) $wplrets = $rets_objects[$mls_query->mls_server_id];
			else $wplrets = new wpl_addon_mls($mls_query->mls_server_id);
			
			if(trim($wplrets->mls_server_data->mls_unique_field) == '')
			{
			    echo "Unique field Should be set on MLS Connection";
			    continue;
			}
            
            $unique_field = $wplrets->get_wpl_unique_field($mls_query->id);

			$connect_per_class = $wplrets->mls_server_data->connect_per_class;
			
			/** set to rets objects **/
			$rets_objects[$mls_query->mls_server_id] = $wplrets;
			
			/** connect **/
			if((!$connection) or $connect_per_class) 
			{
				$connection = $wplrets->connect($wplrets->mls_server_data->mls_use_post);
			}
			
			$selects = $wplrets->mls_server_data->mls_unique_field.','.$mls_fields;
			
			$options = array('Count'=>1, 'Select'=>$selects);
			$params = !empty($mls_query->params) ? json_decode($mls_query->params) : array();
			
		    if(isset($params->raw_import_limit)) $options['Limit'] = $params->raw_import_limit;
			if(isset($params->default_offset)) $options['Offset'] = $params->default_offset;
			
			/** set query **/
			$mls_query_string = $mls_query->query;
			if(stristr($mls_query_string, 'date+s')) $mls_query_string = str_replace('date+s', $mls_query->start_purge_date.'+', $mls_query_string);
			elseif(stristr($mls_query_string, 'dateT00:00:00Z'))
			{
			    $mls_query_string = str_replace('dateT00:00:00Z', $mls_query->start_purge_date.'T00:00:00Z', $mls_query_string);
			}
			elseif(stristr($mls_query_string, 'date+t')) $mls_query_string = str_replace('date+t', $mls_query->start_purge_date.'T00:00:00+', $mls_query_string);
			else $mls_query_string = str_replace('date+', $mls_query->start_purge_date.'+', $mls_query_string);
            
			try
			{
				$wplrets = new wpl_addon_mls($mls_query->mls_server_id);
				$wplrets->connect($wplrets->mls_server_data->mls_use_post);
				$values = $wplrets->rets->Search($wplrets->mls_server_data->resource, $mls_query->mls_class_id, $mls_query_string, $options);
				$count = $values->getTotalResultsCount();
			}
			catch (Exception $e)
			{
				wpl_global::event_handler('wpl_mls_connection_log', array('error_code'=>$e->getCode(), 'error_message'=>wpl_db::escape($e->getMessage())));
				continue;
			}
			
			foreach($values as $row) $results[] = $row->toArray();
		    
		    /* Checking if the Result count less than Total count, then fetching all Result */
		    $count_all = $values->getTotalResultsCount();
		    $count_res = count($results);
		    $limit = $count_res;
		    $options['Offset'] = 0;
            
		    if(!empty($results) and $count_all > $count_res)
		    {
		    	$round = round($count_all / $count_res);
			    for($i=0; $i <= $round; $i++)
			    {   
				    if($count_all > count($results))
				    {
					    $options['Offset'] += $count_res;

					    $search = $wplrets->rets->Search($wplrets->mls_server_data->resource, $mls_query->mls_class_id, $mls_query_string, $options);

					    foreach($search as $row) $results[] = $row->toArray();
				    }
				    else continue;
			    }
			}
			
			foreach($results as $row)
			{
				$where = $row[$wplrets->mls_server_data->mls_unique_field];
				unset($row[$wplrets->mls_server_data->mls_unique_field]);
				
				$params = array_combine($wpl_fields_array, $row);
				
				wpl_db::update('wpl_properties', $params, $unique_field['table_column'], $where);
			}
            
        }
        
	    echo 'Done!'; exit;
	}
}