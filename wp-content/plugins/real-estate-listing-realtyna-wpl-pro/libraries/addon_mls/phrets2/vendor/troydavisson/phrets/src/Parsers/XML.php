<?php namespace PHRETS\Parsers;

use PHRETS\Http\Response;
use Psr\Http\Message\ResponseInterface;

class XML
{
    public function parse($string)
    {
        if ($string instanceof ResponseInterface or $string instanceof Response) {
            $string = $string->getBody()->__toString();
        }

        $string = (string) $string;
        $string = iconv(mb_detect_encoding($string), 'UTF-8//IGNORE', $string);
        
        return new \SimpleXMLElement($string);
    }
}
