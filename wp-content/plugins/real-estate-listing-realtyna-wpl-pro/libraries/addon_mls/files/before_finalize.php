<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');

$property_data = wpl_property::get_property_raw_data($pid);

/** Geo Position **/
if(($mls_server->mls_get_geo_coordinates and !trim($property_data['googlemap_lt'])) or (!trim($property_data['googlemap_lt']) or !trim($property_data['googlemap_ln'])))
{
	wpl_locations::update_LatLng($property_data);
}

// Automatically create neighborhoods and assign listings to the related neighborhood
if(trim($mls_server->neighborhood_field) != '' and wpl_global::check_addon('neighborhoods'))
{
	$neighborhood_wpl_field = wpl_addon_mls::get_mls_field_data($mls_server->neighborhood_field);
	if($neighborhood_wpl_field)
	{
		$neighborhood_name = trim($property_data[$neighborhood_wpl_field]);
		if($neighborhood_name != '')
		{
			$neighborhood_id = wpl_db::select("SELECT `id` FROM `#__wpl_properties` WHERE LOWER(`sys_neighborhood_name`)='".strtolower($neighborhood_name)."' OR (LOWER(`field_313`)='".strtolower($neighborhood_name)."' AND `kind`='4')", 'loadResult');

			if(!$neighborhood_id)
			{
				$neighborhood_id = wpl_property::create_property_default($property_data['user_id'], 4);
				$check_range_mls_id = wpl_db::select("SELECT max(`mls_id`) FROM `#__wpl_properties` WHERE `mls_id` between 5000 AND 6000 AND kind=4 LIMIT 1", 'loadResult');

				if($check_range_mls_id) $neighborhood_mls_id = $check_range_mls_id+1;
				else $neighborhood_mls_id = 5001;

				wpl_db::q("UPDATE `#__wpl_properties` SET `mls_id`='{$neighborhood_mls_id}', `sys_neighborhood_name`='".strtolower($neighborhood_name)."', `field_313`='$neighborhood_name' WHERE `id`='$neighborhood_id'", 'update');

				// Finalize the new neighborhood
				wpl_property::finalize($neighborhood_id, 'add', $property_data['user_id']);
			}

			wpl_db::q("UPDATE `#__wpl_properties` SET `neighborhood_id` = '{$neighborhood_id}' WHERE `id`='".$property_data['id']."'");
		}
	}
}

// Automatically create complexes and assign listings to the related complex
if(trim($mls_server->complex_field) != '' and wpl_global::check_addon('complex'))
{
	$complex_wpl_field = wpl_addon_mls::get_mls_field_data($mls_server->complex_field);
	if($complex_wpl_field)
	{
		$complex_name = trim($property_data[$complex_wpl_field]);
		if($complex_name != '')
		{
			$complex_id = wpl_db::select("SELECT `id` FROM `#__wpl_properties` WHERE LOWER(`sys_complex_name`)='".strtolower($complex_name)."' OR (LOWER(`field_313`)='".strtolower($complex_name)."' AND `kind`='1')", 'loadResult');

			if(!$complex_id)
			{
				$complex_id = wpl_property::create_property_default($property_data['user_id'], 1);
				$check_range_mls_id = wpl_db::select("SELECT max(`mls_id`) FROM `#__wpl_properties` WHERE `mls_id` between 6000 AND 7000 AND kind=1 LIMIT 1", 'loadResult');

				if($check_range_mls_id) $complex_mls_id = $check_range_mls_id+1;
				else $complex_mls_id = 6001;

				wpl_db::q("UPDATE `#__wpl_properties` SET `mls_id`='{$complex_mls_id}', `sys_complex_name`='".strtolower($complex_name)."', `field_313`='$complex_name' WHERE `id`='$complex_id'", 'update');

				// Finalize the new neighborhood
				wpl_property::finalize($complex_id, 'add', $property_data['user_id']);
			}

			wpl_db::q("UPDATE `#__wpl_properties` SET `parent` = '{$complex_id}' WHERE `id`='".$property_data['id']."'");
		}
	}
}
//Remove Cached Property Page Title
wpl_db::q("UPDATE `#__wpl_properties` SET `field_312` = '' WHERE `id`='".$property_data['id']."'");