<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');

/** Set Country if not mapped **/
if(!isset($wpl_listing['location1_name']['wpl_value']))
{
    $arr = array();
    $arr['wpl_value'] = 254;
    $wpl_table_column = 'location1_id';
    $arr['wpl_table_column'] = $wpl_table_column;
    $wpl_listing[$wpl_table_column] = $arr;
	
	$arr = array();
    $arr['wpl_value'] = 'United States';
    $wpl_table_column = 'location1_name';
    $arr['wpl_table_column'] = $wpl_table_column;
    $wpl_listing[$wpl_table_column] = $arr;
}

/** location integration with wpl **/
for($location_level=1; $location_level<=7; $location_level++)
{
	if(!isset($wpl_listing['location'.$location_level.'_id']) and isset($wpl_listing['location'.$location_level.'_name']))
	{
		$parent_query = "";
		if ($location_level > 1)
		{
			$parent_query = " and `parent`='".$wpl_listing['location'.($location_level-1).'_id']['wpl_value']."'";
		}
		
		$query = "SELECT `id` FROM `#__wpl_location".$location_level."` WHERE (LOWER(`name`)='".wpl_db::escape(strtolower($wpl_listing['location'.$location_level.'_name']['wpl_value']))."' OR LOWER(`abbr`)='".wpl_db::escape(strtolower($wpl_listing['location'.$location_level.'_name']['wpl_value']))."')" . $parent_query;
		$location_id = wpl_db::select($query, 'loadResult');
		
		if($location_id) $wpl_value = $location_id;
		else
		{
			$query = "INSERT INTO `#__wpl_location".$location_level."`(`parent`, `name`) VALUES ('".$wpl_listing['location'.($location_level-1).'_id']['wpl_value']."','".wpl_db::escape($wpl_listing['location'.$location_level.'_name']['wpl_value'])."')";
			$wpl_value = wpl_db::q($query, 'insert');
		}
		
        $arr = array();
        $arr['wpl_value'] = $wpl_value;
        $wpl_table_column = 'location'.$location_level.'_id';
        $arr['wpl_table_column'] = $wpl_table_column;

        $wpl_listing[$wpl_table_column] = $arr;
	}
}

/** zip id integration with wpl **/
if(!isset($wpl_listing['zip_id']) and isset($wpl_listing['zip_name']))
{
    $query = "SELECT `id` FROM `#__wpl_locationzips` WHERE LOWER(`name`)='".wpl_db::escape(strtolower($wpl_listing['zip_name']['wpl_value']))."' and `parent`='".$wpl_value."'";
    $zip_id = wpl_db::select($query, 'loadResult');

    if($zip_id) $wpl_zipid = $zip_id;
    else
    {
        $query = "INSERT INTO `#__wpl_locationzips`(`parent`, `name`) VALUES ('".$wpl_value."','".$wpl_listing['zip_name']['wpl_value']."')";
        $wpl_zipid = wpl_db::q($query, 'insert');
    }

    $arr = array();
    $arr['wpl_value'] = $wpl_zipid;
    $wpl_table_column = 'zip_id';
    $arr['wpl_table_column'] = $wpl_table_column;

    $wpl_listing[$wpl_table_column] = $arr;
}

/** Set listing type if not mapped **/
if(!isset($wpl_listing['listing']['wpl_value']))
{
    $arr = array();
    $arr['wpl_value'] = 9;
    $wpl_table_column = 'listing';
    $arr['wpl_table_column'] = $wpl_table_column;

    $wpl_listing[$wpl_table_column] = $arr;
}