<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');

/** MLS field mapper **/
class wpl_mls_date_map extends wpl_addon_mls_mapper
{
	/** map function **/
	public function map($wpl_field, $mls_value, $mls_listing, $mls_field)
	{
		if(empty($mls_value)) return false;
        $wpl_value = wpl_render::derender_date(wpl_render::render_date($mls_value));
        
        if($wpl_field['table_column'] == 'add_date') $wpl_value = date("Y-m-d H:i:s", strtotime($wpl_value));
        
        return array('value'=>$wpl_value);
	}
}