<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');

/** MLS field mapper **/
class wpl_mls_select_map extends wpl_addon_mls_mapper
{
    /** map function **/
	public function map($wpl_field, $mls_value, $mls_listing, $mls_field, &$field_options = array())
	{
	    if(empty($mls_value)) return false;

        $mls_value = stripslashes($mls_value);
		$select_options = $field_options[$wpl_field['id']];
        
        $wpl_value = NULL;
        $max_id = 0;

        // avoid to throw PHP warning error
        if(!is_array($select_options['params'])) $select_options['params'] = array();

        foreach($select_options['params'] as $item)
        {
            if(strtolower(trim($item['value'])) == strtolower(trim($mls_value))) $wpl_value = $item['key'];
            if($mls_value == $item['value']) $wpl_value = $item['key'];
            $max_id = max($max_id, $item['key'])+1;
        }
        
        /** Add the option into field options **/
        if(is_null($wpl_value))
        {
			/* Delete (") at the start and end of strings if there's any */
			if ($mls_value[0] == '"' && $mls_value[strlen($mls_value) - 1] == '"') 
			{
				$mls_value = trim($mls_value, '" ');
			}
			
			/* failsafe, select type for multiple valued fields */
			$mls_value = explode('","', $mls_value)[0];
			
            $select_options['params'][$max_id] = array('value'=>ucwords(strtolower($mls_value)), 'key'=>$max_id, 'enabled'=>1);
            
            $wpl_value = $max_id;
        }
		
		$field_options[$wpl_field['id']] = $select_options;
		
		return array('value'=>$wpl_value);
	}
}