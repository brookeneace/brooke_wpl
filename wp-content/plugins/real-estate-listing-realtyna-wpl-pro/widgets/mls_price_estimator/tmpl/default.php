<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');

$property_types = wpl_global::get_property_types();
$listings = wpl_global::get_listings();

$this->_wpl_import('widgets.mls_price_estimator.scripts.js', true, true);
?>
<div id="wpl_mls_price_estimator_widget_cnt<?php echo $this->widget_id; ?>" class="wpl-mls-price-estimator-widget wpl-row wpl-expanded wpl-small-up-1 wpl-medium-up-2 wpl-large-up-2 <?php echo $this->css_class; ?>">
    <?php echo $args['before_title'].__($this->title, 'real-estate-listing-realtyna-wpl').$args['after_title']; ?>
    
    <div class="mls-price-estimator wpl-column">
        <div class="mls-price-estimator_cnt">
            <div class="mls_price_estimator_location"><input type="text" id="mls_price_estimator_location<?php echo $this->widget_id; ?>" placeholder="<?php echo __('Enter a location', 'real-estate-listing-realtyna-wpl'); ?>"></div>
            <div class="form-field">
                <label for="mls_price_estimator_property_type<?php echo $this->widget_id; ?>"><?php echo __("Property type", 'real-estate-listing-realtyna-wpl'); ?></label>
                <select id="mls_price_estimator_property_type<?php echo $this->widget_id; ?>">
                    <option value=""><?php echo __('Any', 'real-estate-listing-realtyna-wpl'); ?></option>
                    <?php foreach ($property_types as $property_type): ?>
                        <option value="<?php echo $property_type['id']; ?>"><?php echo $property_type['name']; ?></option>
                    <?php endforeach;?>
                </select>
            </div>
            <div class="form-field">
                <label for="mls_price_estimator_bedrooms<?php echo $this->widget_id; ?>"><?php echo __("Beds", 'real-estate-listing-realtyna-wpl'); ?></label>
               <select id="mls_price_estimator_bedrooms<?php echo $this->widget_id; ?>">
                    <option value=""><?php echo __('Any', 'real-estate-listing-realtyna-wpl'); ?></option>
                    <?php for ($i = 1; $i < 11; $i++): ?>
                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php endfor;?>
               </select>
            </div>
            <div class="form-field">
                <label for="mls_price_estimator_bathrooms<?php echo $this->widget_id; ?>"><?php echo __("Baths", 'real-estate-listing-realtyna-wpl'); ?></label>
               <select id="mls_price_estimator_bathrooms<?php echo $this->widget_id; ?>">
                   <option value=""><?php echo __('Any', 'real-estate-listing-realtyna-wpl'); ?></option>
                   <?php for ($i = 1; $i < 11; $i++): ?>
                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php endfor;?>
               </select>
            </div>
            <div class="form-field">
                <label for="mls_price_estimator_living_area<?php echo $this->widget_id; ?>"><?php echo __("SqFt", 'real-estate-listing-realtyna-wpl'); ?></label>
               <input type="text" id="mls_price_estimator_living_area<?php echo $this->widget_id; ?>" placeholder="">
            </div>
            <div class="form-field">
                <label for="mls_price_estimator_lot_area<?php echo $this->widget_id; ?>"><?php echo __("Lot Area", 'real-estate-listing-realtyna-wpl'); ?></label>
               <input type="text" id="mls_price_estimator_lot_area<?php echo $this->widget_id; ?>" placeholder="">
            </div>
            <div class="form-field">
                <label for="mls_price_estimator_build_year<?php echo $this->widget_id; ?>"><?php echo __("Year Built", 'real-estate-listing-realtyna-wpl'); ?></label>
               <input type="text" id="mls_price_estimator_build_year<?php echo $this->widget_id; ?>" placeholder="">
            </div>

           <input class="btn btn-primary" type="button" onclick="estimate_price<?php echo $this->widget_id; ?>()" value="<?php echo __('Calculate', 'real-estate-listing-realtyna-wpl'); ?>">
        </div>
    </div>

    <div class="mls-price-estimator-result wpl-column">
        <div class="mls-price-estimator-result_cnt">
            <span id="wpl_ajax_loader_mls_price_estimator<?php echo $this->widget_id; ?>"></span>
            <table>
                <tr>
                    <th><?php echo __('Listing Type', 'real-estate-listing-realtyna-wpl'); ?></th>
                    <th><?php echo __('No. of Listings', 'real-estate-listing-realtyna-wpl'); ?></th>
                    <th><?php echo __('Average Price', 'real-estate-listing-realtyna-wpl'); ?></th>
                </tr>
                <?php foreach ($listings as $listing): ?>
                    <tr>
                        <td><?php echo __($listing['name'], 'real-estate-listing-realtyna-wpl'); ?></td>
                        <td id="mls_price_estimator_count_<?php echo $listing['id']; ?>_<?php echo $this->widget_id; ?>"></td>
                        <td id="mls_price_estimator_price_<?php echo $listing['id']; ?>_<?php echo $this->widget_id; ?>"></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</div>