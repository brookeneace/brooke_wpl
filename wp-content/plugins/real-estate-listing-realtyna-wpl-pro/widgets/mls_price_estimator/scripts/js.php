<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');

wpl_global::include_google_maps();
?>
<script type="text/javascript">

function estimate_price<?php echo $this->widget_id; ?>()
{
    var ajax_loader_element = '#wpl_ajax_loader_mls_price_estimator<?php echo $this->widget_id; ?>';
    var condition = '&sf_locationtextsearch='+wplj('#mls_price_estimator_location<?php echo $this->widget_id; ?>').val();
    condition += '&sf_select_property_type='+wplj('#mls_price_estimator_property_type<?php echo $this->widget_id; ?>').val();
    condition += '&sf_select_bedrooms='+wplj('#mls_price_estimator_bedrooms<?php echo $this->widget_id; ?>').val();
    condition += '&sf_select_bathrooms='+wplj('#mls_price_estimator_bathrooms<?php echo $this->widget_id; ?>').val();
    condition += '&sf_select_living_area='+wplj('#mls_price_estimator_living_area<?php echo $this->widget_id; ?>').val();
    condition += '&sf_select_lot_area='+wplj('#mls_price_estimator_lot_area<?php echo $this->widget_id; ?>').val();
    condition += '&sf_select_build_year='+wplj('#mls_price_estimator_build_year<?php echo $this->widget_id; ?>').val();
    wplj(ajax_loader_element).html('&nbsp;<img src="<?php echo wpl_global::get_wpl_asset_url('img/ajax-loader3.gif'); ?>" />');

    wplj.ajax(
    {
        url: '<?php echo wpl_global::get_full_url(); ?>',
        data: 'wpl_format=f:property_listing:ajax&wpl_function=estimate_price'+condition,
        type: 'GET',
        dataType: 'json',
        async: false,
        cache: false,
        timeout: 30000,
        success: function(result)
        {
            wplj(ajax_loader_element).html('');
            if(result.success == 1)
            {
                for (var key in result.data)
                {
                    wplj('#mls_price_estimator_count_'+key+'_<?php echo $this->widget_id; ?>').html(result.data[key].count);
                    wplj('#mls_price_estimator_price_'+key+'_<?php echo $this->widget_id; ?>').html(result.data[key].price);
                }
            }
        }
    }); 

    (function($){$(function()
    {
        wpl_add_googlemaps_callbacks(function()
        {
            var input = document.getElementById('mls_price_estimator_location<?php echo $this->widget_id; ?>');
                autocomplete = new google.maps.places.Autocomplete(input);
        });
    });})(jQuery);
}
</script>