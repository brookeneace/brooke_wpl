<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');
?>
<div class="page fixed" id="option_container">
    <div class="odd">
		<span class="settings_name"><label for="rets_version"><?php echo __('RETS Version', 'real-estate-listing-realtyna-wpl'); ?> :</label></span>
		<input type="text" name="rets_version" id="rets_version" value="<?php echo (isset($this->mls_server->rets_version) ? $this->mls_server->rets_version : 'RETS/1.5'); ?>" />
	</div>
	<div>
		<span class="settings_name"><label for="resource"><?php echo __('Resource', 'real-estate-listing-realtyna-wpl'); ?> :</label></span>
		<input type="text" name="resource" id="resource" value="<?php echo (isset($this->mls_server->resource) ? $this->mls_server->resource : 'Property'); ?>" />
	</div>
	<div class="odd">
		<span class="settings_name"><label for="mls_use_post"><?php echo __('Post method', 'real-estate-listing-realtyna-wpl'); ?> :</label></span>
		<select name="mls_use_post" id="mls_use_post">
			<option value="0" <?php if($this->mls_server->mls_use_post == 0) echo 'selected="selected"'; ?>><?php echo __('No', 'real-estate-listing-realtyna-wpl'); ?></option>
			<option value="1" <?php if($this->mls_server->mls_use_post == 1) echo 'selected="selected"'; ?>><?php echo __('Yes', 'real-estate-listing-realtyna-wpl'); ?></option>
		</select>
	</div>
    <div>
		<span class="settings_name"><label for="image_resource"><?php echo __('Image Type', 'real-estate-listing-realtyna-wpl'); ?> :</label></span>
		<input type="text" name="image_resource" id="image_resource" value="<?php echo (isset($this->mls_server->image_resource) ? $this->mls_server->image_resource : 'Photo'); ?>" />
	</div>
    <div class="odd">
		<span class="settings_name"><label for="image_location"><?php echo __('External Image', 'real-estate-listing-realtyna-wpl'); ?> :</label></span>
		<select name="image_location" id="image_location">
        	<option value="0" <?php if($this->mls_server->image_location == 0) echo 'selected="selected"'; ?>><?php echo __('No', 'real-estate-listing-realtyna-wpl'); ?></option>
            <option value="1" <?php if($this->mls_server->image_location == 1) echo 'selected="selected"'; ?>><?php echo __('Yes', 'real-estate-listing-realtyna-wpl'); ?></option>
        </select>
	</div>

	<div>
		<span class="settings_name"><label for="alt_image_resource"><?php echo __('Alt. Image Type', 'real-estate-listing-realtyna-wpl'); ?> :</label></span>
		<input type="text" name="alt_image_resource" id="alt_image_resource" value="<?php echo (isset($this->mls_server->alt_image_resource) ? $this->mls_server->alt_image_resource : ''); ?>" />
	</div>
    <div class="odd">
		<span class="settings_name"><label for="alt_image_location"><?php echo __('Alt. External Image', 'real-estate-listing-realtyna-wpl'); ?> :</label></span>
		<select name="alt_image_location" id="alt_image_location">
        	<option value="0" <?php if($this->mls_server->alt_image_location == 0) echo 'selected="selected"'; ?>><?php echo __('No', 'real-estate-listing-realtyna-wpl'); ?></option>
            <option value="1" <?php if($this->mls_server->alt_image_location == 1) echo 'selected="selected"'; ?>><?php echo __('Yes', 'real-estate-listing-realtyna-wpl'); ?></option>
        </select>
	</div>

	<div <?php if($this->mls_server->image_location == 1) echo 'style="display:block;"'; else echo 'style="display:none;"' ?> id="download_external_image_box">
		<span class="settings_name"><label for="download_external_image"><?php echo __('Download External Image', 'real-estate-listing-realtyna-wpl'); ?> :</label></span>
		<select name="download_external_image" id="download_external_image">
			<option value="0" <?php if($this->mls_server->download_external_image == 0) echo 'selected="selected"'; ?>><?php echo __('No', 'real-estate-listing-realtyna-wpl'); ?></option>
			<option value="1" <?php if($this->mls_server->download_external_image == 1) echo 'selected="selected"'; ?>><?php echo __('Yes', 'real-estate-listing-realtyna-wpl'); ?></option>
		</select>
	</div>

	<!-- Skip First Image -->
    <div>
		<span class="settings_name"><label for="skip_first_image"><?php echo __('Skip Index Image', 'real-estate-listing-realtyna-wpl'); ?> :</label></span>
		<select name="skip_first_image" id="skip_first_image">
        	<option value="0" <?php if($this->mls_server->skip_first_image == 0) echo 'selected="selected"'; ?>><?php echo __('No', 'real-estate-listing-realtyna-wpl'); ?></option>
            <option value="1" <?php if($this->mls_server->skip_first_image == 1 or ($this->mls_server->skip_first_image == NULL)) echo 'selected="selected"'; ?>><?php echo __('Yes', 'real-estate-listing-realtyna-wpl'); ?></option>
        </select>
	</div>
    
	<?php /** If the MLS Agent add-on is installed **/ if(wpl_global::check_addon('mls_agents')): ?>
	<div class="odd">
		<span class="settings_name"><label for="mls_agent_id_field"><?php echo __('MLS Agent Field', 'real-estate-listing-realtyna-wpl'); ?> :</label></span>
		<input type="text" value="<?php echo $this->mls_server->mls_agent_id_field; ?>" id="mls_agent_id_field" name="mls_agent_id_field" />
	</div>
    
    <?php /** If the Multi Agent add-on is installed **/ if(wpl_global::check_addon('multi_agents')): ?>
    <div>
        <span class="settings_name"><label for="mls_multi_agent_id_fields"><?php echo __('MLS Multi Agent Fields', 'real-estate-listing-realtyna-wpl'); ?> :</label></span>
        <input type="text" value="<?php echo $this->mls_server->mls_multi_agent_id_fields; ?>" id="mls_multi_agent_id_fields" name="mls_multi_agent_id_fields" placeholder="field1,field2..."/>
    </div>
    <?php endif; ?>
    
	<?php endif; ?>
    
    <?php /** If the neighborhoods add-on is installed **/ if(wpl_global::check_addon('neighborhoods')): ?>
	<div>
		<span class="settings_name"><label for="neighborhood_field"><?php echo __('Neighborhood Field', 'real-estate-listing-realtyna-wpl'); ?> :</label></span>
		<input type="text" value="<?php echo $this->mls_server->neighborhood_field; ?>" id="neighborhood_field" name="neighborhood_field" />
	</div>
	<?php endif; ?>
    
    <?php /** If the Complex add-on is installed **/ if(wpl_global::check_addon('complex')): ?>
	<div class="odd">
		<span class="settings_name"><label for="complex_field"><?php echo __('Complex Field', 'real-estate-listing-realtyna-wpl'); ?> :</label></span>
		<input type="text" value="<?php echo $this->mls_server->complex_field; ?>" id="complex_field" name="complex_field" />
	</div>
	<?php endif; ?>
    
    <!-- Google Geo Coordinates -->
     <div class="odd">
		<span class="settings_name"><label for="mls_get_geo_coordinates"><?php echo __('Google Geo Coordinates', 'real-estate-listing-realtyna-wpl'); ?> :</label></span>
		<select name="mls_get_geo_coordinates" id="mls_get_geo_coordinates">
        	<option value="0" <?php if($this->mls_server->mls_get_geo_coordinates == 0) echo 'selected="selected"'; ?>><?php echo __('No', 'real-estate-listing-realtyna-wpl'); ?></option>
            <option value="1" <?php if($this->mls_server->mls_get_geo_coordinates == 1 or ($this->mls_server->mls_get_geo_coordinates == NULL)) echo 'selected="selected"'; ?>><?php echo __('Yes', 'real-estate-listing-realtyna-wpl'); ?></option>
        </select>
	</div>
    
    <!-- MLS Photos update -->
    <div>
		<span class="settings_name"><label for="mls_photo_update"><?php echo __('MLS Photos update', 'real-estate-listing-realtyna-wpl'); ?> :</label></span>
		<select name="mls_photo_update" id="mls_photo_update">
        	<option value="0" <?php if($this->mls_server->mls_photo_update == 0) echo 'selected="selected"'; ?>><?php echo __('No', 'real-estate-listing-realtyna-wpl'); ?></option>
            <option value="1" <?php if($this->mls_server->mls_photo_update == 1 or ($this->mls_server->mls_photo_update == NULL)) echo 'selected="selected"'; ?>><?php echo __('Yes', 'real-estate-listing-realtyna-wpl'); ?></option>
        </select>
	</div>
	
    <!-- Authentication method -->
    <div class="odd">
		<span class="settings_name"><label for="mls_auth_method"><?php echo __('MLS Auth method', 'real-estate-listing-realtyna-wpl'); ?> :</label></span>
		<select name="mls_auth_method" id="mls_auth_method">
			<option value="digest" <?php if($this->mls_server->mls_auth_method == 'digest' or ($this->mls_server->mls_auth_method == NULL)) echo 'selected="selected"'; ?>><?php echo __('Digest', 'real-estate-listing-realtyna-wpl'); ?></option>
        	<option value="basic" <?php if($this->mls_server->mls_auth_method == 'basic') echo 'selected="selected"'; ?>><?php echo __('Basic', 'real-estate-listing-realtyna-wpl'); ?></option>
        </select>
	</div>

		<!-- Connect Per Class -->
    <div>
		<span class="settings_name"><label for="connect_per_class"><?php echo __('Connect to Server Per Calss', 'real-estate-listing-realtyna-wpl'); ?> :</label></span>
		<select name="connect_per_class" id="connect_per_class">
			<option value="0" <?php if($this->mls_server->connect_per_class == 0 or ($this->mls_server->connect_per_class == NULL)) echo 'selected="selected"'; ?>><?php echo __('No', 'real-estate-listing-realtyna-wpl'); ?></option>
        	<option value="1" <?php if($this->mls_server->connect_per_class == 1) echo 'selected="selected"'; ?>><?php echo __('Yes', 'real-estate-listing-realtyna-wpl'); ?></option>
        </select>
	</div>
    
    <!-- Map Per Class -->
    <div class="odd">
		<span class="settings_name"><label for="map_per_class"><?php echo __('Map Field Per Calss', 'real-estate-listing-realtyna-wpl'); ?> :</label></span>
		<select name="map_per_class" id="map_per_class">
			<option value="0" <?php if($this->mls_server->map_per_class == 0 or ($this->mls_server->map_per_class == NULL)) echo 'selected="selected"'; ?>><?php echo __('No', 'real-estate-listing-realtyna-wpl'); ?></option>
        	<option value="1" <?php if($this->mls_server->map_per_class == 1) echo 'selected="selected"'; ?>><?php echo __('Yes', 'real-estate-listing-realtyna-wpl'); ?></option>
        </select>
	</div>
</div>