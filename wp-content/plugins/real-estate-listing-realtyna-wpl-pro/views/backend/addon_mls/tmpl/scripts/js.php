<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');
?>
<script type="text/javascript">
var clicked_on_connect_button = false;
var timeout_import_mapping_process;
var result_connection_server_id = 0;

jQuery(document).ready(function()
{
	wplj('#image_location').change(function(){
		if(wplj(this).val() == 1) wplj('#download_external_image_box').slideDown('normal');
		else wplj('#download_external_image_box').slideUp('normal');
	});
});

function wpl_generate_modify_mls(mls_server_id)
{
	if(!mls_server_id) mls_server_id = 0;
	else result_connection_server_id = mls_server_id;
	
	wpl_remove_message('.wpl_mls_servers_list .wpl_show_message');
	request_str = 'wpl_format=b:addon_mls:ajax&wpl_function=generate_modify_page&id='+mls_server_id;

	clicked_on_connect_button = false;

	/** run ajax query **/
	wplj.ajax(
	{
		type: "POST",
		url: '<?php echo wpl_global::get_full_url(); ?>',
		data: request_str,
		success: function(data)
		{
			wplj("#wpl_mls_server_edit_div").html(data);
		},
		error: function(jqXHR, textStatus, errorThrown)
		{
			wpl_show_messages('<?php echo __('Error Occured.', 'real-estate-listing-realtyna-wpl'); ?>', '.wpl_mls_servers_list .wpl_show_message', 'wpl_red_msg');
			wplj._realtyna.lightbox.close();
		}
	});
}

function wpl_save_mls()
{
	if(clicked_on_connect_button)
	{
		wplj._realtyna.lightbox.close();
		return;
	}

	request_str = 'wpl_format=b:addon_mls:ajax&wpl_function=save_mls&table=wpl_addon_mls';

	wplj("#wpl_modify_mls input:checkbox").each(function(ind, elm)
	{
		request_str += "&fld_" + elm.id + "=";
		if (elm.checked) request_str += '1';
		else request_str += '0';
	})

	wplj("#wpl_modify_mls input:text, #wpl_modify_mls input:password, #wpl_modify_mls input[type='hidden'], #wpl_modify_mls select").each(function(ind, elm)
	{
		if(elm.id != 'id') request_str += "&fld_" + elm.id + "=";
		else request_str += "&" + elm.id + "=";
		
		request_str += wplj(elm).val();
	});
	
	wpl_remove_message('.wpl_mls_servers_list .wpl_show_message');

	/** run ajax query **/
	wplj.ajax(
	{
		type: "POST",
		url: '<?php echo wpl_global::get_full_url(); ?>',
		data: request_str,
		success: function(data)
		{
			wpl_show_messages('<?php echo __('MLS modified.', 'real-estate-listing-realtyna-wpl'); ?>', '.wpl_mls_servers_list .wpl_show_message', 'wpl_green_msg');
			wplj._realtyna.lightbox.close();
		},
		error: function(jqXHR, textStatus, errorThrown)
		{
			wpl_show_messages('<?php echo __('Error Occured.', 'real-estate-listing-realtyna-wpl'); ?>', '.wpl_mls_servers_list .wpl_show_message', 'wpl_red_msg');
			wplj._realtyna.lightbox.close();
		}
	});
}

function wpl_remove_mls_server(id, confirmed)
{
	if(!id)
	{
		wpl_show_messages("<?php echo __('Invalid MLS ID', 'real-estate-listing-realtyna-wpl'); ?>", '.wpl_mls_servers_list .wpl_show_message');
		return false;
	}

	if (!confirmed)
	{
		message = "<?php echo __('Are you sure you want to remove this item?', 'real-estate-listing-realtyna-wpl'); ?>";
		message += '&nbsp;<span class="wpl_actions" onclick="wpl_remove_mls_server(' + id + ', 1);"><?php echo __('Yes', 'real-estate-listing-realtyna-wpl'); ?></span>&nbsp;<span class="wpl_actions" onclick="wpl_remove_message();"><?php echo __('No', 'real-estate-listing-realtyna-wpl'); ?></span>';
		wpl_show_messages(message, '.wpl_mls_servers_list .wpl_show_message');

		return false;
	}
	else
	{
		wpl_remove_message();
	}

	request_str = 'wpl_format=b:addon_mls:ajax&wpl_function=remove_mls_server&id='+id;

	wplj.ajax(
	{
		type: "POST",
		url: '<?php echo wpl_global::get_full_url(); ?>',
		data: request_str,
		success: function(data)
		{
			wplj("#item_row"+id).remove();
			wpl_show_messages('<?php echo __('MLS server deleted.', 'real-estate-listing-realtyna-wpl'); ?>', '.wpl_mls_servers_list .wpl_show_message', 'wpl_green_msg');
		},
		error: function(jqXHR, textStatus, errorThrown)
		{
			wpl_show_messages('<?php echo __('Error Occured.', 'real-estate-listing-realtyna-wpl'); ?>', '.wpl_mls_servers_list .wpl_show_message', 'wpl_red_msg');
		}
	});
}

function wpl_test_connection(id)
{
	/** remove message **/
	wpl_remove_message();
	
	ajax_loader_element = '#wpl_ajax_loader_mls_server'+id;
	wplj(ajax_loader_element).html('&nbsp;<img src="<?php echo wpl_global::get_wpl_asset_url('img/ajax-loader3.gif'); ?>" />');
	
	var wait_timeout = setTimeout(function(){wpl_show_messages("<?php echo __("We're processing your request. Please wait ...", 'real-estate-listing-realtyna-wpl'); ?>", '.wpl_mls_servers_list .wpl_show_message', 'wpl_gold_msg');}, 15000);
	
	request_str = 'wpl_format=b:addon_mls:ajax&wpl_function=test_connection&id='+id;
	wplj.ajax(
	{
		type: "POST",
		url: '<?php echo wpl_global::get_full_url(); ?>',
		data: request_str,
		dataType: 'JSON',
		success: function(data)
		{
			if(data.success == 1)
			{
				clearTimeout(wait_timeout);
				wplj(ajax_loader_element).html('');
				
				wpl_show_messages(data.message, '.wpl_mls_servers_list .wpl_show_message', 'wpl_green_msg');
				setTimeout(function(){window.location.reload();}, 1000);
			}
			else
			{
				clearTimeout(wait_timeout);
				wplj(ajax_loader_element).html('');
				
				wpl_show_messages(data.message, '.wpl_mls_servers_list .wpl_show_message', 'wpl_red_msg');
			}
		},
		error: function(jqXHR, textStatus, errorThrown)
		{
			wpl_show_messages('<?php echo __('Error Occured.', 'real-estate-listing-realtyna-wpl'); ?>', '.wpl_mls_servers_list .wpl_show_message', 'wpl_red_msg');
			wplj(ajax_loader_element).html('');
			
			clearTimeout(wait_timeout);
		}
	});
}

function wpl_generate_params_page(id)
{
	if(!id) id = '';
	
	request_str = 'wpl_format=b:addon_mls:ajax&wpl_function=generate_params_page&id='+id;
	
	/** run ajax query **/
	wplj.ajax(
	{
		type: "POST",
		url: '<?php echo wpl_global::get_full_url(); ?>',
		data: request_str,
		success: function(data)
		{
			wplj("#wpl_mls_server_edit_div").html(data);
		},
		error: function(jqXHR, textStatus, errorThrown)
		{
			wplj._realtyna.lightbox.close();
		}
	});
}

function wpl_mls_connection(id)
{
	if(!id)
	{
		if(result_connection_server_id != 0) id = result_connection_server_id;
		else id = 0;
	}
	// remove all messages
	wpl_remove_message();

	result_connection_server_id = 0;

	ajax_loader_element = '.connection-result';
	ajax_show_message_element = '.wpl_mls_connection .wpl_show_message';

	var mls_name = wplj('#mls_name').val();
	var url = wplj('#url').val();
	var username = wplj('#username').val();
	var password = wplj('#password').val();
	var agent_user = wplj('#agent_username').val();
	var agent_password = wplj('#agent_password').val();
	var rets_version = wplj('#rets_version').val();
	var mls_use_post = wplj('#mls_use_post').val();

	if(url.trim() == '' || username.trim() == '' || password.trim() == '')
	{
		wplj(ajax_loader_element).html('<?php echo __("The URL / Username / Password is empty!", 'real-estate-listing-realtyna-wpl'); ?>');
		return;
	}

	wplj(ajax_loader_element).html('&nbsp;<img src="<?php echo wpl_global::get_wpl_asset_url('img/ajax-loader3.gif'); ?>" />');

	var wait_timeout = setTimeout(function(){wpl_show_messages("<?php echo __("We're processing your request. Please wait ...", 'real-estate-listing-realtyna-wpl'); ?>", ajax_show_message_element, 'wpl_gold_msg');}, 15000);

	request_str = 'wpl_format=b:addon_mls:ajax&wpl_function=mls_wizard_connection&name='+mls_name+'&url='+url+'&username='+username+'&password='+password+'&agent_user='+agent_user+'&agent_password='+agent_password+'&rets_version='+rets_version+'&mls_use_post='+mls_use_post+"&id="+id;

	wplj.ajax(
	{
		type: "POST",
		url: '<?php echo wpl_global::get_full_url(); ?>',
		data: request_str,
		dataType: 'JSON',
		success: function(data)
		{
			wplj(ajax_loader_element).html('');

			if(data.success == 1)
			{
				result_connection_server_id = data.id;
				clicked_on_connect_button = true;
				clearTimeout(wait_timeout);
				wpl_show_messages(data.message, ajax_show_message_element, 'wpl_green_msg');

				timeout_import_mapping_process = setTimeout(function()
				{
					wpl_mls_import_mapping_files(data.id, data.imported_classes);
				}, 1000);
			}
			else
			{
				result_connection_server_id = data.id;
				clearTimeout(wait_timeout);
				wpl_show_messages(data.message, ajax_show_message_element, 'wpl_red_msg');
			}

		},
		error: function(jqXHR, textStatus, errorThrown)
		{
			wpl_show_messages('<?php echo __('Error Occured.', 'real-estate-listing-realtyna-wpl'); ?>', ajax_show_message_element, 'wpl_red_msg');
			wplj(ajax_loader_element).html('');

			clearTimeout(wait_timeout);
		}
	});
}

function wpl_mls_import_mapping_files(id, imported_classes)
{
	// remove all messages
	wpl_remove_message();

	ajax_show_message_element = '.wpl_mls_connection .wpl_show_message';

	if(!id)
	{
		message = '<?php echo __("There are no imported classes! Try to import the mapping again.", 'real-estate-listing-realtyna-wpl'); ?>';
		wpl_show_messages(message, ajax_show_message_element, 'wpl_gold_msg');
		return false;
	}

	if(!imported_classes)
	{
		message = '<?php echo __("There are no imported MLS! Try to connect again.", 'real-estate-listing-realtyna-wpl'); ?>';
		wpl_show_messages(message, ajax_show_message_element, 'wpl_gold_msg');
		return false;
	}

	message = '<?php echo __("Checking the mapping file. Please wait ...", 'real-estate-listing-realtyna-wpl'); ?>';
	wpl_show_messages(message, ajax_show_message_element, 'wpl_gold_msg');

	request_str = 'wpl_format=b:addon_mls:ajax&wpl_function=import_mapping_files_connection_wizard&id='+id+'&imported_classes='+imported_classes;
	wplj.ajax(
	{
		type: "POST",
		url: '<?php echo wpl_global::get_full_url(); ?>',
		data: request_str,
		dataType: 'JSON',
		success: function(data)
		{
			if(data.success == 1) wpl_show_messages(data.message, ajax_show_message_element, 'wpl_green_msg');
			else wpl_show_messages(data.message, ajax_show_message_element, 'wpl_red_msg');
			clearTimeout(timeout_import_mapping_process);
		},
		error: function(jqXHR, textStatus, errorThrown)
		{
			wpl_show_messages('<?php echo __('Error Occured.', 'real-estate-listing-realtyna-wpl'); ?>', ajax_show_message_element, 'wpl_red_msg');
			clearTimeout(timeout_import_mapping_process);
		}
	});
}
</script>