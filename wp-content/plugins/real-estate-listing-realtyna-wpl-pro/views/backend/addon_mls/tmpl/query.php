<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');

_wpl_import($this->tpl_path.'.scripts.js_query');
_wpl_import($this->tpl_path.'.scripts.css');
?>
<div class="wrap wpl-wp settings-wp">
    <header>
        <div id="icon-settings" class="icon48">
        </div>
        <h2><?php echo __('MLS Add-on / Query Wizard', 'real-estate-listing-realtyna-wpl'); ?></h2>
    </header>
    <div class="wpl_item_list"><div class="wpl_show_message"></div></div>
    <div class="sidebar-wp" id="wpl_mls_addon_query">
    	<table class="widefat page">
            <thead>
                <tr>
                    <th colspan="6">
                        <div class="action-wp">
                            <span data-realtyna-lightbox data-realtyna-lightbox-opts="reloadPage:true" data-realtyna-href="#wpl_mls_query_div" class="wpl_create_new action-btn icon-plus" title="<?php echo __('Add a new Query', 'real-estate-listing-realtyna-wpl'); ?>" onclick="wpl_generate_modify_query();"></span>
                        </div>
                    </th>
                </tr>
                <tr>
                    <th scope="col" class="manage-column"><?php echo __('ID', 'real-estate-listing-realtyna-wpl'); ?></th>
                    <th scope="col" class="manage-column"><?php echo __('MLS Server', 'real-estate-listing-realtyna-wpl'); ?></th>
                    <th scope="col" class="manage-column"><?php echo __('MLS Class', 'real-estate-listing-realtyna-wpl'); ?></th>
                    <th scope="col" class="manage-column"><?php echo __('Default User', 'real-estate-listing-realtyna-wpl'); ?></th>
                    <th scope="col" class="manage-column"><?php echo __('Import', 'real-estate-listing-realtyna-wpl'); ?></th>
                    <th scope="col" class="manage-column"><?php echo __('Actions', 'real-estate-listing-realtyna-wpl'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                	foreach($this->mls_queries as $mls_query):
						
						$user_data = wpl_users::get_user($mls_query->default_user_id);
						$server_data = wpl_addon_mls::get_servers($mls_query->mls_server_id);
				?>
                <tr id="item_row<?php echo $mls_query->id; ?>">
                    <td><?php echo $mls_query->id; ?></td>
                    <td><?php echo $server_data->mls_name; ?></td>
                    <td><?php echo $mls_query->mls_class_name; ?></td>
                    <td><?php echo $user_data->data->user_login; ?></td>
                    <td title="<?php echo __('Last sync').' : '.($mls_query->last_sync_date == '0000-00-00 00:00:00' ? __('Never', 'real-estate-listing-realtyna-wpl') : $mls_query->last_sync_date); ?>"><a href="<?php echo wpl_global::get_full_url(); ?>&tpl=import&id=<?php echo $mls_query->id; ?>"><?php echo __('Import', 'real-estate-listing-realtyna-wpl'); ?></a></td>
                    <td class="wpl_manager_td">
                        <span data-realtyna-lightbox data-realtyna-href="#wpl_mls_query_div" class="action-btn icon-gear" onclick="wpl_generate_params_page('<?php echo $mls_query->id; ?>');"></span>
                        <span data-realtyna-lightbox data-realtyna-lightbox-opts="reloadPage:true" data-realtyna-href="#wpl_mls_query_div" class="action-btn icon-edit" onclick="wpl_generate_modify_query(<?php echo $mls_query->id; ?>)"></span>
                        <span class="action-btn icon-recycle wpl_show" onclick="wpl_remove_mls_query(<?php echo $mls_query->id; ?>);"></span>
                        <span class="action-btn <?php echo $mls_query->enabled ? 'icon-enabled' : 'icon-disabled'; ?>" id="wpl_mls_query_enabled<?php echo $mls_query->id ?>" onclick="wpl_set_enabled_mls_query(<?php echo $mls_query->id ?>);" title="<?php echo $mls_query->enabled ? __('Auto sync is enabled.', 'real-estate-listing-realtyna-wpl') : __('Auto sync is disabled.', 'real-estate-listing-realtyna-wpl'); ?>"></span>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div id="wpl_mls_query_div" class="wpl_hidden_element"></div>

    <?php if(wpl_request::getVar('requirements') == 1) { ?>
    <hr />
        <div class="wpl-util-side-12" id="wpl_mls_requirements">
            <div class="panel-wp">
                <h3><?php echo __('Hosting requirements', 'real-estate-listing-realtyna-wpl'); ?></h3>
                <div class="panel-body">
                    <?php 
                        if(empty($this->mls_queries)) echo __('To see the requirements, create at least one MLS query and import a few listings', 'real-estate-listing-realtyna-wpl');
    
                        else
                        {
                            $result = wpl_addon_mls::get_requirements();

                            foreach ($result as $mls_name=>$server)
                            {
                                if(!count($server) or !isset($server['image'])) continue;
                                
                                echo $mls_name.'<br><br>';

                                if(isset($server['desc']))
                                {
                                    echo $server['desc'];
                                    continue;
                                }

                                if(isset($server['port'])) $port_list[$server['port']] = $server['port'];
                                
                                $tmp = $server['image']*2;
                                echo "Apx Inode Usage WITH WPL gallery resize and thumbnail feature: ".number_format($tmp)." <br>";
                                echo "Inode usage will be around ".number_format($server['image'])." files by disabling resize/thumbnail feature. <br><br>";
                                
                                echo "Apx Hosting Space Usage: ".ceil($server['space']/1024/1024)." GB <br><br>";
                            }
                            
                            if(count($port_list)) echo __('Port/s No '.implode(',', $port_list).' should be opened for outgoing connection on Server.', 'real-estate-listing-realtyna-wpl');
                        }
                    ?>               
                </div>
            </div>
        </div>
    <?php } ?>

    <footer>
        <div class="logo"></div>
    </footer>
</div>