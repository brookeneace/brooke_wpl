<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');

?>
<div class="fanc-content size-width-2">
    <h2><?php echo __('All', 'real-estate-listing-realtyna-wpl').' '.$this->field_data->field_name.' '.__('Values', 'real-estate-listing-realtyna-wpl'); ?></h2>
    <div class="fanc-body">
        <table id="wpl_mls_values_table" class="widefat page">
            <thead>
                <tr>
                    <th scope="col" class="manage-column"><?php echo __('Value', 'real-estate-listing-realtyna-wpl'); ?></th>
                    <th scope="col" class="manage-column"><?php echo __('Short Value', 'real-estate-listing-realtyna-wpl'); ?></th>
                    <th scope="col" class="manage-column"><?php echo __('Long Value', 'real-estate-listing-realtyna-wpl'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php if(count($this->mls_field_values)): ?>
                    <?php foreach($this->mls_field_values as $mls_field_value): ?>
                        <div class="wpl-form-row">
                            <tr>
                                <td><?php echo $mls_field_value['Value']; ?></td>
                                <td><?php echo $mls_field_value['ShortValue']; ?></td>
                                <td><?php echo $mls_field_value['LongValue']; ?></td>
                            </tr>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>