<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');
?>
<script type="text/javascript">
var wpl_listing_request_str = '<?php echo wpl_global::generate_request_str(); ?>';

function wpl_change_blog(blog_id)
{
    wpl_listing_request_str = wpl_update_qs('blog_id', blog_id, wpl_listing_request_str);
    window.location = '<?php echo wpl_global::get_wp_admin_url(); ?>admin.php?'+wpl_listing_request_str;
}
</script>