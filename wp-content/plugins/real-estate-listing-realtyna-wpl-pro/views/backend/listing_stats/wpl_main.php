<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');

_wpl_import('libraries.property');
_wpl_import('libraries.pagination');
_wpl_import('libraries.settings');

class wpl_listing_stats_controller extends wpl_controller
{
    public $tpl_path = 'views.backend.listing_stats.tmpl';
    public $tpl;

    public $message;
    public $crm_stats;
    public $nonce;
    public $is_multisite;
    public $blogs;
    public $stats;
    public $current_blog_id;
    public $page_number;
    public $model;
    public $kind;
    public $wpl_properties;
    public $pagination;

    public function home($instance = array())
    {
		// Check Access
		if(!wpl_users::check_access('propertystats'))
		{
			// Import Message TPL
			$this->message = __("You don't have access to this menu! Maybe your user is not added to WPL as an agent. You can contact to website admin for this.", 'real-estate-listing-realtyna-wpl');
			return parent::render($this->tpl_path, 'message');
		}
		
        // Create Nonce
        $this->nonce = wpl_security::create_nonce('wpl_listing_stats');
        
        // Global Settings
        $settings = wpl_settings::get_settings();

        // Setting Up Variables
        $this->is_multisite = wpl_global::is_multisite();
        $this->blogs = $this->is_multisite ? get_sites() : array();
        $this->stats = array();
        $this->crm_stats = array();
        $where_crm_requests = '';

        // Listing Settings
        $this->page_number = wpl_request::getVar('wplpage', 1);
        $this->current_blog_id = wpl_request::getVar('blog_id', 0);
        $limit = wpl_request::getVar('limit', $settings['default_page_size']);
        $start = wpl_request::getVar('start', (($this->page_number-1)*$limit));
        $orderby = wpl_request::getVar('orderby', $settings['default_orderby']);
        $order = wpl_request::getVar('order', $settings['default_order']);
        $current_user_id = wpl_users::get_cur_user_id();
        $where = array();
        
        // Set page if start var passed
        $this->page_number = ($start/$limit)+1;
        wpl_request::setVar('wplpage', $this->page_number);
        
        $this->model = new wpl_property;
        
        // Detect Kind
        $this->kind = wpl_request::getVar('kind', 0);
        if(!in_array($this->kind, wpl_flex::get_valid_kinds()))
        {
            // Import Message TPL
            $this->message = __('Invalid Request!', 'real-estate-listing-realtyna-wpl');
            parent::render($this->tpl_path, 'message');
            
            return false;
        }
        
        // Load User Properties
        if(!wpl_users::is_administrator($current_user_id))
        {
            $where['sf_select_user_id'] = $current_user_id;
        }
        
        // Add search conditions to the where
        $vars = array_merge(wpl_request::get('POST'), wpl_request::get('GET'));
        $where = array_merge($vars, $where);
        
        $this->model->start($start, $limit, $orderby, $order, $where, $this->kind);
        
        $query = $this->model->query();
        $this->wpl_properties = $this->model->search($query);
        $this->model->finish();
        
        // Get the number of all properties according to our query
        $properties_count = $this->model->get_properties_count();
        
        // Set pagination according to the number of items and limit
        $this->pagination = wpl_pagination::get_pagination($properties_count, $limit);

        // Setting up CRM's where statement
        if(wpl_global::check_addon('crm'))
        {
            _wpl_import('libraries.addon_crm');

            $crm = new wpl_addon_crm();
            $requests_self = $crm->check_access('requests', 'view', 'self');
            $requests_other = $crm->check_access('requests', 'view', 'other');

            if($requests_self and $requests_other)
            {
                $where_crm_requests = '';
            }
            elseif($requests_self and !$requests_other)
            {
                $where_crm_requests = "AND `owner` = '{$current_user_id}'";
            }
            elseif(!$requests_self and $requests_other)
            {
                $where_crm_requests = "AND `owner` <> '{$current_user_id}'";
            }
            else
            {
                $where_crm_requests = 'AND 1 = 2';
            }
        }

        // Generating Property Stats
        foreach($this->wpl_properties as $id => $property)
        {
            $link = wpl_property::get_property_link(array(), $id);
            
            $inc_listings = wpl_property::get_property_stats_item($id, 'inc_in_listings_numb');
            $parent_views = wpl_global::check_addon('complex') ? wpl_property::get_property_stats_item($id, 'view_in_parent_numb') : 0;
            $contacts = wpl_property::get_property_stats_item($id, 'contact_numb');

            if($this->is_multisite and $this->current_blog_id)
            {
                $query = "SELECT SUM(`visits`) FROM `#__wpl_addon_franchise_property_visits` WHERE `pid` = '{$id}' AND `blog_id` = '{$this->current_blog_id}'";
                $visits = wpl_db::select($query, 'loadResult');

                $this->stats[$id] = array('id' => $property->mls_id, 'title' => $property->field_313, 'location' => $property->location_text, 'link' => $link, 'visits' => $visits, 'contacts' => $contacts, 'inc_listings' => $inc_listings, 'parent_views' => $parent_views);
            }
            else
            {
                $this->stats[$id] = array('id' => $property->mls_id, 'title' => $property->field_313, 'location' => $property->location_text, 'link' => $link, 'visits' => wpl_property::get_property_stats_item($id, 'visit_time'), 'contacts' => $contacts, 'inc_listings' => $inc_listings, 'parent_views' => $parent_views);
            }
        }

        // Generating CRM Stats
        if(wpl_global::check_addon('crm'))
        {
            if($this->is_multisite)
            {
                foreach ($this->blogs as $blog) 
                {
                    $requests = wpl_db::select("SELECT COUNT(`id`) FROM `#__wpl_addon_crm_requests` WHERE `blog_id` = {$blog->blog_id} {$where_crm_requests}", 'loadResult');
                    $this->crm_stats[$blog->blog_id] = array('id' => $blog->blog_id, 'title' => $blog->domain, 'link' => 'http://'.$blog->domain, 'requests' => $requests);
                }
            }
            else
            {
                $requests = wpl_db::select("SELECT COUNT(`id`) FROM `#__wpl_addon_crm_requests` WHERE 1 {$where_crm_requests}", 'loadResult');
                $this->crm_stats[1] = array('id' => 1, 'title' => get_bloginfo('name'), 'link' => get_bloginfo('url'), 'requests' => $requests);
            }
        }
        
		$this->tpl = wpl_flex::get_kind_tpl($this->tpl_path, wpl_request::getVar('tpl', 'stats'), $this->kind);
        return parent::render($this->tpl_path, $this->tpl);
    }
}