<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');
?>
<div class="wrap">
	<div class="wpl_message_container wpl_view_container" id="wpl_message_container">
		<?php echo __('You have not added any properties to compare!', 'real-estate-listing-realtyna-wpl'); ?>
		<a class="" href="<?php echo $this->frontend_url ?>"><?php echo __('Back to home page', 'real-estate-listing-realtyna-wpl'); ?></a>
	</div>
</div>