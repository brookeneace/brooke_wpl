<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');

_wpl_import('libraries.settings');
_wpl_import('libraries.addon_pro');

class wpl_features_controller extends wpl_controller
{
	public $tpl_path = 'views.basics.features.tmpl';
	public $tpl;
	public $settings;
	public $rss;
	public $properties;
	public $channel_dates;
	
	public function display()
	{
		if(!wpl_global::check_addon('pro'))
        {
            echo __('PRO Add-on must be installed for this!', 'real-estate-listing-realtyna-wpl');
            exit;
        }

		$function = wpl_request::getVar('wpl_function', 'show');
		if($function == 'show') $this->show();
	}
	
	private function show()
	{
		$this->tpl = 'rss';
		$this->settings = wpl_settings::get_settings(1);

		// RSS Information
		$this->rss = array();
		$this->rss['title'] = __($this->settings['listings_rss_title'], 'real-estate-listing-realtyna-wpl');
		$this->rss['link'] = wpl_global::get_wp_site_url();
		$this->rss['desc'] = __($this->settings['listings_rss_description'], 'real-estate-listing-realtyna-wpl');

		// Get Properties
		wpl_request::setVar('wplmethod', 'get_listings');

		// Import Class
        _wpl_import('views.frontend.property_listing.wpl_get');

        $model = new wpl_property_listing_controller();
        $this->properties = $model->display();

        if(!is_array($this->properties))
        {
        	echo __('Error loading RSS feed!', 'real-estate-listing-realtyna-wpl');
        	exit;
        }

        $this->channel_dates = $this->get_channel_date($this->properties);
        
        // Import TPL
		echo parent::render($this->tpl_path, $this->tpl, false, true);
		exit;
	}

	private function get_channel_date($properties)
	{
		$pub_date = null;
		$last_build = null;

		foreach($properties as $property)
		{
			if(!isset($property['raw'])) continue;

			$property_raw = $property['raw'];
			if(!isset($property_raw['add_date']) or !isset($property_raw['last_modified_time_stamp'])) continue;

			if(!$pub_date) $pub_date = $property_raw['add_date'];
			if(!$last_build) $last_build = $property_raw['last_modified_time_stamp'];

			if(strtotime($pub_date) < strtotime($property_raw['add_date'])) $pub_date = $property_raw['add_date'];
			if(strtotime($last_build) < strtotime($property_raw['last_modified_time_stamp'])) $last_build = $property_raw['last_modified_time_stamp'];
		}

		return (object) array('pub_date'=>$pub_date, 'last_build'=>$last_build);
	}
}