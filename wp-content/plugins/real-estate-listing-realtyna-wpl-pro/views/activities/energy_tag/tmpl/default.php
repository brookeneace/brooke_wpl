<?php
/** no direct access **/
defined('_WPLEXEC') or die('Restricted access');

$image_width       = isset($params['image_width'])       ? $params['image_width'] 	    : '';
$image_height      = isset($params['image_height'])      ? $params['image_height'] 	    : '';
$bar_width         = isset($params['bar_width']) 		 ? $params['bar_width'] 		: '';
$first_bar_length  = isset($params['first_bar_length'])  ? $params['first_bar_length'] 	: '';
$length_increment  = isset($params['length_increment'])  ? $params['length_increment'] 	: '';
$vertical_distance = isset($params['vertical_distance']) ? $params['vertical_distance'] : '';
$peak              = isset($params['peak']) 			 ? $params['peak'] 				: '';
$energy_font_size  = isset($params['energy_font_size'])  ? $params['energy_font_size'] 	: '';
$font_size 		   = isset($params['font_size']) 		 ? $params['font_size'] 		: '';
$show_energy_value = isset($params['show_energy_value']) ? $params['show_energy_value'] : '';
$reverse_levels    = isset($params['reverse_levels'])    ? $params['reverse_levels']    : false;
$wpl_properties    = isset($params['wpl_properties'])    ? $params['wpl_properties']    : NULL;
$energy_value      = $wpl_properties['current']['data']['energy_tag'];

if($energy_value != '' and $energy_value > 0)
{
    /** creating library class for energy tag **/
    $wpl_energy_tag = new wpl_energy_tag();
    
    /** get levels **/
    $levels = $wpl_energy_tag->get_default_levels();
    if(isset($params['levels']) and $wpl_energy_tag->validate_levels($params['levels']))
    {
        $levels = explode(',', $params['levels']);
    }

    if($reverse_levels)
    {
        $texts[] = ">{$levels[0]}";
        $texts[] = ($levels[1]+1)." ". __('to', 'real-estate-listing-realtyna-wpl')." {$levels[0]}";
        $texts[] = ($levels[2]+1)." ". __('to', 'real-estate-listing-realtyna-wpl')." {$levels[1]}";
        $texts[] = ($levels[3]+1)." ". __('to', 'real-estate-listing-realtyna-wpl')." {$levels[2]}";
        $texts[] = ($levels[4]+1)." ". __('to', 'real-estate-listing-realtyna-wpl')." {$levels[3]}";
        $texts[] = ($levels[5]+1)." ". __('to', 'real-estate-listing-realtyna-wpl')." {$levels[4]}";
        $texts[] = "≤{$levels[5]}";
    }
    else
    {
        $texts[] = "≤{$levels[0]}";
        $texts[] = ($levels[0]+1)." ". __('to', 'real-estate-listing-realtyna-wpl')." {$levels[1]}";
        $texts[] = ($levels[1]+1)." ". __('to', 'real-estate-listing-realtyna-wpl')." {$levels[2]}";
        $texts[] = ($levels[2]+1)." ". __('to', 'real-estate-listing-realtyna-wpl')." {$levels[3]}";
        $texts[] = ($levels[3]+1)." ". __('to', 'real-estate-listing-realtyna-wpl')." {$levels[4]}";
        $texts[] = ($levels[4]+1)." ". __('to', 'real-estate-listing-realtyna-wpl')." {$levels[5]}";
        $texts[] = ">{$levels[5]}";
    }
    
    $texts[] = __('Energy Consumption', 'real-estate-listing-realtyna-wpl');
    $texts[] = __('More Efficient', 'real-estate-listing-realtyna-wpl');
    $texts[] = __('Less Efficient', 'real-estate-listing-realtyna-wpl');
    $texts[] = __('Measuring index: ', 'real-estate-listing-realtyna-wpl').' kWhEP/m².y';

    /** initializing energy tag object variables **/
    $wpl_energy_tag->pid 				 = $wpl_properties['current']['data']['id'];
    $wpl_energy_tag->image_width 		 = $image_width;
    $wpl_energy_tag->image_height		 = $image_height;
    $wpl_energy_tag->energy_value		 = $energy_value;
    $wpl_energy_tag->text_font_size		 = $font_size;
    $wpl_energy_tag->bar_width           = $bar_width;
    $wpl_energy_tag->first_bar_length    = $first_bar_length;
    $wpl_energy_tag->length_increment    = $length_increment;
    $wpl_energy_tag->vertical_distance   = $vertical_distance;
    $wpl_energy_tag->peak				 = $peak;
    $wpl_energy_tag->energy_font_size	 = $energy_font_size;
    $wpl_energy_tag->texts               = $texts;
    $wpl_energy_tag->show_energy_value   = $show_energy_value;
    $wpl_energy_tag->levels              = $levels;
    $wpl_energy_tag->reverse_levels      = $reverse_levels;

    /** creating energy tag image **/
    $energy_tag_image = $wpl_energy_tag->create_energy_tag();
}
else
    $energy_tag_image = '';

/** return **/
if(!trim($energy_tag_image)) return NULL;
?>
<div class="wpl_energy_tag">
    <?php echo $energy_tag_image; ?>
</div>